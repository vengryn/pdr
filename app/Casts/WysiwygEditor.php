<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class WysiwygEditor implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, $key, $value, $attributes)
    {
        return $value;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  integer  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, $key, $value, $attributes)
    {
        $value = preg_replace('/<p><div class=\"container\">&nbsp;<\/div><\/p>/', '', $value);

        preg_match_all('/<p><div class=\"container\"><iframe(.*?)><\/iframe><\/div><p>/', $value, $matchesContainerP, PREG_SET_ORDER);
        preg_match_all('/<div class=\"container\"><iframe(.*?)><\/iframe><\/div>/', $value, $matchesContainer, PREG_SET_ORDER);

        foreach ($matchesContainerP as $containerP) {
            $changeContainer = '<iframe' . $containerP[1] . '></iframe>';
            $value = str_replace($containerP[0], $changeContainer, $value);
        }

        foreach ($matchesContainer as $container) {
            $changeContainer = '<iframe' . $container[1] . '></iframe>';
            $value = str_replace($container[0], $changeContainer, $value);
        }

        $value = preg_replace('/(<iframe)(.*)(width=\"[0-9]+\")/i', '$1$2', $value);
        $value = preg_replace('/(<iframe)(.*)(height=\"[0-9]+\")/i', '$1$2', $value);

        preg_match_all('/<iframe(.*?)><\/iframe>/', $value, $matchesIframe);

        foreach ($matchesIframe[0] as $iframe) {
            $changeIframe = '<p><div class="container">' . $iframe . '</div></p>';
            $value = str_replace($iframe, $changeIframe, $value);
        }

        foreach ($matchesIframe[1] as $iframeContent) {
            if (preg_match("/class=\"(.*?)\"/", $iframeContent, $class)) {
                $dataClass = explode(' ', $class[1]);

                if (!in_array('video', $dataClass)) {
                    if (empty($class[1])) {
                        $change = str_replace($class[0], "class=\"video\"", $iframeContent);
                    } else {
                        $change = str_replace($class[0], "class=\"{$class[1]} video\"", $iframeContent);
                    }
                    $value = str_replace($iframeContent, $change, $value);
                }
            } else {
                $change = $iframeContent . ' class="video"';
                $value = str_replace($iframeContent, $change, $value);
            }

            if (preg_match("/frameborder=\"(.*?)\"/", $iframeContent, $frameBorder)) {
                $change = str_replace($frameBorder[0], "frameborder=\"0\"", $iframeContent);
                $value = str_replace($iframeContent, $change, $value);
            } else {
                $change = $iframeContent . ' frameborder="0"';
                $value = str_replace($iframeContent, $change, $value);
            }

            $change = preg_replace('|[\s]+|s', ' ', $iframeContent);
            $value = str_replace($iframeContent, $change, $value);
        }

//        if (!preg_match("/<style>(.*?)<\/style>/", $value)) {
//            $style = '<style></style>';
//            $value = $style . $value;
//        }

        return $value;
    }
}
