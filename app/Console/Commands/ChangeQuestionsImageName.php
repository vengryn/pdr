<?php

namespace App\Console\Commands;

use App\Models\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ChangeQuestionsImageName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'questions:change-image-name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Questions Image Name';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        try {
            $questions = DB::table('questions')->whereNotNull('picture')->get();

            foreach ($questions as $question) {
                $picture = $question->picture;

                if (Storage::exists($picture)) {
                    $array = explode('.', $picture);
                    $extension = end($array);

                    $newName = 'public/questions/QNS_' . $question->id . '.' . $extension;

                    if (!Storage::exists($newName)) {
                        Storage::move($picture, $newName);

                        DB::table('questions')->where('id', $question->id)->update([
                            'picture' => $newName
                        ]);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error( '🐷 '  . $e->getMessage() );
        }

        $this->info("➡ Image name successfully changed");
    }
}
