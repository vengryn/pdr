<?php

namespace App\Console\Commands;

use App\Models\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleaningOldData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:old-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning testing data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        try {
            DB::table('question_answer_histories')
                ->whereNotNull('deleted_at')
                ->where('created_at', '<=', now()->subMonth())
                ->delete();

            DB::table('exams')
                ->whereNotNull('deleted_at')
                ->where('created_at', '<=', now()->subMonth())
                ->delete();

            DB::table('wrong_question_answers')
                ->whereNotNull('deleted_at')
                ->where('created_at', '<=', now()->subMonth())
                ->delete();
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error( '🐷 '  . $e->getMessage() );
        }

        $this->info("➡ Old data successfully cleaning");
    }
}
