<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\PromoCode;
use App\Models\User;
use App\Services\MSCSync;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleaningTestingData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:testing-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning testing data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        try {
            DB::table('users')
                ->whereNotIn('role', [User::ROLE_ADMINISTRATOR, User::ROLE_SUPER_ADMINISTRATOR])
                ->delete();

            DB::table('promo_codes')
                ->where('type', PromoCode::TYPE_MARKETING)
                ->delete();

            DB::table('languages')->whereNotNull('deleted_at')->delete();
            DB::table('groups')->delete();
            DB::table('subscriptions')->delete();
            DB::table('exams')->truncate();
            DB::table('password_resets')->truncate();
            DB::table('social_support_services')->truncate();
            DB::table('questions_complaints')->truncate();
            DB::table('wrong_question_answers')->truncate();
            DB::table('ticket_trainings')->truncate();

            DB::table('payments')->delete();
            DB::table('internal_topic_traffic_rules')->delete();
            DB::table('item_traffic_rules')->delete();
            DB::table('topic_road_signs')->delete();
            DB::table('translate_road_sign_descriptions')->delete();
            DB::table('signs')->delete();
            DB::table('topic_road_markings')->delete();
            DB::table('translate_road_marking_descriptions')->delete();
            DB::table('markings')->delete();
            DB::table('translate_regulators')->delete();
            DB::table('translate_traffic_lights')->delete();
            DB::table('translate_video_lessons_traffic_rules')->delete();
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error( '🐷 '  . $e->getMessage() );
        }

        if ($this->confirm('Очистити питання із усією додатковою інформацією та імпортувати знову?')) {
            DB::table('questions')->delete();

            try {
                $MSCSync = new MSCSync(true, true);
                $MSCSync->run();
            } catch (\Exception $e) {
                Log::saveLog($e);
                $this->error( '🐷 '  . $e->getMessage() );
            }
        }

        $this->info("➡ Testing data successfully cleaning");
    }
}
