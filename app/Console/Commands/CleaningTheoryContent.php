<?php

namespace App\Console\Commands;

use App\Models\ItemTrafficRule;
use App\Models\Log;
use App\Models\Marking;
use App\Models\Question;
use App\Models\Sign;
use App\Models\TextPage;
use App\Models\TopicRoadMarking;
use App\Models\TopicRoadSign;
use App\Models\Translate\TranslateRegulator;
use App\Models\Translate\TranslateRoadMarkingDescription;
use App\Models\Translate\TranslateRoadSignDescription;
use App\Models\Translate\TranslateTrafficLight;
use Illuminate\Console\Command;

class CleaningTheoryContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:theory-content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning testing data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        try {
            Question::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->explanation = null;
                    $translate->save();
                }
            });

            ItemTrafficRule::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->content = null;
                    $translate->description = null;
                    $translate->save();
                }
            });

            TranslateRoadSignDescription::all()->map(function ($item) {
                $item->description = null;
                $item->save();
            });

            TopicRoadSign::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->description = null;
                    $translate->save();
                }
            });

            Sign::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->content = null;
                    $translate->description = null;
                    $translate->save();
                }
            });

            TranslateRoadMarkingDescription::all()->map(function ($item) {
                $item->description = null;
                $item->save();
            });

            TopicRoadMarking::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->description = null;
                    $translate->save();
                }
            });

            Marking::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->content = null;
                    $translate->description = null;
                    $translate->save();
                }
            });

            TranslateRegulator::all()->map(function ($item) {
                $item->description = null;
                $item->save();
            });

            TranslateTrafficLight::all()->map(function ($item) {
                $item->description = null;
                $item->save();
            });

            TextPage::with('translates')->get()->map(function ($item) {
                foreach ($item->translates as $translate) {
                    $translate->content = null;
                    $translate->save();
                }
            });
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error( '🐷 '  . $e->getMessage() );
        }

        $this->info("➡ Testing data successfully cleaning");
    }
}
