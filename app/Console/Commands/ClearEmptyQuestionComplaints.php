<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\PromoCode;
use App\Models\QuestionsComplaint;
use App\Models\User;
use App\Services\MSCSync;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearEmptyQuestionComplaints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:empty-question-complaints';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning testing data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        QuestionsComplaint::doesntHave('question')->delete();

        $this->info("➡ Successfully clear empty question complaints");
    }
}
