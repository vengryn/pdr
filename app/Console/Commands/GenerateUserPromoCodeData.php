<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\PromoCode;
use App\Models\User;
use App\Services\MSCSync;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateUserPromoCodeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promo-code:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning testing data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        User::teacher()->with(['promoCode'])->get()->each(function ($item) {
            if (!$item->promoCode) {
                $item->promoCode()->create([
                    'type' => PromoCode::TYPE_INTERNAL
                ]);
            }
        });

        User::student()->with(['promoCode'])->get()->each(function ($item) {
            if (!$item->promoCode) {
                $item->promoCode()->create([
                    'type' => PromoCode::TYPE_INTERNAL
                ]);
            }
        });

        $this->info("➡ PromoCode for users successfully generated.");
    }
}
