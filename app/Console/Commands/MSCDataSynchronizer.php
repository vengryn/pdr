<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MSCDataSynchronizer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mscdata:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize data from http://mainapi.hsc.gov.ua';

    /**
     * The OAuth2 authorization token
     *
     * @var string
     */
    private $_token = null;
    private $_defaultLanguageId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        try {
            $this->_defaultLanguageId = DB::table('languages')
                ->where('is_default', 1)
                ->whereNull('deleted_at')
                ->value('id');
        } catch (\Exception$exception) {
            $this->_defaultLanguageId = null;
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $defaultTimeLimit = ini_get('max_execution_time');
        set_time_limit(3600);

        try {
            $this->getAccessToken();
            $this->markQuestionsAsInactive();
            $this->getQuestions(0, 100);
        } catch (\Exeption $e) {
            $this->markQuestionsAsActive();
            $this->error($e->getMessage());
        }

        set_time_limit($defaultTimeLimit);

        return 0;
    }

    private function getAccessToken()
    {
        $authorizationToken = base64_encode(implode(':', [
            config('app.msc_oauth2_client_id'),
            config('app.msc_oauth2_client_secret')
        ]));

        $response = Http::withHeaders(['Authorization' => 'Basic ' . base64_encode('pdd_clnt_id:pdd_sec_ret')])
            ->asForm()
            ->post(config('app.msc_oauth2_url'), [
                'grant_type' => config('app.msc_oauth2_grant_type'),
            ]);

        if ($response->ok()) {
            $this->_token = $response->json('access_token');
        }

        if ($this->_token === null) {
            throw new \Exception("Can't retrieve authorization token.");
        }
    }

    private function getQuestions($page = 0, $size = 100)
    {
        if ($this->_token && $this->_defaultLanguageId) {
            $this->info("Page: {$page}");

            $response = Http::withToken($this->_token)->get(config('app.msc_questions_url'), [
                'page' => $page,
                'size' => $size
            ]);

            if ($response->ok()) {
                $questions = $response->json('content');

                if (is_array($questions) && !empty($questions)) {
                    foreach ($questions as $question) {
                        $this->processQuestion($question);
                    }

                    $this->getQuestions($page + 1);
                }
            } else {
                throw new \Exception($response->body());
            }
        }
    }

    private function processQuestion(array $question)
    {
        if ($this->isValidQuestionData($question)) {
            $id = DB::table('questions')
                ->where('external_id', $question['questId'])
                ->whereNull('deleted_at')
                ->value('id');

            if ($id) {
                $this->updateQuestion($id, $question);
            } else {
                $this->createQuestion($question);
            }
        }
    }

    private function isValidQuestionData(array $data)
    {
        $validator = Validator::make($data, [
            'questId'    => 'required',
            'exmTopic'   => 'required|array',
            'questsName' => 'required',
            'picture'    => 'present',
            'answers'    => 'required|array',
            'nquest'     => 'required'
        ]);

        if ($validator->fails()) {
            $this->error('Question data is not valid:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return false;
        }

        return true;
    }

    private function isValidAnswerData(array $data)
    {
        $validator = Validator::make($data, [
            'answerId'    => 'required',
            'answerNum'   => 'required',
            'answersName' => 'required',
            'isCorrect'   => 'present'
        ]);

        if ($validator->fails()) {
            $this->error('Answer data is not valid:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }

            return false;
        }

        return true;
    }

    private function markQuestionsAsInactive()
    {
        DB::table('questions')->update(['status' => 0]);
    }

    private function markQuestionsAsActive()
    {
        DB::table('questions')->update(['status' => 1]);
    }

    private function createQuestion(array $question)
    {
        if (($topicId = $this->getTopicId($question)) !== null) {
            $picture = $this->savePicture($question);

            $questionId = DB::table('questions')->insertGetId([
                'topic_traffic_rule_id' => $topicId,
                'external_id'           => (int)$question['questId'],
                'external_number'       => $question['nquest'],
                'picture'               => $picture,
                'status'                => 1,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s')
            ]);

            if ($questionId) {
                $isSaved = DB::table('translate_questions')->insert([
                    'question_id' => $questionId,
                    'language_id' => $this->_defaultLanguageId,
                    'name'        => $question['questsName'],
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s')
                ]);

                if (!$isSaved) {
                    $this->error("Can't save question's translation: {$this->getQuestionDescription($question)}");
                }

                $this->createAnswers($questionId, $question);
            } else {
                $this->error("Can't save question: {$this->getQuestionDescription($question)}");
            }
        } else {
            $this->error("Can't get internal ID of topic : {$this->getTopicDescription($question)}");
        }
    }

    private function updateQuestion($id, array $question)
    {
        $this->line("The question already exists: {$this->getQuestionDescription($question)}");
        /*
        $isRenewQuestionUpdateDate = false;

        $traslation = DB::table('translate_questions')
            ->where([
                'question_id' => $id,
                'language_id' => $this->_defaultLanguageId
            ])
            ->whereNull('deleted_at')
            ->first();

        if ($traslation !== null) {
            if (strcmp($traslation->name, $question['questsName']) !== 0) {
                DB::table('translate_questions')->where('question_id', $id)
                    ->update([
                        'name' => $question['questsName'],
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        unset($traslation);

        $answers = DB::table('questions_answers AS a')
            ->join('translate_questions_answers AS t', function($join) {
                $join->on('a.id', '=', 't.answer_id')
                    ->where('t.language_id', $this->_defaultLanguageId)
                    ->whereNull('t.deleted_at');
            })
            ->where('a.question_id', $id)
            ->whereNull('deleted_at')
            ->get();

        if (!empty($answers)) {

        }

        DB::table('questions')->where('id', $id)
            ->update([
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        */
    }

    private function getTopicId(array $question)
    {
        $id = DB::table('topic_traffic_rules')
            ->where('external_id', $question['exmTopic']['id'])
            ->whereNull('deleted_at')
            ->value('id');
        // FIXME: part of code for testing
        if (!$id) {
            $id = DB::table('topic_traffic_rules')->insertGetId([
                'external_id' => $question['exmTopic']['id'],
                'number'      => mt_rand(1, 100),
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s')
            ]);

            if ($id) {
                $isSaved = DB::table('translate_topic_traffic_rules')->insert([
                    'topic_traffic_rule_id' => $id,
                    'name'                  => $question['exmTopic']['value'],
                    'language_id'           => $this->_defaultLanguageId,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s')
                ]);

                if (!$isSaved) {
                    $this->error("Can't save topic's tranlation: {$this->getTopicDescription($question)}");
                }
            } else {
                $this->error("Can't save topic: {$this->getTopicDescription($question)}");
            }
        }

        return $id;
    }

    private function savePicture(array $question)
    {
        if ($question['picture']) {
            $path = 'public/questions/' . Str::random(50) . '.jpg';

            if (Storage::disk('local')->put($path, base64_decode($question['picture']), 'public')) {
                return $path;
            } else {
                $this->error("Can't save picture for question: {$this->getQuestionDescription($question)}");
            }
        }

        return null;
    }

    private function createAnswers($questionId, array $question)
    {
        foreach ($question['answers'] as $answer) {
            if ($this->isValidAnswerData($answer)) {
                $answerId = DB::table('questions_answers')->insertGetId([
                    'question_id' => $questionId,
                    'external_id' => (int)$answer['answerId'],
                    'is_correct'  => $answer['isCorrect'] !== null,
                    'order'       => (int)$answer['answerNum'],
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s')
                ]);

                if ($answerId) {
                    $isSaved = DB::table('translate_questions_answers')->insert([
                        'answer_id'   => $answerId,
                        'language_id' => $this->_defaultLanguageId,
                        'name'        => $answer['answersName'],
                        'created_at'  => date('Y-m-d H:i:s'),
                        'updated_at'  => date('Y-m-d H:i:s')
                    ]);

                    if (!$isSaved) {
                        $this->error("Can't save answer's translation: {$this->getAnswerDescription($answer)} for question: {$this->getQuestionDescription($question)}");
                    }
                } else {
                    $this->error("Can't save answer: {$this->getAnswerDescription($answer)} for question: {$this->getQuestionDescription($question)}");
                }
            }
        }
    }

    private function getTopicDescription(array $question)
    {
        $topicName = str_replace(["\r", "\n"], '', $question['exmTopic']['value']);

        return "#{$question['exmTopic']['id']} - {$topicName}";
    }

    private function getQuestionDescription(array $question)
    {
        $questionName = str_replace(["\r", "\n"], '', $question['questsName']);

        return "#{$question['questId']} - {$questionName}";
    }

    private function getAnswerDescription(array $answer)
    {
        $answerName = str_replace(["\r", "\n"], '', $answer['answersName']);

        return "#{$answer['answerId']} - {$answerName}";
    }
}
