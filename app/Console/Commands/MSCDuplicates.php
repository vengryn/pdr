<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class MSCDuplicates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mscdata:duplicates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize data from http://mainapi.hsc.gov.ua';

    /**
     * The OAuth2 authorization token
     *
     * @var string
     */
    private $_token = null;

    private $ids = [];

    private $timestamp;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->timestamp = now()->timestamp;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $defaultTimeLimit = ini_get('max_execution_time');
        set_time_limit(3600);

        try {
            $this->getAccessToken();
            $this->getQuestions(0, 100);
        } catch (\Exeption $e) {
            $this->error($e->getMessage());
        }

        set_time_limit($defaultTimeLimit);
    }

    private function getAccessToken()
    {
        $response = Http::withHeaders(['Authorization' => 'Basic ' . base64_encode('pdd_clnt_id:pdd_sec_ret')])
            ->asForm()
            ->post(config('app.msc_oauth2_url'), [
                'grant_type' => config('app.msc_oauth2_grant_type'),
            ]);

        if ($response->ok()) {
            $this->_token = $response->json('access_token');
        }

        if ($this->_token === null) {
            throw new \Exception("Can't retrieve authorization token.");
        }
    }

    private function getQuestions($page = 0, $size = 100)
    {
        if ($this->_token) {
            $response = Http::withToken($this->_token)
                ->get(config('app.msc_questions_url'), [
                    'page' => $page,
                    'size' => $size
                ]);

            if ($response->ok()) {
                $questions = $response->json('content');

                if (is_array($questions) && !empty($questions)) {
                    foreach ($questions as $question) {
                        $this->processQuestion($question);
                    }

                    $this->ids = array_merge($this->ids, array_column($questions, 'questId'));

                    $this->getQuestions($page + 1, $size);
                }
            } else {
                throw new \Exception($response->body());
            }
        }
    }

    private function processQuestion(array $question)
    {
        if (in_array($question['questId'], $this->ids)) {
            Storage::append('questions/duplicates-'.$this->timestamp.'.json', json_encode($question, JSON_UNESCAPED_UNICODE));
            Storage::append('questions/duplicates-ids-'.$this->timestamp.'.json', $question['questId']);
        }
    }
}
