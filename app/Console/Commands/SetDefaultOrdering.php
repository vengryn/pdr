<?php

namespace App\Console\Commands;

use App\Models\ItemTrafficRule;
use App\Models\Log;
use App\Models\Marking;
use App\Models\Sign;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SetDefaultOrdering extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-default-ordering';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Default Ordering';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("➡ Starting " .$this->signature);

        try {

            if ($this->confirm('for marking?')) {
                Marking::get()->groupBy('topic_road_marking_id')->each(function ($group) {
                    $group->each(function ($item, $key) {
                        $item->order = $key + 1;
                        $item->save();
                    });
                });
            }

            if ($this->confirm('for sign?')) {
                Sign::get()->groupBy('topic_road_sign_id')->each(function ($group) {
                    $group->each(function ($item, $key) {
                        $item->order = $key + 1;
                        $item->save();
                    });
                });
            }

            if ($this->confirm('for item traffic rule?')) {
                ItemTrafficRule::get()->groupBy('internal_topic_traffic_rule_id')->each(function ($group) {
                    $group->each(function ($item, $key) {
                        $item->order = $key + 1;
                        $item->save();
                    });
                });
            }
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error( '🐷 '  . $e->getMessage() );
        }

        $this->info("➡ Default ordering successfully inserts");
    }
}
