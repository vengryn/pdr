<?php

namespace App\Console\Commands;

use App\Models\CategoryDriverLicense;
use App\Models\Log;
use App\Models\Question;
use App\Models\QuestionsAnswer;
use App\Models\Setting;
use App\Models\TopicTrafficRule;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class SyncApiQuestions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api-question:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize api question and generate json file with data';

    /**
     * @var false
     */
    protected $updated;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->updated = false;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("➡ ".trans('translation.starting')."");

        try {
            if ($lastDateUpdatedString = Setting::getSetting('date_of_last_response_file_generation')) {

                $lastDateUpdated = Carbon::createFromTimestamp($lastDateUpdatedString);

                $listResourceUpdated = [
                    [
                        'item' => TopicTrafficRule::with(['folders'])->has('folders'),
                        'params' => ['translates']
                    ],
                    [
                        'item' => CategoryDriverLicense::with(['topicTrafficRules'])->has('topicTrafficRules'),
                        'params' => null
                    ],
                    [
                        'item' => Question::with(['topicTrafficRule', 'translates'])->has('topicTrafficRule'),
                        'params' => ['translates']
                    ],
                    [
                        'item' => QuestionsAnswer::with(['question', 'translates'])->has('question'),
                        'params' => ['translates']
                    ],
                ];

                foreach ($listResourceUpdated as $resource) {
                    if ($this->checkResourceUpdated($resource, $lastDateUpdated)) {
                        $this->updated = true;

                        $this->generateFile($lastDateUpdatedString);
                        break;
                    }
                }

                if (!$this->updated) {
                    $this->info("➡ ".trans('translation.no_changes_to_update')."");
                }
            } else {
                $this->generateFile();
            }
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error('🐷 ' . $e->getMessage());
        }
    }

    /**
     * Check at updated resources
     *
     * @param $resource
     * @param $lastDateUpdated
     * @return bool
     */
    public function checkResourceUpdated($resource, $lastDateUpdated)
    {
        $item = $resource['item']->where('updated_at', '>=', $lastDateUpdated);

        if ($resource['params'] && is_array($resource['params'])) {
            $item->orWhereHas('translates', function ($query) use ($lastDateUpdated) {
                $query->where('updated_at', '>=', $lastDateUpdated);
            });
        }

        return $item->count() > 0;
    }

    /**
     * Generate Json file
     *
     * @param null $lastDateUpdatedString
     */
    public function generateFile($lastDateUpdatedString = null)
    {
        $dateTimeNow = Carbon::now();
        $data = Question::generateApiQuestions() + ['last_questions_updated' => Carbon::parse($dateTimeNow)->timestamp];

        Storage::disk('local')->put(Question::getPathQuestionFile() . Carbon::parse($dateTimeNow)->timestamp . '.json', json_encode($data, JSON_UNESCAPED_UNICODE));

        if (!env('APP_DEBUG')) {
            Storage::disk('digitalocean')->put(Question::getPathQuestionFile() . Carbon::parse($dateTimeNow)->timestamp . '.json', json_encode($data, JSON_UNESCAPED_UNICODE), [
                'ContentType' => 'application/octet-stream'
            ]);
        }

        Setting::storeSetting('date_of_last_response_file_generation', $dateTimeNow);

        $noDeletes = [
            $lastDateUpdatedString ? Question::getPathQuestionFile() . Carbon::parse($lastDateUpdatedString)->timestamp . '.json' : null,
            Question::getPathQuestionFile() . Carbon::parse($dateTimeNow)->timestamp . '.json'
        ];

        if ($files = Storage::disk('local')->files(Question::getPathQuestionFile())) {
            foreach ($files as $file) {
                if (!in_array($file, $noDeletes)) {
                    Storage::disk('local')->delete($file);
                }
            }
        }

        if (!env('APP_DEBUG')) {
            if ($files = Storage::disk('digitalocean')->files(Question::getPathQuestionFile())) {
                foreach ($files as $file) {
                    if (!in_array($file, $noDeletes)) {
                        Storage::disk('digitalocean')->delete($file);
                    }
                }
            }
        }

        $this->info("➡ ".trans('translation.questions_successfully_updated')."");
    }
}
