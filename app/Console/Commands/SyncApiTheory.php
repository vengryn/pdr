<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\Setting;
use App\Services\Theory;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class SyncApiTheory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api-theory:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize api theory and generate json file with data';

    /**
     * @var false
     */
    protected $updated;

    /**
     * @var Theory
     */
    protected $theory;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->updated = false;
        $this->theory = new Theory();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("➡ " . trans('translation.starting') . "");

        try {
            $lastDateUpdatedString = Setting::getSetting('date_of_last_response_theory_file_generation');
            $this->generateFile($lastDateUpdatedString);
        } catch (\Exception $e) {
            Log::saveLog($e);
            $this->error('🐷 ' . $e->getMessage());
        }
    }

    /**
     * Check at updated resources
     *
     * @param $resource
     * @param $lastDateUpdated
     * @return bool
     */
    public function checkResourceUpdated($resource, $lastDateUpdated)
    {
        $item = $resource['item']->where('updated_at', '>=', $lastDateUpdated);

        if ($resource['params'] && is_array($resource['params'])) {
            $item->orWhereHas('translates', function ($query) use ($lastDateUpdated) {
                $query->where('updated_at', '>=', $lastDateUpdated);
            });
        }

        return $item->count() > 0;
    }

    /**
     * Generate Json file
     *
     * @param null $lastDateUpdatedString
     */
    public function generateFile($lastDateUpdatedString = null)
    {
        $dateTimeNow = Carbon::now();
        $data = $this->theory->generateApiTheory() + ['last_theory_updated' => Carbon::parse($dateTimeNow)->timestamp];

        Storage::disk('local')->put($this->theory->getPathTheoryFile() . Carbon::parse($dateTimeNow)->timestamp . '.json', json_encode($data, JSON_UNESCAPED_UNICODE));

        if (!env('APP_DEBUG')) {
            Storage::disk('digitalocean')->put($this->theory->getPathTheoryFile() . Carbon::parse($dateTimeNow)->timestamp . '.json', json_encode($data, JSON_UNESCAPED_UNICODE), [
                'ContentType' => 'application/octet-stream'
            ]);
        }

        Setting::storeSetting('date_of_last_response_theory_file_generation', $dateTimeNow);

        $noDeletes = [
            $lastDateUpdatedString ? $this->theory->getPathTheoryFile() . Carbon::parse($lastDateUpdatedString)->timestamp . '.json' : null,
            $this->theory->getPathTheoryFile() . Carbon::parse($dateTimeNow)->timestamp . '.json'
        ];

        if ($files = Storage::disk('local')->files($this->theory->getPathTheoryFile())) {
            foreach ($files as $file) {
                if (!in_array($file, $noDeletes)) {
                    Storage::disk('local')->delete($file);
                }
            }
        }

        if (!env('APP_DEBUG')) {
            if ($files = Storage::disk('digitalocean')->files($this->theory->getPathTheoryFile())) {
                foreach ($files as $file) {
                    if (!in_array($file, $noDeletes)) {
                        Storage::disk('digitalocean')->delete($file);
                    }
                }
            }
        }

        $this->info("➡ " . trans('translation.theory_successfully_updated') . "");
    }
}
