<?php

namespace App\Exceptions;

use App\Models\Log;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Throwable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (NotFoundHttpException $e, $request) {
            if ($request->wantsJson()) {
                return response()->json(['status' => 'fail', 'message' => 'The specified URL cannot be found'], JsonResponse::HTTP_NOT_FOUND);
            }
        });

        $this->renderable(function (MethodNotAllowedHttpException $e, $request) {
            if ($request->wantsJson()) {
                return response()->json(['status' => 'fail', 'message' => 'The specified method for the request is invalid'], JsonResponse::HTTP_METHOD_NOT_ALLOWED);
            }
        });

        $this->renderable(function (ModelNotFoundException $e, $request) {
            if ($request->wantsJson()) {
                return response()->json(['status' => 'fail', 'message' => 'No query results for model'], JsonResponse::HTTP_NOT_FOUND);
            }
        });

        $this->renderable(function (AuthenticationException $e, $request) {
            if ($request->wantsJson()) {
                return response()->json(['status' => 'fail', 'message' => 'Unauthenticated'], JsonResponse::HTTP_UNAUTHORIZED);
            }
        });

        $this->renderable(function (\Exception $e, $request) {
            if ($request->wantsJson()) {
                Log::saveLog($e);
                return response()->json(['status' => 'fail', 'message' => 'Bad request'], JsonResponse::HTTP_BAD_REQUEST);
            }
        });
    }
}
