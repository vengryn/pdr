<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;

class MSCQuestionUpdatesDifferentExport implements ShouldAutoSize, FromView, WithHeadings
{
    use Exportable;

    public $questions;

    public function __construct($questions)
    {
        $this->questions = $questions;
    }

    public function view(): View
    {
        return view('updating-msc.updates-different', [
            'questions' => $this->questions
        ]);
    }

    public function headings(): array
    {
        return [
            'Before',
            null,
            'After'
        ];
    }

}
