<?php

namespace App\Exports;

use App\Models\Language;
use App\Models\Question;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StudentsExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithStyles
{
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function map($student): array
    {
        return [
            $student->student_last_name . ' ' . $student->student_first_name . ' ' . $student->student_patronymic,
            $student->email,
            $student->teacher_last_name . ' ' . $student->teacher_first_name . ' ' . $student->teacher_patronymic,
            $student->group_name,
            $student->categories,
            $student->promo_code,
            $student->created_at ? Carbon::parse($student->created_at)->format('d.m.Y') : null,
            $student->exam_total_number_attempts,
            $student->exam_success_number_attempts,
            $student->average_score_exam,
            $student->total_progress_theme,
            $student->questions_passed,
            $student->correct_answers,
            $student->wrong_answers,
            $student->subscription,
            $student->subscription && !empty($student->subscription_start_at) && !empty($student->subscription_end_at)
                ? Carbon::parse($student->subscription_start_at)->format('d.m.Y') . '-' . Carbon::parse($student->subscription_end_at)->format('d.m.Y')
                : null
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $totalActiveQuestions = Cache::rememberForever('total_active_questions', function () {
            return Question::where('status', true)->count();
        });

        $lang = Cache::remember('default_language', 900, function () {
            return Language::default()->first()->id ?? null;
        });

        return DB::table('users')
            ->where('users.role', User::ROLE_STUDENT)
            ->leftJoin('info_students', function ($join) {
                $join->on('info_students.user_id', '=', 'users.id')->whereNull('info_students.deleted_at')->limit(1);
            })
            ->leftJoin('groups', function ($join) {
                $join->on('users.group_id', '=', 'groups.id')->whereNull('groups.deleted_at');
                $join->leftJoin('users as teacher', 'groups.user_id', '=', 'teacher.id')->whereNull('teacher.deleted_at');
                $join->leftJoin('info_teachers', 'info_teachers.user_id', '=', 'teacher.id')->whereNull('info_teachers.deleted_at')->limit(1);
            })
            ->leftJoin('category_driver_license_user', function ($join) {
                $join->on('category_driver_license_user.user_id', '=', 'users.id');
            })
            ->leftJoin('category_driver_licenses as category', function ($join) {
                $join->on('category_driver_license_user.category_driver_license_id', '=', 'category.id')
                    ->whereNull('category.deleted_at');
            })
            ->leftJoin('promo_codes', function ($join) {
                $join->on('promo_codes.user_id', '=', 'users.id')->whereNull('promo_codes.deleted_at')->limit(1);
            })
            ->leftJoin(DB::raw("(SELECT user_id, ROUND(SUM(correct_answers) / COUNT(id)) as average_score_exam, COUNT(id) as exam_total_number_attempts, deleted_at FROM exams GROUP BY user_id, deleted_at) as exams"), function ($join) {
                $join->on('exams.user_id', '=', 'users.id')->whereNull('exams.deleted_at');
            })
            ->leftJoin(DB::raw("(SELECT user_id, COUNT(id) as exam_success_number_attempts, is_passed, deleted_at FROM exams GROUP BY user_id, is_passed, deleted_at) as success_exams"), function ($join) {
                $join->on('success_exams.user_id', '=', 'users.id')
                    ->where('success_exams.is_passed', true)
                    ->whereNull('success_exams.deleted_at');
            })
            ->leftJoin(DB::raw("(SELECT user_id, COUNT(id) as questions_passed, deleted_at FROM question_answer_histories GROUP BY user_id, deleted_at) as question_answer_histories"), function ($join) {
                $join->on('question_answer_histories.user_id', '=', 'users.id')
                    ->whereNull('question_answer_histories.deleted_at');
            })
            ->leftJoin(DB::raw("(SELECT user_id, COUNT(id) as correct_answers, ROUND((COUNT(id) / {$totalActiveQuestions}) * 100) as total_progress_theme, is_passed, deleted_at FROM question_answer_histories GROUP BY user_id, is_passed, deleted_at) as success_question_answer_histories"), function ($join) {
                $join->on('success_question_answer_histories.user_id', '=', 'users.id')
                    ->where('success_question_answer_histories.is_passed', true)
                    ->whereNull('success_question_answer_histories.deleted_at');
            })
            ->leftJoin(DB::raw("(SELECT user_id, COUNT(id) as wrong_answers, is_passed, deleted_at FROM question_answer_histories GROUP BY user_id, is_passed, deleted_at) as wrong_question_answer_histories"), function ($join) {
                $join->on('wrong_question_answer_histories.user_id', '=', 'users.id')
                    ->where('wrong_question_answer_histories.is_passed', false)
                    ->whereNull('wrong_question_answer_histories.deleted_at');
            })
            ->leftJoin('user_subscriptions', function ($join) {
                $join->on('user_subscriptions.user_id', '=', 'users.id')
                    ->orderByDesc('user_subscriptions.end_at')
                    ->where('user_subscriptions.end_at', '>', Carbon::now())
                    ->limit(1);
            })
            ->whereNull('users.deleted_at')
            ->select(
                'users.id',
                'users.email',
                'users.created_at',
                'info_students.first_name as student_first_name',
                'info_students.last_name as student_last_name',
                'info_students.patronymic as student_patronymic',
                'groups.id as group_id',
                'groups.title as group_name',
                'info_teachers.first_name as teacher_first_name',
                'info_teachers.last_name as teacher_last_name',
                'info_teachers.patronymic as teacher_patronymic',
                DB::raw("(GROUP_CONCAT(DISTINCT category.title SEPARATOR ',')) as categories"),
                'promo_codes.code as promo_code',
                'exams.exam_total_number_attempts',
                'success_exams.exam_success_number_attempts',
                'exams.average_score_exam',
                'success_question_answer_histories.total_progress_theme',
                'question_answer_histories.questions_passed',
                'success_question_answer_histories.correct_answers',
                'wrong_question_answer_histories.wrong_answers',
                DB::raw("(CASE WHEN user_subscriptions.is_auto_premium = 1 THEN 'Підписка: Авто-преміум'
                    ELSE (SELECT translate_subscriptions.name FROM translate_subscriptions WHERE translate_subscriptions.subscription_id = user_subscriptions.subscription_id AND language_id = {$lang} AND deleted_at IS NULL)
                    END) AS subscription"),
                'user_subscriptions.start_at as subscription_start_at',
                'user_subscriptions.end_at as subscription_end_at',
            )
            ->groupBy('users.id')
//            ->groupBy('users.email')
//            ->groupBy('users.created_at')
//            ->groupBy('student_first_name')
//            ->groupBy('student_last_name')
//            ->groupBy('student_patronymic')
//            ->groupBy('group_id')
//            ->groupBy('group_name')
//            ->groupBy('teacher_first_name')
//            ->groupBy('teacher_last_name')
//            ->groupBy('teacher_patronymic')
//            ->groupBy('promo_code')
//            ->groupBy('exams.exam_total_number_attempts')
//            ->groupBy('success_exams.exam_success_number_attempts')
//            ->groupBy('exams.average_score_exam')
//            ->groupBy('success_question_answer_histories.total_progress_theme')
//            ->groupBy('question_answer_histories.questions_passed')
//            ->groupBy('success_question_answer_histories.correct_answers')
//            ->groupBy('wrong_question_answer_histories.wrong_answers')
//            ->groupBy('user_subscriptions.is_auto_premium')
//            ->groupBy('user_subscriptions.subscription_id')
//            ->groupBy('subscription_start_at')
//            ->groupBy('subscription_end_at')
            ->orderByDesc('created_at')
            ->get();
    }

    public function headings(): array
    {
        return [
            'FullName',
            'Email',
            'Teacher',
            'Group',
            'Categories',
            'Promocode',
            'Registration',
            'ExamsPassed',
            'SuccessfulExams',
            'AverageExamRate',
            'ThemesProgress',
            'QuestionsAnswered',
            'RightAnswers',
            'WrongAnswers',
            'PremiumType',
            'PremiumExpires',
        ];
    }
}
