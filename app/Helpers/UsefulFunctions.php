<?php

namespace App\Helpers;

use App\Models\Marking;
use App\Models\Sign;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

trait UsefulFunctions
{
    /**
     * @param $name
     * @param null $url
     */
    public function saveSessionRedirectUrl($name, $url = null)
    {
        request()->session()->put($name, $url);
    }

    /**
     * Save Translates for Resource
     *
     * @param array $data
     */
    public function saveTranslates($resource, array $data)
    {
        foreach ($data as $name => $translates) {
            foreach ($translates as $id => $value) {
                $resource->translates()->updateOrCreate(['language_id' => $id], [
                    $name => $value
                ]);
            }
        }
    }

    /**
     * Store Session filter params.
     *
     * @param string $groupName
     * @param array $params
     * @return array
     */
    public function sessionStoreParams($groupName, $params = [])
    {
        foreach ($params as $name => $value) {
            session(["$groupName.$name" => $value]);
        }

        return session($groupName);
    }

    /**
     * Return generic json response with the given data.
     *
     * @param array $data
     * @param string $status
     * @param int $statusCode
     * @param array $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiResponse($data = [], $status = 'success', $statusCode = JsonResponse::HTTP_OK, $headers = [])
    {
        if (empty($data)) {
            $data = null;
        }

        return response()->json(['status' => $status, 'data' => $data], $statusCode, $headers);
    }

    /**
     * Return generic json response with the given data with Token.
     *
     * @param array $data
     * @param \App\Models\User $user
     * @param string $status
     * @param int $statusCode
     * @param array $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiResponseWithToken($data = [], $user = null, $status = 'success', $statusCode = JsonResponse::HTTP_OK, $headers = [])
    {
        if (!$user) {
            $user = Auth::user();
        }

        $headers = $headers + [
                'Access-Token' => JWTAuth::fromUser($user),
                'Token-Type' => 'Bearer',
                'Expires-In' => Carbon::now()->addMinutes(auth('api')->factory()->getTTL())->timestamp,
                'Expires-Refresh-In' => Carbon::now()->addMinutes(config('jwt.refresh_ttl'))->timestamp,
                'Access-Control-Expose-Headers' => 'access-token'
            ];

        return response()->json(['status' => $status, 'data' => $data], $statusCode, $headers);
    }

    /**
     * Return generic json error response.
     *
     * @param string $message
     * @param int $statusCode
     * @param string $status
     * @param array $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiResponseWithError($message = null, $status = 'fail', $statusCode = JsonResponse::HTTP_BAD_REQUEST, $headers = [])
    {
        return response()->json(['status' => $status, 'message' => $message], $statusCode, $headers);
    }

    public function paginate(Collection $results, $pageSize)
    {
        $page = Paginator::resolveCurrentPage('page');

        $total = $results->count();

        return $this->paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }

    protected function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items', 'total', 'perPage', 'currentPage', 'options'
        ));
    }

    public function getInfoUpdatedQuestions()
    {
        try {
            request()->session()->flash('info', trans('translation.change_question'));
        } catch (\Exception $exception) {
            //
        }
    }

    public function getInfoUpdatedTheory()
    {
        try {
            request()->session()->flash('info', trans('translation.change_theory'));
        } catch (\Exception $exception) {
            //
        }
    }

    public function replaceTokenImage($translates, $fields = [])
    {
        $translates->map(function ($item) use ($fields) {
            foreach ($fields as $field) {
                $image = null;

                preg_match_all('/\{(.*?)\|(RSS|RMM)_(.*?)\}/', $item->{$field}, $matches, PREG_SET_ORDER);

                foreach ($matches as $match) {
                    switch ($match[2]) {
                        case 'RSS':
                            $image = Cache::rememberForever("RSS_{$match[3]}", function () use ($match) {
                                return Sign::where('number', $match[3])->without('translates')->first()->image26x26 ?? '';
                            });
                            break;
                        case 'RMM':
                            $image = Cache::rememberForever("RMM_{$match[3]}", function () use ($match) {
                                return Marking::where('number', $match[3])->without('translates')->first()->image26x26 ?? '';
                            });
                            break;
                    }

                    if ($image) {
                        $imgPath = env('APP_URL') . Storage::url($image);
                        $item->{$field} = str_replace($match[0], "{{$match[1]}|{$match[2]}_{$match[3]}|{$imgPath}}", $item->{$field});
                    }
                }
            }

            return $item;
        });

        return $translates;
    }
}
