<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ChangePasswordRequest;
use App\Http\Requests\Api\Auth\ValidatePasswordResetTokenRequest;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserWithStatisticsResource;
use App\Models\LinkedSocialAccount;
use App\Models\LinkedSocialInfo;
use App\Models\Log;
use App\Models\Setting;
use App\Models\Subscription;
use App\Models\User;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\LoginSocialRequest;
use App\Http\Requests\Api\Auth\PasswordResetRequest;
use App\Models\UserSubscription;
use App\Notifications\StudentRegistered;
use App\Repositories\Interfaces\UserSubscriptionRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log as Logging;

class AuthController extends Controller
{
    private $userSubscriptionRepository;

    public function __construct(UserSubscriptionRepositoryInterface $userSubscriptionRepository)
    {
        $this->userSubscriptionRepository = $userSubscriptionRepository;
    }

    /**
     * @param $user
     */
    protected function registered($user)
    {
        $user->notify(new StudentRegistered());
    }

    /**
     * Login api
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->validated() + ['role' => User::ROLE_STUDENT];

        if (!auth()->attempt($credentials)) {
            return $this->apiResponseWithError(trans('auth.failed'), 'fail', JsonResponse::HTTP_UNAUTHORIZED);
        }

        return $this->apiResponseWithToken(UserWithStatisticsResource::make(auth()->user()
            ->load('exams', 'questionAnswerHistories'))
        );
    }

    /**
     * Register api
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        if ($promoCode = $request->get('promo_code')) {
            $referer = User::student()->whereHas('promoCode', function ($query) use ($promoCode) {
                $query->where('code', $promoCode);
            })->first();
        }

        $user = User::create($request->only(['email', 'password']) + [
                'role' => User::ROLE_STUDENT,
                'referer_id' => $referer->id ?? null
            ]);

        $user->infoStudent()->create($request->only(['first_name', 'last_name']));

        if (Subscription::isAutoPremium()) {
            $this->userSubscriptionRepository->createAutoPremium($user, UserSubscription::SOURCE_REGISTER);
        }

        try {
            $this->registered($user);
        } catch (\Exception $e) {
            Log::saveLog($e);
        }

        return $this->apiResponseWithToken(UserResource::make($user), $user);
    }

    /**
     * Login Or Register via Social api
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginSocial(LoginSocialRequest $request)
    {
        Logging::channel('social-auth-logging')->info($request->all());

        if ($packageId = $request->get('package_id')) {
            switch ($packageId) {
                case 'com.infotex.testpdr.mobile':
                    config(['services.facebook.client_id' => env('FACEBOOK_INFOTEX_CLIENT_ID')]);
                    config(['services.facebook.client_secret' => env('FACEBOOK_INFOTEX_CLIENT_SECRET')]);
                    break;
                case 'web':
                    config(['services.facebook.client_id' => env('FACEBOOK_WEB_CLIENT_ID')]);
                    config(['services.facebook.client_secret' => env('FACEBOOK_WEB_CLIENT_SECRET')]);
                    break;
            }
        }

        try {
            $userSocial = Socialite::driver($request->input('provider_name'))
                ->userFromToken($request->input('token'));
        } catch (\Exception $e) {
            Log::saveLog($e);
            return $this->apiResponseWithError('Incorrect token', 'fail', JsonResponse::HTTP_UNAUTHORIZED);
        }

        switch ($request->input('provider_name')) {
            case 'apple':
                $linkedSocialInfo = LinkedSocialInfo::where('provider_name', 'apple')
                    ->where('provider_id', $userSocial->getId())
                    ->first();

                $dataUser['email'] = $request->input('email', $userSocial->getEmail())
                    ?: $linkedSocialInfo->email ?? null;
                $dataUser['first_name'] = $request->input('first_name', explode(' ', $userSocial->getName())[0] ?? null)
                    ?: $linkedSocialInfo->first_name ?? null;
                $dataUser['last_name'] = $request->input('last_name', explode(' ', $userSocial->getName())[1] ?? null)
                    ?: $linkedSocialInfo->last_name ?? null;
                break;
            default:
                $dataUser['email'] = $userSocial->getEmail();
                $dataUser['first_name'] = explode(' ', $userSocial->getName())[0] ?? null;
                $dataUser['last_name'] = explode(' ', $userSocial->getName())[1] ?? null;
        }

        $dataUser['avatar'] = $userSocial->getAvatar();

        if ($request->input('provider_name') == 'facebook' && !$dataUser['email']) {
            return $this->apiResponseWithError(trans('validation.empty_email_facebook'),
                'fail', JsonResponse::HTTP_FORBIDDEN);
        }

        if (User::onlyTrashed()->where('email', $dataUser['email'])->first()) {
            return $this->apiResponseWithError(trans('validation.user_exists'), 'fail', JsonResponse::HTTP_FORBIDDEN);
        }

        if ($request->input('provider_name') == 'apple' && empty($dataUser['email'])) {
            if (LinkedSocialAccount::onlyTrashed()->where('provider_name', 'apple')->where('provider_id', $userSocial->getId())->first()) {
                return $this->apiResponseWithError(trans('validation.user_exists'), 'fail', JsonResponse::HTTP_FORBIDDEN);
            }
        }

        $linkedSocialAccount = LinkedSocialAccount::where('provider_name', $request->input('provider_name'))
            ->where('provider_id', $userSocial->getId())
            ->first();

        if ($linkedSocialAccount && $linkedSocialAccount->user) {
            $user = $linkedSocialAccount->user;

            if (!$user->isStudent()) {
                return $this->apiResponseWithError(trans('validation.user_exists'), 'duplicate', JsonResponse::HTTP_FORBIDDEN);
            }
        } else {
            if ($request->input('provider_name') == 'apple' && !$dataUser['email']) {
                return $this->apiResponseWithError(trans('validation.empty_email_apple'), 'fail', JsonResponse::HTTP_FORBIDDEN);
            }

            $user = User::where('email', $dataUser['email'])->first();

            if (!$user) {
                if (!$request->boolean('is_user_agreement', false)) {
                    if ($request->input('provider_name') == 'apple') {
                        LinkedSocialInfo::updateOrCreate(['provider_id' => $userSocial->getId(), 'provider_name' => 'apple'], [
                            'first_name' => $dataUser['first_name'],
                            'last_name' => $dataUser['last_name'],
                            'email' => $dataUser['email'],
                        ]);
                    }

                    return $this->apiResponse(null, 'not_register');
                }

                if ($dataUser['avatar']) {
                    $avatarPath = 'public/users/avatar/' . Str::random(50) . '.jpg';
                    Storage::put($avatarPath, file_get_contents($dataUser['avatar']), 'public');
                }

                if ($promoCode = $request->get('promo_code')) {
                    $referer = User::student()->whereHas('infoStudent', function ($query) use ($promoCode) {
                        $query->where('promo_code', $promoCode);
                    })->first();
                }

                $user = User::create([
                    'email' => $dataUser['email'],
                    'role' => User::ROLE_STUDENT,
                    'referer_id' => $referer->id ?? null
                ]);

                $user->infoStudent()->create([
                    'first_name' => $dataUser['first_name'],
                    'last_name' => $dataUser['last_name'],
                    'avatar' => $avatarPath ?? null
                ]);

                if (!empty($linkedSocialInfo)) {
                    $linkedSocialInfo->delete();
                }

                if (Subscription::isAutoPremium()) {
                    $this->userSubscriptionRepository->createAutoPremium($user, UserSubscription::SOURCE_REGISTER);
                }

                $this->registered($user);
            } else {
                if (!$user->isStudent()) {
                    return $this->apiResponseWithError(trans('validation.user_exists'), 'duplicate', JsonResponse::HTTP_FORBIDDEN);
                }
            }

            $user->linkedSocialAccounts()->updateOrCreate([
                'provider_id' => $userSocial->getId(),
                'provider_name' => $request->input('provider_name'),
            ]);
        }
        $user->provider_name = $request->input('provider_name');

        return $this->apiResponseWithToken(UserWithStatisticsResource::make($user->load('exams', 'questionAnswerHistories')), $user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['status' => 'success', 'message' => 'Successfully logged out'], 200);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            $refreshed = JWTAuth::refresh(JWTAuth::getToken());
        } catch (\Exception $e) {
            return $this->apiResponseWithError($e->getMessage());
        }

        $user = JWTAuth::setToken($refreshed)->toUser();

        return $this->apiResponseWithToken(UserResource::make($user));
    }

    /**
     * Reset Password
     * @param PasswordResetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function passwordReset(PasswordResetRequest $request)
    {
        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? $this->apiResponse(null)
            : $this->apiResponseWithError(__($status));
    }

    /**
     * Validate Password Reset Token
     * @param ValidatePasswordResetTokenRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validatePasswordResetToken(ValidatePasswordResetTokenRequest $request)
    {
        $credentials = $request->only('email', 'token');

        if (is_null($user = Password::getUser($credentials))) {
            return $this->apiResponseWithError(__(Password::INVALID_USER));
        }

        if (!Password::tokenExists($user, $credentials['token'])) {
            return $this->apiResponseWithError(__(Password::INVALID_TOKEN));
        }

        return $this->apiResponse(['isValid' => true]);
    }

    /**
     * Change Password
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $status = Password::reset(
            $request->only('email', 'password', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->save();
            }
        );

        return $status == Password::PASSWORD_RESET
            ? $this->apiResponse(null)
            : $this->apiResponseWithError(__($status));
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return $this->apiResponse(UserWithStatisticsResource::make(auth()->user()
            ->load('exams', 'questionAnswerHistories', 'promoCode', 'subscription'))
            ->isPromoCode(true)
        );
    }

    public function ticketTraining()
    {
        $user = Auth::user();
        $ticketTraining = $user->ticketTraining;

        return $this->apiResponse([
            'success_number_attempts' => $ticketTraining->success_number_attempts ?? 0,
            'passed_number_attempts' => $ticketTraining->passed_number_attempts ?? 0,
            'correct_answers' => $ticketTraining->correct_answers ?? 0,
            'wrong_answers' => $ticketTraining->wrong_answers ?? 0,
        ]);
    }
}
