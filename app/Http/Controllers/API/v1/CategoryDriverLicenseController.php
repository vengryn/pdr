<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryDriverLicenseResource;
use App\Models\CategoryDriverLicense;

class CategoryDriverLicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categoryDriverLicenses = CategoryDriverLicense::whereNotNull('position_y')->whereNotNull('position_x')->get();

        $data = [];

        for ($i = 1; $i <= 4; $i++) {
            for ($x = 1; $x <= 5; $x++) {
                $data[$i][] = $categoryDriverLicenses->where('position_x', $x)->where('position_y', $i)->first()
                    ? CategoryDriverLicenseResource::make($categoryDriverLicenses->where('position_x', $x)->where('position_y', $i)->first())
                    : null;
            }
        }

        $data = array_values($data);

        return $this->apiResponse($data);
    }
}
