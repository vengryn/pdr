<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\GenerateExamResource;
use App\Models\GenerateExam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamQuestionController extends Controller
{
    public function index(Request $request)
    {
        $generateExam = GenerateExam::getUserExam($request->boolean('is_training'))
            ->load('questions.translates', 'questions.questionsAnswers.translates');

        return $this->apiResponse(GenerateExamResource::make($generateExam));
    }

    public function cancelExam(Request $request)
    {
        $user = Auth::user();
        $user->generateExams()->where('is_training', $request->boolean('is_training', false))->delete();

        return $this->apiResponse();
    }
}
