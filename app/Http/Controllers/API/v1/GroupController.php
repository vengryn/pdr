<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\GroupCollection;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return GroupCollection
     */
    public function teacherGroupByPromoCode(Request $request)
    {
        $teacher = User::teacher()->with(['promoCode'])
            ->whereHas('promoCode', function ($query) use ($request) {
                $query->where(DB::raw('BINARY `code`'), $request->get('code'));
            })->first();

        $groups = $teacher
            ? $teacher->groups()
            : Group::query()->where('id', '<', 0);

        return GroupCollection::make($groups->paginate(25));
    }
}
