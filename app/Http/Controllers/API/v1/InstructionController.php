<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\InstructionWebCollection;
use App\Http\Resources\InstructionWebResource;
use App\Models\Instruction;

class InstructionController extends Controller
{
    /**
     * Get Instructions.
     *
     * @return mixed
     */
    public function index()
    {
        $dbInstruction = Instruction::with('textPage.translates');

        return request()->get('paginate')
            ? InstructionWebCollection::make($dbInstruction->paginate(request()->get('paginate')))
            : InstructionWebResource::collection($dbInstruction->get());
    }

    /**
     * Get TopicRoadMarking by ID.
     *
     * @param Instruction $instruction
     * @return InstructionWebResource
     */
    public function show(Instruction $instruction)
    {
        return InstructionWebResource::make($instruction);
    }
}
