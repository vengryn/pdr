<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\InternalTopicTrafficRuleCollection;
use App\Http\Resources\InternalTopicTrafficRuleResource;
use App\Http\Resources\ItemTrafficRuleResource;
use App\Models\InternalTopicTrafficRule;
use App\Models\ItemTrafficRule;
use Illuminate\Http\Request;

class InternalTopicTrafficRuleController extends Controller
{
    /**
     * Get InternalTopicTrafficRules.
     *
     * @return mixed
     */
    public function index()
    {
        $dbInternalTopicTrafficRule = InternalTopicTrafficRule::with('translates', 'itemTrafficRules.translates');

        return request()->get('paginate')
            ? InternalTopicTrafficRuleCollection::make($dbInternalTopicTrafficRule->paginate(request()->get('paginate')))
            : InternalTopicTrafficRuleResource::collection($dbInternalTopicTrafficRule->get());
    }

    /**
     * Get itemTrafficRule by Number.
     *
     * @param Request $request
     * @return ItemTrafficRuleResource
     */
    public function show(Request $request)
    {
        $itemTrafficRule = ItemTrafficRule::where('number', $request->number)->firstOrFail();

        return ItemTrafficRuleResource::make($itemTrafficRule);
    }
}
