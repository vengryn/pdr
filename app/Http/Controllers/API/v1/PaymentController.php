<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Payment\CreatePaymentRequest;
use App\Http\Requests\Api\Payment\StoreRequest;
use App\Http\Resources\UserSubscriptionResource;
use App\Models\Log;
use App\Models\Payment;
use App\Models\PromoCode;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserSubscription;
use App\Notifications\StudentCreateSubscription;
use App\Repositories\Interfaces\UserSubscriptionRepositoryInterface;
use App\Services\UaPay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log as Logging;

class PaymentController extends Controller
{
    private $userSubscriptionRepository;

    public function __construct(UserSubscriptionRepositoryInterface $userSubscriptionRepository)
    {
        $this->userSubscriptionRepository = $userSubscriptionRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Api\Payment\StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $user = Auth::user();
        Logging::channel('payment-mobile-app')->info($request->all() + ['user_id' => $user->id]);

        $userPayment = Payment::whereJsonContains('params->purchase_id', $request->get('purchase_id'))->first();

        $subscription = Subscription::where('product_id', $request->get('product_id'))->first();

        if (empty($userPayment)) {
            $user->payments()->create([
                'promo_code' => $request->get('promo_code'),
                'discount' => $request->get('discount'),
                'marketplace' => $request->get('marketplace'),
                'amount' => $request->get('amount'),
                'transaction_at' => $request->get('transaction_at'),
                'subscription' => $subscription->getTranslate()->name ?? null,
                'params' => [
                    'purchase_id' => $request->get('purchase_id')
                ]
            ]);

            $userSubscription = $this->userSubscriptionRepository->createPaid(
                $user,
                $subscription,
                UserSubscription::SOURCE_PAYMENT_MOBILE,
                $request->boolean('debug')
            );

            try {
                $user->notify(new StudentCreateSubscription($userSubscription));
            } catch (\Exception $e) {
                Log::saveLog($e);
            }
        } else {
            if (!$userSubscription = $user->isActiveSubscription()) {
                $userSubscription = $this->userSubscriptionRepository->createPaid(
                    $user,
                    $subscription,
                    UserSubscription::SOURCE_PAYMENT_MOBILE,
                    $request->boolean('debug')
                );
            }
        }

        return $this->apiResponse(UserSubscriptionResource::make($userSubscription));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPromoCode(Request $request)
    {
        $promoCode = PromoCode::whereIn('type', [
            PromoCode::TYPE_MARKETING, PromoCode::TYPE_INTERNAL
        ])->where('code', $request->get('promo_code'))->first();

        if ($promoCode) {
            if ($discount = $promoCode->discount_internal) {
                return $this->apiResponse([
                    'discount' => $promoCode->discount_internal
                ]);
            }
        }

        return $this->apiResponseWithError('Promo code not found');
    }

    /**
     * @param CreatePaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPayment(CreatePaymentRequest $request)
    {
        $user = Auth::user();
        $defaultPayment = config('services.payments.default');

        if (!$user->isActiveSubscription()) {
            $subscription = Subscription::find($request->get('subscription_id'));

            if ($request->get('promo_code')) {
                $promoCode = PromoCode::whereIn('type', [
                    PromoCode::TYPE_MARKETING, PromoCode::TYPE_INTERNAL
                ])->where('code', $request->get('promo_code'))->first();

                if ($promoCode) {
                    if ($discount = $promoCode->discount_internal) {
                        $amount = $subscription->price - ($subscription->price * ($discount / 100));
                    }
                }
            }

            $info = [
                'user_id' => $user->id,
                'subscription_id' => $subscription->id,
                'promo_code' => !empty($promoCode) ? $promoCode->code : null,
                'discount' => $discount ?? null,
            ];

            switch ($defaultPayment) {
                case 'liqpay':
                    $publicKey = config('services.liqpay.is_sandbox')
                        ? config('services.liqpay.sandbox_public_key')
                        : config('services.liqpay.public_key');

                    $privateKey = config('services.liqpay.is_sandbox')
                        ? config('services.liqpay.sandbox_private_key')
                        : config('services.liqpay.private_key');

                    try {
                        $liqPay = new \LiqPay($publicKey, $privateKey);
                        $amount = (float) bcdiv($amount ?? $subscription->price, 1, 2);

                        $params = [
                            'action' => 'pay',
                            'version' => '3',
                            'phone' => $user->infoStudent->phone,
                            'amount' => $amount,
                            'currency' => 'UAH',
                            'description' => $user->infoStudent->full_name . ';' . $user->id . ';Оплата підписки Тести ПДР (' . $subscription->validity . ' міс.)',
                            'order_id' => 'order_' . now()->timestamp,
                            'language' => 'uk',
                            'server_url' => route('liqPay.checkout'),
                            'public_key' => $publicKey,
                            'dae' => base64_encode(json_encode($info))
                        ];

                        $signature = $liqPay->cnb_signature($params);
                        $data = base64_encode(json_encode($params));

                        return $this->apiResponse(['method' => 'liqpay', 'signature' => $signature, 'data' => $data]);

                    } catch (\Exception $e) {
                        Log::saveLog($e);
                    }
                    break;
                case 'uapay':
                    try {
                        $uaPay = new UaPay();
                        $sessionId = $uaPay->createSession();

                        $invoice = $uaPay->createInvoice($sessionId, [
                            'externalId' => (string) $user->id,
                            'type' => 'PAY',
                            'reusability' => false,
                            'description' => $user->infoStudent->full_name . ';' . $user->id . ';Оплата підписки Тести ПДР (' . $subscription->validity . ' міс.)',
                            'amount' => (int) ($amount ?? $subscription->price) * 100,
                            'redirectUrl' => 'https://pdr.infotech.gov.ua/services',
                            'extraInfo' => json_encode($info),
                            'email' => $user->email,
                            'callbackUrl' => route('uaPay.checkout'),
                        ]);

                        return $this->apiResponse(['method' => 'uapay', 'url' => $invoice->paymentPageUrl]);

                    } catch (\Exception $e) {
                        Log::saveLog($e);
                    }
                    break;
            }

            return $this->apiResponseWithError('Payment Failed');
        }

        return $this->apiResponseWithError('This user exists active subscription');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function liqPayCheckout(Request $request)
    {
        Logging::channel('liq-pay')->info($request->all());

        $privateKey = config('services.liqpay.is_sandbox')
            ? config('services.liqpay.sandbox_private_key')
            : config('services.liqpay.private_key');

        $sign = base64_encode(sha1($privateKey . $request->get('data') . $privateKey, 1));

        if ($sign == $request->get('signature')) {
            $result = json_decode(base64_decode($request->get('data')));

            if ($result->status == 'success') {
                $info = json_decode(base64_decode($result->dae), true);

                $user = User::find($info['user_id'] ?? null);
                $subscription = Subscription::find($info['subscription_id'] ?? null);

                if ($user && $subscription) {
                    if (Payment::whereJsonContains('params->order_id', $result->order_id)->get()->isEmpty()) {
                        $payment = $user->payments()->create(
                            [
                                'promo_code' => $info['promo_code'] ?? null,
                                'discount' => $info['discount'] ?? null,
                                'marketplace' => 3,
                                'amount' => $result->amount,
                                'transaction_at' => $result->create_date ?? now()->timestamp,
                                'subscription' => $subscription->getTranslate()->name ?? null,
                                'params' => [
                                    'payment_id' => $result->payment_id,
                                    'paytype' => $result->paytype,
                                    'acq_id' => $result->acq_id,
                                    'order_id' => $result->order_id,
                                    'liqpay_order_id' => $result->liqpay_order_id,
                                    'description' => $result->description,
                                ]
                            ]
                        );

                        $userSubscription = $this->userSubscriptionRepository->createPaid(
                            $user,
                            $subscription,
                            UserSubscription::SOURCE_PAYMENT_WEB
                        );

                        $payment->setReferer();

                        try {
                            $user->notify(new StudentCreateSubscription($userSubscription));
                        } catch (\Exception $e) {
                            Log::saveLog($e);
                        }
                    }
                    return $this->apiResponse();
                }
            }
        }

        return $this->apiResponseWithError('Failed check payment');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uaPayCheckout(Request $request)
    {
        Logging::channel('ua-pay')->info($request->all());

        try {
            $uaPay = new UaPay();
            $data = $uaPay->getCallback($request->get('token'));

            if ($data->status == 'FINISHED') {
                $info = json_decode($data->extraInfo, true);

                $user = User::find($info['user_id'] ?? null);
                $subscription = Subscription::find($info['subscription_id'] ?? null);

                if ($user && $subscription) {
                    if (Payment::whereJsonContains('params->payment_id', $data->paymentId)->get()->isEmpty()) {
                        $payment = $user->payments()->create(
                            [
                                'promo_code' => $info['promo_code'] ?? null,
                                'discount' => $info['discount'] ?? null,
                                'marketplace' => 4,
                                'amount' => (float) bcdiv($data->amount / 100, 1, 2),
                                'transaction_at' => strtotime($data->payDate),
                                'subscription' => $subscription->getTranslate()->name ?? null,
                                'params' => [
                                    'payment_id' => $data->paymentId,
                                    'payment_number' => $data->paymentNumber,
                                    'receipt_path' => $data->receiptPath,
                                    'type' => $data->type,
                                    'system_type' => $data->systemType,
                                ]
                            ]
                        );

                        $userSubscription = $this->userSubscriptionRepository->createPaid(
                            $user,
                            $subscription,
                            UserSubscription::SOURCE_PAYMENT_WEB
                        );

                        $payment->setReferer();

                        try {
                            $user->notify(new StudentCreateSubscription($userSubscription));
                        } catch (\Exception $e) {
                            Log::saveLog($e);
                        }
                    }
                    return $this->apiResponse();
                }
            }

            return $this->apiResponse();
        } catch (\Exception $e) {
            Log::saveLog($e);
        }

        return $this->apiResponseWithError('Failed check payment');
    }
}
