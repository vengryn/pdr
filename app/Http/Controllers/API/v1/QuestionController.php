<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Question\StoreExamQuestionAnswerHistoryRequest;
use App\Http\Requests\Api\Question\StoreExamRequest;
use App\Http\Requests\Api\Question\StoreQuestionAnswerAllHistoryRequest;
use App\Http\Requests\Api\Question\StoreQuestionAnswerHistoryRequest;
use App\Http\Resources\ExamResource;
use App\Http\Resources\QuestionAnswerHistoryResource;
use App\Http\Resources\QuestionExamAnswerHistoryResource;
use App\Http\Resources\WrongQuestionAnswerResource;
use App\Models\Exam;
use App\Models\Question;
use App\Models\WrongQuestionAnswer;
use App\Rules\Timestamp;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    /**
     * Get questions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        #$defaultMemoryLimit = ini_get('memory_limit');
        ini_set('memory_limit', '1024M');

        $files = Storage::files(Question::getPathQuestionFile());

        if (!end($files)) {
            Artisan::call('api-question:sync');
            $files = Storage::files(Question::getPathQuestionFile());
        }

        $file = end($files);

        $data = Storage::disk('local')->exists($file)
            ? json_decode(Storage::disk('local')->get($file), true)
            : [];

        #ini_set('memory_limit', $defaultMemoryLimit);

        return $this->apiResponse($data);
    }

    /**
     * Get file questions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexV2()
    {
        $files = Storage::files(Question::getPathQuestionFile());

        if (!end($files)) {
            Artisan::call('api-question:sync');
        }

        $file = end($files);

        return $this->apiResponse(null, 'success', 200, [
            'X-Accel-Redirect' => '/questions/' . basename($file),
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename=' . basename($file),
        ]);
    }

    /**
     * Save history answer for question.
     *
     * @param \App\Http\Requests\Api\Question\StoreQuestionAnswerHistoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeQuestionAnswerHistory(StoreQuestionAnswerHistoryRequest $request)
    {
        $questionAnswerHistory = Question::storeQuestionAnswerHistory([
            'question_id' => $request->get('question_id'),
            'questions_answer_id' => $request->get('questions_answer_id'),
            'client_created_at' => $request->get('client_created_at', Carbon::now()->timestamp)
        ]);

        if ($questionAnswerHistory) {
            if (!$questionAnswerHistory->is_passed) {
                WrongQuestionAnswer::updateOrCreate([
                    'user_id' => Auth::id(),
                    'question_id' => $questionAnswerHistory->question_id
                ], [
                    'client_created_at' => Carbon::now()->timestamp,
                    'action' => true
                ]);
            }
        }

        return $questionAnswerHistory
            ? $this->apiResponse(QuestionAnswerHistoryResource::make($questionAnswerHistory)->isCorrectAnswer(true))
            : $this->apiResponseWithError('Failed');
    }

    /**
     * Save history answer for questions and save Exams.
     *
     * @param \App\Http\Requests\Api\Question\StoreQuestionAnswerAllHistoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeQuestionAnswerAllHistory(StoreQuestionAnswerAllHistoryRequest $request)
    {
        $user = Auth::user();

        Question::storeQuestionAnswerHistories($request->get('question_answers_history', []));

        foreach ($request->get('exams', []) as $key => $exam) {
            $validator = Validator::make($request->all(), [
                "exams.{$key}.total_answers" => 'required|integer',
                "exams.{$key}.correct_answers" => 'required|integer',
                "exams.{$key}.wrong_answers" => 'required|integer',
                "exams.{$key}.is_passed" => 'required|boolean',
                "exams.{$key}.time" => 'required|integer',
                "exams.{$key}.client_created_at" => 'nullable|integer',
            ]);

            if (!$validator->fails()) {
                Exam::create([
                    'user_id' => $user->id,
                    'total_answers' => $exam['total_answers'],
                    'correct_answers' => $exam['correct_answers'],
                    'wrong_answers' => $exam['wrong_answers'],
                    'is_passed' => $exam['is_passed'],
                    'time' => $exam['time'],
                    'client_created_at' => $exam['client_created_at'] ?? null,
                ]);
            }
        }

        foreach ($request->get('wrong_question_answers', []) as $key => $wrongQuestionAnswer) {
            $validator = Validator::make($request->all(), [
                "wrong_question_answers.{$key}.question_id" => 'required|exists:questions,id,deleted_at,NULL',
                "wrong_question_answers.{$key}.client_created_at" => ['nullable', new Timestamp],
                "wrong_question_answers.{$key}.action" => ['required', 'boolean'],
            ]);

            if (!$validator->fails()) {
                WrongQuestionAnswer::updateOrCreate([
                    'user_id' => $user->id,
                    'question_id' => $wrongQuestionAnswer['question_id']
                ],
                    [
                        'client_created_at' => $wrongQuestionAnswer['client_created_at'] ?? Carbon::now()->timestamp,
                        'action' => $wrongQuestionAnswer['action']
                    ]);
            }
        }

        $histories = Question::getHistory($request, $user);

        return $this->apiResponse([
            'last_date_exam_history' => $user->exams()->latest()->first()->created_at->timestamp ?? null,
            'last_date_question_history' => $user->questionAnswerHistories()->latest('updated_at')->first()->updated_at->timestamp ?? null,
            'last_date_wrong_question_answers' => $user->wrongQuestionAnswers()->latest('updated_at')->first()->updated_at->timestamp ?? null,
            'exams' => ExamResource::collection($histories['exams']),
            'passed_questions' => QuestionAnswerHistoryResource::collection($histories['questionAnswerHistories']),
            'wrong_question_answers' => WrongQuestionAnswerResource::collection($histories['wrongQuestionAnswers'])
        ]);
    }

    /**
     * Save exam by user.
     *
     * @param \App\Http\Requests\Api\Question\StoreExamRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeExam(StoreExamRequest $request)
    {
        return $this->apiResponse(ExamResource::make(Exam::create($request->validated() + [
                'user_id' => Auth::user()->id
            ]))
        );
    }

    /**
     * @param StoreExamQuestionAnswerHistoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeExamQuestionAnswerHistory(StoreExamQuestionAnswerHistoryRequest $request)
    {
        $user = Auth::user();
        $generateExam = $user->generateExams()
            ->where('is_training', $request->boolean('is_training'))
            ->latest()
            ->first();

        if ($generateExam) {
            if ($generateExam->is_active) {

                if ($generateExam->questions->contains('id', $request->get('question_id'))) {
                    $result = Question::storeQuestionAnswerHistory([
                        'question_id' => $request->get('question_id'),
                        'questions_answer_id' => $request->get('questions_answer_id')
                    ]);

                    if ($result) {
                        $generateExam->questions()
                            ->where('question_id', $request->get('question_id'))
                            ->update([
                                'questions_answer_id' => $result->questions_answer_id,
                                'is_passed' => $result->is_passed
                            ]);

                        if (!$result->is_passed) {
                            WrongQuestionAnswer::updateOrCreate([
                                'user_id' => $user->id,
                                'question_id' => $result->question_id
                            ], [
                                'client_created_at' => Carbon::now()->timestamp,
                                'action' => true
                            ]);
                        }

                        $exam = null;

                        if (!$request->boolean('is_training')) {
                            $correctAnswers = $generateExam->questions()->wherePivot('is_passed', true)->wherePivotNotNull('questions_answer_id')->count();
                            $wrongAnswers = $generateExam->questions()->wherePivot('is_passed', false)->wherePivotNotNull('questions_answer_id')->count();

                            if ($wrongAnswers > 2 || !$generateExam->questions()->whereNull('questions_answer_id')->count()) {
                                $exam = $user->exams()->create([
                                    'total_answers' => $correctAnswers + $wrongAnswers,
                                    'correct_answers' => $correctAnswers,
                                    'wrong_answers' => $wrongAnswers,
                                    'is_passed' => $wrongAnswers <= 2,
                                    'time' => Carbon::now()->diffInSeconds($generateExam->created_at),
                                ]);

                                $generateExam->delete();
                            }
                        } else {
                            if (!$generateExam->questions()->whereNull('questions_answer_id')->count()) {
                                $generateExam->delete();
                            }
                        }

                        return $this->apiResponse(QuestionExamAnswerHistoryResource::make($result)->exam(ExamResource::make($exam)));
                    }
                }

                return $this->apiResponseWithError('Failed');
            }

            return $this->apiResponseWithError('Time is over');
        }

        return $this->apiResponseWithError('Exam not found');
    }
}
