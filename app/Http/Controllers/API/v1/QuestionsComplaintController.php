<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\QuestionsComplaint\StoreRequest;
use App\Models\QuestionsComplaint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionsComplaintController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Api\QuestionsComplaint\StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeComplaint(StoreRequest $request)
    {
        QuestionsComplaint::create($request->validated() + [
                'user_id' => Auth::user()->id
            ]);

        return $this->apiResponse();
    }

    /**
     * Store a newly created resources in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeComplaints(Request $request)
    {
        $user = Auth::user();

        foreach ($request->get('complaints', []) as $key => $complaint) {
            $validator = Validator::make($request->all(), [
                "complaints.{$key}.question_id" => 'required|exists:questions,id,deleted_at,NULL',
                "complaints.{$key}.comment" => 'required|string|max:1000',
            ]);

            if (!$validator->fails()) {
                QuestionsComplaint::create([
                    'question_id' => $complaint['question_id'],
                    'comment' => $complaint['comment'],
                    'user_id' => $user->id
                ]);
            }
        }

        return $this->apiResponse();
    }
}
