<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\DigitalOceanResource;
use App\Http\Resources\InstructionResource;
use App\Http\Resources\SocialSupportServiceResource;
use App\Http\Resources\SocialSupportServiceWebResource;
use App\Http\Resources\TextPageResource;
use App\Models\Instruction;
use App\Models\Question;
use App\Models\Setting;
use App\Models\SocialSupportService;
use App\Models\TextPage;
use App\Services\Theory;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    /**
     * Get settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $theory = new Theory();

        $questionFiles = Storage::files(Question::getPathQuestionFile());
        $theoryFiles = Storage::files($theory->getPathTheoryFile());

        $settings = Setting::getSettings(['date_of_last_response_file_generation', 'date_of_last_response_theory_file_generation',
            'contact_email', 'privacy_policy_page', 'digitalocean']);

        $socialSupportServices = SocialSupportService::select('id', 'name', 'link', 'logo')->toBase()->get();

        $textPage = TextPage::find($settings['privacy_policy_page']);

        $data['social_support_services'] = SocialSupportServiceResource::collection($socialSupportServices);
        $data['last_questions_updated'] = $settings['date_of_last_response_file_generation'];
        $data['questions_original_size'] = ($questionFile = end($questionFiles)) ? json_decode(Storage::disk('local')->size($questionFile), true) : 0;

        $data['last_theory_updated'] = $settings['date_of_last_response_theory_file_generation'];
        $data['theory_original_size'] = ($theoryFile = end($theoryFiles)) ? json_decode(Storage::disk('local')->size($theoryFile), true) : 0;

        $data['contact_email'] = $settings['contact_email'];
        $data['user_agreement'] = $textPage ? TextPageResource::make($textPage->load('translates')) : null;
        $data['instructions'] = InstructionResource::collection(Instruction::all());
        $data['cdn_digitalocean'] = DigitalOceanResource::make($settings);

        return $this->apiResponse($data);
    }

    /**
     * Get web settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexWeb()
    {
        $settings = Setting::getSettings(['contact_email', 'google_play_link', 'app_store_link',
            'date_of_last_response_file_generation']);
        $socialSupportServices = SocialSupportService::select('id', 'name', 'link', 'logo')->toBase()->get();

        $data['contact_email'] = $settings['contact_email'];
        $data['google_play_link'] = $settings['google_play_link'];
        $data['app_store_link'] = $settings['app_store_link'];
        $data['social_support_services'] = SocialSupportServiceWebResource::collection($socialSupportServices);
        $data['last_questions_updated'] = $settings['date_of_last_response_file_generation'];

        return $this->apiResponse($data);
    }
}
