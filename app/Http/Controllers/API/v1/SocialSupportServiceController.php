<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\SocialSupportServiceResource;
use App\Models\SocialSupportService;

class SocialSupportServiceController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->apiResponse(SocialSupportServiceResource::collection(SocialSupportService::all()));
    }
}
