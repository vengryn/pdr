<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\SubscriptionCollection;
use App\Models\Subscription;

class SubscriptionController extends Controller
{
    /**
     * @return SubscriptionCollection
     */
    public function index()
    {
        return SubscriptionCollection::make(Subscription::with(['translates'])->paginate(25));
    }
}
