<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\TextPageResource;
use App\Models\TextPage;

class TextPageController extends Controller
{
    /**
     * Get settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($slug)
    {
        $textPage = TextPage::where('slug', $slug)->first();

        return $this->apiResponse(TextPageResource::make($textPage)->web(true));
    }
}
