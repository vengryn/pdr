<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemTrafficRuleResource;
use App\Http\Resources\MarkingResource;
use App\Http\Resources\RegulatorCollection;
use App\Http\Resources\SignResource;
use App\Http\Resources\TrafficLightCollection;
use App\Http\Resources\VideoLessonsTrafficRuleCollection;
use App\Models\ItemTrafficRule;
use App\Models\Language;
use App\Models\Marking;
use App\Models\Sign;
use App\Models\Translate\TranslateRegulator;
use App\Models\Translate\TranslateTrafficLight;
use App\Models\Translate\TranslateVideoLessonsTrafficRule;
use Illuminate\Http\Request;
use App\Services\Theory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

class TheoryController extends Controller
{
    /**
     * Get questions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $theory = new Theory();
        $files = Storage::files($theory->getPathTheoryFile());

        if (!end($files)) {
            Artisan::call('api-theory:sync');
            $files = Storage::files($theory->getPathTheoryFile());
        }

        $file = end($files);

        $data = Storage::disk('local')->exists($file)
            ? json_decode(Storage::disk('local')->get($file), true)
            : [];

        return $this->apiResponse($data);
    }

    /**
     * Get file questions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexV2()
    {
        $theory = new Theory();
        $files = Storage::files($theory->getPathTheoryFile());

        if (!end($files)) {
            Artisan::call('api-theory:sync');
        }

        $file = end($files);

        return $this->apiResponse(null, 'success', 200, [
            'X-Accel-Redirect' => '/theory/' . basename($file),
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename=' . basename($file),
        ]);
    }

    /**
     * Get Regulator.
     *
     * @return RegulatorCollection
     */
    public function regulator()
    {
        $translateRegulator = $this->replaceTokenImage(TranslateRegulator::get(), ['description']);

        return RegulatorCollection::make($translateRegulator);
    }

    /**
     * Get Traffic Light.
     *
     * @return TrafficLightCollection
     */
    public function trafficLight()
    {
        $translateTrafficLight = $this->replaceTokenImage(TranslateTrafficLight::get(), ['description']);

        return TrafficLightCollection::make($translateTrafficLight);
    }

    /**
     * Get Video Lessons Traffic Rule.
     *
     * @return VideoLessonsTrafficRuleCollection
     */
    public function videoLessonsTrafficRule()
    {
        $translateTrafficLight = $this->replaceTokenImage(TranslateVideoLessonsTrafficRule::get(), ['description']);

        return VideoLessonsTrafficRuleCollection::make($translateTrafficLight);
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $language = Language::where('abbreviation', app()->getLocale())->first();

        if (strlen($search) >= 4) {
            switch ($request->get('section')) {
                case 'pdr':
                    $itemTrafficRule = ItemTrafficRule::with(['translates'])
                        ->whereHas('translates', function ($query) use ($search, $language) {
                            $query->where('language_id', $language->id);
                            $query->where(function($query) use ($search) {
                                $query->where('content', 'like', "%$search%");
                                $query->orWhere('description', 'like', "%$search%");
                            });
                        })->get();

                    return $itemTrafficRule->isNotEmpty()
                        ? ItemTrafficRuleResource::collection($itemTrafficRule)->additional(['section' => $request->get('section')])
                        : $this->apiResponse();
                case 'markings':
                    $marking = Marking::with(['translates'])->whereHas('translates', function ($query) use ($search, $language) {
                        $query->where('language_id', $language->id);
                        $query->where(function($query) use ($search) {
                            $query->where('name', 'like', "%$search%");
                            $query->orWhere('content', 'like', "%$search%");
                            $query->orWhere('description', 'like', "%$search%");
                        });
                    })->get();

                    return $marking->isNotEmpty()
                        ? MarkingResource::collection($marking)->additional(['section' => $request->get('section')])
                        : $this->apiResponse();
                case 'signs':
                    $signs = Sign::with(['translates'])->whereHas('translates', function ($query) use ($search, $language) {
                        $query->where('language_id', $language->id);
                        $query->where(function($query) use ($search) {
                            $query->where('name', 'like', "%$search%");
                            $query->orWhere('content', 'like', "%$search%");
                            $query->orWhere('description', 'like', "%$search%");
                        });
                    })->get();

                    return $signs->isNotEmpty()
                        ? SignResource::collection($signs)->additional(['section' => $request->get('section')])
                        : $this->apiResponse();
            }
        }

        return $this->apiResponse();
    }
}
