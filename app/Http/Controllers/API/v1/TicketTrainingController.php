<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\TicketTraining\StoreRequest;
use App\Http\Resources\TicketTrainingResource;
use App\Models\TicketTraining;
use Illuminate\Support\Facades\Auth;

class TicketTrainingController extends Controller
{
    /**
     * Update or Create the specified resource in storage.
     *
     * @param \App\Http\Requests\Api\TicketTraining\StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $ticketTraining = TicketTraining::firstOrCreate([
            'user_id' => Auth::user()->id
        ]);

        $ticketTraining->increment('success_number_attempts', $request->get('success_number_attempts', 0));
        $ticketTraining->increment('passed_number_attempts', $request->get('passed_number_attempts', 0));
        $ticketTraining->increment('correct_answers', $request->get('correct_answers', 0));
        $ticketTraining->increment('wrong_answers', $request->get('wrong_answers', 0));

        return $this->apiResponse(TicketTrainingResource::make($ticketTraining));
    }
}
