<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\MarkingResource;
use App\Http\Resources\RoadMarkingDescriptionCollection;
use App\Http\Resources\TopicRoadMarkingCollection;
use App\Http\Resources\TopicRoadMarkingResource;
use App\Models\Marking;
use App\Models\TopicRoadMarking;
use App\Models\Translate\TranslateRoadMarkingDescription;
use Illuminate\Http\Request;

class TopicRoadMarkingController extends Controller
{
    /**
     * Get TopicRoadMarkings.
     *
     * @return mixed
     */
    public function index()
    {
        $dbTopicRoadMarking = TopicRoadMarking::with('translates', 'marking')->orderBy('number');
        $translateRoadMarkingDescription = $this->replaceTokenImage(TranslateRoadMarkingDescription::get(), ['description']);

        return request()->get('paginate')
            ? TopicRoadMarkingCollection::make($dbTopicRoadMarking->paginate(request()->get('paginate')))
            : TopicRoadMarkingResource::collection($dbTopicRoadMarking->get())->additional([
                'description' => RoadMarkingDescriptionCollection::make($translateRoadMarkingDescription)
            ]);
    }

    /**
     * Get Marking by Number.
     *
     * @param Request $request
     * @return MarkingResource
     */
    public function show(Request $request)
    {
        $marking = Marking::where('number', $request->number)->firstOrFail();

        return MarkingResource::make($marking);
    }
}
