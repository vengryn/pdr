<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoadSignDescriptionCollection;
use App\Http\Resources\SignResource;
use App\Http\Resources\TopicRoadSignCollection;
use App\Http\Resources\TopicRoadSignResource;
use App\Models\Sign;
use App\Models\TopicRoadSign;
use App\Models\Translate\TranslateRoadSignDescription;
use Illuminate\Http\Request;

class TopicRoadSignController extends Controller
{
    /**
     * Get TopicRoadMarkings.
     *
     * @return mixed
     */
    public function index()
    {
        $dbTopicRoadSign = TopicRoadSign::with('translates', 'signs')->orderBy('number');
        $translateRoadSignDescription = $this->replaceTokenImage(TranslateRoadSignDescription::get(), ['description']);

        return request()->get('paginate')
            ? TopicRoadSignCollection::make($dbTopicRoadSign->paginate(request()->get('paginate')))
            : TopicRoadSignResource::collection($dbTopicRoadSign->get())->additional([
                'description' => RoadSignDescriptionCollection::make($translateRoadSignDescription)
            ]);
    }

    /**
     * Get TopicRoadMarking by ID.
     *
     * @param Request $request
     * @return SignResource
     */
    public function show(Request $request)
    {
        $sign = Sign::where('number', $request->number)->firstOrFail();

        return SignResource::make($sign);
    }
}
