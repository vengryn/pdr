<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\Language;

class TranslateController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function translate()
    {
        $languages = Language::get()->each(function ($item) {
            $item->abbreviation = lcfirst($item->abbreviation);
            return $item;
        })
        ->keyBy('abbreviation')->map(function ($item) {
           return [
               'language' => $item->title,
               'translations' => trans('mobileApp', [], $item->abbreviation)
           ];
        })->toArray();

        return $this->apiResponse($languages);
    }
}
