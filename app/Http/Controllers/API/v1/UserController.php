<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\RefererStatisticsRequest;
use App\Http\Requests\Api\User\UpdateWebRequest;
use App\Http\Resources\ExamResource;
use App\Http\Resources\QuestionAnswerHistoryResource;
use App\Http\Resources\UserFrequentMistakeResource;
use App\Http\Resources\UserQuestionsByThemeResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserSubscriptionResource;
use App\Http\Resources\UserThemeWithProgressResource;
use App\Http\Resources\UserWrongQuestionAnswerResource;
use App\Http\Resources\WrongQuestionAnswerResource;
use App\Models\PromoCode;
use App\Models\Question;
use App\Models\QuestionAnswerHistory;
use App\Models\TicketTraining;
use App\Models\TopicTrafficRule;
use App\Models\User;
use App\Models\WrongQuestionAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Api\User\UpdateRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log as Logging;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Api\User\UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        $user = Auth::user();

        $user->update($request->only(['email', 'password', 'group_id']));

        $user->infoStudent()->update($request->only([
            'first_name',
            'last_name',
            'phone'
        ]));

        if ($request->hasFile('avatar')) {
            $user->infoStudent->avatar = $request->avatar->storePublicly('public/users/avatar');
            $user->infoStudent->save();
        }

        if ($request->get('categories')) {
            $user->categoryDriverLicenses()->sync($request->get('categories'));
        }

        return $this->apiResponse(UserResource::make($user));
    }

    /**
     * @param UpdateWebRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(UpdateWebRequest $request)
    {
        $user = Auth::user();

        $user->update($request->only(['email', 'password', 'group_id']));

        $user->infoStudent()->update($request->only([
            'first_name',
            'last_name',
            'phone'
        ]));

        if ($request->get('categories')) {
            $user->categoryDriverLicenses()->sync($request->get('categories'));
        }

        if ($request->get('avatar')) {
            $image = base64_decode($request->get('avatar'));

            $img = 'public/users/avatar/' . Str::random(50) . '.png';
            Storage::put($img, $image, 'public');

            $user->infoStudent->avatar = $img;
            $user->infoStudent->save();
        }

        return $this->apiResponse(UserResource::make($user));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function userSubscription()
    {
        $subscription = Auth::user()->isActiveSubscription();
        $data = $subscription ? UserSubscriptionResource::make($subscription) : null;

        return $this->apiResponse($data);
    }

    /**
     * Return histories by user. Passed questions, wrong question answers and exams.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function histories(Request $request)
    {
        $user = Auth::user();
        $histories = Question::getHistory($request, $user);

        return $this->apiResponse([
            'exams' => ExamResource::collection($histories['exams']),
            'passed_questions' => QuestionAnswerHistoryResource::collection($histories['questionAnswerHistories']),
            'wrong_question_answers' => WrongQuestionAnswerResource::collection($histories['wrongQuestionAnswers']),
            'last_date_exam_history' => $user->exams()->latest()->first()->created_at->timestamp ?? null,
            'last_date_question_history' => $user->questionAnswerHistories()->latest('updated_at')->first()->updated_at->timestamp ?? null,
            'last_date_wrong_question_answers' => $user->wrongQuestionAnswers()->latest('updated_at')->first()->updated_at->timestamp ?? null
        ]);
    }

    /**
     * Clear histories by user. Passed questions, wrong question answers and exams.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetHistories()
    {
        $user = Auth::user();

        Logging::channel('reset-histories')->info(['user_id' => $user->id]);

        $user->questionAnswerHistories()->delete();
        $user->exams()->delete();
        $user->wrongQuestionAnswers()->delete();

        TicketTraining::where('user_id', $user->id)->delete();

        return $this->apiResponse();
    }

    /**
     * Get UserThemesWithProgress.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userThemesWithProgress()
    {
        $user = Auth::user();
        $userTopicTrafficRules = TopicTrafficRule::with(['questions', 'translates'])
            ->get()
            ->each(function ($item) use ($user) {
                $userQuestionAnswerHistories = $user->questionAnswerHistories->where('is_passed', true)
                    ->whereIn('question_id', $item->questions->where('status', true)->pluck('id')->toArray())
                    ->count();

                $item->progress_theme = $userQuestionAnswerHistories
                    ? round(($userQuestionAnswerHistories / $item->questions->where('status', true)->count()) * 100)
                    : 0;
                $item->count_questions = $item->questions->where('status', true)->count();

                return $item;
            });

        return $this->apiResponse(UserThemeWithProgressResource::collection($userTopicTrafficRules));
    }

    /**
     * Get UserQuestionsByTheme.
     *
     * @param int $topicTrafficRule
     * @return \Illuminate\Http\JsonResponse
     */
    public function userQuestionsByTheme(int $topicTrafficRule)
    {
        $topicTrafficRule = TopicTrafficRule::where('number', $topicTrafficRule)->firstOrFail();

        return $this->apiResponse([
            'questions' => UserQuestionsByThemeResource::collection($topicTrafficRule->questions
                ->where('status', true)->load([
                    'translates', 'markdownImages', 'questionsAnswers.translates'
                ])),
            'user_answers' => Auth::user()->questionAnswerHistories()
                ->whereIn('question_id', $topicTrafficRule->questions->pluck('id')->toArray())
                ->get()
                ->map(function ($item) {
                    return [
                        'question_id' => $item->question_id,
                        'answer_id' => $item->questions_answer_id,
                        'is_passed' => $item->is_passed
                    ];
                })
        ]);
    }

    /**
     * Get userWrongQuestionAnswers.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userWrongQuestionAnswers()
    {
        $user = Auth::user();
        $wrongQuestionAnswers = $user->wrongQuestionAnswers()
            ->with('question.translates', 'question.questionsAnswers', 'question.questionsAnswers.translates')
            ->where('action', true)->get();

        return $this->apiResponse(UserWrongQuestionAnswerResource::collection($wrongQuestionAnswers));
    }

    /**
     * Store userWrongQuestionAnswers
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeUserWrongQuestionAnswers(Request $request)
    {
        $user = Auth::user();
        $wrongQuestionAnswer = WrongQuestionAnswer::where('question_id', $request->get('question_id'))
            ->where('user_id', $user->id)
            ->where('action', true)
            ->first();

        if ($wrongQuestionAnswer) {
            if (!empty($request->get('question_id')) && !empty($request->get('questions_answer_id'))) {
                $questionData = Question::getQuestionData($request->get('question_id'));

                if ($questionData && in_array($request->get('questions_answer_id'), $questionData['answer_ids'])) {
                    $isPassed = $request->get('questions_answer_id') == $questionData['correct_answer_id'];
                    $wrongQuestionAnswer->update(['action' => !$isPassed]);

                    QuestionAnswerHistory::updateOrCreate([
                        'question_id' => $questionData['question_id'],
                        'user_id' => $user->id,
                    ], [
                        'questions_answer_id' => $request->get('questions_answer_id'),
                        'is_passed' => $isPassed,
                        'correct_questions_answer_id' => $questionData['correct_answer_id'],
                        'client_created_at' => Carbon::now()->timestamp
                    ]);

                    return $this->apiResponse([
                        'is_passed' => $isPassed,
                        'correct_answer_id' => $questionData['correct_answer_id']
                    ]);
                }
            }
        }

        return $this->apiResponseWithError('Failed');
    }

    /**
     * Get userFrequentMistakes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userFrequentMistakes()
    {
        $questions = Question::with(['translates'])
            ->where('count_wrong_answer', '>', 0)
            ->orderByDesc('count_wrong_answer')
            ->limit(100)
            ->get();

        if ($questions->count() < 100) {
            $questionsRandom = Question::with(['translates'])
                ->where('count_wrong_answer',0)
                ->get()
                ->random(100 - $questions->count());

            $questions = $questions->merge($questionsRandom);
        }

        return $this->apiResponse(UserFrequentMistakeResource::collection($questions));
    }

    public function refererStatistics(RefererStatisticsRequest $request)
    {
        $promoCode = PromoCode::where('type', PromoCode::TYPE_INTERNAL)
            ->whereHas('user', function ($query) {
                $query->where('role', User::ROLE_STUDENT);
            })
            ->where('code', $request->get('promo_code'))
            ->first();

        if ($promoCode) {
            $paidUsers = $promoCode->user->refererPayments()->has('student')->latest('transaction_at')->limit(10)
                ->get()->map(function ($item) {
                    return isset($item->student->infoStudent) ? $item->student->infoStudent->full_name : null;
                })->toArray();

            return $this->apiResponse([
                'count_registers' => User::where('referer_id', $promoCode->user->id)->count(),
                'count_payments' => $promoCode->user->refererPayments->count(),
                'amount_remuneration' => (float) bcdiv($promoCode->user->refererPayments->sum('pivot.cashback'), 1, 2),
                'paid_users' => [
                    'content' => implode(', ', $paidUsers)
                ],
            ]);
        }

        return $this->apiResponseWithError('Promo code not found');
    }
}
