<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        switch ($request->getHost()) {
            case 'admin-panel.' . env('APP_DOMAIN_NAME'):
                $roles = [User::ROLE_ADMINISTRATOR, User::ROLE_SUPER_ADMINISTRATOR, User::ROLE_PAYMENT_REPORT];
                break;
            case 'teacher.' . env('APP_DOMAIN_NAME'):
                $roles = [User::ROLE_TEACHER];
                break;
        }

        return $request->only($this->username(), 'password') + [
                'role' => $roles ?? null
            ];
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        if (auth()->user()->isAdministrator())
            return route('teachers.index');

        if (auth()->user()->isTeacher())
            return route('groups.index');

        if (auth()->user()->isPaymentReport())
            return route('payments.index');

        return RouteServiceProvider::HOME;
    }
}
