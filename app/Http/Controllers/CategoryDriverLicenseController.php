<?php

namespace App\Http\Controllers;

use App\Models\CategoryDriverLicense;
use App\Rules\Position;
use Illuminate\Http\Request;

class CategoryDriverLicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $categoryDriverLicenses = CategoryDriverLicense::all();

        return view('category-driver-licenses.index', compact('categoryDriverLicenses'));
    }

    /**
     * Update position categories.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'categories' => new Position(),
            'categories.*.position_x' => 'nullable|integer|min:1|max:5',
            'categories.*.position_y' => 'nullable|integer|min:1|max:4',
        ], [], [
            'categories.*.position_x' => 'X',
            'categories.*.position_y' => 'Y',
        ]);

        $data = $request->get('categories');

        CategoryDriverLicense::find(array_keys($data))->each(function ($item) use ($data) {
            $item->update($data[$item->id]);
        });

        return redirect()->route('category-driver-licenses.index');
    }
}
