<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeneralSettings\StoreRequest;
use App\Models\Setting;
use App\Models\TextPage;

class GeneralSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $setting = Setting::getSettings(['contact_email', 'privacy_policy_page', 'date_of_last_response_file_generation',
            'date_of_last_response_theory_file_generation', 'digitalocean', 'chat_bot', 'liqpay', 'uapay', 'google_play_link',
            'app_store_link', 'payments']);
        $textPages = TextPage::with(['translates'])->get();

        return view('general-settings.index', compact('setting', 'textPages'));
    }

    /**
     * Update position categories.
     *
     * @param  \App\Http\Requests\GeneralSettings\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        foreach ($request->validated() as $name => $value) {
            Setting::storeSetting($name, $value);
        }

        return redirect()->route('general-settings.index')->with('success', 'Налаштування успішно оновлено!');
    }
}
