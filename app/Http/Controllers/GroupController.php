<?php

namespace App\Http\Controllers;

use App\Helpers\UsefulFunctions;
use App\Http\Requests\Groups\StoreRequest;
use App\Http\Requests\Groups\UpdateRequest;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Group::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['group_filter' => []]);
            return redirect()->route('groups.index');
        } else {
            $dataFilters = auth()->user()->isAdministrator()
                ? ['teacher_name', 'group_name']
                : ['group_name'];

            $filters = $this->sessionStoreParams('group_filter', $request->only($dataFilters));
        }

        $groups = Group::with(['teacher', 'teacher.infoTeacher', 'teacher.promoCode'])
            ->withCount(['students'])
            ->filterBy($filters)
            ->latest()->paginate(request()->paginate ?? 25);

        return view('groups.index', compact('groups', 'filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $teachers = User::teacher()->get();

        return view('groups.create', compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Groups\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $group = Group::create($request->validated());

        return $request->action == 'save_and_stay' ?
            redirect()->route('groups.edit', $group)->with('success', 'Групу успішно створено!') :
            redirect()->route('groups.index')->with('success', 'Групу успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Group $group
     * @return mixed
     */
    public function edit(Group $group)
    {
        $teachers = User::teacher()->get();

        return view('groups.edit', compact('group', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Groups\UpdateRequest $request
     * @param \App\Models\Group $group
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Group $group)
    {
        $group->update($request->validated());

        return $request->action == 'save_and_stay' ?
            redirect()->route('groups.edit', $group)->with('success', 'Групу успішно оновлено!') :
            redirect()->route('groups.index')->with('success', 'Групу успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        try {
            $group->delete();
            request()->session()->flash('success', 'Групу успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Групу не видалено!');
        }

        return response($group, 200);
    }


    public function getAutocompleteTeacher(Request $request)
    {
        $response = Group::with(['teacher', 'teacher.infoTeacher'])
            ->filterBy(['teacher_name' => $request->input('search')])->get()
            ->map(function ($item) {
                return $item->name = $item->teacher->infoTeacher->full_name;
            })->unique()->take(20);

        return response()->json($response);
    }

    public function getAutocompleteGroup(Request $request)
    {
        $response = Group::filterBy(['group_name' => $request->input('q')])->get()
            ->map(function ($item) {
                return $item->name = $item->title;
            })->unique()->take(20);

        return response()->json($response);
    }
}
