<?php

namespace App\Http\Controllers;

use App\Models\MarkdownImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageUploadController extends Controller
{
    /**
     * Handle image upload from TinyMCE file browser window
     *
     * @param Request $request
     * @return mixed
     */
    public function tinymce(Request $request)
    {
        $path = $request->file->storePublicly('public/tinymce');

        return response()->json(['location' => Storage::url($path)]);
    }

    /**
     * Handle image upload from TinyMCE file browser window
     *
     * @param Request $request
     * @return mixed
     */
    public function markdownEditor(Request $request)
    {
        $images = [];
        foreach ($request->all() as $file) {
            $path = $file->storePublicly('public/markdown-images');

            $images[] = [
                'path' => $path,
                'name' => basename($path),
                'link' => Storage::url($path)
            ];
        }

        return response()->json($images);
    }

    public function removeMarkdownEditor(Request $request)
    {
        Storage::delete($request->path);

        return response()->json(['path' => $request->path]);
    }

    public function destroyMarkdownEditor(Request $request)
    {
        $markdownImage = MarkdownImage::find($request->id);

        if ($markdownImage) {
            $markdownImage->delete();
        }

        return response()->json($markdownImage);
    }
}
