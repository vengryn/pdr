<?php

namespace App\Http\Controllers;

use App\Http\Requests\Instructions\StoreRequest;
use App\Http\Requests\Instructions\UpdateRequest;
use App\Models\Instruction;
use App\Models\Language;
use App\Models\TextPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InstructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $instructions = Instruction::with('textPage.translates')
            ->paginate(request()->paginate ?? 25);
        $languages = Language::all();

        return view('instructions.index', compact('instructions', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $textPages = TextPage::with('translates')->get();

        return view('instructions.create', compact('textPages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Instructions\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $instruction = Instruction::create([
            'text_page_id' => $request->get('text_page_id')
        ]);

        if ($request->hasFile('image')) {
            $instruction->image = $request->image->storePublicly('public/instructions/images');
            $instruction->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('instructions.edit', $instruction)->with('success', 'Інструкцію успішно створено!') :
            redirect()->route('instructions.index')->with('success', 'Інструкцію успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Instruction  $instruction
     * @return mixed
     */
    public function edit(Instruction $instruction)
    {
        $textPages = TextPage::with('translates')->get();

        return view('instructions.edit', compact('instruction', 'textPages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Instructions\UpdateRequest  $request
     * @param  \App\Models\Instruction  $instruction
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Instruction $instruction)
    {
        $instruction->update(['text_page_id' => $request->get('text_page_id')]);

        if ($request->hasFile('image')) {
            Storage::delete($instruction->image);
            $instruction->image = $request->image->storePublicly('public/instructions/images');
            $instruction->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('instructions.edit', $instruction)->with('success', 'Інструкцію успішно оновлено!') :
            redirect()->route('instructions.index')->with('success', 'Інструкцію успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Instruction  $instruction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instruction $instruction)
    {
        try {
            $instruction->delete();
            request()->session()->flash('success', 'Інструкцію успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Інструкцію не видалено!');
        }

        return response($instruction);
    }

    public function sort(Request $request){
        $instruction = Instruction::find($request->id);
        $instructionEntity = Instruction::find($request->positionEntity);

        if ($instruction && $instructionEntity) {
            switch ($request->type) {
                case 'moveAfter':
                    $instruction->moveAfter($instructionEntity);
                    break;
                case 'moveBefore':
                    $instruction->moveBefore($instructionEntity);
                    break;
                default:
                    return response([], 400);
            }
        }

        return response([], 200);
    }
}
