<?php

namespace App\Http\Controllers;

use App\Models\InternalTopicTrafficRule;
use App\Models\Language;
use App\Http\Requests\InternalTopicTrafficRules\StoreRequest;
use App\Http\Requests\InternalTopicTrafficRules\UpdateRequest;

class InternalTopicTrafficRuleController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'internal-theme-traffic-rules-route';

    protected $languages;
    protected $default_language;
    protected $redirect_route;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->default_language = $language->default()->first();

        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('theory.internal-theme-traffic-rules.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $internalTopicTrafficRules = InternalTopicTrafficRule::with(['translates'])
            ->search(request()->search ?? null)
            ->orderBy('number')
            ->paginate(request()->paginate ?? 25);

        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());

        return view('handbook.internal-theme-traffic-rules.index', [
            'internalTopicTrafficRules' => $internalTopicTrafficRules,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('handbook.internal-theme-traffic-rules.create', [
            'languages' => $this->languages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\InternalTopicTrafficRules\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $internalTopicTrafficRule = InternalTopicTrafficRule::create($request->validated());

        $this->saveTranslates($internalTopicTrafficRule, $request->translate ?? []);

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.internal-theme-traffic-rules.edit', $internalTopicTrafficRule)->with('success', 'Тему успішно створено!') :
            redirect()->route('theory.internal-theme-traffic-rules.index')->with('success', 'Тему успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InternalTopicTrafficRule  $internalTopicTrafficRule
     * @return mixed
     */
    public function edit(InternalTopicTrafficRule $internalTopicTrafficRule)
    {
        return view('handbook.internal-theme-traffic-rules.edit', [
            'internalTopicTrafficRule' => $internalTopicTrafficRule,
            'languages' => $this->languages,
            'redirectRoute' => $this->redirect_route
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\InternalTopicTrafficRules\UpdateRequest  $request
     * @param  \App\Models\InternalTopicTrafficRule  $internalTopicTrafficRule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, InternalTopicTrafficRule $internalTopicTrafficRule)
    {
        $internalTopicTrafficRule->update($request->validated());
        $this->saveTranslates($internalTopicTrafficRule, $request->translate ?? []);

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.internal-theme-traffic-rules.edit', $internalTopicTrafficRule)->with('success', 'Тему успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Тему успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InternalTopicTrafficRule  $internalTopicTrafficRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(InternalTopicTrafficRule $internalTopicTrafficRule)
    {
        if ($internalTopicTrafficRule->itemTrafficRules->isNotEmpty()) {
            request()->session()->flash('error', 'Тему видалити неможливо, тому що є пункти ПДР, що прив\'язані до цієї теми.');
            abort(403);
        }

        try {
            $internalTopicTrafficRule->delete();
            request()->session()->flash('success', 'Тему успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Тему не видалено!');
        }

        return response($internalTopicTrafficRule);
    }
}
