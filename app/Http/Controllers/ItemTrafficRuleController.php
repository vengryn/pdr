<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemTrafficRules\StoreRequest;
use App\Http\Requests\ItemTrafficRules\UpdateRequest;
use App\Models\InternalTopicTrafficRule;
use App\Models\ItemTrafficRule;
use App\Models\Language;
use Illuminate\Http\Request;

class ItemTrafficRuleController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'item-traffic-rules-route';

    protected $languages;
    protected $default_language;
    protected $redirect_route;
    protected $filters;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->default_language = $language->default()->first();

        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('theory.item-traffic-rules.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['item_traffic_rule_filter' => []]);
            return redirect()->route('theory.item-traffic-rules.index');
        } else {
            $this->filters = $this->sessionStoreParams('item_traffic_rule_filter', $request->only([
                'item',
                'internal_topic_traffic_rule'
            ]));
        }

        $isDisplaySorting = $this->isDisplaySorting('internal_topic_traffic_rule');

        $itemTrafficRules = ItemTrafficRule::with(['translates', 'internalTopicTrafficRule.translates'])
            ->join('internal_topic_traffic_rules', 'item_traffic_rules.internal_topic_traffic_rule_id', '=', 'internal_topic_traffic_rules.id')
            ->filterBy($this->filters)
            ->orderBy('internal_topic_traffic_rules.number')
            ->orderBy('order')
            ->select('item_traffic_rules.*')
            ->paginate(request()->paginate ?? 25);

        $internalTopicTrafficRules = InternalTopicTrafficRule::with('translates')
            ->orderBy('number')
            ->get();

        $itemFilterSelected = ItemTrafficRule::find($this->filters['item'] ?? null);

        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());

        return view('handbook.item-traffic-rules.index', [
            'itemTrafficRules' => $itemTrafficRules,
            'itemFilterSelected' => $itemFilterSelected,
            'internalTopicTrafficRules' => $internalTopicTrafficRules,
            'filters' => $this->filters,
            'isDisplaySorting' => $isDisplaySorting,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $internalTopicTrafficRules = InternalTopicTrafficRule::with('translates')
            ->orderBy('number')
            ->get();

        return view('handbook.item-traffic-rules.create', [
            'internalTopicTrafficRules' => $internalTopicTrafficRules,
            'languages' => $this->languages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\ItemTrafficRules\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $itemTrafficRule = ItemTrafficRule::create($request->only(['internal_topic_traffic_rule_id', 'number']));
        $this->saveTranslates($itemTrafficRule, $request->translate ?? []);

        foreach ($request->get('markdownImages', []) as $img) {
            $itemTrafficRule->markdownImages()->create(['path' => $img]);
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.item-traffic-rules.edit', $itemTrafficRule)->with('success', 'Пункт успішно створено!') :
            redirect()->route('theory.item-traffic-rules.index')->with('success', 'Пункт успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ItemTrafficRule $itemTrafficRule
     * @return mixed
     */
    public function edit(ItemTrafficRule $itemTrafficRule)
    {
        $internalTopicTrafficRules = InternalTopicTrafficRule::with('translates')
            ->orderBy('number')
            ->get();

        return view('handbook.item-traffic-rules.edit', [
            'itemTrafficRule' => $itemTrafficRule,
            'internalTopicTrafficRules' => $internalTopicTrafficRules,
            'languages' => $this->languages,
            'redirectRoute' => $this->redirect_route
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\ItemTrafficRules\UpdateRequest $request
     * @param \App\Models\ItemTrafficRule $itemTrafficRule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, ItemTrafficRule $itemTrafficRule)
    {
        $itemTrafficRule->update($request->only(['internal_topic_traffic_rule_id', 'number']));
        $this->saveTranslates($itemTrafficRule, $request->translate ?? []);

        foreach ($request->get('markdownImages', []) as $img) {
            $itemTrafficRule->markdownImages()->create(['path' => $img]);
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.item-traffic-rules.edit', $itemTrafficRule)->with('success', 'Пункт успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Пункт успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ItemTrafficRule $itemTrafficRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemTrafficRule $itemTrafficRule)
    {
        try {
            $itemTrafficRule->delete();
            request()->session()->flash('success', 'Пункт успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Пункт не видалено!');
        }

        return response($itemTrafficRule);
    }

    public function getAutocompleteItem(Request $request)
    {
        $response = ItemTrafficRule::select(['id', 'number'])
            ->filterBy([
                'number' => $request->input('q'),
                'internal_topic_traffic_rule' => $request->input('internal_topic_traffic_rule')
            ])
            ->get()
            ->map(function ($item) {
                return $item->only(['id', 'number']);
            })
            ->unique('number')
            ->take(20);

        return response()->json($response);
    }

    public function sort(Request $request){
        $itemTrafficRule = ItemTrafficRule::find($request->id);
        $itemTrafficRuleEntity = ItemTrafficRule::find($request->positionEntity);

        if ($itemTrafficRule && $itemTrafficRuleEntity) {
            switch ($request->type) {
                case 'moveAfter':
                    $itemTrafficRule->moveAfter($itemTrafficRuleEntity);
                    break;
                case 'moveBefore':
                    $itemTrafficRule->moveBefore($itemTrafficRuleEntity);
                    break;
                default:
                    return response([], 400);
            }
        }

        return response([]);
    }

    /**
     * @param $display
     * @return bool
     */
    protected function isDisplaySorting($display)
    {
        $filters = $this->filters;

        if (!empty($filters[$display])) {
            unset($filters[$display]);

            foreach ($filters as $filter) {
                if (!empty($filter)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
