<?php

namespace App\Http\Controllers;

use App\Http\Requests\Languages\StoreRequest;
use App\Http\Requests\Languages\UpdateRequest;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $languages = Language::paginate(25);

        return view('languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Languages\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $language = Language::create($request->only([
            'title', 'abbreviation'
        ]));

        if ($request->hasFile('flag')) {
            $language->flag = $request->flag->storePublicly('public/languages/flags');
            $language->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('languages.edit', $language)->with('success', 'Мову успішно створено!') :
            redirect()->route('languages.index')->with('success', 'Мову успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return mixed
     */
    public function edit(Language $language)
    {
        return view('languages.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Languages\UpdateRequest  $request
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Language $language)
    {
        $language->update($request->only([
            'title', 'abbreviation'
        ]));

        if ($request->hasFile('flag')) {
            Storage::delete($language->flag);
            $language->flag = $request->flag->storePublicly('public/languages/flags');
            $language->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('languages.edit', $language)->with('success', 'Мову успішно оновлено!') :
            redirect()->route('languages.index')->with('success', 'Мову успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        try {
            $language->delete();
            request()->session()->flash('success', 'Мову успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Мову не видалено!');
        }

        return response($language, 200);
    }

    public function sort(Request $request){
        $language = Language::find($request->id);
        $languageEntity = Language::find($request->positionEntity);

        if ($language && $languageEntity) {
            switch ($request->type) {
                case 'moveAfter':
                    $language->moveAfter($languageEntity);
                    break;
                case 'moveBefore':
                    $language->moveBefore($languageEntity);
                    break;
                default:
                    return response([], 400);
            }
        }

        return response([], 200);
    }
}
