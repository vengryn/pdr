<?php

namespace App\Http\Controllers;

use App\Models\Log;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $logs = Log::latest()->paginate(request()->paginate ?? 25);

        return view('logs.index', compact('logs'));
    }
}
