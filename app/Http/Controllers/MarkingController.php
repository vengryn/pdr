<?php

namespace App\Http\Controllers;

use App\Http\Requests\Markings\StoreRequest;
use App\Http\Requests\Markings\UpdateRequest;
use App\Models\Language;
use App\Models\Marking;
use App\Models\TopicRoadMarking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MarkingController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'markings-route';

    protected $languages;
    protected $default_language;
    protected $redirect_route;
    protected $filters;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->default_language = $language->default()->first();

        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('theory.markings.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['marking_filter' => []]);
            return redirect()->route('theory.markings.index');
        } else {
            $this->filters = $this->sessionStoreParams('marking_filter', $request->only([
                'marking',
                'topic_road_marking'
            ]));
        }

        $isDisplaySorting = $this->isDisplaySorting('topic_road_marking');

        $topicRoadMarkings = TopicRoadMarking::with(['translates'])->get();
        $markingFilterSelected = Marking::find($this->filters['marking'] ?? null);
        $markings = Marking::with(['topicRoadMarking.translates'])
            ->join('topic_road_markings', 'markings.topic_road_marking_id', '=', 'topic_road_markings.id')
            ->filterBy($this->filters)
            ->orderBy('topic_road_markings.number')
            ->orderBy('order')
            ->select('markings.*')
            ->paginate(request()->paginate ?? 20);

        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());

        return view('handbook.markings.index', [
            'markings' => $markings,
            'markingFilterSelected' => $markingFilterSelected,
            'topicRoadMarkings' => $topicRoadMarkings,
            'filters' => $this->filters,
            'isDisplaySorting' => $isDisplaySorting,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $topicRoadMarkings = TopicRoadMarking::with(['translates'])->get();

        return view('handbook.markings.create', [
            'topicRoadMarkings' => $topicRoadMarkings,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Markings\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $marking = Marking::create($request->only(['number', 'topic_road_marking_id', 'video_example']));
        $this->saveTranslates($marking, $request->translate ?? []);

        if ($request->hasFile('image')) {
            $marking->storeImage($request->image);
        }

        if ($request->hasFile('image_example')) {
            $marking->image_example = $request->image_example->storePublicly('markings/image-example', 'digitalocean');
            $marking->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.markings.edit', $marking)->with('success', 'Розмітку успішно створено!') :
            redirect()->route('theory.markings.index')->with('success', 'Розмітку успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Marking  $marking
     * @return mixed
     */
    public function edit(Marking $marking)
    {
        $topicRoadMarkings = TopicRoadMarking::with(['translates'])->get();

        return view('handbook.markings.edit', [
            'marking' => $marking,
            'topicRoadMarkings' => $topicRoadMarkings,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language,
            'redirectRoute' => $this->redirect_route
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Markings\UpdateRequest  $request
     * @param  \App\Models\Marking  $marking
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Marking $marking)
    {
        $marking->update($request->only(['number', 'topic_road_marking_id', 'video_example']));
        $this->saveTranslates($marking, $request->translate ?? []);

        if ($request->hasFile('image')) {
            Storage::delete($marking->image);
            Storage::delete($marking->image26x26);

            $marking->storeImage($request->image);
        }

        if (!$request->get('image_example_path')) {
            Storage::disk('digitalocean')->delete($marking->image_example);
            $marking->image_example = null;
            $marking->save();
        }

        if ($request->hasFile('image_example')) {
            $marking->image_example = $request->image_example->storePublicly('markings/image-example', 'digitalocean');
            $marking->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.markings.edit', $marking)->with('success', 'Розмітку успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Розмітку успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Marking  $marking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marking $marking)
    {
        try {
            $marking->delete();
            request()->session()->flash('success', 'Розмітку успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Розмітку не видалено!');
        }

        return response($marking);
    }

    public function getAutocompleteMarking(Request $request)
    {
        $response = Marking::select(['id', 'number'])
            ->filterBy([
                'name' => $request->input('q'),
                'topic_road_marking' => $request->input('topic_road_marking')
            ])
            ->get()
            ->map(function ($item) {
                return $item->only(['id', 'name']);
            })
            ->unique('name')
            ->take(20);

        return response()->json($response);
    }

    public function sort(Request $request){
        $marking = Marking::find($request->id);
        $markingEntity = Marking::find($request->positionEntity);

        if ($marking && $markingEntity) {
            switch ($request->type) {
                case 'moveAfter':
                    $marking->moveAfter($markingEntity);
                    break;
                case 'moveBefore':
                    $marking->moveBefore($markingEntity);
                    break;
                default:
                    return response([], 400);
            }
        }

        return response([]);
    }

    /**
     * @param $display
     * @return bool
     */
    protected function isDisplaySorting($display)
    {
        $filters = $this->filters;

        if (!empty($filters[$display])) {
            unset($filters[$display]);

            foreach ($filters as $filter) {
                if (!empty($filter)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
