<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['payment_filter' => []]);
            return redirect()->route('payments.index', $request->only(['sort', 'direction']));
        } else {
            $period = [
                $request->get('date_start', session('student_filter.date_start')),
                $request->get('date_end', session('student_filter.date_end'))
            ];

            $filters = $this->sessionStoreParams('payment_filter', $request->only([
                'student_name', 'marketplace', 'date_start', 'date_end'
            ]) + ['period' => $period]);
        }

        $payments = Payment::with(['student.infoStudent'])
            ->filterBy($filters)
            ->orderByDesc('transaction_at')
            ->paginate(request()->paginate ?? 25);

        $marketplaces = Payment::getMarketplaceList();

        return view('payments.index', compact('payments', 'marketplaces', 'filters'));
    }

    public function getAutocompleteStudent(Request $request)
    {
        $response =  Payment::with(['student.infoStudent'])
            ->filterBy(['student_name' => $request->input('search')])->get()
            ->map(function ($item) {
                return $item->name = $item->student->infoStudent->full_name ?? '';
            })->unique()->take(20);

        return response()->json($response);
    }
}
