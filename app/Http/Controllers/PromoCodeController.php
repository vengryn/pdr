<?php

namespace App\Http\Controllers;

use App\Http\Requests\PromoCodes\StoreRequest;
use App\Http\Requests\PromoCodes\UpdateRequest;
use App\Models\PromoCode;
use Illuminate\Http\Request;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['promo_code_filter' => []]);
            return redirect()->route('promo-codes.index');
        } else {
            $filters = $this->sessionStoreParams('promo_code_filter', $request->only(['code']));
        }

        $promoCodes = PromoCode::filterBy($filters)
            ->where('type', '!=', PromoCode::TYPE_INTERNAL)
            ->orderBy('type')
            ->orderBy('created_at')
            ->paginate(request()->paginate ?? 25);

        return view('promo-codes.index', compact('promoCodes', 'filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('promo-codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PromoCodes\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $promoCode = PromoCode::create($request->validated() + [
                'type' => PromoCode::TYPE_MARKETING
            ]);

        return $request->action == 'save_and_stay' ?
            redirect()->route('promo-codes.edit', $promoCode)->with('success', 'Промокод успішно створено!') :
            redirect()->route('promo-codes.index')->with('success', 'Промокод успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return mixed
     */
    public function edit(PromoCode $promoCode)
    {
        return view('promo-codes.edit', compact('promoCode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PromoCodes\UpdateRequest  $request
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, PromoCode $promoCode)
    {
        $promoCode->update($request->validated());

        return $request->action == 'save_and_stay' ?
            redirect()->route('promo-codes.edit', $promoCode)->with('success', 'Промокод успішно оновлено!') :
            redirect()->route('promo-codes.index')->with('success', 'Промокод успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromoCode $promoCode)
    {
        try {
            $promoCode->delete();
            request()->session()->flash('success', 'Промокод успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Промокод не видалено!');
        }

        return response($promoCode, 200);
    }

    public function getAutocompletePromoCode(Request $request)
    {
        $response =  PromoCode::filterBy(['code' => $request->input('search')])
            ->get()->unique()
            ->take(20)->pluck('code');

        return response()->json($response);
    }
}
