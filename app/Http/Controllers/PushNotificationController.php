<?php

namespace App\Http\Controllers;

use App\Models\PushNotification;
use Illuminate\Http\Request;

class PushNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $pushNotifications = PushNotification::latest()->paginate(request()->paginate ?? 25);

        return view('push-notifications.index', compact('pushNotifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $message = $request->get('message');
        $title = $request->get('title');

        if ($message && $title) {
            PushNotification::create(['title' => $title, 'message' => $message]);

            return redirect()->route('push-notifications.index', [
                'page' => $request->get('page'),
                'paginate' => $request->get('paginate')
            ])->with('success', 'Повідомлення успішно відправлено!');
        }

        return redirect()->route('push-notifications.index')->with('error', 'Помилка. Повідомлення не відправлено!');
    }
}
