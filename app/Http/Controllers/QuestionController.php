<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\StoreRequest;
use App\Http\Requests\Questions\UpdateRequest;
use App\Models\Folder;
use App\Models\Language;
use App\Models\Question;
use App\Models\Setting;
use App\Models\TopicTrafficRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class QuestionController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'questions-route';

    protected $redirect_route;

    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('issue-management.questions.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['question_filter' => []]);
            return redirect()->route('issue-management.questions.index');
        } else {
            $filters = $this->sessionStoreParams('question_filter',
                $request->only(['content', 'topic_traffic_rule', 'folder', 'status', 'type']) + [
                    'kind' => $request->get('kind', false)
                ]
            );
        }

        $questions = Question::with(['topicTrafficRule', 'topicTrafficRule.folders', 'translates', 'copy'])
            ->filterBy($filters)
            ->paginate(request()->paginate ?? 25);

        $topicTrafficRules = TopicTrafficRule::with(['translates'])->orderBy('number')->get();
        $folders = Folder::all();

        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());
        $lastDateUpdating = Setting::getSetting('date_of_last_update_questions');

        return view('issue-management.questions.index', compact('questions', 'filters',
            'topicTrafficRules', 'folders', 'lastDateUpdating'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $languages = Language::all();
        $topicTrafficRules = TopicTrafficRule::with(['translates'])->get();

        return view('issue-management.questions.create', compact('languages', 'topicTrafficRules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Questions\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $question = Question::create($request->only(['topic_traffic_rule_id']) + [
                'is_internal' => true,
                'status' => $request->get('status') ? true : false
            ]);

        foreach ($request->get('markdownImages', []) as $img) {
            $question->markdownImages()->create(['path' => $img]);
        }

        if ($request->hasFile('picture')) {
            $imageName = 'QNS_' . $question->id . '.' . $request->picture->extension();

            $question->picture = $request->picture->storePubliclyAs('public/questions', $imageName);
            $question->save();
        }

        $this->saveTranslates($question, $request->translate ?? []);
        $question->syncAnswers($request);

        return $request->action == 'save_and_stay' ?
            redirect()->route('issue-management.questions.edit', $question)->with('success', 'Питання успішно створено!') :
            redirect()->route('issue-management.questions.index')->with('success', 'Питання успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Question $question
     * @return mixed
     */
    public function edit(Question $question)
    {
        $question->load('questionsAnswers.translates');
        $languages = Language::all();
        $topicTrafficRules = TopicTrafficRule::with(['translates'])->get();
        $redirectRoute = $this->redirect_route;

        return view('issue-management.questions.edit', compact('question', 'languages',
            'topicTrafficRules', 'redirectRoute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Questions\UpdateRequest $request
     * @param \App\Models\Question $question
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Question $question)
    {
        $question->update($request->only(['topic_traffic_rule_id', 'external_id']) + [
                'status' => $request->get('status') ? true : false
            ]);

        foreach ($request->get('markdownImages', []) as $img) {
            $question->markdownImages()->create(['path' => $img]);
        }

        if (!$request->get('picture_path')) {
            Storage::delete($question->picture);
            $question->picture = null;
            $question->save();
        }

        if ($request->hasFile('picture')) {
            Storage::delete($question->picture);
            $imageName = 'QNS_' . $question->id . '.' . $request->picture->extension();

            $question->picture = $request->picture->storePubliclyAs('public/questions', $imageName);
            $question->save();
        }

        $this->saveTranslates($question, $request->translate ?? []);
        $question->syncAnswers($request);

        return $request->action == 'save_and_stay' ?
            redirect()->route('issue-management.questions.edit', $question)->with('success', 'Питання успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Питання успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        try {
            $question->delete();
            request()->session()->flash('success', 'Питання успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Питання не видалено!');
        }

        return response($question, 200);
    }

    /**
     * Show the form for duplicate the specified resource.
     *
     * @param \App\Models\Question $question
     * @return mixed
     */
    public function duplicate(Question $question)
    {
        if ($question->copy) {
            abort(404);
        }
        $question->load('questionsAnswers.translates');
        $languages = Language::all();
        $topicTrafficRules = TopicTrafficRule::with(['translates'])->get();

        return view('issue-management.questions.duplicate', compact('question', 'languages', 'topicTrafficRules'));
    }

    /**
     * Duplicate resource in storage.
     *
     * @param \App\Http\Requests\Questions\StoreRequest $request
     * @param \App\Models\Question $duplicateQuestion
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeDuplicate(StoreRequest $request, Question $duplicateQuestion)
    {
        if ($duplicateQuestion->copy) {
            abort(404);
        }

        $duplicateQuestion->status = false;
        $duplicateQuestion->save();

        $question = Question::create($request->only(['topic_traffic_rule_id']) + [
                'is_internal' => true,
                'is_copy' => true,
                'status' => $request->get('status') ? true : false,
                'external_id' => $request->get('external_id')
            ]);

        foreach ($request->get('markdownImages', []) as $img) {
            $question->markdownImages()->create(['path' => $img]);
        }

        $translates = $request->get('translate', []);

        foreach ($request->get('markdownDuplicateImages', []) as $imgDuplicate) {
            if (Storage::exists($imgDuplicate)) {
                $ext = pathinfo($imgDuplicate, PATHINFO_EXTENSION);
                $imgMarkdownDuplicate = 'public/markdown-images/' . Str::random(40) . '.' . $ext;

                foreach (($translates['explanation'] ?? []) as $key => $explanation) {
                    preg_match_all('/!\[(.*?)\]\((.*?)\)/', $explanation, $matches);
                    $links = array_unique($matches[2]);

                    foreach ($links as $link) {
                        if (basename($link) == basename($imgDuplicate)) {
                            $explanation = str_replace(basename($link), basename($imgMarkdownDuplicate), $explanation);
                        }
                    }

                    $translates['explanation'][$key] = $explanation;
                }

                Storage::delete($imgMarkdownDuplicate);
                Storage::copy($imgDuplicate, $imgMarkdownDuplicate);
                $question->markdownImages()->create(['path' => $imgMarkdownDuplicate]);
            }
        }

        if ($picturePath = $request->get('picture_path')) {
            if (Storage::exists($picturePath)) {
                $ext = pathinfo($picturePath, PATHINFO_EXTENSION);
                $imageName = 'public/questions/QNS_' . $question->id . '.' . $ext;

                Storage::delete($imageName);
                Storage::copy($picturePath, $imageName);

                $question->picture = $imageName;
                $question->save();
            }
        } else {
            if ($request->hasFile('picture')) {
                $imageName = 'QNS_' . $question->id . '.' . $request->picture->extension();

                $question->picture = $request->picture->storePubliclyAs('public/questions', $imageName);
                $question->save();
            }
        }

        $this->saveTranslates($question, $translates);
        $question->syncAnswers($request);

        return $request->action == 'save_and_stay' ?
            redirect()->route('issue-management.questions.edit', $question)->with('success', 'Питання успішно створено!') :
            redirect($this->redirect_route)->with('success', 'Питання успішно створено!');
    }

    public function frequentMistakes()
    {
        $questions = Question::with(['translates'])
            ->where('count_wrong_answer', '>', 0)
            ->orderByDesc('count_wrong_answer')
            ->limit(100)
            ->get();

        $questions = $this->paginate($questions, request()->paginate ?? 25);

        return view('issue-management.frequent-mistakes.index', compact('questions'));
    }

    public function resetFrequentMistakes(Request $request)
    {
        if ($request->get('question_id')) {
            if ($question = Question::find($request->get('question_id'))) {
                $question->update(['count_wrong_answer' => 0]);
            }
        } else {
            Question::where('count_wrong_answer', '>', 0)->update(['count_wrong_answer' => 0]);
        }

        return response(null);
    }
}
