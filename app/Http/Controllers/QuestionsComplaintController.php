<?php

namespace App\Http\Controllers;

use App\Models\QuestionsComplaint;

class QuestionsComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $questionsComplaints = QuestionsComplaint::with(['user.infoStudent', 'question.translates'])
            ->latest()
            ->paginate(request()->paginate ?? 25);

        return view('issue-management.questions-complaints.index', compact('questionsComplaints'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuestionsComplaint  $questionsComplaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionsComplaint $questionsComplaint)
    {
        try {
            $questionsComplaint->delete();
            request()->session()->flash('success', 'Скарга успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Скаргу не видалено!');
        }

        return response($questionsComplaint);
    }
}
