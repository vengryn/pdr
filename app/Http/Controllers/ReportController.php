<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use PDF;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['report_filter' => []]);
            return redirect()->route('report.index');
        } else {
            $filters = $this->sessionStoreParams('report_filter', $request->only(['promo_code']));
        }

        $payments = Payment::with(['student.infoStudent', 'student.group'])
            ->filterBy($filters)
            ->leftJoin('users', function($join) {
                $join->on('payments.user_id', '=', 'users.id')->whereNull('users.deleted_at');
                $join->leftJoin('groups', 'users.group_id', '=', 'groups.id')->whereNull('groups.deleted_at');
            })
            ->select(['payments.*', 'groups.title as group_name'])
            ->paginate(request()->paginate ?? 25);

        return view('report.index', compact('payments', 'filters'));
    }

    public function import()
    {
        $filters = $this->sessionStoreParams('report_filter');

        $payments = Payment::with(['student.infoStudent', 'student.group'])
            ->filterBy($filters)
            ->get();

        $pdf = PDF::loadView('report.import', ['payments' => $payments]);
        return $pdf->download('report.pdf');
    }
}
