<?php

namespace App\Http\Controllers;

use App\Http\Requests\Signs\StoreRequest;
use App\Http\Requests\Signs\UpdateRequest;
use App\Models\Language;
use App\Models\Sign;
use App\Models\TopicRoadSign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SignController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'signs-route';

    protected $languages;
    protected $default_language;
    protected $redirect_route;
    protected $filters;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->default_language = $language->default()->first();

        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('theory.signs.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['sign_filter' => []]);
            return redirect()->route('theory.signs.index');
        } else {
            $this->filters = $this->sessionStoreParams('sign_filter', $request->only([
                'sign',
                'topic_road_sign'
            ]));
        }

        $isDisplaySorting = $this->isDisplaySorting('topic_road_sign');

        $topicRoadSigns = TopicRoadSign::with(['translates'])->get();
        $signFilterSelected = Sign::find($this->filters['sign'] ?? null);
        $signs = Sign::with(['topicRoadSign.translates'])
            ->join('topic_road_signs', 'signs.topic_road_sign_id', '=', 'topic_road_signs.id')
            ->filterBy($this->filters)
            ->orderBy('topic_road_signs.number')
            ->orderBy('order')
            ->select('signs.*')
            ->paginate(request()->paginate ?? 20);


        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());

        return view('handbook.signs.index', [
            'signs' => $signs,
            'signFilterSelected' => $signFilterSelected,
            'topicRoadSigns' => $topicRoadSigns,
            'filters' => $this->filters,
            'isDisplaySorting' => $isDisplaySorting,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $topicRoadSigns = TopicRoadSign::with(['translates'])->get();

        return view('handbook.signs.create', [
            'topicRoadSigns' => $topicRoadSigns,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Signs\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $sign = Sign::create($request->only(['number', 'topic_road_sign_id', 'video_example']));
        $this->saveTranslates($sign, $request->translate ?? []);

        if ($request->hasFile('image')) {
            $sign->storeImage($request->image);
        }

        if ($request->hasFile('image_example')) {
            $sign->image_example = $request->image_example->storePublicly('signs/image-example', 'digitalocean');
            $sign->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.signs.edit', $sign)->with('success', 'Знак успішно створено!') :
            redirect()->route('theory.signs.index')->with('success', 'Знак успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Sign $sign
     * @return mixed
     */
    public function edit(Sign $sign)
    {
        $topicRoadSigns = TopicRoadSign::with(['translates'])->get();

        return view('handbook.signs.edit', [
            'sign' => $sign,
            'topicRoadSigns' => $topicRoadSigns,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language,
            'redirectRoute' => $this->redirect_route
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Signs\UpdateRequest $request
     * @param \App\Models\Sign $sign
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Sign $sign)
    {
        $sign->update($request->only(['number', 'topic_road_sign_id', 'video_example']));
        $this->saveTranslates($sign, $request->translate ?? []);

        if ($request->hasFile('image')) {
            Storage::delete($sign->image);
            Storage::delete($sign->image26x26);

            $sign->storeImage($request->image);
        }

        if (!$request->get('image_example_path')) {
            Storage::disk('digitalocean')->delete($sign->image_example);
            $sign->image_example = null;
            $sign->save();
        }

        if ($request->hasFile('image_example')) {
            $sign->image_example = $request->image_example->storePublicly('signs/image-example', 'digitalocean');
            $sign->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.signs.edit', $sign)->with('success', 'Знак успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Знак успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Sign $sign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sign $sign)
    {
        try {
            $sign->delete();
            request()->session()->flash('success', 'Знак успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Знак не видалено!');
        }

        return response($sign);
    }

    public function getAutocompleteSign(Request $request)
    {
        $response = Sign::select(['id', 'number'])
            ->filterBy([
                'name' => $request->input('q'),
                'topic_road_sign' => $request->input('topic_road_sign')
            ])
            ->get()
            ->map(function ($item) {
                return $item->only(['id', 'name']);
            })
            ->unique('name')
            ->take(20);

        return response()->json($response);
    }

    public function sort(Request $request)
    {
        $sign = Sign::find($request->id);
        $signEntity = Sign::find($request->positionEntity);

        if ($sign && $signEntity) {
            switch ($request->type) {
                case 'moveAfter':
                    $sign->moveAfter($signEntity);
                    break;
                case 'moveBefore':
                    $sign->moveBefore($signEntity);
                    break;
                default:
                    return response([], 400);
            }
        }

        return response([]);
    }

    /**
     * @param $display
     * @return bool
     */
    protected function isDisplaySorting($display)
    {
        $filters = $this->filters;

        if (!empty($filters[$display])) {
            unset($filters[$display]);

            foreach ($filters as $filter) {
                if (!empty($filter)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
