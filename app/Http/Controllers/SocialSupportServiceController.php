<?php

namespace App\Http\Controllers;

use App\Http\Requests\SocialSupportServices\StoreRequest;
use App\Http\Requests\SocialSupportServices\UpdateRequest;
use App\Models\SocialSupportService;
use Illuminate\Support\Facades\Storage;

class SocialSupportServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $socialSupportServices = SocialSupportService::all();

        return view('social-support-services.index', compact('socialSupportServices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('social-support-services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SocialSupportServices\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $socialSupportService = SocialSupportService::create($request->validated());

        if ($request->hasFile('logo')) {
            $socialSupportService->logo = $request->logo->storePublicly('public/support-services/logo');
            $socialSupportService->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('support-services.edit', $socialSupportService)->with('success', 'Службу підтримки успішно створено!') :
            redirect()->route('support-services.index')->with('success', 'Службу підтримки успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SocialSupportService  $socialSupportService
     * @return mixed
     */
    public function edit(SocialSupportService $socialSupportService)
    {
        return view('social-support-services.edit', compact('socialSupportService'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SocialSupportServices\UpdateRequest  $request
     * @param  \App\Models\SocialSupportService  $socialSupportService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, SocialSupportService $socialSupportService)
    {
        $socialSupportService->update($request->validated());

        if ($request->hasFile('logo')) {
            Storage::delete($socialSupportService->logo);

            $socialSupportService->logo = $request->logo->storePublicly('public/support-services/logo');
            $socialSupportService->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('support-services.edit', $socialSupportService)->with('success', 'Службу підтримки успішно оновлено!') :
            redirect()->route('support-services.index')->with('success', 'Службу підтримки успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SocialSupportService  $socialSupportService
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialSupportService $socialSupportService)
    {
        try {
            $socialSupportService->delete();
            request()->session()->flash('success', 'Служба підтримки успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Служба підтримки не видалено!');
        }

        return response($socialSupportService, 200);
    }
}
