<?php

namespace App\Http\Controllers;

use App\Exports\StudentsExport;
use App\Models\CategoryDriverLicense;
use App\Models\Group;
use App\Models\InfoStudent;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserSubscription;
use App\Repositories\Interfaces\UserSubscriptionRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Students\StoreRequest;
use App\Http\Requests\Students\UpdateRequest;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    private $userSubscriptionRepository;

    public function __construct(UserSubscriptionRepositoryInterface $userSubscriptionRepository)
    {
        $this->userSubscriptionRepository = $userSubscriptionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['student_filter' => []]);
            return redirect()->route('students.index');
        } else {
            $period = [
                $request->get('created_start', session('student_filter.created_start')),
                $request->get('created_end', session('student_filter.created_end'))
            ];

            $dataFilters = auth()->user()->isAdministrator()
                ? ['email', 'student_name', 'teacher_name', 'group_name', 'subscription', 'promo_code_student', 'created_start', 'created_end']
                : ['email', 'student_name', 'group_name', 'subscription', 'promo_code_student', 'created_start', 'created_end'];

            $filters = $this->sessionStoreParams('student_filter', $request->only($dataFilters) + ['period' => $period]);
        }

        if (auth()->user()->isAdministrator()) {
            $students = User::student()
                ->leftJoin('user_subscriptions', function ($join) {
                    $join->on('user_subscriptions.user_id', '=', 'users.id')
                        ->orderByDesc('end_at')
                        ->where('end_at', '>', Carbon::now())
                        ->limit(1);
                })
                ->userByGroup()
                ->filterBy($filters)
                ->with(['infoStudent', 'infoTeacher', 'group.teacher.infoTeacher', 'categoryDriverLicenses', 'promoCode',
                    'questionAnswerHistories', 'exams', 'subscription', 'subscription.subscription.translates', 'ticketTraining'])
                ->sortable(['created_at' => 'desc'])
                ->select('users.id', 'users.email', 'users.role', 'users.group_id', 'users.referer_id', 'users.status',
                    'users.created_at', 'users.updated_at', 'users.deleted_at',
                    'user_subscriptions.start_at as subscriptions_start_at',
                    'user_subscriptions.end_at as subscriptions_end_at')
                ->groupBy('users.id')
                ->groupBy('users.email')
                ->groupBy('users.role')
                ->groupBy('users.group_id')
                ->groupBy('users.referer_id')
                ->groupBy('users.status')
                ->groupBy('users.created_at')
                ->groupBy('users.updated_at')
                ->groupBy('users.deleted_at')
//                ->groupBy('subscriptions_start_at')
//                ->groupBy('subscriptions_end_at')
                ->paginate(request()->paginate ?? 25);
        } else {
            $students = User::student()
                ->userByGroup()
                ->filterBy($filters)
                ->with(['infoStudent', 'infoTeacher', 'group.teacher.infoTeacher', 'categoryDriverLicenses', 'promoCode',
                    'questionAnswerHistories', 'exams', 'subscription', 'subscription.subscription.translates', 'ticketTraining'])
                ->latest('users.created_at')
                ->paginate(request()->paginate ?? 25);
        }

        $subscriptions = Subscription::with(['translates'])->orderBy('validity')->get();

        return view('students.index', compact('students', 'filters', 'subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $groups = Group::all();
        $categoryDriverLicenses = CategoryDriverLicense::all();
        $subscriptions = Subscription::orderBy('validity')->get();

        return view('students.create', compact('groups', 'categoryDriverLicenses', 'subscriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Students\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $student = User::create($request->only(['email', 'password', 'group_id']) + ['role' => User::ROLE_STUDENT]);
        $student->infoStudent()->create($request->only(['first_name', 'last_name', 'phone',]));
        $student->categoryDriverLicenses()->attach($request->get('category_driver_license_id'));

        if (Gate::check('edit-premium-students')) {
            if ($subscription = Subscription::find($request->get('subscription'))) {
                $this->userSubscriptionRepository->createPaid(
                    $student,
                    $subscription,
                    UserSubscription::SOURCE_ADMIN
                );
            }
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('students.edit', $student)->with('success', 'Учня успішно створено!') :
            redirect()->route('students.index')->with('success', 'Учня успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\User $student
     * @return mixed
     */
    public function edit(User $student)
    {
        Gate::authorize('students', $student);
        $groups = Group::all();
        $categoryDriverLicenses = CategoryDriverLicense::all();
        $subscriptions = Subscription::orderBy('validity')->get();

        return view('students.edit', compact('student', 'groups', 'categoryDriverLicenses', 'subscriptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Students\UpdateRequest $request
     * @param \App\Models\User $student
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, User $student)
    {
        Gate::authorize('students', $student);
        $student->update($request->only(['email', 'password', 'group_id']));

        InfoStudent::updateOrCreate(['user_id' => $student->id],
            $request->only(['first_name', 'last_name', 'phone'])
        );

        $student->categoryDriverLicenses()->sync($request->get('category_driver_license_id'));

        if (Gate::check('edit-premium-students')) {

            $userSubscription = $student->isActiveSubscription();

            if (!$request->boolean('is_premium') && $userSubscription) {
                $student->subscription()->delete();
            } elseif ($subscription = Subscription::find($request->get('subscription'))) {
                if (!$userSubscription || (!empty($userSubscription) && $userSubscription->subscription_id != $subscription->id)) {
                    $this->userSubscriptionRepository->createPaid(
                        $student,
                        $subscription,
                        UserSubscription::SOURCE_ADMIN
                    );
                }
            }
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('students.edit', $student)->with('success', 'Учня успішно оновлено!') :
            redirect()->route('students.index')->with('success', 'Учня успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\User $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $student)
    {
        try {
            Gate::authorize('delete-students');

            $student->delete();
            request()->session()->flash('success', 'Учня успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Учня не видалено!');
        }

        return response($student, 200);
    }

    public function getAutocompleteStudent(Request $request)
    {
        $response = User::student()->with(['infoStudent'])
            ->userByGroup()
            ->filterBy(['student_name' => $request->input('search')])->get()
            ->map(function ($item) {
                return $item->name = $item->infoStudent->full_name ?? '';
            })->unique()->take(20);

        return response()->json($response);
    }

    public function getAutocompleteTeacher(Request $request)
    {
        $response = User::student()->with(['group.teacher.infoTeacher'])
            ->userByGroup()
            ->filterBy(['teacher_name' => $request->input('search')])->get()
            ->map(function ($item) {
                return $item->name = $item->group->teacher->infoTeacher->full_name ?? '';
            })->unique()->take(20);

        return response()->json($response);
    }

    public function getAutocompleteGroup(Request $request)
    {
        $response = User::student()->userByGroup()->with(['group'])
            ->filterBy(['group_name' => $request->input('q')])->get()
            ->map(function ($item) {
                return $item->name = $item->group->title ?? '';
            })->unique()->take(20);

        return response()->json($response);
    }

    public function getAutocompleteEmail(Request $request)
    {
        $response = User::student()
            ->userByGroup()
            ->filterBy(['email' => $request->input('search')])->get()
            ->map(function ($item) {
                return $item->name = $item->email ?? '';
            })->unique()->take(20);

        return response()->json($response);
    }

    public function getAutocompletePromoCode(Request $request)
    {
        $response = User::student()->with(['promoCode'])
            ->filterBy(['promo_code_student' => $request->input('search')])->get()
            ->pluck('promoCode.code')->take(20);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actionMultiple(Request $request)
    {
        $ids = $request->get('ids', []);

        switch ($request->get('action')) {
            case 'premium':
                if ($subscription = Subscription::find($request->get('subscription'))) {
                    User::student()->whereIn('id', $ids)->get()->each(function ($item) use ($subscription) {
                        $this->userSubscriptionRepository->createPaid(
                            $item,
                            $subscription,
                            UserSubscription::SOURCE_ADMIN
                        );
                    });
                }
                return redirect()->back()->with('success', 'Обраним учням підписка успішно встановлена');
        }

        return redirect()->back()->with('error', 'Помилка. Дія не знайдена.');
    }

    public function export()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(3600);

        return Excel::download(new StudentsExport, 'students.xlsx');
    }
}
