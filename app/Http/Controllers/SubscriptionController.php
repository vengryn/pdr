<?php

namespace App\Http\Controllers;

use App\Http\Requests\Subscriptions\StoreRequest;
use App\Http\Requests\Subscriptions\UpdateRequest;
use App\Models\Language;
use App\Models\Setting;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $languages = Language::all();
        $subscriptions = Subscription::all();

        return view('subscriptions.index', compact('subscriptions', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $languages = Language::all();

        return view('subscriptions.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Subscriptions\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $subscription = Subscription::create($request->validated());
        $this->saveTranslates($subscription, $request->get('translate', []));

        return $request->action == 'save_and_stay' ?
            redirect()->route('subscriptions.edit', $subscription)->with('success', 'Підписку успішно створено!') :
            redirect()->route('subscriptions.index')->with('success', 'Підписку успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return mixed
     */
    public function edit(Subscription $subscription)
    {
        $languages = Language::all();

        return view('subscriptions.edit', compact('subscription', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Subscriptions\UpdateRequest  $request
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Subscription $subscription)
    {
        $subscription->update($request->validated());
        $this->saveTranslates($subscription, $request->get('translate', []));

        return $request->action == 'save_and_stay' ?
            redirect()->route('subscriptions.edit', $subscription)->with('success', 'Підписку успішно оновлено!') :
            redirect()->route('subscriptions.index')->with('success', 'Підписку успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        try {
            $subscription->delete();
            request()->session()->flash('success', 'Підписку успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Підписку не видалено!');
        }

        return response($subscription, 200);
    }

    public function autoPremium()
    {
        $settings = Setting::getSettings(['auto_premium_date_start', 'auto_premium_days', 'auto_premium_paid_video']);
        $isAutoPremium = Subscription::isAutoPremium();
        $lastUpdate = Setting::where('name', 'is_auto_premium')->first();
        $offDate = Setting::where('name', 'auto_premium_days')->first();

        return view('auto-premium.index', compact('settings', 'isAutoPremium', 'lastUpdate', 'offDate'));
    }

    public function updateAutoPremium(Request $request)
    {
        $request->validate([
            'is_auto_premium' => 'nullable|boolean',
            'auto_premium_paid_video' => 'nullable|boolean',
            'auto_premium_days' => 'required_if:is_auto_premium,1|nullable|integer|min:1|max:999'
        ], [
            'auto_premium_days.required_if' => 'Кількість Днів "Авто-Преміум" є обов\'язковим для заповнення'
        ]);

        $isAutoPremium = Subscription::isAutoPremium();

        if ($request->boolean('is_auto_premium') && !$isAutoPremium) {
            Setting::storeSetting('auto_premium_date_start', Carbon::now());
        }

        Setting::storeSetting('is_auto_premium', $request->boolean('is_auto_premium'));
        Setting::storeSetting('auto_premium_paid_video', $request->boolean('auto_premium_paid_video'));

        Setting::storeSetting('auto_premium_days', $request->boolean('is_auto_premium')
            ? $request->get('auto_premium_days')
            : null);

        return redirect()->route('auto-premium.index')->with('success', 'Авто-Премiум успішно оновлено!');
    }
}
