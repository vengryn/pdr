<?php

namespace App\Http\Controllers;

use App\Http\Requests\Teachers\StoreRequest;
use App\Http\Requests\Teachers\UpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['teacher_filter' => []]);
            return redirect()->route('teachers.index');
        } else {
            $period = [
                $request->get('created_start', session('student_filter.created_start')),
                $request->get('created_end', session('student_filter.created_end'))
            ];

            $filters = $this->sessionStoreParams('teacher_filter', $request->only([
                    'name', 'email', 'promo_code', 'created_start', 'created_end'
                ]) + ['period' => $period]);
        }

        $teachers = User::teacher()->with([
            'infoTeacher',
            'promoCode'
        ])
            ->filterBy($filters)
            ->latest('users.created_at')
            ->paginate(request()->paginate ?? 25);

        return view('teachers.index', compact('teachers', 'filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Teachers\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $teacher = User::create($request->only(['email', 'password']));
        $teacher->infoTeacher()->create($request->only([
            'first_name',
            'last_name',
            'patronymic',
            'phone'
        ]));

        return $request->action == 'save_and_stay' ?
            redirect()->route('teachers.edit', $teacher)->with('success', 'Викладача успішно створено!') :
            redirect()->route('teachers.index')->with('success', 'Викладача успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\User $teacher
     * @return mixed
     */
    public function edit(User $teacher)
    {
        return view('teachers.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Teachers\UpdateRequest $request
     * @param \App\Models\User $teacher
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, User $teacher)
    {
        $teacher->update($request->only(['email', 'password']));
        $teacher->infoTeacher()->update($request->only([
            'first_name',
            'last_name',
            'patronymic',
            'phone'
        ]));

        return $request->action == 'save_and_stay' ?
            redirect()->route('teachers.edit', $teacher)->with('success', 'Викладача успішно оновлено!') :
            redirect()->route('teachers.index')->with('success', 'Викладача успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\User $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $teacher)
    {
        try {
            $teacher->delete();
            request()->session()->flash('success', 'Викладача успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Викладача не видалено!');
        }

        return response($teacher, 200);
    }

    public function getAutocompleteTeacher(Request $request)
    {
        $response = User::teacher()->with(['infoTeacher'])
            ->filterBy(['name' => $request->input('search')])->get()
            ->map(function ($item) {
                return $item->name = $item->infoTeacher->full_name ?? '';
            })->unique()->take(20);

        return response()->json($response);
    }

    public function getAutocompleteEmail(Request $request)
    {
        $response = User::teacher()
            ->filterBy(['email' => $request->input('search')])->get()
            ->pluck('email')->take(20);

        return response()->json($response);
    }

    public function getAutocompletePromoCode(Request $request)
    {
        $response = User::teacher()->with(['promoCode'])
            ->filterBy(['promo_code' => $request->input('search')])->get()
            ->pluck('promoCode.code')->take(20);

        return response()->json($response);
    }
}
