<?php

namespace App\Http\Controllers;

use App\Http\Requests\TextPages\StoreRequest;
use App\Http\Requests\TextPages\UpdateRequest;
use App\Models\Language;
use App\Models\TextPage;

class TextPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $textPages = TextPage::paginate(request()->paginate ?? 25);
        $languages = Language::all();

        return view('text-pages.index', compact('textPages', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $languages = Language::all();

        return view('text-pages.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TextPages\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $textPage = TextPage::create($request->only('slug'));
        $this->saveTranslates($textPage, $request->translate ?? []);

        foreach ($request->get('markdownImages', []) as $img) {
            $textPage->markdownImages()->create(['path' => $img]);
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('text-pages.edit', $textPage)->with('success', 'Сторінку успішно створено!') :
            redirect()->route('text-pages.index')->with('success', 'Сторінку успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TextPage  $textPage
     * @return mixed
     */
    public function edit(TextPage $textPage)
    {
        $languages = Language::all();

        return view('text-pages.edit', compact('textPage', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TextPages\UpdateRequest  $request
     * @param  \App\Models\TextPage  $textPage
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, TextPage $textPage)
    {
        $this->saveTranslates($textPage, $request->translate ?? []);

        foreach ($request->get('markdownImages', []) as $img) {
            $textPage->markdownImages()->create(['path' => $img]);
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('text-pages.edit', $textPage)->with('success', 'Сторінку успішно оновлено!') :
            redirect()->route('text-pages.index')->with('success', 'Сторінку успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TextPage  $textPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(TextPage $textPage)
    {
        try {
            $textPage->delete();
            request()->session()->flash('success', 'Сторінку успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Сторінку не видалено!');
        }

        return response($textPage, 200);
    }
}
