<?php

namespace App\Http\Controllers;

use App\Http\Requests\TopicRoadMarkings\StoreRequest;
use App\Http\Requests\TopicRoadMarkings\UpdateRequest;
use App\Models\Language;
use App\Models\TopicRoadMarking;
use App\Models\Translate\TranslateRoadMarkingDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TopicRoadMarkingController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'road-markings-route';

    protected $languages;
    protected $default_language;
    protected $redirect_route;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->default_language = $language->default()->first();

        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('theory.road-markings.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $topicRoadMarkings = TopicRoadMarking::with(['translates'])->get();
        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());

        return view('handbook.road-markings.index', [
            'topicRoadMarkings' => $topicRoadMarkings,
            'languages' => $this->languages,
            'defaultLanguage' => $this->default_language
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('handbook.road-markings.create', [
            'languages' => $this->languages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TopicRoadMarkings\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $topicRoadMarking = TopicRoadMarking::create($request->only(['number']));
        $this->saveTranslates($topicRoadMarking, $request->translate ?? []);

        if ($request->hasFile('image')) {
            $topicRoadMarking->image = $request->image->storePublicly('public/road-markings/image');
            $topicRoadMarking->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.road-markings.edit', $topicRoadMarking)->with('success', 'Тип розмітки успішно створено!') :
            redirect()->route('theory.road-markings.index')->with('success', 'Тип розмітки успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TopicRoadMarking  $topicRoadMarking
     * @return mixed
     */
    public function edit(TopicRoadMarking $topicRoadMarking)
    {
        return view('handbook.road-markings.edit', [
            'topicRoadMarking' => $topicRoadMarking,
            'languages' => $this->languages,
            'redirectRoute' => $this->redirect_route
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TopicRoadMarkings\UpdateRequest  $request
     * @param  \App\Models\TopicRoadMarking  $topicRoadMarking
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, TopicRoadMarking $topicRoadMarking)
    {
        $topicRoadMarking->update($request->only(['number']));
        $this->saveTranslates($topicRoadMarking, $request->translate ?? []);

        if ($request->hasFile('image')) {
            Storage::delete($topicRoadMarking->image);

            $topicRoadMarking->image = $request->image->storePublicly('public/road-markings/image');
            $topicRoadMarking->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.road-markings.edit', $topicRoadMarking)->with('success', 'Тип розмітки успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Тип розмітки успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TopicRoadMarking  $topicRoadMarking
     * @return \Illuminate\Http\Response
     */
    public function destroy(TopicRoadMarking $topicRoadMarking)
    {
        if ($topicRoadMarking->marking->isNotEmpty()) {
            request()->session()->flash('error', 'Тип розмітки видалити неможливо, тому що є розмітки, що прив\'язані до цього типу.');
            abort(403);
        }

        try {
            $topicRoadMarking->delete();
            request()->session()->flash('success', 'Тип розмітки успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Тип розмітки не видалено!');
        }

        return response($topicRoadMarking, 200);
    }

    /**
     * @return mixed
     */
    public function editDescription() {
        $data = TranslateRoadMarkingDescription::select('language_id', 'description')->get()
            ->keyBy('language_id')
            ->toArray();

        return view('handbook.road-markings.description', [
            'languages' => $this->languages,
            'data' => $data
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateDescription(Request $request) {
        foreach ($request->get('translate') as $filed => $translates) {
            foreach ($translates as $language => $translate) {
                TranslateRoadMarkingDescription::updateOrCreate(['language_id' => $language], [
                    $filed => $translate
                ]);
            }
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.road-markings.editDescription')->with('success', 'Опис успішно оновлено!') :
            redirect()->route('theory.road-markings.index')->with('success', 'Опис успішно оновлено!');
    }
}
