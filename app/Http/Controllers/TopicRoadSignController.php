<?php

namespace App\Http\Controllers;

use App\Http\Requests\TopicRoadSigns\StoreRequest;
use App\Http\Requests\TopicRoadSigns\UpdateRequest;
use App\Models\Language;
use App\Models\TopicRoadSign;
use App\Models\Translate\TranslateRoadSignDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TopicRoadSignController extends Controller
{
    const REDIRECT_ROUTE_NAME = 'road-signs-route';

    protected $languages;
    protected $defaultLanguage;
    protected $redirect_route;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->defaultLanguage = $language->default()->first();

        $this->middleware(function ($request, $next) {
            $this->redirect_route = session()->get(self::REDIRECT_ROUTE_NAME) ??
                route('theory.road-signs.index');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $topicRoadSigns = TopicRoadSign::with(['translates'])->paginate(request()->paginate ?? 25);
        $this->saveSessionRedirectUrl(self::REDIRECT_ROUTE_NAME, url()->full());

        return view('handbook.road-signs.index', [
            'topicRoadSigns' => $topicRoadSigns,
            'languages' => $this->languages,
            'defaultLanguage' => $this->defaultLanguage
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('handbook.road-signs.create', [
            'languages' => $this->languages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TopicRoadSigns\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $topicRoadSigns = TopicRoadSign::create($request->only(['number']));
        $this->saveTranslates($topicRoadSigns, $request->translate ?? []);

        if ($request->hasFile('image')) {
            $topicRoadSigns->image = $request->image->storePublicly('public/road-signs/image');
            $topicRoadSigns->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.road-signs.edit', $topicRoadSigns)->with('success', 'Категорію успішно створено!') :
            redirect()->route('theory.road-signs.index')->with('success', 'Категорію успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TopicRoadSign  $topicRoadSign
     * @return mixed
     */
    public function edit(TopicRoadSign $topicRoadSign)
    {
        return view('handbook.road-signs.edit', [
            'topicRoadSign' => $topicRoadSign,
            'languages' => $this->languages,
            'redirectRoute' => $this->redirect_route
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TopicRoadSigns\UpdateRequest  $request
     * @param  \App\Models\TopicRoadSign  $topicRoadSign
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, TopicRoadSign $topicRoadSign)
    {
        $topicRoadSign->update($request->only(['number']));
        $this->saveTranslates($topicRoadSign, $request->translate ?? []);

        if ($request->hasFile('image')) {
            Storage::delete($topicRoadSign->image);

            $topicRoadSign->image = $request->image->storePublicly('public/road-signs/image');
            $topicRoadSign->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.road-signs.edit', $topicRoadSign)->with('success', 'Категорію успішно оновлено!') :
            redirect($this->redirect_route)->with('success', 'Категорію успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TopicRoadSign  $topicRoadSign
     * @return \Illuminate\Http\Response
     */
    public function destroy(TopicRoadSign $topicRoadSign)
    {
        if ($topicRoadSign->signs->isNotEmpty()) {
            request()->session()->flash('error', 'Категорію видалити неможливо, тому що є знаки, що прив\'язані до цієї категорії.');
            abort(403);
        }

        try {
            $topicRoadSign->delete();
            request()->session()->flash('success', 'Категорію успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Категорію не видалено!');
        }

        return response($topicRoadSign, 200);
    }

    /**
     * @return mixed
     */
    public function editDescription() {
        $data = TranslateRoadSignDescription::select('language_id', 'description')->get()
            ->keyBy('language_id')
            ->toArray();

        return view('handbook.road-signs.description', [
            'languages' => $this->languages,
            'data' => $data
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateDescription(Request $request) {
        foreach ($request->get('translate') as $filed => $translates) {
            foreach ($translates as $language => $translate) {
                TranslateRoadSignDescription::updateOrCreate(['language_id' => $language], [
                    $filed => $translate
                ]);
            }
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('theory.road-signs.editDescription')->with('success', 'Опис успішно оновлено!') :
            redirect()->route('theory.road-signs.index')->with('success', 'Опис успішно оновлено!');
    }
}
