<?php

namespace App\Http\Controllers;

use App\Models\CategoryDriverLicense;
use App\Models\Folder;
use App\Models\Language;
use App\Models\TopicTrafficRule;
use App\Http\Requests\TopicTrafficRules\StoreRequest;
use App\Http\Requests\TopicTrafficRules\UpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TopicTrafficRuleController extends Controller
{
    protected $languages;
    protected $defaultLanguage;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
        $this->defaultLanguage = $language->default()->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['topic_traffic_rule_filter' => []]);
            return redirect()->route('handbook.theme-traffic-rules.index');
        } else {
            $filters = $this->sessionStoreParams('topic_traffic_rule_filter', $request->only([
                'folder', 'name', 'external_id', 'number'
                ]) + [
                    'kind' => $request->get('kind', false)
                ]);
        }

        $topicTrafficRules = TopicTrafficRule::with(['translates', 'categoryDriverLicenses', 'folders'])
            ->withCount('questions')
            ->filterBy($filters)
            ->orderBy('number')
            ->paginate(request()->paginate ?? 25);

        $folders = Folder::all();

        return view('handbook.theme-traffic-rules.index', [
            'topicTrafficRules' => $topicTrafficRules,
            'languages' => $this->languages,
            'defaultLanguage' => $this->defaultLanguage,
            'folders' => $folders,
            'filters' => $filters
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $categoryDriverLicenses = CategoryDriverLicense::all();
        $folders = Folder::all();

        return view('handbook.theme-traffic-rules.create', [
            'categoryDriverLicenses' => $categoryDriverLicenses,
            'folders' => $folders,
            'languages' => $this->languages,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\TopicTrafficRules\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $topicTrafficRule = TopicTrafficRule::create($request->validated());
        $topicTrafficRule->categoryDriverLicenses()->attach($request->get('category_driver_license_id'));
        $topicTrafficRule->folders()->attach($request->get('folder_ids'));

        $this->saveTranslates($topicTrafficRule, $request->translate ?? []);

        return $request->action == 'save_and_stay' ?
            redirect()->route('handbook.theme-traffic-rules.edit', $topicTrafficRule)->with('success', 'Тему успішно створено!') :
            redirect()->route('handbook.theme-traffic-rules.index')->with('success', 'Тему успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\TopicTrafficRule $topicTrafficRule
     * @return mixed
     */
    public function edit(TopicTrafficRule $topicTrafficRule)
    {
        $categoryDriverLicenses = CategoryDriverLicense::all();
        $folders = Folder::all();

        return view('handbook.theme-traffic-rules.edit', [
            'topicTrafficRule' => $topicTrafficRule,
            'categoryDriverLicenses' => $categoryDriverLicenses,
            'folders' => $folders,
            'languages' => $this->languages
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\TopicTrafficRules\UpdateRequest $request
     * @param \App\Models\TopicTrafficRule $topicTrafficRule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, TopicTrafficRule $topicTrafficRule)
    {
        $topicTrafficRule->update($request->validated());
        $this->saveTranslates($topicTrafficRule, $request->translate ?? []);

        if ($topicTrafficRule->categoryDriverLicenses->pluck('id')->toArray() != $request->get('category_driver_license_id')) {
            $topicTrafficRule->categoryDriverLicenses()->sync($request->get('category_driver_license_id'));
            $topicTrafficRule->updated_at = Carbon::now();
            $topicTrafficRule->save();
        }

        if ($topicTrafficRule->folders->pluck('id')->toArray() != $request->get('folder_ids')) {
            $topicTrafficRule->folders()->sync($request->get('folder_ids'));
            $topicTrafficRule->updated_at = Carbon::now();
            $topicTrafficRule->save();
        }

        return $request->action == 'save_and_stay' ?
            redirect()->route('handbook.theme-traffic-rules.edit', $topicTrafficRule)->with('success', 'Тему успішно оновлено!') :
            redirect()->route('handbook.theme-traffic-rules.index')->with('success', 'Тему успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\TopicTrafficRule $topicTrafficRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(TopicTrafficRule $topicTrafficRule)
    {
        try {
            $topicTrafficRule->delete();
            request()->session()->flash('success', 'Тему успішно видалено!');
        } catch (\Exception $e) {
            request()->session()->flash('error', 'Помилка. Тему не видалено!');
        }

        return response($topicTrafficRule, 200);
    }
}
