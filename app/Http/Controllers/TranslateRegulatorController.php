<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\MarkdownImage;
use App\Models\Translate\TranslateRegulator;
use Illuminate\Http\Request;

class TranslateRegulatorController extends Controller
{
    const morphName = 'App\Models\TranslateRegulator';

    protected $languages;

    /**
     * Create the controller instance.
     *
     * @param Language $language
     * @return void
     */
    public function __construct(Language $language)
    {
        $this->languages = $language->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $data = TranslateRegulator::select('language_id', 'description')->get()
            ->keyBy('language_id')
            ->toArray();

        $markdownImages = MarkdownImage::where('imagetable_type', self::morphName)->get();

        return view('handbook.regulator.index', [
            'languages' => $this->languages,
            'data' => $data,
            'markdownImages' => $markdownImages
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $request->validate([
            'translate.description.*' => 'nullable|max:65535'
        ], [], ['translate.description.*' => 'Опис']);

        foreach ($request->get('translate') as $filed => $translates) {
            foreach ($translates as $language => $translate) {
                TranslateRegulator::updateOrCreate(['language_id' => $language], [
                    $filed => $translate
                ]);
            }
        }

        foreach ($request->get('markdownImages', []) as $img) {
            $markdownImage = new MarkdownImage();
            $markdownImage->imagetable_type = self::morphName;
            $markdownImage->imagetable_id = 0;
            $markdownImage->path = $img;
            $markdownImage->save();
        }

        return redirect()->route('theory.regulator.index')->with('success', 'Опис успішно оновлено!');
    }
}
