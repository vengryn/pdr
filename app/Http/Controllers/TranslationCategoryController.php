<?php

namespace App\Http\Controllers;

use App\Http\Requests\TranslationCategories\StoreRequest;
use App\Http\Requests\TranslationCategories\UpdateRequest;
use App\Models\TranslationCategory;
use Illuminate\Support\Facades\Gate;

class TranslationCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $translationCategories = TranslationCategory::paginate(request()->paginate ?? 25);

        return view('translation-categories.index', compact('translationCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        Gate::authorize('superAdmin');
        return view('translation-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TranslationCategories\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        Gate::authorize('superAdmin');
        $translationCategory = TranslationCategory::create($request->validated());

        return $request->action == 'save_and_stay' ?
            redirect()->route('translation-categories.edit', $translationCategory)->with('success', 'Категорію перекладу успішно створено!') :
            redirect()->route('translation-categories.index')->with('success', 'Категорію перекладу успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TranslationCategory  $translationCategory
     * @return mixed
     */
    public function edit(TranslationCategory $translationCategory)
    {
        return view('translation-categories.edit', compact('translationCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TranslationCategories\UpdateRequest  $request
     * @param  \App\Models\TranslationCategory  $translationCategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, TranslationCategory $translationCategory)
    {
        $translationCategory->update($request->validated());

        return $request->action == 'save_and_stay' ?
            redirect()->route('translation-categories.edit', $translationCategory)->with('success', 'Категорію перекладу успішно оновлено!') :
            redirect()->route('translation-categories.index')->with('success', 'Категорію перекладу успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TranslationCategory  $translationCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(TranslationCategory $translationCategory)
    {
        try {
            $translationCategory->delete();
            request()->session()->flash('success', 'Категорію перекладу успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Категорію перекладу не видалено!');
        }

        return response($translationCategory, 200);
    }
}
