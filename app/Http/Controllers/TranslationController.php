<?php

namespace App\Http\Controllers;

use App\Http\Requests\Translations\StoreRequest;
use App\Http\Requests\Translations\UpdateRequest;
use App\Models\Language;
use App\Models\Translation;
use App\Models\TranslationCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->action == 'clear_filter') {
            session(['translation_filter' => []]);
            return redirect()->route('translations.index');
        } else {
            $filters = $this->sessionStoreParams('translation_filter', $request->only(['category', 'key', 'value']));
        }

        $translations = Translation::with(['translates', 'translationCategory'])
            ->filterBy($filters)
            ->orderBy('key')
            ->paginate(request()->paginate ?? 25);
        $translationCategories = TranslationCategory::all();
        $languages = Language::all();

        return view('translations.index', compact('translations', 'translationCategories', 'filters', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        Gate::authorize('superAdmin');
        $languages = Language::all();
        $translationCategories = TranslationCategory::all();

        return view('translations.create', compact('languages', 'translationCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Translations\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        Gate::authorize('superAdmin');
        $translation = Translation::create($request->validated());
        $this->saveTranslates($translation, $request->translate ?? []);

        Translation::syncTranslationFiles();

        return $request->action == 'save_and_stay' ?
            redirect()->route('translations.edit', $translation)->with('success', 'Переклад успішно створено!') :
            redirect()->route('translations.index')->with('success', 'Переклад успішно створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Translation  $translation
     * @return mixed
     */
    public function edit(Translation $translation)
    {
        $languages = Language::all();
        $translationCategories = TranslationCategory::all();

        return view('translations.edit', compact('translation', 'languages', 'translationCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Translations\UpdateRequest  $request
     * @param  \App\Models\Translation  $translation
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Translation $translation)
    {
        $translation->update($request->validated());
        $this->saveTranslates($translation, $request->translate ?? []);

        Translation::syncTranslationFiles();

        return $request->action == 'save_and_stay' ?
            redirect()->route('translations.edit', $translation)->with('success', 'Переклад успішно оновлено!') :
            redirect()->route('translations.index')->with('success', 'Переклад успішно оновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Translation $translation)
    {
        try {
            $translation->delete();
            Translation::syncTranslationFiles();
            request()->session()->flash('success', 'Переклад успішно видалено!');
        } catch(\Exception $e) {
            request()->session()->flash('error', 'Помилка. Переклад не видалено!');
        }

        return response($translation, 200);
    }
}
