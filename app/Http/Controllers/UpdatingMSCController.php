<?php

namespace App\Http\Controllers;

use App\Exports\MSCQuestionUpdatesDifferentExport;
use App\Models\Question;
use App\Models\Setting;
use App\Services\MSCSync;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Excel;

class UpdatingMSCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $isUpdating = Setting::getSetting('access_to_update_questions');
        $lastDateUpdating = Setting::getSetting('date_of_last_update_questions');

        return view('updating-msc.index', compact('isUpdating', 'lastDateUpdating'));
    }

    public function update(Request $request)
    {
        Setting::storeSetting('access_to_update_questions', ($request->get('updating')));

        return response([], 200);
    }

    public function sync()
    {
        $MSCSync = new MSCSync();
        $MSCSync->run();

        return redirect()->route('updating-msc.index');
    }

    public function diffSync()
    {
        if (Storage::exists('updates_different.json')) {
            $defaultLanguageId = DB::table('languages')
                ->where('is_default', 1)
                ->whereNull('deleted_at')
                ->value('id');

            $json = Storage::get('updates_different.json');
            $diffQuestions = json_decode($json, true);

            $questions = DB::table('questions')
                ->whereIn('questions.id', array_keys($diffQuestions ?? []))
                ->whereNull('questions.deleted_at')
                ->leftJoin('translate_questions', function ($join) use ($defaultLanguageId) {
                    $join->on('questions.id', '=', 'translate_questions.question_id')
                        ->where('translate_questions.language_id', $defaultLanguageId)
                        ->whereNull('translate_questions.deleted_at')
                        ->limit(1);
                })
                ->select('questions.id', 'translate_questions.name')
                ->get()
                ->each(function ($item) use ($diffQuestions) {
                    return $item->changes = $diffQuestions[$item->id];
                })
                ->toArray();

            foreach ($questions as $key => $question) {
                $questions[$key]->questions_answers = DB::table('questions_answers')
                    ->where('questions_answers.question_id', $question->id)
                    ->whereNull('questions_answers.deleted_at')
                    ->leftJoin('translate_questions_answers', function ($join) use ($defaultLanguageId) {
                        $join->on('questions_answers.id', '=', 'translate_questions_answers.answer_id')
                            ->where('translate_questions_answers.language_id', $defaultLanguageId)
                            ->whereNull('translate_questions_answers.deleted_at')
                            ->limit(1);
                    })
                    ->select('questions_answers.id', 'questions_answers.external_id',
                        'questions_answers.is_correct', 'translate_questions_answers.name')
                    ->get()
                    ->toArray();
            }

            $lastDateUpdating = Setting::getSetting('date_of_last_update_questions');
            $lastDateUpdatingFormat = $lastDateUpdating ? \Carbon\Carbon::parse($lastDateUpdating)->format('d-m-Y_H:i:s') : '';

            return Excel::download(new MSCQuestionUpdatesDifferentExport($questions),
                "LastUpdateDifference_{$lastDateUpdatingFormat}.csv");
        }

        request()->session()->flash('error', 'Немає даних для генерування файлу!');
        return redirect()->route('updating-msc.index');
    }
}
