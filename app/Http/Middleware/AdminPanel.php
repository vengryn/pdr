<?php

namespace App\Http\Middleware;

use App\Models\TopicTrafficRule;
use Closure;
use Illuminate\Http\Request;

class AdminPanel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        if (TopicTrafficRule::doesntHave('folders')->count() > 0) {
            $request->session()->put('themes_without_folder', true);
        } elseif ($request->session()->has('themes_without_folder')) {
            $request->session()->forget('themes_without_folder');
        }

        return $next($request);
    }
}
