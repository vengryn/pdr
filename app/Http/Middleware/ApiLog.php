<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log as Logging;

class ApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->start = microtime(true);

        if (env('LOG_API_SQL')) {
            Logging::channel('api-sql')->info($request->getPathInfo());

            DB::listen(function ($query) {
                Logging::channel('api-sql')->info($query->sql);
            });
        }

        return $next($request);
    }

    public function terminate($request, $response)
    {
        $request->end = microtime(true);
        $this->log($request, $response);
    }

    protected function log($request, $response)
    {
        if (env('LOG_API_REQUESTS')) {
            $duration = round($request->end - $request->start, 4);
            $url = $request->getPathInfo();
            $method = $request->getMethod();
            $ip = $request->getClientIp();
            $log = "{$ip}: {$method}@{$url} - {$duration}s";

            Logging::channel('api-requests')->info($log);
        }
    }
}
