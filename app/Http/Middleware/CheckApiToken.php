<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $token = date('d.m.Y') . config('app.control_phrase');

        if (md5($token) != $request->header('token', $request->get('token'))) {
            return response()->json(['status' => 'fail', 'message' => 'Forbidden'], JsonResponse::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
