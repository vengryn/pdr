<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckChatBotApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $token = Setting::getSetting('chat_bot')['api_key'] ?? null;

        if ($token != $request->header('token', $request->get('token'))) {
            return response()->json(['status' => 'fail', 'message' => 'Forbidden'], JsonResponse::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
