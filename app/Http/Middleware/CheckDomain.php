<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckDomain
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        if (!in_array($request->getHost(), [
            env('APP_DOMAIN_NAME'),
            'admin-panel.' . env('APP_DOMAIN_NAME'),
            'teacher.' . env('APP_DOMAIN_NAME')
        ])) {
            abort(404);
        }

        if ($user = Auth::user()) {

            if ($request->getHost() == 'teacher.' . env('APP_DOMAIN_NAME') && $user->role == User::ROLE_TEACHER) {
                return $next($request);
            }

            if ($request->getHost() == 'admin-panel.' . env('APP_DOMAIN_NAME') && in_array($user->role, [
                    User::ROLE_SUPER_ADMINISTRATOR,
                    User::ROLE_ADMINISTRATOR,
                    User::ROLE_PAYMENT_REPORT,
                ])) {
                return $next($request);
            }

            Auth::logout();
            abort(403);
        }

        return $next($request);
    }
}
