<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!JWTAuth::parseToken()->authenticate()) {
                throw new AuthenticationException('Unauthenticated.', ['api']);
            }
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException) {
                throw new AuthenticationException('Unauthenticated.', ['api'], null);
            } else if ($e instanceof TokenExpiredException) {
                try {
                    $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                    JWTAuth::setToken($refreshed)->toUser();

                    $request->headers->set('Authorization', 'Bearer ' . $refreshed);

                    $response = $next($request);
                    $response->headers->set('Access-Token', JWTAuth::fromUser(Auth::user()));
                    $response->headers->set('Token-Type', 'Bearer');
                    $response->headers->set('Expires-In', Carbon::now()->addMinutes(auth('api')->factory()->getTTL())->timestamp);
                    $response->headers->set('Expires-Refresh-In', Carbon::now()->addMinutes(config('jwt.refresh_ttl'))->timestamp);

                    return $response;
                } catch (JWTException $e) {
                    throw new AuthenticationException('Unauthenticated.', ['api'], null);
                }
            } else {
                throw new AuthenticationException('Unauthenticated.', ['api'], null);
            }
        }

        return $next($request);
    }
}
