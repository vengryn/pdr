<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log as Logging;

class WebLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->start = microtime(true);

        return $next($request);
    }

    public function terminate($request, $response)
    {
        $request->end = microtime(true);
        $this->log($request, $response);
    }

    protected function log($request, $response)
    {
        if (env('LOG_WEB_REQUESTS')) {
            $duration = round($request->end - $request->start, 4);
            $url = $request->getPathInfo();
            $method = $request->getMethod();
            $ip = $request->getClientIp();
            $log = "{$ip}: {$method}@{$url} - {$duration}s";

            Logging::channel('web-requests')->info($log);
        }
    }
}
