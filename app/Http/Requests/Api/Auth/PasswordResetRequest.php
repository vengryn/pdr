<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\FormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;

class PasswordResetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::exists('users')->where(function ($query) {
                    return $query->where('role', User::ROLE_STUDENT);
                }),
            ],
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => trans('validation.exists_email'),
        ];
    }
}
