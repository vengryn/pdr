<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|regex:/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i|max:255|unique:users',
            'password' => 'required|string|min:6',
            'promo_code' => 'nullable|string|max:255',
            'is_user_agreement' => 'required|in:true,1'
        ];
    }

    public function messages()
    {
        return [
            'is_user_agreement.*' => 'Користувацька Угода не прийнята',
        ];
    }
}
