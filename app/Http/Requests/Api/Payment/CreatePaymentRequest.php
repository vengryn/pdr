<?php

namespace App\Http\Requests\Api\Payment;

use App\Http\Requests\Api\FormRequest;

class CreatePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscription_id' => 'required|integer|exists:subscriptions,id,deleted_at,NULL',
            'promo_code' => 'nullable|string|max:255',
        ];
    }
}
