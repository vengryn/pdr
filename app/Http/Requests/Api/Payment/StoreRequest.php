<?php

namespace App\Http\Requests\Api\Payment;

use App\Http\Requests\Api\FormRequest;
use App\Models\Payment;
use App\Rules\Timestamp;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $marketplaces = Payment::getMarketplaceList();

        return [
            'product_id' => 'required|string|exists:subscriptions,product_id,deleted_at,NULL',
            'promo_code' => 'nullable|string|max:255',
            'discount' => 'nullable|integer',
            'marketplace' => 'required|in:' . implode(',', array_keys($marketplaces)),
            'amount' => 'required|numeric',
            'transaction_at' => ['required', new Timestamp],
            'purchase_id' => 'required|string|max:255',
            'debug' => 'nullable|boolean'
        ];
    }
}
