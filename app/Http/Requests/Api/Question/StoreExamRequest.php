<?php

namespace App\Http\Requests\Api\Question;

use App\Http\Requests\Api\FormRequest;

class StoreExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total_answers' => 'required|integer',
            'correct_answers' => 'required|integer',
            'wrong_answers' => 'required|integer',
            'is_passed' => 'required|boolean',
            'time' => 'required|integer',
            'client_created_at' => 'nullable|integer',
        ];
    }
}
