<?php

namespace App\Http\Requests\Api\Question;

use App\Http\Requests\Api\FormRequest;

class StoreQuestionAnswerAllHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_answers_history' => 'nullable|array',
            'exams' => 'nullable|array',
            'wrong_question_answers' => 'nullable|array'
        ];
    }
}
