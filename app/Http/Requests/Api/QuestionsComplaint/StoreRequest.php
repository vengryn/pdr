<?php

namespace App\Http\Requests\Api\QuestionsComplaint;

use App\Http\Requests\Api\FormRequest;
use App\Models\Payment;
use App\Rules\Timestamp;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id' => 'required|exists:questions,id,deleted_at,NULL',
            'comment' => 'required|string|max:1000'
        ];
    }
}
