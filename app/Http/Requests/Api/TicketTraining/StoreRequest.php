<?php

namespace App\Http\Requests\Api\TicketTraining;

use App\Http\Requests\Api\FormRequest;
use App\Models\Payment;
use App\Rules\Timestamp;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'success_number_attempts' => 'sometimes|required|integer|min:0|digits_between:0,9',
            'passed_number_attempts' => 'sometimes|required|integer|min:0|digits_between:0,9',
            'correct_answers' => 'sometimes|required|integer|min:0|digits_between:0,9',
            'wrong_answers' => 'sometimes|required|integer|min:0|digits_between:0,9'
        ];
    }
}
