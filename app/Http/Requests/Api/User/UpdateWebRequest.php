<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\FormRequest;

class UpdateWebRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'sometimes|required|regex:/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i|unique:users,email,' . auth()->user()->id,
            'first_name' => 'sometimes|required|string|max:255',
            'last_name' => 'sometimes|required|string|max:255',
            'phone' => 'sometimes|required|string|max:255|regex:/^\+38 \([0-9]{3}\) ([0-9]{3})-([0-9]{2})-([0-9]{2})$/',
            'avatar' => 'nullable|string',
            'password' => 'nullable|string|max:255|min:6',
            'group_id' => 'sometimes|required|exists:groups,id',
            'categories' => 'sometimes|required|array|exists:category_driver_licenses,id'
        ];
    }
}
