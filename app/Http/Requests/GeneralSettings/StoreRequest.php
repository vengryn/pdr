<?php

namespace App\Http\Requests\GeneralSettings;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_email' => 'nullable|email',
            'privacy_policy_page' => 'nullable|exists:text_pages,id,deleted_at,NULL',
            'digitalocean' => 'array|nullable',
            'chat_bot' => 'array|nullable',
            'payments' => 'array|nullable',
            'liqpay' => 'array|nullable',
            'uapay' => 'array|nullable',
            'google_play_link' => 'nullable|url',
            'app_store_link' => 'nullable|url',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Налаштування не оновлено!');
    }

    public function attributes()
    {
        return [
            'contact_email' => 'Контактний емейл',
            'privacy_policy_page' => 'Сторінка Політики Конфеденційності',
        ];
    }
}
