<?php

namespace App\Http\Requests\Instructions;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text_page_id' => 'required|exists:text_pages,id,deleted_at,NULL|unique:instructions,text_page_id,NULL,id,deleted_at,NULL',
            'image' => "required|image|dimensions:width=25,height=25"
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Інструкцію не створено!');
    }

    public function attributes()
    {
        return [
            'text_page_id' => 'Інструкція',
            'image' => 'Зображення',
        ];
    }
}
