<?php

namespace App\Http\Requests\Instructions;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text_page_id' => "required|exists:text_pages,id,deleted_at,NULL|unique:instructions,text_page_id,{$this->instruction->id},id,deleted_at,NULL",
            'image' => "required_without_all:image_path|image|dimensions:width=25,height=25"
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Інструкцію не оновлено!');
    }

    public function messages()
    {
        return [
            'required_without_all' => 'Поле :attribute є обов\'язковим для заповнення.'
        ];
    }

    public function attributes()
    {
        return [
            'text_page_id' => 'Інструкція',
            'image' => 'Зображення',
        ];
    }
}
