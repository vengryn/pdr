<?php

namespace App\Http\Requests\InternalTopicTrafficRules;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'number' => "required|integer|gt:0|unique:internal_topic_traffic_rules,number,{$this->internal_topic_traffic_rule->id},id,deleted_at,NULL",
            "translate.name.{$defaultLanguage->id}" => 'required|string|max:255'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Тему не оновлено!');
    }

    public function messages()
    {
        return [
            'number.required' => 'Введіть номер теми',
            'translate.name.*.required' => 'Введіть назву теми'
        ];
    }

    public function attributes()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'number' => 'Тема №',
            "translate.name.{$defaultLanguage->id}" => 'Назва'
        ];
    }
}
