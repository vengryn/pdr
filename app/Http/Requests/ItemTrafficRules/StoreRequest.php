<?php

namespace App\Http\Requests\ItemTrafficRules;

use App\Models\InternalTopicTrafficRule;
use App\Models\Language;
use App\Rules\Number;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();
        $parentNumber = InternalTopicTrafficRule::find($this->get('internal_topic_traffic_rule_id'));

        return [
            'internal_topic_traffic_rule_id' => 'required|exists:internal_topic_traffic_rules,id,deleted_at,NULL',
            'number' => ['required', 'string', 'regex:/^\d+([.]\d+)*$/', "unique:item_traffic_rules,number,NULL,id,deleted_at,NULL",
                new Number($parentNumber->number ?? 0, $this->messages()['number.number'])],
            "translate.content.{$defaultLanguage->id}" => 'required|string|max:65535',
            "translate.content.*" => 'nullable|string|max:65535',
            "translate.description.{$defaultLanguage->id}" => 'required|string|max:65535',
            "translate.description.*" => 'nullable|string|max:65535'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Пункт не створено!');
    }

    public function messages()
    {
        return [
            'internal_topic_traffic_rule_id.required' => 'Виберіть тему',
            'number.required' => 'Введіть номер пункту',
            'number.number' => 'Невірний номер теми, до якої відноситься пункт правил дорожнього руху'
        ];
    }

    public function attributes()
    {
        return [
            'internal_topic_traffic_rule_id' => 'Тема',
            'number' => 'Пункт №',
            'translate.content.*' => 'Опис',
            'translate.description.*' => 'Короткий опис',
        ];
    }
}
