<?php

namespace App\Http\Requests\Languages;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => "required|string|max:255|unique:languages,title,{$this->language->id},id,deleted_at,NULL",
            'abbreviation' => "required|string|max:255|unique:languages,abbreviation,{$this->language->id},id,deleted_at,NULL",
            'flag' => 'required_without_all:flag_path|image'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Мову не оновлено!');
    }

    public function messages()
    {
        return [
            'required_without_all' => 'Поле :attribute є обов\'язковим для заповнення.'
        ];
    }
}
