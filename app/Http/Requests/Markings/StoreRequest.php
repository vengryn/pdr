<?php

namespace App\Http\Requests\Markings;

use App\Models\Language;
use App\Models\TopicRoadMarking;
use App\Rules\Number;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();
        $parentNumber = TopicRoadMarking::find($this->get('topic_road_marking_id'));

        return [
            'topic_road_marking_id' => 'required|exists:topic_road_markings,id,deleted_at,NULL',
            'number' => ['required', 'string', 'regex:/^\d+([.]\d+)(.*)*$/', "unique:markings,number,NULL,id,deleted_at,NULL",
                new Number($parentNumber->number ?? 0, $this->messages()['number.number'])],
            'image' => 'required|mimes:png|dimensions:width=200,height=200',
            'image_example' => 'nullable|image',
            'video_example' => 'nullable|url|max:255',
            "translate.name.{$defaultLanguage->id}" => 'required|string|max:255',
            "translate.name.*" => 'nullable|string|max:255',
            "translate.content.{$defaultLanguage->id}" => 'required|string|max:65535',
            "translate.content.*" => 'nullable|string|max:65535',
            "translate.description.{$defaultLanguage->id}" => 'required|string|max:65535',
            "translate.description.*" => 'nullable|string|max:65535'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Розмітку не створено!');
    }

    public function messages()
    {
        return [
            'translate.name.*.required' => 'Введіть назву розмітки',
            'number.number' => 'Невірний номер типу до якого відноситься дорожня розмітка'
        ];
    }

    public function attributes()
    {
        return [
            'topic_road_marking_id' => 'Тип',
            'number' => 'Розмітка №',
            'image' => 'Зображення',
            'image_example' => 'Фото-приклад',
            'video_example' => 'Відео-приклад',
            'translate.name.*' => 'Назва',
            'translate.content.*' => 'Опис',
            'translate.description.*' => 'Короткий опис',
        ];
    }
}
