<?php

namespace App\Http\Requests\PromoCodes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            'discount' => "nullable|integer|between:0,100",
            'cashback' => "nullable|integer|between:0,100",
        ];

        if ($this->promo_code->isTypeMarketing()) {
            $data['code'] = "required|string|max:25|unique:promo_codes,code,{$this->promo_code->id},id,deleted_at,NULL";
            $data['date_start'] = 'required|date';
            $data['date_end'] = 'required|date|after_or_equal:date_start';
        }

        return $data;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Промокод не оновлено!');
    }

    public function attributes()
    {
        return [
            'code' => 'Промокод',
            'discount' => 'Розмір знижки',
            'date_start' => 'Дата початку',
            'date_end' => 'Дата завершення',
        ];
    }

    public function messages()
    {
        return [
            'code.max' => 'Промокод має містити менше 25 символів',
            'discount.between' => 'Будь ласка, введіть розмір знижки від 0 до 100',
            'date_end.after_or_equal' => 'Дата завершення не може бути раніше дати початку',
        ];
    }
}
