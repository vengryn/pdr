<?php

namespace App\Http\Requests\Questions;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();
        $languages = Language::where('is_default', false)->get();

        $rules = [
            'status' => 'nullable|boolean',
            'picture' => 'image|max:5000',
            'topic_traffic_rule_id' => 'required|integer|exists:topic_traffic_rules,id',
            "translate.name.{$defaultLanguage->id}" => 'required',
            "added_answers.*.translate.name.{$defaultLanguage->id}" => 'required',
            'translate.explanation.*' => 'nullable|string|max:65535'
        ];


        foreach ($languages as $language) {
            $field = [];

            foreach ($this->get('added_answers', []) as $key => $addedAnswer) {
                $field[] = "added_answers.{$key}.translate.name.{$language->id}";
            }

            $field[] = "translate.explanation.{$language->id}";

            $rules = $rules + [
                    "added_answers.*.translate.name.{$language->id}" => "required_with:translate.name.{$language->id}," . implode(',', $field),
                    "translate.name.{$language->id}" => 'required_with:' . implode(',', $field),
                ];
        }

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Питання не створено!');
    }

    public function messages()
    {
        return [
            'external_number.required' => 'Введіть номер питання',
            'translate.name.*.required' => 'Введіть питання',
            'translate.name.*.required_with' => 'Введіть питання',
            'topic_traffic_rule_id.required' => 'Виберіть тему',
            'added_answers.*.translate.name.*.required' => 'Необхідно заповнити відповідь',
            'added_answers.*.translate.name.*.required_with' => 'Необхідно заповнити відповідь',
        ];
    }

    public function attributes()
    {
        return [
            'external_number' => 'Питання №',
            'status' => 'Статус',
            'picture' => 'Зображення',
            "translate.name.*" => 'Питання',
            'translate.explanation.*' => 'Пояснення'
        ];
    }
}
