<?php

namespace App\Http\Requests\SocialSupportServices;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:social_support_services,name,NULL,id,deleted_at,NULL',
            'link' => 'required|url|max:255|unique:social_support_services,link,NULL,id,deleted_at,NULL',
            'logo' => 'required|mimes:jpeg,jpg,png,gif|max:10|dimensions:width=50,height=50'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Службу підтримки не створено!');
    }
}
