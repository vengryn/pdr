<?php

namespace App\Http\Requests\SocialSupportServices;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|string|max:255|unique:social_support_services,name,{$this->social_support_service->id},id,deleted_at,NULL",
            'link' => "required|url|max:255|unique:social_support_services,link,{$this->social_support_service->id},id,deleted_at,NULL",
            'logo' => 'required_without_all:logo_path|mimes:jpeg,jpg,png,gif|max:10|dimensions:width=50,height=50'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Службу підтримки не оновлено!');
    }

    public function messages()
    {
        return [
            'required_without_all' => 'Поле :attribute є обов\'язковим для заповнення.'
        ];
    }
}
