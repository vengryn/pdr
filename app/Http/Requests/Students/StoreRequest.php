<?php

namespace App\Http\Requests\Students;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users|max:255',
            'phone' => 'nullable|string|max:255|regex:/^\+380\d{9}$/',
            'password' => 'required|string|max:255|min:6',
            'group_id' => [Rule::requiredIf(Gate::check('group-students')), 'nullable', 'exists:groups,id'],
            'category_driver_license_id' => 'required|array|exists:category_driver_licenses,id',
            'is_premium' => 'nullable|boolean',
            'subscription' => 'required_if:is_premium,1|nullable|exists:subscriptions,id',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Учня не створено!');
    }

    public function messages()
    {
        return [
            'subscription.required_if' => 'Поле :attribute є обов\'язковим для заповнення.'
        ];
    }

    public function attributes()
    {
        return [
            'is_premium' => 'Premium',
            'subscription' => 'Термін дії',
        ];
    }
}
