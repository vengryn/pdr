<?php

namespace App\Http\Requests\Subscriptions;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'product_id' => 'required|string|max:255|unique:subscriptions,product_id,NULL,id,deleted_at,NULL',
            "translate.name.{$defaultLanguage->id}" => 'required|string|max:255',
            'translate.name.*' => 'nullable|string|max:255',
            'validity' => 'required|integer|min:1|digits_between:1,9',
            'price' => 'required|numeric|min:0.1|regex:/^\d{1,6}(\.\d{2})?$/',
            'is_paid_video' => 'nullable|boolean',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Підписку не створено!');
    }

    public function attributes()
    {
        return [
            'product_id' => 'Product ID',
            'translate.name.*' => 'Назва підписки',
        ];
    }

    public function validated()
    {
        return $this->validator->validated() + [
                'is_paid_video' => $this->boolean('is_paid_video')
            ];
    }
}
