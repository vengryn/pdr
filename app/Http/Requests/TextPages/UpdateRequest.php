<?php

namespace App\Http\Requests\TextPages;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();

        return [
            "translate.name.{$defaultLanguage->id}" => 'required|string|max:255',
            "translate.name.*" => 'nullable|string|max:255',
            "translate.meta_title.*" => 'nullable|string|max:255',
            "translate.meta_description.*" => 'nullable|string|max:65535',
            "translate.meta_keywords.*" => 'nullable|string|max:65535',
            "translate.content.*" => 'nullable|max:4294967295',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Сторінку не оновлено!');
    }

    public function attributes()
    {
        return [
            "translate.name.*" => 'Назва',
            "translate.meta_title.*" => 'Meta:Заголовок',
            "translate.meta_description.*" => 'Meta:Опис',
            "translate.meta_keywords.*" => 'Meta:Ключові слова',
            "translate.content.*" => 'Вміст'
        ];
    }
}
