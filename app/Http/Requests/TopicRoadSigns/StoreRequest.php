<?php

namespace App\Http\Requests\TopicRoadSigns;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'number' => 'required|integer|unique:topic_road_signs,number,NULL,id,deleted_at,NULL',
            'image' => 'required|image|dimensions:width=200,height=200',
            "translate.name.{$defaultLanguage->id}" => 'required|string|max:255',
            "translate.name.*" => 'nullable|string|max:255',
            "translate.description.*" => 'nullable|string|max:65535',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Категорію не створено!');
    }

    public function messages()
    {
        return [
            'translate.name.*.required' => 'Введіть назву типу'
        ];
    }

    public function attributes()
    {
        return [
            'number' => 'Тип №',
            'image' => 'Зображення',
            "translate.name.*" => 'Назва',
            "translate.description.*" => 'Опис'
        ];
    }
}
