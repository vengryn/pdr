<?php

namespace App\Http\Requests\TopicTrafficRules;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'external_id' => 'required|integer|gt:0|unique:topic_traffic_rules,external_id,NULL,id,deleted_at,NULL',
            'number' => "required|integer|unique:topic_traffic_rules,number,NULL,id,deleted_at,NULL",
            "translate.name.{$defaultLanguage->id}" => 'required|string|max:255',
            'category_driver_license_id' => 'required|array|exists:category_driver_licenses,id',
            'folder_ids' => 'required|array|exists:folders,id'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Тему не створено!');
    }

    public function messages()
    {
        return [
            'external_id.required' => 'Введіть ID теми',
            'number.required' => 'Введіть номер теми',
            'translate.name.*.required' => 'Введіть назву теми',
            'category_driver_license_id.required' => 'Виберіть одну або кілька категорій',
            'folder_ids.required' => 'Виберіть одну або кілька папок'
        ];
    }

    public function attributes()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'external_id' => 'ID',
            'number' => 'Тема №',
            "translate.name.{$defaultLanguage->id}" => 'Назва',
            'folder_ids' => 'Папка'
        ];
    }
}
