<?php

namespace App\Http\Requests\Translations;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $defaultLanguage = Language::default()->first();

        return [
            'key' => 'required|regex:/^[0-9A-Za-z.\-_]+$/u|unique:translations,key,NULL,id,deleted_at,NULL',
            "translate.value.{$defaultLanguage->id}" => 'required|string|max:255',
            'translate.value.*' => 'nullable|string|max:255',
            'translation_category_id' => 'nullable|exists:translation_categories,id'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
            request()->session()->flash('error', 'Переклад не створено!');
    }

    public function attributes()
    {
        return [
            'key' => 'Ключ',
            'translate.value.*' => 'Текст',
        ];
    }

    public function messages()
    {
        return [
            "translate.value.*.required" => 'Введіть переклад',
        ];
    }
}
