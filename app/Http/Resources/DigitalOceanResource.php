<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DigitalOceanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'key' => $this['digitalocean']['key'],
            'key_secret' => $this['digitalocean']['key_secret'],
            'endpoint' => $this['digitalocean']['endpoint'],
            'region' => $this['digitalocean']['region'],
            'bucket' => $this['digitalocean']['bucket'],
            'question_file_path' => $this['digitalocean']['question_path'] . $this['date_of_last_response_file_generation'] . '.json',
            'theory_file_path' => $this['digitalocean']['theory_path'] . $this['date_of_last_response_theory_file_generation'] . '.json',
        ];
    }
}
