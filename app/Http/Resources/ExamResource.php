<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'total_answers' => $this->total_answers,
            'correct_answers' => $this->correct_answers,
            'wrong_answers' => $this->wrong_answers,
            'is_passed' => $this->is_passed,
            'time' => $this->time,
            'client_created_at' => $this->client_created_at,
            'created_at' => $this->created_at->timestamp
        ];
    }
}
