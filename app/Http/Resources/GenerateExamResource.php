<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class GenerateExamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $examQuestionAnswerHistories = $this->questions
            ->whereNotNull('pivot.questions_answer_id')
            ->whereNotNull('pivot.is_passed')
            ->pluck('id')
            ->toArray();

        return [
            'start_at' => $this->created_at->timestamp,
            'questions' => ExamQuestionResource::collection($this->questions),
            'question_answer_history' => GenerateQuestionAnswerHistoryResource::collection(Auth::user()->questionAnswerHistories()
                ->whereIn('question_id', $examQuestionAnswerHistories)->get())
        ];
    }
}
