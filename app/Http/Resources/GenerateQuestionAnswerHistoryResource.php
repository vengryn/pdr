<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GenerateQuestionAnswerHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'question_id' => $this->question_id,
            'answer_id' => $this->questions_answer_id,
            'correct_answer_id' => $this->correct_questions_answer_id,
            'is_passed' => $this->is_passed,
            'created_at' => $this->created_at->timestamp
        ];
    }
}
