<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class InstructionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => TranslatesResource::make($this->textPage->translates ?? null)->field('name'),
            'image' => $this->image
                ? base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $this->image))
                : null,
            'content' => TranslatesResource::make($this->textPage->translates ?? null)->field('content'),
            'markdown_images' => MarkDownResource::collection($this->textPage->markdownImages)
        ];
    }
}
