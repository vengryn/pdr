<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class InstructionWebResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => TranslatesResource::make($this->textPage->translates ?? null)->field('name'),
            'image' => $this->image ? env('APP_URL') . Storage::url($this->image) : null,
            'content' => TranslatesResource::make($this->textPage->translates ?? null)->field('content')
                ->isReplacePathMarkdownImage(true)->isOriginal(true),
        ];
    }
}
