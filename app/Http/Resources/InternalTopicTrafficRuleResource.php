<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InternalTopicTrafficRuleResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => (string) $this->number,
            'name' => TranslatesResource::make($this->translates)->field('name'),
            'item_traffic_rules' => ItemTrafficRuleResource::collection($this->whenLoaded('itemTrafficRules'))
        ];
    }
}
