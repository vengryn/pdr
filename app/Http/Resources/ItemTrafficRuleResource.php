<?php

namespace App\Http\Resources;

use App\Helpers\UsefulFunctions;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemTrafficRuleResource extends JsonResource
{
    use UsefulFunctions;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $translates = $this->replaceTokenImage($this->translates, ['description', 'content']);

        return [
            'id' => $this->id,
            'topic_traffic_rule_id' => $this->internal_topic_traffic_rule_id,
            'number' => (string) $this->number,
            'description' => TranslatesResource::make($translates)->field('description')->isReplacePathMarkdownImage(true)->isCheckPremiumVideo(true),
            'content' => TranslatesResource::make($translates)->field('content')->isReplacePathMarkdownImage(true)->isCheckPremiumVideo(true),
        ];
    }
}
