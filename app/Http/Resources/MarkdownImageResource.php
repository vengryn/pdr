<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class MarkdownImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $imgPath = str_replace('/storage', 'public', $this->path);
        $dataImage = base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $imgPath));

        return [
            'name' => basename($this->path),
            'content' => $dataImage
        ];
    }
}
