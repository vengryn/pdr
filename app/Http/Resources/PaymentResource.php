<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subscription' => $this->subscription,
            'promo_code' => $this->promo_code,
            'discount' => $this->discount,
            'marketplace' => $this->marketplace,
            'amount' => $this->amount,
            'transaction_at' => $this->transaction_at->format('d-m-Y H:i:s'),
        ];
    }
}
