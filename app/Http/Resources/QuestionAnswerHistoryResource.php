<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionAnswerHistoryResource extends JsonResource
{
    protected $is_correct_answer;

    public function isCorrectAnswer($value = false)
    {
        $this->is_correct_answer = $value;

        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'question_id' => $this->question_id,
            'answer_id' => $this->questions_answer_id,
            'correct_answer_id' => $this->when($this->is_correct_answer, function () {
                return $this->correct_questions_answer_id;
            }),
            'is_passed' => $this->is_passed,
            'client_created_at' => $this->client_created_at,
            'created_at' => $this->created_at->timestamp
        ];
    }
}
