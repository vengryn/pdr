<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionExamAnswerHistoryResource extends JsonResource
{
    protected $exam;

    public function exam($value=null){
        $this->exam = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'exam_finished' => $this->exam ? true : false,
            'question_answer_history' => [
                'question_id' => $this->question_id,
                'answer_id' => (int) $this->questions_answer_id,
                'correct_answer_id' => $this->correct_questions_answer_id,
                'is_passed' => $this->is_passed,
            ],
            'exam' => $this->exam
        ];
    }
}
