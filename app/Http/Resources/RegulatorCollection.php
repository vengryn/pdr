<?php

namespace App\Http\Resources;

use App\Models\MarkdownImage;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RegulatorCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'description' => TranslatesResource::make($this->collection)
                    ->field('description')
                    ->isReplacePathMarkdownImage(true)
                    ->isCheckPremiumVideo(true),
            ],
        ];
    }
}
