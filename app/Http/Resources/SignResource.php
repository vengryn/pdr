<?php

namespace App\Http\Resources;

use App\Helpers\UsefulFunctions;
use App\Models\Sign;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class SignResource extends JsonResource
{
    use UsefulFunctions;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sign = new Sign();
        $translates = $this->replaceTokenImage($this->translates, ['description', 'content']);

        return [
            'id' => $this->id,
            'category_road_sign_id' => $this->topic_road_sign_id,
            'number' => (string) $this->number,
            'image' => [
                'original' => env('APP_URL') . Storage::url($sign->getImageDirectory('original') . $this->getRawOriginal('image')),
                'small' => env('APP_URL') . Storage::url($sign->getImageDirectory('26_26') . $this->getRawOriginal('image')),
            ],
            'name' => TranslatesResource::make($this->translates)->field('name'),
            'description' => TranslatesResource::make($translates)->field('description')->isCheckPremiumVideo(true),
            'content' => TranslatesResource::make($translates)->field('content')->isCheckPremiumVideo(true),
            'image_example' => $this->image_example ? Storage::disk('digitalocean')->url($this->image_example) : null,
            'video_example' => $this->video_example,
        ];
    }
}
