<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class SocialSupportServiceWebResource extends JsonResource
{
    protected $isImagePath;

    public function isImagePath($value = false)
    {
        $this->isImagePath = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'link' => $this->link,
            'logo' => $this->logo ? env('APP_URL') . Storage::url($this->logo) : null
        ];

    }
}
