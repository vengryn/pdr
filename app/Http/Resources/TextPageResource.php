<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TextPageResource extends JsonResource
{
    protected $web;

    public function web($value = false)
    {
        $this->web = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'meta_title' => TranslatesResource::make($this->translates ?? null)->field('meta_title'),
            'meta_description' => TranslatesResource::make($this->translates ?? null)->field('meta_description'),
            'meta_keywords' => TranslatesResource::make($this->translates ?? null)->field('meta_keywords'),
            'name' => TranslatesResource::make($this->translates ?? null)->field('name'),
        ];

        if ($this->web) {
            $data = $data + [
                    'content' => TranslatesResource::make($this->translates ?? null)->field('content')
                        ->isOriginal(true)
                        ->isReplacePathMarkdownImage(true)
                ];
        } else {
            $data = $data + [
                    'content' => TranslatesResource::make($this->translates ?? null)
                        ->field('content')
                        ->isUnicodeSmile(true),
                    'markdown_images' => MarkDownResource::collection($this->markdownImages)
                ];
        }

        return $data;
    }
}
