<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketTrainingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'success_number_attempts' => $this->success_number_attempts,
            'passed_number_attempts' => $this->passed_number_attempts,
            'correct_answers' => $this->correct_answers,
            'wrong_answers' => $this->wrong_answers
        ];
    }
}
