<?php

namespace App\Http\Resources;

use App\Helpers\UsefulFunctions;
use App\Models\Translate\TranslateRoadMarkingDescription;

class TopicRoadMarkingCollection extends ResourceCollection
{
    use UsefulFunctions;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $translateRoadMarkingDescription = $this->replaceTokenImage(TranslateRoadMarkingDescription::get(), ['description']);

        return [
            'data' => $this->collection,
            'description' => RoadMarkingDescriptionCollection::make($translateRoadMarkingDescription),
            'pagination' => $this->pagination()
        ];
    }
}
