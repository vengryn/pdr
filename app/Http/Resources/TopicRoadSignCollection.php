<?php

namespace App\Http\Resources;

use App\Helpers\UsefulFunctions;
use App\Models\Translate\TranslateRoadSignDescription;

class TopicRoadSignCollection extends ResourceCollection
{
    use UsefulFunctions;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $translateRoadSignDescription = $this->replaceTokenImage(TranslateRoadSignDescription::get(), ['description']);

        return [
            'data' => $this->collection,
            'description' => RoadSignDescriptionCollection::make($translateRoadSignDescription),
            'pagination' => $this->pagination()
        ];
    }
}
