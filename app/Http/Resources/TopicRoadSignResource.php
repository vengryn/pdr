<?php

namespace App\Http\Resources;

use App\Helpers\UsefulFunctions;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TopicRoadSignResource extends JsonResource
{
    use UsefulFunctions;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $translates = $this->replaceTokenImage($this->translates, ['description']);

        return [
            'id' => $this->id,
            'number' => (string) $this->number,
            'image' => $this->image ? env('APP_URL') . Storage::url($this->image) : null,
            'name' => TranslatesResource::make($this->translates)->field('name'),
            'description' => TranslatesResource::make($translates)->field('description')->isCheckPremiumVideo(true),
            'signs' => SignResource::collection($this->whenLoaded('signs')->sortBy('order'))
        ];
    }
}
