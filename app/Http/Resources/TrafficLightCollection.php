<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TrafficLightCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'description' => TranslatesResource::make($this->collection)
                    ->field('description')
                    ->isReplacePathMarkdownImage(true)
                    ->isCheckPremiumVideo(true),
            ],
        ];
    }
}
