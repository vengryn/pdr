<?php

namespace App\Http\Resources;

use App\Models\Language;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class TranslatesResource extends JsonResource
{
    protected $field;

    protected $isReplacePathMarkdownImage;

    protected $isOriginal;

    protected $isCheckPremiumVideo;

    protected $isUnicodeSmile;

    public function field($value)
    {
        $this->field = $value;
        return $this;
    }

    public function isReplacePathMarkdownImage($value = false)
    {
        $this->isReplacePathMarkdownImage = $value;
        return $this;
    }

    public function isOriginal($value = false)
    {
        $this->isOriginal = $value;
        return $this;
    }

    public function isCheckPremiumVideo($value = false)
    {
        $this->isCheckPremiumVideo = $value;
        return $this;
    }

    public function isUnicodeSmile($value = false)
    {
        $this->isUnicodeSmile = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $languages = Cache::rememberForever('languages', function () {
            return Language::query()->get();
        });

        return $languages->each(function ($language) {
            return $language->abbreviation = lcfirst($language->abbreviation);
        })->keyBy('abbreviation')->map(function ($language) {
            if (!empty($this->resource)) {
                $translate = $this->where('language_id', $language->id)->first();
                $data = $translate
                    ? ($this->isOriginal ? ($translate->getRawOriginal($this->field)) ?? null : ($translate->{$this->field} ?? null))
                    : null;
            } else {
                $data = null;
            }

            if ($this->isReplacePathMarkdownImage) {
                $data = $this->replacePathMarkdownImage($data);
            }

            if ($this->isCheckPremiumVideo) {
                $data = $this->checkPremiumVideo($data);
            }

            if ($this->isUnicodeSmile) {
                $data = $this->replaceSmileToUnicode($data);
            }

            return $data;
        });
    }

    protected function replacePathMarkdownImage($text)
    {
        preg_match_all('/!\[(.*?)\]\((.*?)\)/', $text, $matches);
        $links = array_unique($matches[2]);

        foreach ($links as $data) {
            $text = str_replace($data, env('APP_URL') . $data, $text);
        }

        return $text;
    }

    protected function checkPremiumVideo($text)
    {
        if (!empty($text)) {
            preg_match_all('/#\((.*?)\|premium_video\)/', $text, $matches);
            $items = $matches[0];

            try {
                $user = Auth::user();

                if ($user) {
                    if ($userSubscription = $user->isUserActiveSubscription()) {
                        if ($userSubscription->subscription->is_paid_video) {
                            array_walk($matches[1], function(&$a) {
                                $a = '#('. $a .')';
                            });

                            return str_replace($items, $matches[1], $text);
                        }
                    }
                }
            } catch (\Exception $e) {
                //
            }

            return str_replace($items, '', $text);
        }

        return $text;
    }

    protected function replaceSmileToUnicode($text) {
        preg_match_all('/&#x(.*?);/', $text, $emodji);

        array_walk($emodji[1], function(&$a) {
            $a = '\u' . $a;
        });

        return str_replace($emodji[0], $emodji[1], $text);
    }
}
