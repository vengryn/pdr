<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserFrequentMistakeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $userQuestionAnswer = Auth::user()->questionAnswerHistories->where('question_id', $this->id)->first();

        return [
            'id' => $this->id,
            'name' => TranslatesResource::make($this->translates)->field('name'),
            'explanation' => TranslatesResource::make($this->translates)->field('explanation')
                ->isReplacePathMarkdownImage(true)
                ->isCheckPremiumVideo(true),
            'topic_traffic_rule_id' => $this->topic_traffic_rule_id,
            'count_wrong_answer' => $this->count_wrong_answer,
            'picture' => $this->picture ? env('APP_URL') . Storage::url($this->picture) : null,
            'questions_answers' => ExamQuestionAnswerResource::collection($this->questionsAnswers),
            'start_wrong_answer_at' => $this->start_wrong_answer_at ? $this->start_wrong_answer_at->timestamp : '',
            'user_answer' => $userQuestionAnswer ? $userQuestionAnswer->is_passed : null,
        ];
    }
}
