<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->infoStudent->first_name ?? null,
            'last_name' => $this->infoStudent->last_name ?? null,
            'patronymic' => $this->infoStudent->patronymic ?? null,
            'phone' => $this->infoStudent->phone ?? null,
            'avatar' => $this->infoStudent && $this->infoStudent->avatar ? env('APP_URL') . Storage::url($this->infoStudent->avatar) : null,
            'role' => $this->role,
            'provider_name' => $this->provider_name,
            'group' => GroupResource::make($this->group),
            'categories' => CategoryDriverLicenseResource::collection($this->categoryDriverLicenses),
            'subscription' => UserSubscriptionResource::make($this->isActiveSubscription())
        ];
    }
}
