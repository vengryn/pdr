<?php

namespace App\Http\Resources;

use App\Models\Language;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;

class UserSubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'start_at' => $this->start_at ? $this->start_at->timestamp : null,
            'end_at' => $this->end_at ? $this->end_at->timestamp : null,
            'capabilities' => array_filter([
                $this->is_paid_video ? 'premium_video' : null
            ])
        ];

        if ($this->is_auto_premium) {
            $languages = Cache::rememberForever('languages', function () {
                return Language::query()->get();
            });

            $data['product_id'] = 'ua.testpdr.mobile.trial';
            $data['name'] = $languages->each(function ($language) {
                return $language->abbreviation = lcfirst($language->abbreviation);
            })->keyBy('abbreviation')->map(function ($language) {
                return trans('translation.auto_premium_subscription', [], $language->abbreviation);
            });
        } else {
            $data['product_id'] = $this->subscription->product_id ?? null;
            $data['name'] = TranslatesResource::make($this->subscription->translates ?? [])->field('name');
        }

        return $data;
    }
}
