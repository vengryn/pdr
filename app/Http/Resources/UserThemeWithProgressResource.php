<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserThemeWithProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'external_id' => $this->external_id,
            'number' => $this->number,
            'name' => TranslatesResource::make($this->translates)->field('name'),
            'progress_theme' => $this->progress_theme,
            'count_questions' => $this->count_questions
        ];
    }
}
