<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserWithStatisticsResource extends JsonResource
{
    protected $isPromoCode;

    public function isPromoCode($value = false)
    {
        $this->isPromoCode = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->infoStudent->first_name ?? null,
            'last_name' => $this->infoStudent->last_name ?? null,
            'patronymic' => $this->infoStudent->patronymic ?? null,
            'phone' => $this->infoStudent->phone ?? null,
            'avatar' => $this->infoStudent->avatar ? env('APP_URL') . Storage::url($this->infoStudent->avatar) : null,
            'role' => $this->role,
            'provider_name' => $this->provider_name,
            'group' => GroupResource::make($this->group),
            'categories' => CategoryDriverLicenseResource::collection($this->categoryDriverLicenses),
            'subscription' => UserSubscriptionResource::make($this->isUserActiveSubscription()),
            'statistics' => $this->getStatistics()
        ];

        if ($this->isPromoCode) {
            $data = $data + [
                    'promo_code' => $this->promoCode->code ?? null,
                    'partner_link' => 'https://pdr.infotech.gov.ua/partner?userid='.$this->id.'&user_code=' . ($this->promoCode->code ?? null)
                ];
        }

        return $data;
    }
}
