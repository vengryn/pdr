<?php

namespace App\Jobs;

use App\Models\GenerateExam;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DisableUserExam implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $generateExamId;

    /**
     * Create a new job instance.
     *
     * @param int $generateExamId
     * @return void
     */
    public function __construct(int $generateExamId)
    {
        $this->generateExamId = $generateExamId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $generateExam = GenerateExam::find($this->generateExamId);

        if ($generateExam) {
            $generateExam->delete();
        }
    }
}
