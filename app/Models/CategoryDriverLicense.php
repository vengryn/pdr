<?php

namespace App\Models;

use App\Helpers\UsefulFunctions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryDriverLicense extends Model
{
    use HasFactory, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['position_x', 'position_y'];

    /**
     * The Topic Traffic Rules that belong to the Category Driver License.
     */
    public function topicTrafficRules()
    {
        return $this->belongsToMany(TopicTrafficRule::class);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::updating(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedQuestions();
        });
    }
}
