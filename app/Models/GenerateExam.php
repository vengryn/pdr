<?php

namespace App\Models;

use App\Jobs\DisableUserExam;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GenerateExam extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'is_training'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = ['is_training' => 'boolean',];

    /**
     * Check if is active User GenerateExam.
     *
     * @return boolean
     */
    public function getIsActiveAttribute()
    {
        $endDate = $this->is_training
            ? $this->created_at->addHours(24)
            : $this->created_at->addMinutes(20)->addSeconds(30);

        return $endDate->gt(Carbon::now());
    }

    /**
     * The questions that belong to the Generate Exam.
     */
    public function questions()
    {
        return $this->belongsToMany(Question::class)->withPivot(['number', 'questions_answer_id', 'is_passed']);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Generate and return User Exam with Questions.
     *
     * @param false $isTraining
     * @return mixed
     */
    public static function getUserExam($isTraining = false)
    {
        $user = Auth::user();

        if ($user->categoryDriverLicenses->count()) {
            $generateExam = $user->generateExams()->where('is_training', $isTraining)->latest()->first();

            if (!$generateExam || !$generateExam->is_active) {
                $user->generateExams()->where('is_training', $isTraining)->delete();

                $data = [];

                for ($i = 1; $i <= 20; $i++) {
                    $topicTrafficRuleIds = TopicTrafficRule::whereHas('folders', function ($query) use ($i) {
                            $query->where('name', $i);
                        })
                        ->whereHas('categoryDriverLicenses', function ($query) use ($user) {
                            $query->whereIn('id', $user->categoryDriverLicenses->pluck('id')->toArray());
                        })
                        ->pluck('id')
                        ->toArray();

                    $questions = Question::where('status', true)
                        ->whereIn('topic_traffic_rule_id', $topicTrafficRuleIds)
                        ->whereNotIn('id', $user->questionAnswerHistories->pluck('question_id')->toArray())
                        ->pluck('id')
                        ->toArray();


                    if (!count($questions)) {
                        $questions = Question::where('status', true)
                            ->whereIn('topic_traffic_rule_id', $topicTrafficRuleIds)
                            ->pluck('id')
                            ->toArray();
                    }

                    $data[] = [
                        'question_id' => count($questions) ? $questions[array_rand($questions)] : null,
                        'number' => $i
                    ];
                }

                $generateExam = static::create([
                    'user_id' => $user->id,
                    'is_training' => $isTraining,
                ]);

                $generateExam->questions()->attach($data);
            }

            return $generateExam;
        }

        $generateExam = new GenerateExam();
        $generateExam->user_id = $user->id;
        $generateExam->is_training = $isTraining;
        $generateExam->created_at = Carbon::now();

        return $generateExam;
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($generateExam) {
            $delay = $generateExam->is_training
                ? Carbon::now()->addHours(24)
                : Carbon::now()->addMinutes(20)->addSeconds(30);

            dispatch((new DisableUserExam($generateExam->id))
                ->delay($delay)
                ->onQueue('disable_user_exam'));
        });

        static::deleting(function ($generateExam) {
            if ($generateExam->is_training && $generateExam->user) {
                $ticketTraining = TicketTraining::firstOrCreate([
                    'user_id' => $generateExam->user->id
                ]);

                if (!$generateExam->questions()->whereNull('questions_answer_id')->count()) {
                    $ticketTraining->increment('passed_number_attempts');

                    if ($generateExam->questions()->where('is_passed', true)->count() <= 2) {
                        $ticketTraining->increment('success_number_attempts');
                    }
                }

                $ticketTraining->increment('correct_answers', $generateExam->questions()->where('is_passed', true)->count());
                $ticketTraining->increment('wrong_answers', $generateExam->questions()->where('is_passed', false)->count());
            }
        });
    }
}
