<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Utilities\FilterBuilder;

class Group extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'user_id'];

    /**
     * Get the user that owns the InfoTeacher.
     */
    public function teacher()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the Students for the group.
     */
    public function students()
    {
        return $this->hasMany(User::class);
    }

    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\GroupFilters');

        return $filter->apply();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating( function($group) {
            $user = auth()->user();

            if ($user->isTeacher()) {
                $group->user_id = $user->id;
            }
        });

        static::deleting( function ($group) {
           $group->students->each(function ($item) {
               $item->group_id = null;
               $item->save();
           });
        });
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('by_user', function (Builder $builder) {
            if ($user = Auth::user()) {
                if ($user->isTeacher()) {
                    $builder->where('user_id', $user->id);
                }
            }
        });
    }
}
