<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InfoStudent extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'patronymic', 'phone', 'avatar'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * Return full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->attributes['last_name'] . ' ' . $this->attributes['first_name'] . ' ' . $this->attributes['patronymic']);
    }

    public function getPhoneAttribute($value)
    {
        if (empty($value)) {
            return null;
        } else {
            $regex = "/^([+]\\d\\d)(\\d{3})(\\d{3})(\\d{2})(\\d{2})$/";
            $subst = '\1 (\2) \3-\4-\5';

            return preg_replace($regex, $subst, $value, 1);
        }
    }

    /**
     * Get the user that owns the InfoStudent.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
