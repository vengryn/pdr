<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InfoTeacher extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'patronymic', 'phone'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * Return full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->attributes['last_name'] . ' ' . $this->attributes['first_name'] . ' ' . $this->attributes['patronymic']);
    }

    /**
     * Get the user that owns the InfoTeacher.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
