<?php

namespace App\Models;

use App\Models\Translate\TranslateInternalTopicTrafficRule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class InternalTopicTrafficRule extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'created_user_id', 'updated_user_id'];

    /**
     * Get the Translates for the TopicTrafficRule.
     */
    public function translates()
    {
        return $this->hasMany(TranslateInternalTopicTrafficRule::class);
    }

    /**
     * Get the Translate by Language for the TopicTrafficRule.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Get the ItemTrafficRules for the InternalTopicTrafficRule.
     */
    public function itemTrafficRules()
    {
        return $this->hasMany(ItemTrafficRule::class);
    }

    /**
     * Scope a query search.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param String $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $search = null)
    {
        if ($search) {
            return $query->where('number', 'like', "%{$search}%")
                ->orWhereHas('translates.language', function ($query) use ($search) {
                    $query->where('name', 'like', "%{$search}%");
                    $query->where('is_default', true);
                });
        }

        return $query;
    }

    protected static function boot()
    {
        parent::boot();

        static::creating( function($internalTopicTrafficRule) {
            $internalTopicTrafficRule->created_user_id = Auth::user()->id;
            $internalTopicTrafficRule->updated_user_id = Auth::user()->id;
        });

        static::updating( function($internalTopicTrafficRule) {
            $internalTopicTrafficRule->updated_user_id = Auth::user()->id;
        });
    }
}
