<?php

namespace App\Models;

use App\Models\Translate\TranslateItemTrafficRule;
use App\Utilities\FilterBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rutorika\Sortable\SortableTrait;

class ItemTrafficRule extends Model
{
    use HasFactory, SoftDeletes, SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'internal_topic_traffic_rule_id'];

    protected static $sortableField = 'order';

    protected static $sortableGroupField = 'internal_topic_traffic_rule_id';

    /**
     * Get the Translates for the ItemTrafficRule.
     */
    public function translates()
    {
        return $this->hasMany(TranslateItemTrafficRule::class);
    }

    /**
     * Get the Translate by Language for the ItemTrafficRule.
     */
    public function getTranslate($lang)
    {
        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Get the internalTopicTrafficRule that owns the ItemTrafficRule.
     */
    public function internalTopicTrafficRule()
    {
        return $this->belongsTo(InternalTopicTrafficRule::class);
    }

    public function markdownImages()
    {
        return $this->morphMany(MarkdownImage::class, 'imagetable');
    }

    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\ItemTrafficRuleFilters');

        return $filter->apply();
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($itemTrafficRule) {
            $itemTrafficRule->next()->decrement(self::$sortableField);
        });
    }
}
