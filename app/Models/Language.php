<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use \Rutorika\Sortable\SortableTrait;

;

class Language extends Model
{
    use HasFactory, SoftDeletes, SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'abbreviation', 'flag', 'is_default', 'order'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_default' => 'boolean',
    ];

    protected static $sortableField = 'order';

    /**
     * Scope a query get default language.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDefault($query)
    {
        return $query->where('is_default', true);
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sorted', function (Builder $builder) {
            $builder->orderBy(self::$sortableField);
        });

        static::creating(function () {
            Cache::forget('languages');
        });

        static::updating(function () {
            Cache::forget('languages');
        });

        static::deleting(function ($language) {
            if ($language->is_default) {
                abort(403);
            }

            Cache::forget('languages');
            Storage::delete($language->flag);
            $language->next()->decrement(self::$sortableField);
        });
    }
}
