<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['message', 'trace', 'data'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'trace' => 'array',
    ];

    /**
     * Create new log.
     *
     * @param $exeption
     */
    public static function saveLog($exeption)
    {
        Log::create([
            'message' => $exeption->getMessage(),
            'trace' => [
                'file' => $exeption->getFile(),
                'line' => $exeption->getLine(),
                'code' => $exeption->getCode(),
            ],
            'data' => $exeption->getTraceAsString()
        ]);
    }
}
