<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class MarkdownImage extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['path'];

    /**
     * Get all of the models that own comments.
     */
    public function imagetable()
    {
        return $this->morphTo();
    }

    public function getLinkAttribute()
    {
        return Storage::url($this->path);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($resource) {
            Storage::delete($resource->path);
        });
    }
}
