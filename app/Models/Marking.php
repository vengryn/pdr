<?php

namespace App\Models;

use App\Models\Translate\TranslateMarking;
use App\Utilities\FilterBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Rutorika\Sortable\SortableTrait;
use Image;

class Marking extends Model
{
    use HasFactory, SoftDeletes, SortableTrait;

    const IMAGE_PATH = 'public/road-markings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'image', 'image_example', 'video_example', 'topic_road_marking_id'];

    protected $appends = ['name'];

    protected $with = ['translates'];

    protected static $sortableField = 'order';

    protected static $sortableGroupField = 'topic_road_marking_id';

    /**
     * Get the Translates for the Sign.
     */
    public function translates()
    {
        return $this->hasMany(TranslateMarking::class);
    }

    /**
     * Get the Translate by Language for the TopicRoadSign.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Get the topicRoadMarking that owns the Sign.
     */
    public function topicRoadMarking()
    {
        return $this->belongsTo(TopicRoadMarking::class);
    }

    public function getNameAttribute() {
        return $this->number . '. ' . $this->getTranslate()->name ?? '';
    }

    public function getImageAttribute($value)
    {
        return $value ? $this->getImageDirectory('original') . $value : null;
    }

    public function getImage26x26Attribute($value)
    {
        return $this->getRawOriginal('image') ? $this->getImageDirectory('26_26') . $this->getRawOriginal('image') : null;
    }

    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\MarkingFilters');

        return $filter->apply();
    }

    public function getImageDirectory($name)
    {
        $data = [
            'original' => self::IMAGE_PATH . '/original/',
            '26_26' => self::IMAGE_PATH . '/small/',
        ];

        return $data[$name] ?? null;
    }

    public function storeImage($image)
    {
        $imageName = 'RMM_' . str_replace('.', '_', $this->number) . '.' . $image->extension();

        $imageThumbName_26_26 = $this->getImageDirectory('26_26') . $imageName;
        $image_26_26 = Image::make($image->path())->resize(26, 26, function ($const) {
            $const->aspectRatio();
        });

        $image->storePubliclyAs($this->getImageDirectory('original'), $imageName);
        Storage::put($imageThumbName_26_26, $image_26_26->stream(), 'public');

        $this->image = $imageName;
        $this->save();
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sorted', function (Builder $builder) {
            $builder->orderBy('number');
        });

        static::created( function($marking) {
            Cache::forget("RMM_{$marking->number}");
        });

        static::updating( function($marking) {
            if ($marking->isDirty(['image', 'number'])) {
                Cache::forget("RMM_{$marking->getOriginal('number')}");
            }
        });

        static::deleting( function($marking) {
            Storage::delete($marking->image);
            Storage::delete($marking->image26x26);
            Storage::disk('digitalocean')->delete($marking->image_example);

            $marking->next()->decrement(self::$sortableField);
        });
    }
}
