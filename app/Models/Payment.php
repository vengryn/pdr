<?php

namespace App\Models;

use App\Casts\Timestamp;
use App\Utilities\FilterBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    const MARKETPLACE_PLAY_MARKET = 1;
    const MARKETPLACE_APP_STORE = 2;
    const MARKETPLACE_LIQPAY = 3;
    const MARKETPLACE_UAPAY = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'subscription', 'promo_code', 'discount', 'marketplace',
        'amount', 'transaction_at', 'params'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'transaction_at' => Timestamp::class,
        'params' => 'array'
    ];

    /**
     * Get types Promo Code
     *
     * @return array
     */
    public static function getMarketplaceList()
    {
        return [
            self::MARKETPLACE_PLAY_MARKET => 'Play Market',
            self::MARKETPLACE_APP_STORE => 'App Store',
            self::MARKETPLACE_LIQPAY => 'LiqPay',
            self::MARKETPLACE_UAPAY => 'UaPay',
        ];
    }

    /**
     * Get the student that owns the payment.
     */
    public function student()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function setReferer()
    {
        if ($promoCode = $this->promo_code) {
            $promoCode = PromoCode::where('type', PromoCode::TYPE_INTERNAL)->where('code', $promoCode)->first();

            if ($promoCode) {
                if ($promoCode->user && $promoCode->user->role == User::ROLE_STUDENT) {
                    if ($this->amount && $promoCode->cashback) {
                        $cashback = (float) bcdiv($this->amount * ($promoCode->cashback / 100), 1, 2);
                    }

                    $promoCode->user->refererPayments()->attach($this, ['cashback' => $cashback ?? null]);
                }
            }
        }
    }

    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\PaymentFilters');

        return $filter->apply();
    }
}
