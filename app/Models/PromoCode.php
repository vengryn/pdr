<?php

namespace App\Models;

use App\Utilities\FilterBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use HasFactory, SoftDeletes;

    const TYPE_TEACHER = 1;
    const TYPE_STUDENT = 2;
    const TYPE_MARKETING = 3;
    const TYPE_INTERNAL = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'discount', 'cashback', 'code', 'date_start', 'date_end', 'user_id'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * The attributes that date.
     *
     * @var array
     */
    protected $dates = ['date_start', 'date_end'];

    /**
     * Get types Promo Code
     *
     * @return array
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_TEACHER => 'Для викладачів',
            self::TYPE_STUDENT => 'Для учнів',
            self::TYPE_MARKETING => 'Маркетинговий',
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDateTermAttribute()
    {
        return $this->date_start && $this->date_end
            ? $this->date_start->format('d.m.Y') . ' - ' . $this->date_end->format('d.m.Y')
            : null;
    }

    public function getTypeNameAttribute()
    {
        return self::getTypeList()[$this->type] ?? null;
    }

    public function isTypeTeacher()
    {
        return $this->type == self::TYPE_TEACHER;
    }

    public function isTypeStudent()
    {
        return $this->type == self::TYPE_STUDENT;
    }

    public function isTypeMarketing()
    {
        return $this->type == self::TYPE_MARKETING;
    }

    public function getStatusAttribute()
    {
        if (in_array($this->type, [self::TYPE_TEACHER, self::TYPE_STUDENT])) {
            return $this->discount ? true : false;
        }

        if ($this->isTypeMarketing()) {
            if ($this->discount) {
                return Carbon::now()->between($this->date_start, $this->date_end->endOfDay()) ||
                    Carbon::now()->lte($this->date_start);
            }
        }

        return false;
    }

    public function getIsActiveAttribute()
    {
        if ($this->discount) {
            return Carbon::now()->between($this->date_start, $this->date_end->endOfDay());
        }

        return false;
    }

    public function scopeMarketing($query)
    {
        return $query->where('type', self::TYPE_MARKETING);
    }

    /**
     * Scope a query filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\PromoCode');

        return $filter->apply();
    }

    public function getDiscountInternalAttribute()
    {
        switch ($this->type) {
            case self::TYPE_MARKETING:
                if ($this->is_active) {
                    return $this->discount;
                }
                break;
            case self::TYPE_INTERNAL:
                if ($this->user) {
                    switch ($this->user->role) {
                        case User::ROLE_TEACHER:
                            return self::where('type', self::TYPE_TEACHER)->first()->discount ?? null;
                        case User::ROLE_STUDENT:
                            return self::where('type', self::TYPE_STUDENT)->first()->discount ?? null;
                    }
                }
                break;
        }

        return null;
    }

    public function getCashbackAttribute($value)
    {
        if ($this->type == self::TYPE_INTERNAL) {
            if ($this->user) {
                switch ($this->user->role) {
                    case User::ROLE_TEACHER:
                        return self::where('type', self::TYPE_TEACHER)->first()->cashback ?? null;
                    case User::ROLE_STUDENT:
                        return self::where('type', self::TYPE_STUDENT)->first()->cashback ?? null;
                }
            }
        }

        return $value;
    }

    public static function generatePromoCode() {
        $code = mt_rand(100000, 999999);

        if (static::barcodeNumberExists($code)) {
            return static::generatePromoCode();
        }

        return $code;
    }

    public static function barcodeNumberExists($code) {
        return static::where('code', $code)->exists();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating( function($promoCode) {
            if ($promoCode->type == self::TYPE_INTERNAL) {
                $promoCode->code = static::generatePromoCode();
            }
        });

        static::deleting(function () {
            if (in_array($this->type, [self::TYPE_TEACHER, self::TYPE_STUDENT])) {
                abort(403);
            }
        });
    }
}
