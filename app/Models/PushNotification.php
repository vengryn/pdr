<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'message'];

    public static function sendNotification($title, $message)
    {
        $fields = ['to' => '/topics/general', 'notification' => [
            'body' => $message,
            'title' => $title,
            'priority' => 'high',
            'sound' => 'default'
        ]];

        $headers = [
            'Authorization: key=' . config('app.google_server_key'),
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    protected static function boot()
    {
        parent::boot();

        static::created( function($item) {
            static::sendNotification($item->title, $item->message);
        });
    }
}
