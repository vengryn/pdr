<?php

namespace App\Models;

use App\Helpers\IncrementDecrement;
use App\Helpers\UsefulFunctions;
use App\Models\Translate\TranslateQuestion;
use App\Utilities\FilterBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Question extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions, IncrementDecrement;

    /**
     * Holds the methods' names of Eloquent Relations
     * to fall on delete cascade or on restoring
     *
     * @var array
     */
    protected static $cascadeRelations = ['translates', 'questionsAnswers', 'questionAnswerHistories',
        'markdownImages', 'questionsComplaints', 'wrongQuestionAnswers'];

    /**copy
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['topic_traffic_rule_id', 'external_id', 'external_number', 'picture', 'status', 'is_internal',
        'is_copy', 'count_wrong_answer', 'start_wrong_answer_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
        'is_internal' => 'boolean',
        'is_copy' => 'boolean',
        'count_wrong_answer' => 'integer',
    ];
    /**
     * The attributes that date.
     *
     * @var array
     */
    protected $dates = ['start_wrong_answer_at'];

    public static function getPathQuestionFile()
    {
        return Setting::getSetting('digitalocean')['question_path'] ?? null;
    }

    /**
     * Save histories answers for questions.
     *
     * @param array $data
     */
    public static function storeQuestionAnswerHistories($data)
    {
        foreach ($data as $datum) {
            static::storeQuestionAnswerHistory($datum);
        }
    }

    /**
     * Save history answer for question.
     *
     * @param $datum
     * @return false
     */
    public static function storeQuestionAnswerHistory($datum)
    {
        if (!empty($datum['question_id']) && !empty($datum['questions_answer_id'])) {
            $questionData = self::getQuestionData($datum['question_id']);

            if ($questionData && in_array($datum['questions_answer_id'], $questionData['answer_ids'])) {

                if (!$isPassed = ($datum['questions_answer_id'] == $questionData['correct_answer_id'])) {
                    static::find($questionData['question_id'])->increment('count_wrong_answer');
                }

                return QuestionAnswerHistory::updateOrCreate([
                    'question_id' => $questionData['question_id'],
                    'user_id' => Auth::user()->id,
                ],
                    [
                        'questions_answer_id' => $datum['questions_answer_id'],
                        'is_passed' => $isPassed,
                        'correct_questions_answer_id' => $questionData['correct_answer_id'],
                        'client_created_at' => $datum['client_created_at'] ?? Carbon::now()->timestamp
                    ]);
            }
        }

        return false;
    }

    /**
     * Generate questions
     *
     * @return mixed
     */
    public static function generateApiQuestions()
    {
        DB::statement('SET SESSION group_concat_max_len = 1000000');

        $languages = Language::get()->each(function ($language) {
            return $language->abbreviation = lcfirst($language->abbreviation);
        });

        $foldersDB = DB::table('folders')->select('id', 'name')
            ->whereNull('folders.deleted_at')->get();

        $categoryDriverLicensesDB = DB::table('category_driver_licenses')
            ->select(
                'category_driver_licenses.id',
                'category_driver_licenses.title',
                DB::raw("GROUP_CONCAT(DISTINCT topic_traffic_rules.id SEPARATOR '|') AS topicIds")
            )
            ->join('category_driver_license_topic_traffic_rule', function ($join) {
                $join->on('category_driver_license_topic_traffic_rule.category_driver_license_id', '=', 'category_driver_licenses.id');
            })
            ->join('topic_traffic_rules', function ($join) {
                $join->on('topic_traffic_rules.id', DB::raw('category_driver_license_topic_traffic_rule.topic_traffic_rule_id'))
                    ->whereNull('topic_traffic_rules.deleted_at');
            })
            ->whereNull('category_driver_licenses.deleted_at')
            ->groupBy('category_driver_licenses.id')
            ->groupBy('category_driver_licenses.title')
            ->get();

        $topicTrafficRulesDB = DB::table('topic_traffic_rules')
            ->select(
                'topic_traffic_rules.id',
                'topic_traffic_rules.number',
                DB::raw("GROUP_CONCAT(DISTINCT folders.id SEPARATOR '|') AS folderIds")
            )->join('folder_topic_traffic_rule', function ($join) {
                $join->on('folder_topic_traffic_rule.topic_traffic_rule_id', '=', 'topic_traffic_rules.id');
            })->join('folders', function ($join) {
                $join->on('folders.id', DB::raw('folder_topic_traffic_rule.folder_id'))->whereNull('folders.deleted_at');
            });

        $questionsDB = DB::table('questions')
            ->select(
                'questions.id',
                'questions.picture',
                'questions.count_wrong_answer',
                'questions.topic_traffic_rule_id as topicId'
            )->whereExists(function ($query) {
                $query->select('*')
                    ->from('topic_traffic_rules')
                    ->whereColumn('questions.topic_traffic_rule_id', 'topic_traffic_rules.id')
                    ->whereNull('topic_traffic_rules.deleted_at');
            })->where('status', true);

        $questionsAnswerDB = DB::table('questions_answers')
            ->select(
                'questions_answers.id',
                'questions_answers.is_correct as correct',
                'questions_answers.order',
                'questions_answers.question_id as questionId',
            )->whereExists(function ($query) {
                $query->select('*')
                    ->from('questions')
                    ->whereColumn('questions_answers.question_id', 'questions.id')
                    ->where('status', true)
                    ->whereNull('questions.deleted_at');
            });

        foreach ($languages as $language) {
            $topicTrafficRulesDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as trans_{$language->abbreviation}"))
                ->leftJoin("translate_topic_traffic_rules as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('topic_traffic_rules.id', '=', "translate_{$language->id}.topic_traffic_rule_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $questionsDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as trans_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.explanation SEPARATOR '')) as explanation_{$language->abbreviation}"))
                ->leftJoin("translate_questions as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('questions.id', '=', "translate_{$language->id}.question_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $questionsAnswerDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as trans_{$language->abbreviation}"))
                ->leftJoin("translate_questions_answers as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('questions_answers.id', '=', "translate_{$language->id}.answer_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });
        }

        $topicTrafficRulesDB = $topicTrafficRulesDB->whereNull('topic_traffic_rules.deleted_at')
            ->groupBy('topic_traffic_rules.id')
            ->groupBy('topic_traffic_rules.number')
            ->get();

        $questionsDB = $questionsDB->whereNull('questions.deleted_at')
            ->groupBy('questions.id')
            ->groupBy('questions.picture')
            ->groupBy('questions.count_wrong_answer')
            ->groupBy('questions.topic_traffic_rule_id')
            ->get();

        $questionsAnswerDB = $questionsAnswerDB->whereNull('questions_answers.deleted_at')
            ->orderBy('order', 'asc')
            ->groupBy('questions_answers.id')
            ->groupBy('questions_answers.is_correct')
            ->groupBy('questions_answers.order')
            ->groupBy('questions_answers.question_id')
            ->get();

        $questionImages = DB::table('markdown_images')
            ->where('markdown_images.imagetable_type', 'App\Models\Question')
            ->get();

        return [
            'folders' => $foldersDB->toArray(),
            'topics' => $topicTrafficRulesDB->map(function ($item) use ($languages) {
                return [
                    'id' => $item->id,
                    'number' => $item->number,
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'trans_' . $lang->abbreviation};
                    })->toArray(),
                    'folderIds' => explode('|', $item->folderIds),
                ];
            })->toArray(),
            'categories' => $categoryDriverLicensesDB->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->title,
                    'topicIds' => explode('|', $item->topicIds),
                ];
            })->toArray(),
            'questions' => $questionsDB->map(function ($item) use ($languages, $questionImages) {
                return [
                    'id' => $item->id,
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'trans_' . $lang->abbreviation};
                    })->toArray(),
                    'explanation' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return self::changeContent($item->{'explanation_' . $lang->abbreviation});
                    })->toArray(),
                    'count_wrong_answer' => $item->count_wrong_answer,
                    'picture' => base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $item->picture)),
                    'topicId' => $item->topicId,
                    'markdown_images' => $questionImages->where('imagetable_id', $item->id)->map(function ($item) {
                        $imgPath = str_replace('/storage', 'public', $item->path);
                        $dataImage = base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $imgPath));

                        return [
                            'name' => basename($item->path),
                            'content' => $dataImage,
                        ];
                    })->values()->toArray()
                ];
            })->toArray(),
            'answers' => $questionsAnswerDB->map(function ($item) use ($languages) {
                return [
                    'id' => $item->id,
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'trans_' . $lang->abbreviation};
                    })->toArray(),
                    'correct' => $item->correct,
                    'order' => $item->order,
                    'questionId' => $item->questionId,
                ];
            })->toArray(),
        ];
    }

    public static function changeContent($text)
    {
        preg_match_all('/!\[(.*?)\]\((.*?)\)/', $text, $matches);

        foreach ($matches[2] as $data) {
            $text = str_replace($data, basename($data), $text);
        }

        return $text;
    }

    /**
     * @param Request $request
     * @param User $user
     * @return array
     */
    public static function getHistory(Request $request, User $user)
    {
        $exams = $user->exams;
        $questionAnswerHistories = $user->questionAnswerHistories;
        $wrongQuestionAnswers = $user->wrongQuestionAnswers;

        if ($questionsHistoryDate = $request->get('questions_history_date')) {
            $questionAnswerHistories = $questionAnswerHistories->where('updated_at', '>', Carbon::createFromTimestamp($questionsHistoryDate));
        }

        if ($examHistoryDate = $request->get('exam_history_date')) {
            $exams = $exams->where('created_at', '>', Carbon::createFromTimestamp($examHistoryDate));
        }

        if ($wrongAnswerHistoryDate = $request->get('wrong_answer_history_date')) {
            $wrongQuestionAnswers = $wrongQuestionAnswers->where('updated_at', '>', Carbon::createFromTimestamp($wrongAnswerHistoryDate));
        } else {
            $wrongQuestionAnswers = $wrongQuestionAnswers->where('action', true);
        }

        return [
            'questionAnswerHistories' => $questionAnswerHistories,
            'exams' => $exams,
            'wrongQuestionAnswers' => $wrongQuestionAnswers,
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function () {
            (new static)->getInfoUpdatedQuestions();
            Cache::forget("total_active_questions");
        });

        static::updating(function ($resource) {
            (new static)->getInfoUpdatedQuestions();

            Cache::forget("total_active_questions");

            if ($resource->isDirty('count_wrong_answer') && !$resource->getOriginal('count_wrong_answer')) {
                $resource->start_wrong_answer_at = Carbon::now();
            }
        });

        static::deleting(function ($resource) {
            if ($resource->copy && !$resource->is_internal) {
                $resource->copy->delete();
            }

            (new static)->getInfoUpdatedQuestions();
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }
            Cache::forget("question_{$resource->id}");
            Cache::forget("total_active_questions");
            Storage::delete($resource->picture);
        });

        static::restoring(function ($resource) {
            (new static)->getInfoUpdatedQuestions();
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->onlyTrashed()->get() as $item) {
                    $item->restore();
                }
            }
        });
    }

    public function markdownImages()
    {
        return $this->morphMany(MarkdownImage::class, 'imagetable');
    }

    /**
     * Get the Translates for the Question.
     */
    public function translates()
    {
        return $this->hasMany(TranslateQuestion::class);
    }

    /**
     * Get the Translate by Language for the Question.
     */
    public function getTranslate($lang = null)
    {
        if (!$lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Return copy question if exists.
     *
     */
    public function copy()
    {
        return $this->belongsTo(Question::class, 'external_id', 'external_id')
            ->where('is_copy', true);
    }

    /**
     * Return original question.
     *
     */
    public function original()
    {
        return $this->belongsTo(Question::class, 'external_id', 'external_id')
            ->where('is_copy', false);
    }

    public function topicTrafficRule()
    {
        return $this->belongsTo(TopicTrafficRule::class);
    }

    /**
     * Get the questionAnswerHistories for the Question.
     */
    public function questionAnswerHistories()
    {
        return $this->hasMany(QuestionAnswerHistory::class);
    }

    /**
     * Get the QuestionsComplaints for the Question.
     */
    public function questionsComplaints()
    {
        return $this->hasMany(QuestionsComplaint::class);
    }

    /**
     * Get the WrongQuestionAnswers for the Question.
     */
    public function wrongQuestionAnswers()
    {
        return $this->hasMany(WrongQuestionAnswer::class);
    }

    /**
     * Scope a query filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\Question');

        return $filter->apply();
    }

    public function syncAnswers($request)
    {
        $this->questionsAnswers->each(function ($questionsAnswer) use ($request) {
            if (!in_array($questionsAnswer->id, array_keys($request->get('answers', [])))) {
                $questionsAnswer->order = 0;
                $questionsAnswer->delete();
            } else {
                $questionsAnswer->is_correct = $request->get('type_answer_correct', 'answers') == 'answers' &&
                    $request->get('answer_correct') == $questionsAnswer->id;
                $questionsAnswer->order = array_search($questionsAnswer->id, array_keys($request->get('answers', []))) + 1;
                $this->saveTranslates($questionsAnswer, $request->get('answers', [])[$questionsAnswer->id]['translate'] ?? []);
            }

            $questionsAnswer->save();
        });

        $maxOrder = $this->questionsAnswers->max('order');

        foreach ($request->get('added_answers', []) as $key => $answer) {
            $questionsAnswer = $this->questionsAnswers()->create([
                'is_correct' => $request->get('type_answer_correct', 'answers') == 'added_answers' &&
                    $request->get('answer_correct') == $key,
                'order' => $maxOrder + $key,
            ]);

            $this->saveTranslates($questionsAnswer, $answer['translate'] ?? []);
        }
    }

    public static function getQuestionData($id)
    {
        return Cache::rememberForever("question_{$id}", function () use ($id) {
            $question = Question::find($id);

            return $question ? [
                'question_id' => $question->id,
                'correct_answer_id' => $question->questionsAnswers->where('is_correct', true)->first()->id ?? null,
                'answer_ids' => $question->questionsAnswers->pluck('id')->toArray() ?? []
            ] : null;
        });
    }

    /**
     * Get the QuestionsAnswers for the Question.
     */
    public function questionsAnswers()
    {
        return $this->hasMany(QuestionsAnswer::class);
    }
}
