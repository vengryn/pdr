<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\MassPrunable;

class QuestionAnswerHistory extends Model
{
    use HasFactory, SoftDeletes, MassPrunable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question_id', 'user_id', 'questions_answer_id', 'is_passed', 'correct_questions_answer_id', 'client_created_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_passed' => 'boolean',
        'client_created_at' => 'integer'
    ];

    /**
     * Get the prunable model query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function prunable()
    {
        return static::onlyTrashed()->where('deleted_at', '<=', now()->subMonths(3));
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function scopeCorrect($query)
    {
        return $query->where('is_passed', true);
    }

    public function scopeWrong($query)
    {
        return $query->where('is_passed', false);
    }
}
