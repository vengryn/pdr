<?php

namespace App\Models;

use App\Helpers\UsefulFunctions;
use App\Models\Translate\TranslateQuestionsAnswer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class QuestionsAnswer extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question_id', 'external_id', 'is_correct', 'order'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_correct' => 'boolean'
    ];

    /**
     * Holds the methods' names of Eloquent Relations
     * to fall on delete cascade or on restoring
     *
     * @var array
     */
    protected static $cascadeRelations = ['translates'];

    protected static $sortableField = 'order';

    /**
     * Get the Translates for the Question.
     */
    public function translates()
    {
        return $this->hasMany(TranslateQuestionsAnswer::class, 'answer_id');
    }

    /**
     * Get the Translate by Language for the Question.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::boot();

        static::addGlobalScope('sorted', function (Builder $builder) {
            $builder->orderBy(self::$sortableField);
        });

        static::creating(function($resource) {
            (new static)->getInfoUpdatedQuestions();
            Cache::forget("question_{$resource->question->id}");
        });

        static::updating(function($resource) {
            (new static)->getInfoUpdatedQuestions();
            Cache::forget("question_{$resource->question->id}");
        });

        static::deleting(function($resource) {
            (new static)->getInfoUpdatedQuestions();
            Cache::forget("question_{$resource->question->id}");
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }
        });

        static::restoring(function($resource) {
            (new static)->getInfoUpdatedQuestions();
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->onlyTrashed()->get() as $item) {
                    $item->restore();
                }
            }
        });
    }
}
