<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected const TIMESTAMP = ['date_of_last_response_file_generation', 'date_of_last_response_theory_file_generation'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'value'];

    protected static function convertResult($name, $value)
    {
        if (is_null($value)) {
            return null;
        }

        if (in_array($name, self::TIMESTAMP)) {
            try {
                return Carbon::parse($value)->timestamp;
            } catch (\Exception $e) {
                return null;
            }
        }

        return json_decode($value, true) ?? $value;
    }

    public static function getSetting($name)
    {
        try {
            $result = static::where('name', $name)->first()->value ?? null;
        } catch (\Exception$exception) {
            $result = null;
        }
        return static::convertResult($name, $result);
    }

    public static function getSettings($names = [])
    {
        try {
            $settings = static::whereIn('name', $names)->get()->keyBy('name')->toArray();
        } catch (\Exception$exception) {
            $settings = [];
        }
        foreach ($names as $name) {
            $value = $settings[$name]['value'] ?? null;
            $result[$name] = static::convertResult($name, $value);
        }

        return $result ?? [];
    }

    public static function storeSetting($name, $value)
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }

        static::updateOrCreate(['name' => $name], [
            'value' => $value
        ]);
    }

    public static function storeSettings($settings = [])
    {
        foreach ($settings as $setting) {
            if (isset($setting['name']) && isset($setting['value'])) {
                static::storeSetting($setting['name'], $setting['value']);
            }
        }
    }
}
