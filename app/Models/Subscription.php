<?php

namespace App\Models;

use App\Models\Translate\TranslateSubscription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Subscription extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'validity', 'price', 'is_paid_video'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_paid_video' => 'boolean',
    ];

    /**
     * Holds the methods' names of Eloquent Relations
     * to fall on delete cascade or on restoring
     *
     * @var array
     */
    protected static $cascadeRelations = ['translates'];
    /**
     * Get the Translates for the Subscription.
     */
    public function translates()
    {
        return $this->hasMany(TranslateSubscription::class);
    }

    /**
     * Get the Translate by Language for the Subscription.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Get the user user subscription for the subscription.
     */
    public function subscription()
    {
        return $this->hasMany(UserSubscription::class);
    }

    public static function isAutoPremium()
    {
        $isAutoPremium = Setting::getSetting('is_auto_premium');

        return $isAutoPremium ?? false;
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($resource) {
            if ($resource->subscription->isNotEmpty()) {
                $lastSubscription = $resource->subscription->sortByDesc('end_at')->first();

                if (Carbon::now()->lt($lastSubscription->end_at)) {
                    abort(403);
                }
            }

            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }
        });

        static::restoring(function($resource) {
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->onlyTrashed()->get() as $item) {
                    $item->restore();
                }
            }
        });
    }
}
