<?php

namespace App\Models;

use App\Models\Translate\TranslateTextPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class TextPage extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug'];

    /**
     * Holds the methods' names of Eloquent Relations
     * to fall on delete cascade or on restoring
     *
     * @var array
     */
    protected static $cascadeRelations = ['translates', 'instructions'];
    /**
     * Get the Translates for the TextPage.
     */
    public function translates()
    {
        return $this->hasMany(TranslateTextPage::class);
    }

    public function markdownImages()
    {
        return $this->morphMany(MarkdownImage::class, 'imagetable');
    }

    /**
     * Get the Translate by Language for the TextPage.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Get the Instructions for the TextPage.
     */
    public function instructions()
    {
        return $this->hasMany(Instruction::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($resource) {
            if ($resource->slug == 'user_agreement') {
                abort(403);
            }

            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }
        });
    }
}
