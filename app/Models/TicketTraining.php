<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\MassPrunable;

class TicketTraining extends Model
{
    use HasFactory, SoftDeletes, MassPrunable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'success_number_attempts', 'passed_number_attempts', 'correct_answers', 'wrong_answers'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'success_number_attempts' => 'integer',
        'passed_number_attempts' => 'integer',
        'correct_answers' => 'integer',
        'wrong_answers' => 'integer'
    ];

    /**
     * Get the prunable model query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function prunable()
    {
        return static::onlyTrashed()->where('deleted_at', '<=', now()->subMonths(3));
    }
}
