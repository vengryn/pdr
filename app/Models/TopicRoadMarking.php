<?php

namespace App\Models;

use App\Models\Translate\TranslateTopicRoadMarking;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class TopicRoadMarking extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'image', 'created_user_id', 'updated_user_id'];

    /**
     * Get the Translates for the TopicRoadMarking.
     */
    public function translates()
    {
        return $this->hasMany(TranslateTopicRoadMarking::class);
    }

    /**
     * Get the Markings for the TopicRoadMarking.
     */
    public function marking()
    {
        return $this->hasMany(Marking::class);
    }

    /**
     * Get the Translate by Language for the TopicRoadMarking.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sorted', function (Builder $builder) {
            $builder->orderBy('number');
        });

        static::creating( function($topicRoadMarking) {
            if (Auth::check()) {
                $topicRoadMarking->created_user_id = Auth::user()->id;
                $topicRoadMarking->updated_user_id = Auth::user()->id;
            }
        });

        static::updating( function($topicRoadMarking) {
            if (Auth::check()) {
                $topicRoadMarking->updated_user_id = Auth::user()->id;
            }
        });

        static::deleting( function($topicRoadMarking) {
            Storage::delete($topicRoadMarking->image);
        });
    }
}
