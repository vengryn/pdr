<?php

namespace App\Models;

use App\Models\Translate\TranslateTopicRoadSign;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class TopicRoadSign extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'image', 'created_user_id', 'updated_user_id'];

    /**
     * Get the Translates for the TopicRoadSign.
     */
    public function translates()
    {
        return $this->hasMany(TranslateTopicRoadSign::class);
    }

    /**
     * Get the Signs for the TopicRoadSign.
     */
    public function signs()
    {
        return $this->hasMany(Sign::class);
    }

    /**
     * Get the Translate by Language for the TopicRoadSign.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sorted', function (Builder $builder) {
            $builder->orderBy('number');
        });

        static::creating( function($topicRoadSign) {
            if (Auth::check()) {
                $topicRoadSign->created_user_id = Auth::user()->id;
                $topicRoadSign->updated_user_id = Auth::user()->id;
            }
        });

        static::updating( function($topicRoadSign) {
            if (Auth::check()) {
                $topicRoadSign->updated_user_id = Auth::user()->id;
            }
        });

        static::deleting( function($topicRoadSign) {
            Storage::delete($topicRoadSign->image);
        });
    }
}
