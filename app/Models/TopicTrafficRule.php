<?php

namespace App\Models;

use App\Helpers\UsefulFunctions;
use App\Models\Translate\TranslateTopicTrafficRule;
use App\Utilities\FilterBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class TopicTrafficRule extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['external_id', 'number', 'created_user_id', 'updated_user_id'];

    /**
     * Get the Translates for the TopicTrafficRule.
     */
    public function translates()
    {
        return $this->hasMany(TranslateTopicTrafficRule::class);
    }

    /**
     * Get the Translate by Language for the TopicTrafficRule.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * The categoryDriverLicenses that belong to the Topic Traffic Rule.
     */
    public function categoryDriverLicenses()
    {
        return $this->belongsToMany(CategoryDriverLicense::class);
    }

    /**
     * The Folders that belong to the Topic Traffic Rule.
     */
    public function folders()
    {
        return $this->belongsToMany(Folder::class);
    }

    /**
     * Get the Questions for the TopicTrafficRule.
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * Scope a query filter.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param String $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\TopicTrafficRule');

        return $filter->apply();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating( function($topicTrafficRule) {
            (new static)->getInfoUpdatedQuestions();
            $topicTrafficRule->created_user_id = Auth::user()->id;
            $topicTrafficRule->updated_user_id = Auth::user()->id;
        });

        static::updating( function($topicTrafficRule) {
            (new static)->getInfoUpdatedQuestions();
            $topicTrafficRule->updated_user_id = Auth::user()->id;
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedQuestions();
        });
    }
}
