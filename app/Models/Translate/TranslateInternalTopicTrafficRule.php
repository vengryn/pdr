<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use App\Models\InternalTopicTrafficRule;
use App\Models\Language;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TranslateInternalTopicTrafficRule extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'language_id'];

    public function internalTopicTrafficRule()
    {
        return $this->belongsTo(InternalTopicTrafficRule::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::updating( function($translateInternalTopicTrafficRule) {
            $translateInternalTopicTrafficRule->internalTopicTrafficRule->update([
                'updated_user_id' => Auth::user()->id
            ]);
            (new static)->getInfoUpdatedTheory();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedTheory();
        });
    }
}
