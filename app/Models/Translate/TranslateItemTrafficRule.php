<?php

namespace App\Models\Translate;

use App\Casts\WysiwygEditor;
use App\Helpers\UsefulFunctions;
use App\Models\ItemTrafficRule;
use App\Models\Language;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslateItemTrafficRule extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'content', 'description', 'language_id'];

    protected $casts = [
        'content' => WysiwygEditor::class,
    ];

    public function itemTrafficRule()
    {
        return $this->belongsTo(ItemTrafficRule::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::updating( function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedTheory();
        });
    }
}
