<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use App\Models\Marking;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslateMarking extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'content', 'description', 'language_id'];

    public function marking()
    {
        return $this->belongsTo(Marking::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::updating( function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedTheory();
        });
    }
}
