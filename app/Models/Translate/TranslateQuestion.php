<?php

namespace App\Models\Translate;

use App\Casts\WysiwygEditor;
use App\Helpers\UsefulFunctions;
use App\Models\Question;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslateQuestion extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'language_id', 'explanation'];

    protected $casts = [
        'explanation' => WysiwygEditor::class,
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::updating(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedQuestions();
        });
    }
}
