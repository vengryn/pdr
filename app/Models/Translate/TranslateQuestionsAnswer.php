<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use App\Models\Question;
use App\Models\QuestionsAnswer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslateQuestionsAnswer extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'answer_id', 'language_id'];

    public function questionsAnswer()
    {
        return $this->belongsTo(QuestionsAnswer::class);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::updating(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedQuestions();
        });
    }
}
