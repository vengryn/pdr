<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TranslateRegulator extends Model
{
    use HasFactory, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::updating( function() {
            (new static)->getInfoUpdatedTheory();
        });
    }
}
