<?php

namespace App\Models\Translate;

use App\Models\Subscription;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslateSubscription extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'advantages', 'language_id'];

    protected $casts = [
        'advantages' => 'array'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
