<?php

namespace App\Models\Translate;

use App\Models\TextPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslateTextPage extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['meta_title', 'meta_description', 'meta_keywords', 'name', 'content', 'language_id'];

    public function textPage()
    {
        return $this->belongsTo(TextPage::class);
    }

    public function getContentAttribute($value)
    {
        preg_match_all('/!\[(.*?)\]\((.*?)\)/', $value, $matches);

        foreach ($matches[2] as $data) {
            $value = str_replace($data, basename($data), $value);
        }

        return $value;
    }
}
