<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use App\Models\TopicRoadMarking;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TranslateTopicRoadMarking extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'language_id'];

    public function topicRoadMarking()
    {
        return $this->belongsTo(TopicRoadMarking::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::updating( function($translateTopicRoadMarking) {
            if (Auth::check()) {
                $translateTopicRoadMarking->topicRoadMarking->update([
                    'updated_user_id' => Auth::user()->id
                ]);
            }
            (new static)->getInfoUpdatedTheory();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedTheory();
        });
    }
}
