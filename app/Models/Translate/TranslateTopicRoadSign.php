<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use App\Models\TopicRoadSign;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TranslateTopicRoadSign extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'language_id'];

    public function topicRoadSign()
    {
        return $this->belongsTo(TopicRoadSign::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::updating( function($translateTopicRoadSign) {
            if (Auth::check()) {
                $translateTopicRoadSign->topicRoadSign->update([
                    'updated_user_id' => Auth::user()->id
                ]);
            }
            (new static)->getInfoUpdatedTheory();
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedTheory();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedTheory();
        });
    }
}
