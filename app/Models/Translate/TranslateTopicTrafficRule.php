<?php

namespace App\Models\Translate;

use App\Helpers\UsefulFunctions;
use App\Models\Question;
use App\Models\TopicTrafficRule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TranslateTopicTrafficRule extends Model
{
    use HasFactory, SoftDeletes, UsefulFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'language_id'];

    public function topicTrafficRule()
    {
        return $this->belongsTo(TopicTrafficRule::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating( function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::updating( function($topicTrafficRule) {
            (new static)->getInfoUpdatedQuestions();
            $topicTrafficRule->topicTrafficRule->update([
                'updated_user_id' => Auth::user()->id
            ]);
        });

        static::deleting(function() {
            (new static)->getInfoUpdatedQuestions();
        });

        static::restoring(function() {
            (new static)->getInfoUpdatedQuestions();
        });
    }
}
