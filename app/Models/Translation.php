<?php

namespace App\Models;

use App\Models\Translate\TranslateTranslation;
use App\Utilities\FilterBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class Translation extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'translation_category_id'];


    /**
     * Holds the methods' names of Eloquent Relations
     * to fall on delete cascade or on restoring
     *
     * @var array
     */
    protected static $cascadeRelations = ['translates'];

    /**
     * Get the Translates for the Question.
     */
    public function translates()
    {
        return $this->hasMany(TranslateTranslation::class);
    }

    /**
     * Get the Translate by Language for the Question.
     */
    public function getTranslate($lang = null)
    {
        if (! $lang) {
            $lang = Cache::remember('default_language', 900, function () {
                return Language::default()->first()->id ?? null;
            });
        }

        return $this->translates->where('language_id', $lang)->first();
    }

    /**
     * Get the TranslationCategory that owns the Translation.
     */
    public function translationCategory()
    {
        return $this->belongsTo(TranslationCategory::class);
    }

    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\TranslationFilters');

        return $filter->apply();
    }

    public static function syncTranslationFiles()
    {
        $languages = Language::all();

        foreach ($languages as $language) {
            $name = lcfirst($language->abbreviation);

            $translates = DB::table('translations')
                ->leftJoin("translate_translations as translate", function($join) use ($language) {
                    $join->on('translations.id', '=', "translate.translation_id")
                        ->where("translate.language_id", $language->id)
                        ->whereNull("translate.deleted_at")->limit(1);
                })
                ->select('translations.key', 'translate.value')
                ->whereNull('translations.deleted_at')
                ->pluck('value', 'key')
                ->toArray();

            Storage::disk('lang')
                ->put("{$name}/mobileApp.php", "<?php \r\nreturn " . var_export($translates, true) . ";");
        }
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($resource) {
            if (Gate::denies('superAdmin')) {
                abort(403);
            }

            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }
        });

        static::restoring(function($resource) {
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->onlyTrashed()->get() as $item) {
                    $item->restore();
                }
            }
        });
    }
}
