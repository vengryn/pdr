<?php

namespace App\Models;

use App\Utilities\FilterBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Notifications\PasswordReset;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes, Sortable;

    const ROLE_ADMINISTRATOR = 1;
    const ROLE_TEACHER = 2;
    const ROLE_STUDENT = 3;
    const ROLE_SUPER_ADMINISTRATOR = 4;
    const ROLE_PAYMENT_REPORT = 5;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'role' => self::ROLE_TEACHER,
        'status' => self::STATUS_ACTIVE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'role',
        'status',
        'group_id',
        'referer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'email_verified_at',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Holds the methods' names of Eloquent Relations
     * to fall on delete cascade or on restoring
     *
     * @var array
     */
    protected static $cascadeRelations = ['infoTeacher', 'infoStudent', 'linkedSocialAccounts', 'generateExams'];

    public $sortable = ['created_at'];

    public $sortableAs = ['subscriptions_start_at', 'subscriptions_end_at'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $resetUrl = in_array(request()->getHost(), ['admin-panel.' . env('APP_DOMAIN_NAME'), 'teacher.' . env('APP_DOMAIN_NAME')])
            ? url(request()->getSchemeAndHttpHost() . "/password/reset/{$token}?email={$this->email}")
            : url("https://pdr.infotech.gov.ua/reset/password?token={$token}&email={$this->email}");

        $this->notify(new PasswordReset($resetUrl));
    }

    public static function getRoleList()
    {
        return [
            self::ROLE_SUPER_ADMINISTRATOR => 'Super Administrator',
            self::ROLE_ADMINISTRATOR => 'Administrator',
            self::ROLE_TEACHER => 'Teacher',
            self::ROLE_PAYMENT_REPORT => 'Payment Report',
        ];
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ];
    }

    /**
     * Check user is super administrator.
     *
     * @var bool
     */
    public function isSuperAdministrator()
    {
        return $this->role == self::ROLE_SUPER_ADMINISTRATOR;
    }

    /**
     * Check user is administrator.
     *
     * @var bool
     */
    public function isAdministrator()
    {
        return in_array($this->role, [self::ROLE_ADMINISTRATOR, self::ROLE_SUPER_ADMINISTRATOR]);
    }

    /**
     * Check user is teacher.
     *
     * @var bool
     */
    public function isTeacher()
    {
        return $this->role == self::ROLE_TEACHER;
    }

    /**
     * Check user is student.
     *
     * @var bool
     */
    public function isStudent()
    {
        return $this->role == self::ROLE_STUDENT;
    }

    /**
     * Check user is payment report.
     *
     * @var bool
     */
    public function isPaymentReport()
    {
        return $this->role == self::ROLE_PAYMENT_REPORT;
    }

    public function setPasswordAttribute($value)
    {
        if ($value) $this->attributes['password'] = Hash::needsRehash($value) ? Hash::make($value) : $value;
    }

    /**
     * Scope a query to only teacher role users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTeacher($query)
    {
        return $query->where('role', self::ROLE_TEACHER);
    }

    /**
     * Get the infoTeacher associated with the user.
     */
    public function infoTeacher()
    {
        return $this->hasOne(InfoTeacher::class);
    }

    /**
     * Scope a query to only student role users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStudent($query)
    {
        return $query->where('role', self::ROLE_STUDENT);
    }

    /**
     * Get the infoStudent associated with the user.
     */
    public function infoStudent()
    {
        return $this->hasOne(InfoStudent::class);
    }

    /**
     * Get the questionAnswerHistories for the User.
     */
    public function questionAnswerHistories()
    {
        return $this->hasMany(QuestionAnswerHistory::class);
    }

    /**
     * Get the wrongQuestionAnswers for the User.
     */
    public function wrongQuestionAnswers()
    {
        return $this->hasMany(WrongQuestionAnswer::class);
    }

    /**
     * Get the linkedSocialAccounts for the User.
     */
    public function linkedSocialAccounts()
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }

    /**
     * Get the Exams for the User.
     */
    public function exams()
    {
        return $this->hasMany(Exam::class);
    }

    /**
     * Get the GenerateExams for the User.
     */
    public function generateExams()
    {
        return $this->hasMany(GenerateExam::class);
    }

    /**
     * Get the TicketTraining for the User.
     */
    public function ticketTraining()
    {
        return $this->hasOne(TicketTraining::class);
    }

    /**
     * Get the payments for the User.
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * Get the user subscription for the user.
     */
    public function subscription()
    {
        return $this->hasMany(UserSubscription::class);
    }

    /**
     * Get the user that owns the Group for Student.
     */
    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    /**
     * Get the user that owns the Group for Teacher.
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    /**
     * The categoryDriverLicenses that belong to the student.
     */
    public function categoryDriverLicenses()
    {
        return $this->belongsToMany(CategoryDriverLicense::class);
    }

    public function promoCode()
    {
        return $this->hasOne(PromoCode::class);
    }

    public function refererPayments()
    {
        return $this->belongsToMany(Payment::class, 'referer_payment')->withPivot(['cashback']);
    }

    /**
     * @param string $option
     * @return array|mixed|null
     */
    public function getStatistics($option = null)
    {
        $options = [];

        if ($option == 'total_progress_theme' || is_null($option)) {
            $totalActiveQuestions = Cache::rememberForever('total_active_questions', function () {
                return Question::where('status', true)->count();
            });

            $options += [
                'total_progress_theme' => $this->exams->count()
                ? round(($this->questionAnswerHistories->where('is_passed', true)->count() / $totalActiveQuestions) * 100)
                : 0
            ];
        }

        if ($option == 'exam_total_number_attempts' || is_null($option)) {
            $options += ['exam_total_number_attempts' => $this->exams->count()];
        }

        if ($option == 'exam_success_number_attempts' || is_null($option)) {
            $options += ['exam_success_number_attempts' => $this->exams->where('is_passed', true)->count()];
        }

        if ($option == 'average_score_exam' || is_null($option)) {
            $options += ['average_score_exam' => $this->exams->count() ? round($this->exams->sum('correct_answers') / $this->exams->count()) : 0];
        }

        if ($option == 'questions_passed' || is_null($option)) {
            $options += ['questions_passed' => $this->questionAnswerHistories->count()];
        }

        if ($option == 'correct_answers' || is_null($option)) {
            $options += ['correct_answers' => $this->questionAnswerHistories->where('is_passed', true)->count()];
        }

        if ($option == 'wrong_answers' || is_null($option)) {
            $options += ['wrong_answers' => $this->questionAnswerHistories->where('is_passed', false)->count()];
        }

        if ($option == 'ticket_training' || is_null($option)) {
            $ticketTraining = $this->ticketTraining;
            $options += [
                'ticket_training' => [
                    'success_number_attempts' => $ticketTraining->success_number_attempts ?? 0,
                    'passed_number_attempts' => $ticketTraining->passed_number_attempts ?? 0,
                    'correct_answers' => $ticketTraining->correct_answers ?? 0,
                    'wrong_answers' => $ticketTraining->wrong_answers ?? 0,
                ]
            ];
        }

        return $option ? ($options[$option] ?? null) : $options;
    }

    /**
     * Check if user active subscription.
     *
     * @return mixed
     */
    public function isActiveSubscription()
    {
        if ($this->subscription()->get()->isNotEmpty()) {
            $lastSubscription = $this->subscription()->latest()->first();

            if (Carbon::now()->lt($lastSubscription->end_at)) {
                return $lastSubscription;
            }
        }

        return null;
    }

    /**
     * Check if user active subscription.
     *
     * @return mixed
     */
    public function isUserActiveSubscription()
    {
        if ($this->subscription->isNotEmpty()) {
            $lastSubscription = $this->subscription->sortByDesc('created_at')->first();

            if (Carbon::now()->lt($lastSubscription->end_at)) {
                return $lastSubscription;
            }
        }

        return null;
    }

    public function getSubscriptionInfoAttribute()
    {
        if ($this->isUserActiveSubscription()) {
            return [
                'name' => isset($this->isUserActiveSubscription()->subscription) ? ($this->isUserActiveSubscription()->subscription->getTranslate()->name ?? '') : 'Підписка: Авто-преміум',
                'start' => $this->isUserActiveSubscription()->start_at->format('d.m.Y'),
                'end' => $this->isUserActiveSubscription()->end_at->format('d.m.Y'),
            ];
        }

        return [
            'name' => null,
            'start' => null,
            'end' => null,
        ];
    }

    public function scopeUserByGroup($query)
    {
        return auth()->user()->isTeacher() ?
            $query->whereHas('group', function ($query) {
                return $query->where('user_id', auth()->user()->id);
            }) : $query;
    }

    public function scopeFilterBy($query, $filters)
    {
        $filter = new FilterBuilder($query, $filters, 'App\Utilities\UserFilters');

        return $filter->apply();
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            if (in_array($user->role, [self::ROLE_TEACHER, self::ROLE_STUDENT])) {
                $user->promoCode()->create([
                    'type' => PromoCode::TYPE_INTERNAL
                ]);
            }
        });

        static::deleting(function ($resource) {
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->get() as $item) {
                    $item->delete();
                }
            }

            $resource->email = $resource->email . '_' . Carbon::now()->timestamp;
            $resource->save();
        });

        static::restoring(function ($resource) {
            foreach (static::$cascadeRelations as $relation) {
                foreach ($resource->{$relation}()->onlyTrashed()->get() as $item) {
                    $item->restore();
                }
            }
        });
    }
}
