<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    use HasFactory;

    const SOURCE_REGISTER = 1;
    const SOURCE_PAYMENT_MOBILE = 2;
    const SOURCE_PAYMENT_WEB = 3;
    const SOURCE_ADMIN = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'subscription_id', 'term', 'is_auto_premium', 'start_at', 'end_at', 'source'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_auto_premium' => 'boolean'
    ];

    /**
     * The attributes that date.
     *
     * @var array
     */
    protected $dates = ['start_at', 'end_at'];

    /**
     * Get the userSubscription that owns the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the userSubscription that owns the subscription.
     */
    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function getIsPaidVideoAttribute()
    {
        if ($this->is_auto_premium) {
            return (bool) Setting::getSetting('auto_premium_paid_video');
        }

        return $this->subscription->is_paid_video ?? false;
    }
}
