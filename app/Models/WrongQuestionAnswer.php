<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\MassPrunable;

class WrongQuestionAnswer extends Model
{
    use HasFactory, SoftDeletes, MassPrunable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'question_id', 'client_created_at', 'last_updating_at', 'action'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'client_created_at' => \App\Casts\Timestamp::class,
        'action' => 'boolean'
    ];

    /**
     * The attributes that date.
     *
     * @var array
     */
    protected $dates = ['last_updating_at'];

    /**
     * Get the prunable model query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function prunable()
    {
        return static::onlyTrashed()->where('deleted_at', '<=', now()->subMonths(3));
    }

    /**
     * Get the user that owns the WrongQuestionAnswer.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the question that owns the WrongQuestionAnswer.
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::boot();

        static::creating(function($resource) {
            $resource->last_updating_at = now();
        });

        static::updating(function($resource) {
            $resource->last_updating_at = now();
        });
    }
}
