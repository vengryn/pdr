<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Schema::defaultStringLength(255);

        Builder::macro('whereLatestRelation', function ($table, $parentRelatedColumn) {
            return $this->where($table . '.id', function ($sub) use ($table, $parentRelatedColumn) {
                $sub->select('id')
                    ->from($table . ' AS other')
                    ->whereColumn('other.' . $parentRelatedColumn, $table . '.' . $parentRelatedColumn)
                    ->latest()
                    ->take(1);
            });
        });
    }
}
