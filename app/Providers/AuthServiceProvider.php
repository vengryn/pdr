<?php

namespace App\Providers;

use App\Models\Group;
use App\Policies\GroupPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Group::class => GroupPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('superAdmin', function (User $user) {
            return $user->isSuperAdministrator();
        });

        Gate::define('item-menu', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('teachers', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('payments', function (User $user) {
            return $user->isAdministrator() || $user->isPaymentReport();
        });

        Gate::define('push-notifications', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('promo-codes', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('updating-msc', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('languages', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('translations', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('category-driver-licenses', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('logs', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('support-services', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('handbook', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('theory', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('issue-management', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('students', function (User $user, User $student) {
            return $user->isAdministrator()
                ? true
                : $student->group && $student->group->user_id == $user->id;
        });

        Gate::define('editable-students', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('group-students', function (User $user) {
            return $user->isTeacher();
        });

        Gate::define('edit-premium-students', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('delete-students', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('text-pages', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('auto-premium', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('general-settings', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('student-actions', function (User $user) {
            return $user->isAdministrator();
        });

        Gate::define('groups', function (User $user) {
            return $user->isAdministrator() || $user->isTeacher() || $user->isStudent();
        });

        Gate::define('display-students', function (User $user) {
            return $user->isAdministrator() || $user->isTeacher() || $user->isStudent();
        });
    }
}
