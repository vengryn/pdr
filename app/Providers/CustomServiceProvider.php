<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Setting;

class CustomServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $settings = Setting::getSettings(['payments', 'liqpay', 'uapay']);

        $this->app['config']['services.liqpay'] = [
            'is_sandbox' => (boolean)($settings['liqpay']['is_sandbox'] ?? false),
            'public_key' => $settings['liqpay']['public_key'] ?? null,
            'private_key' => $settings['liqpay']['private_key'] ?? null,
            'sandbox_public_key' => $settings['liqpay']['sandbox_public_key'] ?? null,
            'sandbox_private_key' => $settings['liqpay']['sandbox_private_key'] ?? null
        ];

        $this->app['config']['services.uapay'] = [
            'id' => $settings['uapay']['id'] ?? '',
            'merchant_secret_key' => $settings['uapay']['merchant_secret_key'] ?? '',
        ];

        $this->app['config']['services.payments'] = [
            'default' => $settings['payments']['default'] ?? 'liqpay',
        ];
    }
}
