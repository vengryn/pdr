<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Setting;

class FileSystemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $digitalocean = Setting::getSetting('digitalocean');

        $this->app['config']['filesystems.disks.digitalocean'] =
            [
                'driver' => 's3',
                'key' => $digitalocean['key'] ?? null,
                'secret' => $digitalocean['key_secret'] ?? null,
                'endpoint' => $digitalocean['endpoint'] ?? null,
                'region' => $digitalocean['region'] ?? null,
                'bucket' => $digitalocean['bucket'] ?? null
            ];
    }
}
