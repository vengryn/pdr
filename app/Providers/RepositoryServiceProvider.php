<?php

namespace App\Providers;

use App\Repositories\Interfaces\ReviewRepositoryInterface;
use App\Repositories\Interfaces\UserSubscriptionRepositoryInterface;
use App\Repositories\ReviewRepository;
use App\Repositories\UserSubscriptionRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserSubscriptionRepositoryInterface::class, UserSubscriptionRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
