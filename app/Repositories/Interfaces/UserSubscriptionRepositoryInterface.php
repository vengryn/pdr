<?php
namespace App\Repositories\Interfaces;

use App\Models\Subscription;
use App\Models\User;
use App\Models\UserSubscription;

interface UserSubscriptionRepositoryInterface
{
    /**
     * @param array $data
     * @return UserSubscription
     */
    public function store(array $data) : UserSubscription;

    /**
     * @param User $user
     * @param int $source
     * @return UserSubscription
     */
    public function createAutoPremium(User $user, int $source) : UserSubscription;

    /**
     * @param User $user
     * @param Subscription $subscription
     * @param int $source
     * @param bool $debug
     * @return UserSubscription
     */
    public function createPaid(User $user, Subscription $subscription, int $source, bool $debug=false) : UserSubscription;
}
