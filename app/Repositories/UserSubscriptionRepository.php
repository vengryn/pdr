<?php
namespace App\Repositories;

use App\Models\Setting;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserSubscription;
use App\Repositories\Interfaces\UserSubscriptionRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log as Logging;

class UserSubscriptionRepository implements UserSubscriptionRepositoryInterface
{
    /**
     * @param array $data
     * @return UserSubscription
     */
    public function store(array $data) : UserSubscription
    {
        $data = [
            'user_id' => $data['user_id'],
            'subscription_id' => $data['subscription_id'] ?? null,
            'term' => $data['term'] ?? null,
            'is_auto_premium' => $data['is_auto_premium'] ?? false,
            'start_at' => $data['start_at'],
            'end_at' => $data['end_at'],
            'source' => $data['source'],
        ];

        Logging::channel('user-subscription')->info($data);

        return UserSubscription::create($data);
    }

    public function createAutoPremium(User $user, int $source): UserSubscription
    {
        return $this->store([
            'user_id' => $user->id,
            'is_auto_premium' => true,
            'start_at' => Carbon::now()->startOfDay()->timestamp,
            'end_at' => Carbon::now()->addDays(Setting::getSetting('auto_premium_days'))->endOfDay()->timestamp,
            'source' => $source
        ]);
    }

    public function createPaid(User $user, Subscription $subscription, int $source, bool $debug = false): UserSubscription
    {
        return $this->store([
            'user_id' => $user->id,
            'subscription_id' => $subscription->id,
            'term' => $subscription->validity,
            'start_at' => Carbon::now()->timestamp,
            'end_at' => $debug
                ? Carbon::now()->addMinutes(4)->timestamp
                : Carbon::now()->addMonths($subscription->validity)->timestamp,
            'source' => $source
        ]);
    }
}
