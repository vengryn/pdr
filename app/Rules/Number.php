<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class Number implements Rule
{
    public $parentNumber;
    public $message;

    /**
     * Create a new rule instance.
     *
     * @param int $parentNumber
     * @param string $message
     * @return void
     */
    public function __construct(int $parentNumber, string $message = null)
    {
        $this->parentNumber = $parentNumber;
        $this->message = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->parentNumber) {
            $data = explode('.', $value);

            if (isset($data[0])) {
                return $this->parentNumber == $data[0];
            }
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
