<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Position implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($value as $key => $item) {
            if ($item['position_x'] && $item['position_y']) {
                foreach ($value as $k => $i) {
                    if ($key != $k) {
                        if ($item['position_x'] == $i['position_x'] && $item['position_y'] == $i['position_y']) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        request()->session()->flash('error', 'Позиції категорій не є унікальними!');

        return 'Позиції категорій не є унікальними';
    }
}
