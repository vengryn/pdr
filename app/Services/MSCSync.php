<?php

namespace App\Services;

use App\Models\Exam;
use App\Models\Log;
use App\Models\Question;
use App\Models\QuestionsAnswer;
use App\Models\Setting;
use App\Models\TicketTraining;
use App\Models\Translate\TranslateQuestion;
use App\Models\Translate\TranslateQuestionsAnswer;
use App\Models\WrongQuestionAnswer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MSCSync
{
    /**
     * The OAuth2 authorization token
     *
     * @var string
     */
    private $_token = null;

    private $_defaultLanguageId;

    private $now_date_time;

    private $console;

    private $access_to_update_questions;

    private $existing_questions = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($console = false, $accessToUpdateQuestions = null)
    {
        $this->_defaultLanguageId = DB::table('languages')
            ->where('is_default', 1)
            ->whereNull('deleted_at')
            ->value('id');
        $this->now_date_time = date('Y-m-d H:i:s');

        $this->console = $console;

        $this->access_to_update_questions = $accessToUpdateQuestions
            ? true
            : Setting::getSetting('access_to_update_questions');
    }

    public function run()
    {
        $defaultTimeLimit = ini_get('max_execution_time');
        $defaultMemoryLimit = ini_get('memory_limit');
        set_time_limit(3600);
        ini_set('memory_limit', '2056M');

        try {
            $this->getAccessToken();
            $this->getQuestions();
        } catch (\Exeption $e) {
            Log::saveLog($e->getMessage());
        }

        set_time_limit($defaultTimeLimit);
        ini_set('memory_limit', $defaultMemoryLimit);
    }

    private function getAccessToken()
    {
        $response = Http::withHeaders(['Authorization' => 'Basic ' . base64_encode('pdd_clnt_id:pdd_sec_ret')])
            ->asForm()
            ->post(config('app.msc_oauth2_url'), [
                'grant_type' => config('app.msc_oauth2_grant_type'),
            ]);

        if ($response->ok()) {
            $this->_token = $response->json('access_token');
        }

        if ($this->_token === null) {
            if (!$this->console) {
                request()->session()->flash('error', 'Помилка під час зєднання з сервісом ГСЦ!');
            }
        }
    }

    private function getQuestions($page = 0, $size = 100)
    {
        if ($this->_token && $this->_defaultLanguageId) {
            $response = Http::withToken($this->_token)->acceptJson()
                ->get(config('app.msc_questions_url'), [
                    'page' => $page,
                    'size' => $size
                ]);

            if ($response->ok()) {
                $questions = $response->json('content');

                if (is_array($questions) && !empty($questions)) {

                    if ($page == 0) {
                        $firstQuestion = array_key_first($questions);

                        session(['complect_id' => (int) $questions[$firstQuestion]['exmSet']['id']]);

                        if (Setting::getSetting('complect_id_msc_sync') != session('complect_id')) {
                            if ($this->access_to_update_questions) {
                                Question::where('is_internal', false)->get()->each(function ($question) {
                                    $question->delete();
                                });

                                Exam::query()->delete();
                                TicketTraining::query()->delete();
                                WrongQuestionAnswer::query()->delete();
                            } else {
                                if (!$this->console) {
                                    request()->session()->flash('warning', '<b>Увага!</b> Доступна нова версія комплекту з питаннями.
                                                Для продовження синхронізації потрібно надати дозвіл на оновлення та повторити спробу.');
                                }

                                return false;
                            }
                        }
                        Storage::put('updates_different.json', null);
                    }

                    $this->existing_questions = DB::table('questions')
                        ->whereIn('external_id', array_column($questions, 'questId'))
                        ->where('is_internal', false)
                        ->whereNull('deleted_at')
                        ->pluck('id', 'external_id')
                        ->toArray();


                    foreach ($questions as $question) {
                        $this->processQuestion($question);
                    }

                    if ($response->json('last')) {
                        $this->finished();
                    } else {
                        $this->getQuestions($page + 1);
                    }
                }
            } else {
                if (!$this->console) {
                    request()->session()->flash('error', 'Помилка при отриманні питань!');
                }
                return false;
            }
        }
    }

    private function processQuestion(array $question)
    {
        if ($this->isValidQuestionData($question)) {
            if (isset($this->existing_questions[$question['questId']])) {
                $this->updateQuestion($this->existing_questions[$question['questId']], $question);
            } else {
                $this->createQuestion($question);
            }
        }
    }

    private function createQuestion(array $question, bool $status=true)
    {
        if (($topicId = $this->getTopicId($question)) !== null) {
            $questionId = DB::table('questions')->insertGetId([
                'topic_traffic_rule_id' => $topicId,
                'external_id' => (int)$question['questId'],
                'external_number' => $question['nquest'],
                'status' => $status,
                'created_at' => $this->now_date_time,
                'updated_at' => $this->now_date_time
            ]);

            DB::table('questions')->where('id', $questionId)->update([
                'picture' => $this->savePicture($question, $questionId)
            ]);

            if ($questionId) {
                $isSaved = DB::table('translate_questions')->insert([
                    'question_id' => $questionId,
                    'language_id' => $this->_defaultLanguageId,
                    'name' => $question['questsName'],
                    'created_at' => $this->now_date_time,
                    'updated_at' => $this->now_date_time
                ]);

                if (!$isSaved) {
                    Log::saveLog("Can't save question's translation: {$this->getQuestionDescription($question)}");
                }

                $this->createOrUpdateAnswers($questionId, $question);
            } else {
                Log::saveLog("Can't save question: {$this->getQuestionDescription($question)}");
            }
        } else {
            Log::saveLog("Can't get internal ID of topic : {$this->getTopicDescription($question)}");
        }
    }

    private function updateQuestion($id, array $question)
    {
        tap(TranslateQuestion::firstOrNew([
            'question_id' => $id, 'language_id' => $this->_defaultLanguageId
        ]), function ($instance) use ($question, $id) {
            $instance->fill([
                'name' => $question['questsName'],
                'created_at' => $this->now_date_time,
                'updated_at' => $this->now_date_time
            ]);

            if ($instance->exists && $instance->isDirty('name')) {
                $this->saveDifferent($id, 'translate_questions', [
                    'name' => $instance->getOriginal('name')
                ]);
            }

            $instance->save();
        });

        $this->createOrUpdateAnswers($id, $question);

        $q = Question::find($id);
        Storage::delete($q->picture);

        $q->picture = $this->savePicture($question, $id);
        $q->updated_at = $this->now_date_time;
        $q->save();

        return true;
    }

    private function createOrUpdateAnswers($questionId, array $question)
    {
        foreach ($question['answers'] as $answer) {
            if ($this->isValidAnswerData($answer)) {
                $isSavedQuestionAnswer = tap(QuestionsAnswer::firstOrNew([
                    'question_id' => $questionId, 'external_id' => (int)$answer['answerId']
                ]), function ($instance) use ($answer, $questionId) {
                    $instance->fill([
                        'is_correct' => $answer['isCorrect'] !== null,
                        'order' => (int)$answer['answerNum'],
                        'created_at' => $this->now_date_time,
                        'updated_at' => $this->now_date_time
                    ]);

                    if ($instance->exists) {
                        if ($instance->isDirty('is_correct') && $instance->getOriginal('is_correct')) {
                            $this->saveDifferent($questionId, 'is_correct_question_answers', [
                                'id' => $instance->id,
                                'external_id' => $instance->external_id
                            ]);
                        }
                        $instance->updated_at = $this->now_date_time;
                    } else {
                        $this->saveDifferent($questionId, 'add_question_answers', [
                            $instance->external_id
                        ]);
                    }

                    $instance->save();
                });

                $answerId = DB::table('questions_answers')
                    ->where('question_id', $questionId)
                    ->where('external_id', (int)$answer['answerId'])
                    ->whereNull('deleted_at')
                    ->value('id');

                if ($isSavedQuestionAnswer && $answerId) {
                    $isSavedTranslateQuestionAnswer = tap(TranslateQuestionsAnswer::firstOrNew([
                        'answer_id' => $answerId, 'language_id' => $this->_defaultLanguageId
                    ]), function ($instance) use ($answer, $questionId, $answerId) {
                        $instance->fill([
                            'name' => $answer['answersName'],
                            'created_at' => $this->now_date_time,
                            'updated_at' => $this->now_date_time
                        ]);

                        if ($instance->exists && $instance->isDirty('name')) {
                            $this->saveDifferent($questionId, 'translate_question_answers', [
                                $answerId => ['name' => $instance->getOriginal('name')]
                            ]);
                        }

                        $instance->save();
                    });


                    if (!$isSavedTranslateQuestionAnswer) {
                        Log::saveLog("Can't save answer's translation: {$this->getAnswerDescription($answer)} for question: {$this->getQuestionDescription($question)}");
                    }
                } else {
                    Log::saveLog("Can't save answer: {$this->getAnswerDescription($answer)} for question: {$this->getQuestionDescription($question)}");
                }
            }
        }
    }

    private function getTopicId(array $question)
    {
        $id = DB::table('topic_traffic_rules')
            ->where('external_id', $question['exmTopic']['id'])
            ->whereNull('deleted_at')
            ->value('id');
        // FIXME: part of code for testing
        if (!$id) {
            $id = DB::table('topic_traffic_rules')->insertGetId([
                'external_id' => $question['exmTopic']['id'],
                'number' => mt_rand(1, 100),
                'created_at' => $this->now_date_time,
                'updated_at' => $this->now_date_time
            ]);

            if ($id) {
                $isSaved = DB::table('translate_topic_traffic_rules')->insert([
                    'topic_traffic_rule_id' => $id,
                    'name' => $question['exmTopic']['value'],
                    'language_id' => $this->_defaultLanguageId,
                    'created_at' => $this->now_date_time,
                    'updated_at' => $this->now_date_time
                ]);

                if (!$isSaved) {
                    Log::saveLog("Can't save topic's tranlation: {$this->getTopicDescription($question)}");
                }
            } else {
                Log::saveLog("Can't save topic: {$this->getTopicDescription($question)}");
            }
        }

        return $id;
    }

    private function savePicture(array $question, $questionId)
    {
        if ($question['picture']) {
            $path = 'public/questions/QNS_' . $questionId . '.jpg';

            if (Storage::disk('local')->put($path, base64_decode($question['picture']), 'public')) {
                return $path;
            } else {
                Log::saveLog("Can't save picture for question: {$this->getQuestionDescription($question)}");
            }
        }

        return null;
    }

    private function isValidQuestionData(array $data)
    {
        $validator = Validator::make($data, [
            'questId' => 'required',
            'exmTopic' => 'required|array',
            'questsName' => 'required',
            'picture' => 'present',
            'answers' => 'required|array',
            'nquest' => 'required'
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                Log::saveLog($error);
            }

            return false;
        }

        return true;
    }

    private function isValidAnswerData(array $data)
    {
        $validator = Validator::make($data, [
            'answerId' => 'required',
            'answerNum' => 'required',
            'answersName' => 'required',
            'isCorrect' => 'present'
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                Log::saveLog($error);
            }

            return false;
        }

        return true;
    }

    private function getTopicDescription(array $question)
    {
        $topicName = str_replace(["\r", "\n"], '', $question['exmTopic']['value']);

        return "#{$question['exmTopic']['id']} - {$topicName}";
    }

    private function getQuestionDescription(array $question)
    {
        $questionName = str_replace(["\r", "\n"], '', $question['questsName']);

        return "#{$question['questId']} - {$questionName}";
    }

    private function getAnswerDescription(array $answer)
    {
        $answerName = str_replace(["\r", "\n"], '', $answer['answersName']);

        return "#{$answer['answerId']} - {$answerName}";
    }

    private function finished()
    {
        Question::where('is_internal', false)
            ->where('updated_at', '<', Carbon::parse($this->now_date_time))
            ->get()
            ->each(function ($item) {
                $item->delete();
            });

        $questionAnswers = DB::table('questions_answers AS qa')
            ->where('qa.updated_at', '<', Carbon::parse($this->now_date_time))
            ->whereNotNull('qa.external_id')
            ->whereNull('qa.deleted_at')
            ->leftJoin('translate_questions_answers AS tqa', function ($join) {
                $join->on('qa.id', '=', 'tqa.answer_id')
                    ->where('language_id', $this->_defaultLanguageId)
                    ->whereNull('tqa.deleted_at')
                    ->limit(1);
            })
            ->select('qa.question_id', 'qa.external_id', 'qa.is_correct', 'tqa.name')
            ->get();

        DB::table('questions_answers AS qa')
            ->where('qa.updated_at', '<', Carbon::parse($this->now_date_time))
            ->whereNotNull('qa.external_id')
            ->whereNull('qa.deleted_at')
            ->leftJoin('translate_questions_answers AS tqa', function ($join) {
                $join->on('qa.id', '=', 'tqa.answer_id')->whereNull('tqa.deleted_at');
            })
            ->update([
                'qa.deleted_at' => $this->now_date_time,
                'tqa.deleted_at' => $this->now_date_time,
            ]);

        foreach ($questionAnswers as $questionAnswer) {
            $this->saveDifferent($questionAnswer->question_id, 'remove_question_answers', [
                $questionAnswer->external_id => [
                    'name' => $questionAnswer->name,
                    'is_correct' => $questionAnswer->is_correct
                ]
            ]);
        }

        Setting::storeSetting('complect_id_msc_sync', session()->pull('complect_id'));
        Setting::storeSetting('date_of_last_update_questions', $this->now_date_time);

        if (!$this->console) {
            request()->session()->flash('success', 'Питання успішно оновлено!');
        }
    }

    private function saveDifferent($id, string $name, array $params)
    {
        $diff = Storage::get('updates_different.json');
        $diff = json_decode($diff, true);

        if ($diff) {
            if (isset($diff[$id])) {
                if ($name == 'translate_question_answers' && isset($diff[$id]['translate_question_answers'])) {
                    $diff[$id][$name] = $diff[$id][$name] + $params;
                } elseif ($name == 'add_question_answers' && isset($diff[$id]['add_question_answers'])) {
                    $diff[$id][$name] = array_merge($diff[$id][$name], $params);
                } elseif ($name == 'remove_question_answers' && isset($diff[$id]['remove_question_answers'])) {
                    $diff[$id][$name] = array_merge($diff[$id][$name], $params);
                } else {
                    $diff[$id] = array_merge($diff[$id], [$name => $params]);
                }

                $data = $diff;
            } else {
                $data = $diff + [$id => [$name => $params]];
            }

        } else {
            $data[$id][$name] = $params;
        }

        Storage::put('updates_different.json', json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}
