<?php

namespace App\Services;

use App\Models\Language;
use App\Models\Marking;
use App\Models\Setting;
use App\Models\Sign;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Theory
{

    /**
     * @var string
     */
    protected $path_theory_file;

    /**
     * @var array
     */
    protected $images;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->path_theory_file = Setting::getSetting('digitalocean')['theory_path'] ?? null;
    }

    public function getPathTheoryFile()
    {
        return $this->path_theory_file;
    }

    /**
     * Generate Theory information
     *
     * @return mixed
     */
    public function generateApiTheory()
    {
        DB::statement('SET SESSION group_concat_max_len = 1000000');

        $languages = Language::get()->each(function ($language) {
            return $language->abbreviation = lcfirst($language->abbreviation);
        });

        $internalTopicTrafficRulesDB = DB::table('internal_topic_traffic_rules')
            ->select('internal_topic_traffic_rules.id', 'internal_topic_traffic_rules.number')
            ->whereNull('internal_topic_traffic_rules.deleted_at');

        $itemTrafficRulesDB = DB::table('item_traffic_rules')
            ->select('item_traffic_rules.id', 'item_traffic_rules.number', 'item_traffic_rules.order', 'item_traffic_rules.internal_topic_traffic_rule_id')
            ->whereNull('item_traffic_rules.deleted_at');

        $topicRoadSignsDB = DB::table('topic_road_signs')
            ->select('topic_road_signs.id', 'topic_road_signs.number', 'topic_road_signs.image')
            ->whereNull('topic_road_signs.deleted_at');

        $signsDB = DB::table('signs')
            ->select('signs.id', 'signs.number', 'signs.order', 'signs.image', 'signs.image_example', 'signs.video_example', 'signs.topic_road_sign_id')
            ->whereNull('signs.deleted_at');

        $topicRoadMarkingsDB = DB::table('topic_road_markings')
            ->select('topic_road_markings.id', 'topic_road_markings.number', 'topic_road_markings.image')
            ->whereNull('topic_road_markings.deleted_at');

        $markingsDB = DB::table('markings')
            ->select('markings.id', 'markings.number', 'markings.order', 'markings.image', 'markings.image_example', 'markings.video_example', 'markings.topic_road_marking_id')
            ->whereNull('markings.deleted_at');


        foreach ($languages as $language) {
            $internalTopicTrafficRulesDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as name_{$language->abbreviation}"))
                ->leftJoin("translate_internal_topic_traffic_rules as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('internal_topic_traffic_rules.id', '=', "translate_{$language->id}.internal_topic_traffic_rule_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $itemTrafficRulesDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.content SEPARATOR '')) as content_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.description SEPARATOR '')) as description_{$language->abbreviation}"))
                ->leftJoin("translate_item_traffic_rules as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('item_traffic_rules.id', '=', "translate_{$language->id}.item_traffic_rule_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $topicRoadSignsDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as name_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.description SEPARATOR '')) as description_{$language->abbreviation}"))
                ->leftJoin("translate_topic_road_signs as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('topic_road_signs.id', '=', "translate_{$language->id}.topic_road_sign_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $signsDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as name_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.content SEPARATOR '')) as content_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.description SEPARATOR '')) as description_{$language->abbreviation}"))
                ->leftJoin("translate_signs as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('signs.id', '=', "translate_{$language->id}.sign_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $topicRoadMarkingsDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as name_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.description SEPARATOR '')) as description_{$language->abbreviation}"))
                ->leftJoin("translate_topic_road_markings as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('topic_road_markings.id', '=', "translate_{$language->id}.topic_road_marking_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });

            $markingsDB->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.name SEPARATOR '')) as name_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.content SEPARATOR '')) as content_{$language->abbreviation}"))
                ->addSelect(DB::raw("(GROUP_CONCAT(DISTINCT translate_{$language->id}.description SEPARATOR '')) as description_{$language->abbreviation}"))
                ->leftJoin("translate_markings as translate_{$language->id}", function ($join) use ($language) {
                    $join->on('markings.id', '=', "translate_{$language->id}.marking_id")
                        ->where("translate_{$language->id}.language_id", $language->id)
                        ->whereNull("translate_{$language->id}.deleted_at")->limit(1);
                });
        }

        $internalTopicTrafficRules = $internalTopicTrafficRulesDB
            ->groupBy('internal_topic_traffic_rules.id')
            ->groupBy('internal_topic_traffic_rules.number')
            ->orderBy('number')
            ->get();

        $itemTrafficRules = $itemTrafficRulesDB
            ->groupBy('item_traffic_rules.id')
            ->groupBy('item_traffic_rules.number')
            ->groupBy('item_traffic_rules.order')
            ->groupBy('item_traffic_rules.internal_topic_traffic_rule_id')
            ->orderBy('number')
            ->get();

        $topicRoadSigns = $topicRoadSignsDB
            ->groupBy('topic_road_signs.id')
            ->groupBy('topic_road_signs.number')
            ->groupBy('topic_road_signs.image')
            ->orderBy('number')
            ->get();

        $signs = $signsDB
            ->groupBy('signs.id')
            ->groupBy('signs.number')
            ->groupBy('signs.order')
            ->groupBy('signs.image')
            ->groupBy('signs.image_example')
            ->groupBy('signs.video_example')
            ->groupBy('signs.topic_road_sign_id')
            ->orderBy('number')
            ->get();

        $topicRoadMarkings = $topicRoadMarkingsDB
            ->groupBy('topic_road_markings.id')
            ->groupBy('topic_road_markings.number')
            ->groupBy('topic_road_markings.image')
            ->orderBy('number')
            ->get();

        $markings = $markingsDB
            ->groupBy('markings.id')
            ->groupBy('markings.number')
            ->groupBy('markings.order')
            ->groupBy('markings.image')
            ->groupBy('markings.image_example')
            ->groupBy('markings.video_example')
            ->groupBy('markings.topic_road_marking_id')
            ->orderBy('number')
            ->get();

        $translateRegulators = DB::table('translate_regulators')
            ->select('translate_regulators.description', 'translate_regulators.language_id')
            ->groupBy('translate_regulators.description')
            ->groupBy('translate_regulators.language_id')
            ->get()
            ->keyBy('language_id');

        $translateTrafficLights = DB::table('translate_traffic_lights')
            ->select('translate_traffic_lights.description', 'translate_traffic_lights.language_id')
            ->groupBy('translate_traffic_lights.description')
            ->groupBy('translate_traffic_lights.language_id')
            ->get()
            ->keyBy('language_id');

        $translateVideoLessonsTrafficRules = DB::table('translate_video_lessons_traffic_rules')
            ->select('translate_video_lessons_traffic_rules.description', 'translate_video_lessons_traffic_rules.language_id')
            ->groupBy('translate_video_lessons_traffic_rules.description')
            ->groupBy('translate_video_lessons_traffic_rules.language_id')
            ->get()
            ->keyBy('language_id');

        $translateRoadSignDescriptions = DB::table('translate_road_sign_descriptions')
            ->select('translate_road_sign_descriptions.description', 'translate_road_sign_descriptions.language_id')
            ->groupBy('translate_road_sign_descriptions.description')
            ->groupBy('translate_road_sign_descriptions.language_id')
            ->get()
            ->keyBy('language_id');

        $translateRoadMarkingDescriptions = DB::table('translate_road_marking_descriptions')
            ->select('translate_road_marking_descriptions.description', 'translate_road_marking_descriptions.language_id')
            ->groupBy('translate_road_marking_descriptions.description')
            ->groupBy('translate_road_marking_descriptions.language_id')
            ->get()
            ->keyBy('language_id');

        $itemTrafficRulesImages = DB::table('markdown_images')
            ->where('markdown_images.imagetable_type', 'App\Models\ItemTrafficRule')
            ->get();

        $regulatorImages = DB::table('markdown_images')
            ->where('markdown_images.imagetable_type', 'App\Models\TranslateRegulator')
            ->get();

        $trafficLightImages = DB::table('markdown_images')
            ->where('markdown_images.imagetable_type', 'App\Models\TranslateTrafficLight')
            ->get();

        $videoLessonsTrafficRulesImages = DB::table('markdown_images')
            ->where('markdown_images.imagetable_type', 'App\Models\TranslateVideoLessonsTrafficRule')
            ->get();

        $sign = new Sign();
        $marking = new Marking();

        return [
            'topic_traffic_rules' => $internalTopicTrafficRules->map(function ($item) use ($languages) {
                return [
                    'id' => $item->id,
                    'number' => $item->number,
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'name_' . $lang->abbreviation};
                    })->toArray()
                ];
            })->toArray(),
            'item_traffic_rules' => $itemTrafficRules->map(function ($item) use ($languages, $itemTrafficRulesImages) {
                return [
                    'id' => $item->id,
                    'topic_traffic_rule_id' => $item->internal_topic_traffic_rule_id,
                    'number' => (string)$item->number,
                    'order' => $item->order,
                    'description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'description_' . $lang->abbreviation});
                    })->toArray(),
                    'content' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'content_' . $lang->abbreviation});
                    })->toArray(),
                    'markdown_images' => $itemTrafficRulesImages->where('imagetable_id', $item->id)->map(function ($item) {
                        $imgPath = str_replace('/storage', 'public', $item->path);
                        $dataImage = base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $imgPath));

                        return [
                            'name' => basename($item->path),
                            'content' => $dataImage,
                        ];
                    })->values()->toArray()
                ];
            })->toArray(),
            'category_road_signs' => $topicRoadSigns->map(function ($item) use ($languages) {
                return [
                    'id' => $item->id,
                    'number' => $item->number,
                    'image' => $this->encodeImage($item->image),
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'name_' . $lang->abbreviation};
                    })->toArray(),
                    'description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'description_' . $lang->abbreviation});
                    })->toArray()
                ];
            })->toArray(),
            'road_signs' => $signs->map(function ($item) use ($languages, $sign) {
                return [
                    'id' => $item->id,
                    'category_road_sign_id' => $item->topic_road_sign_id,
                    'number' => (string)$item->number,
                    'order' => $item->order,
                    'image' => [
                        'original' => $this->encodeImage($sign->getImageDirectory('original') . $item->image),
                        'small' => $this->encodeImage($sign->getImageDirectory('26_26') . $item->image),
                    ],
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'name_' . $lang->abbreviation};
                    })->toArray(),
                    'description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'description_' . $lang->abbreviation});
                    })->toArray(),
                    'content' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'content_' . $lang->abbreviation});
                    })->toArray(),
                    'image_example' => $item->image_example ? Storage::disk('digitalocean')->url($item->image_example) : null,
                    'video_example' => $item->video_example,
                ];
            })->toArray(),
            'type_road_markings' => $topicRoadMarkings->map(function ($item) use ($languages) {
                return [
                    'id' => $item->id,
                    'number' => $item->number,
                    'image' => $this->encodeImage($item->image),
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'name_' . $lang->abbreviation};
                    })->toArray(),
                    'description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'description_' . $lang->abbreviation});
                    })->toArray()
                ];
            })->toArray(),
            'road_markings' => $markings->map(function ($item) use ($languages, $marking) {
                return [
                    'id' => $item->id,
                    'type_road_marking_id' => $item->topic_road_marking_id,
                    'number' => (string)$item->number,
                    'order' => $item->order,
                    'image' => [
                        'original' => $this->encodeImage($marking->getImageDirectory('original') . $item->image),
                        'small' => $this->encodeImage($marking->getImageDirectory('26_26') . $item->image),
                    ],
                    'name' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $item->{'name_' . $lang->abbreviation};
                    })->toArray(),
                    'description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'description_' . $lang->abbreviation});
                    })->toArray(),
                    'content' => $languages->keyBy('abbreviation')->map(function ($lang) use ($item) {
                        return $this->changeContent($item->{'content_' . $lang->abbreviation});
                    })->toArray(),
                    'image_example' => $item->image_example ? Storage::disk('digitalocean')->url($item->image_example) : null,
                    'video_example' => $item->video_example,
                ];
            })->toArray(),
            'regulator' =>
                [
                    'content' => $languages->keyBy('abbreviation')->map(function ($lang) use ($translateRegulators) {
                        return $this->changeContent($translateRegulators[$lang->id]->description ?? null);
                    })->toArray(),
                    'markdown_images' => $regulatorImages->map(function ($item) {
                        $imgPath = str_replace('/storage', 'public', $item->path);
                        $dataImage = base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $imgPath));

                        return [
                            'name' => basename($item->path),
                            'content' => $dataImage,
                        ];
                    })->toArray()
                ],
            'traffic_lights' =>
                [
                    'content' => $languages->keyBy('abbreviation')->map(function ($lang) use ($translateTrafficLights) {
                        return $this->changeContent($translateTrafficLights[$lang->id]->description ?? null);
                    })->toArray(),
                    'markdown_images' => $trafficLightImages->map(function ($item) {
                        $imgPath = str_replace('/storage', 'public', $item->path);
                        $dataImage = base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $imgPath));

                        return [
                            'name' => basename($item->path),
                            'content' => $dataImage,
                        ];
                    })->toArray()
                ],
            'video_lessons_traffic_rules' =>
                [
                    'content' => $languages->keyBy('abbreviation')->map(function ($lang) use ($translateVideoLessonsTrafficRules) {
                        return $this->changeContent($translateVideoLessonsTrafficRules[$lang->id]->description ?? null);
                    })->toArray(),
                    'markdown_images' => $videoLessonsTrafficRulesImages->map(function ($item) {
                        $imgPath = str_replace('/storage', 'public', $item->path);
                        $dataImage = base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $imgPath));

                        return [
                            'name' => basename($item->path),
                            'content' => $dataImage,
                        ];
                    })->toArray()
                ],
            'category_road_sign_description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($translateRoadSignDescriptions) {
                return $this->changeContent($translateRoadSignDescriptions[$lang->id]->description ?? null);
            })->toArray(),
            'category_road_marking_description' => $languages->keyBy('abbreviation')->map(function ($lang) use ($translateRoadMarkingDescriptions) {
                return $this->changeContent($translateRoadMarkingDescriptions[$lang->id]->description ?? null);
            })->toArray(),
        ];
    }

    protected function changeContent($text)
    {
        preg_match_all('/!\[(.*?)\]\((.*?)\)/', $text, $matches);

        foreach ($matches[2] as $data) {
            $text = str_replace($data, basename($data), $text);
        }

        preg_match_all('/&#x(.*?);/', $text, $emodji);

        array_walk($emodji[1], function(&$a) {
            $a = '\u' . $a;
        });

        return str_replace($emodji[0], $emodji[1], $text);
    }

    protected function encodeImage($path)
    {
        return Storage::exists($path)
            ? base64_encode(@file_get_contents(Storage::getDriver()->getAdapter()->getPathPrefix() . $path))
            : null;
    }
}
