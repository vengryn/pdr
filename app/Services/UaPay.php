<?php

namespace App\Services;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Http;

class UaPay
{
    const URL = 'https://api.uapay.ua/api';

    private $id;
    private $key;

    public function __construct()
    {
        $this->id = config('services.uapay.id');
        $this->key = config('services.uapay.merchant_secret_key');
    }

    public function createSession()
    {
        $payload = [
            'params' => [
                'clientId' => $this->id
            ],
            'iat' => now()->timestamp,
        ];

        $payload += ['token' => JWT::encode($payload, $this->key)];

        $response = Http::withHeaders(['Accept' => 'application/json'])
            ->post(static::URL . '/sessions/create', $payload);

        $status = $response->json('status');

        if ($status && $response->json('data.token')) {
            $data = JWT::decode($response->json('data.token'), $this->key, ['HS256']);
            return $data->id;
        }

        return false;
    }

    public function createInvoice(string $sessionId, array $data)
    {
        $payload = [
            'params' => [
                'sessionId' => $sessionId,
                'systemType' => 'ECOM',
            ],
            'data' => $data,
            'iat' => now()->timestamp,
        ];

        $payload += ['token' => JWT::encode($payload, $this->key)];

        $response = Http::withHeaders(['Accept' => 'application/json'])
            ->post(static::URL . '/invoicer/invoices/create', $payload);

        $status = $response->json('status');

        if ($status && $response->json('data.token')) {
            $data = JWT::decode($response->json('data.token'), $this->key, ['HS256']);
            return $data;
        }

        return false;
    }

    public function getCallback($token)
    {
        return JWT::decode($token, $this->key, ['HS256']);
    }
}
