<?php

namespace App\Utilities;

use Illuminate\Support\Str;

class FilterBuilder
{
    protected $query;
    protected $filters;
    protected $namespace;

    public function __construct($query, $filters, $namespace)
    {
        $this->query = $query;
        $this->filters = $filters;
        $this->namespace = $namespace;
    }

    public function apply()
    {
        if (is_array($this->filters)) {
            foreach ($this->filters as $name => $value) {
                $normailizedName = Str::studly($name);

                $class = $this->namespace . "\\{$normailizedName}";

                if (! class_exists($class) || ! $value) {
                    continue;
                }

                (new $class($this->query))->handle($value);
            }
        }

        return $this->query;
    }
}
