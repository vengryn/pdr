<?php

namespace App\Utilities\ItemTrafficRuleFilters;

use App\Utilities\FilterContract;

class InternalTopicTrafficRule implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('internal_topic_traffic_rule_id', $value);
    }
}
