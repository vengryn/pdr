<?php

namespace App\Utilities\ItemTrafficRuleFilters;

use App\Utilities\FilterContract;

class Item implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('item_traffic_rules.id', $value);
    }
}
