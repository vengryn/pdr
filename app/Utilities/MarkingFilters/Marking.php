<?php

namespace App\Utilities\MarkingFilters;

use App\Utilities\FilterContract;

class Marking implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('markings.id', $value);
    }
}
