<?php

namespace App\Utilities\MarkingFilters;

use App\Utilities\FilterContract;

class Name implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $pieces = array_map('trim', explode(' ', $value));

        $this->query->where(function ($query) use ($value, $pieces) {
            if (count($pieces) > 1) {
                foreach ($pieces as $piece) {
                    $query->where(function ($query) use ($piece) {
                        $query->where('number', 'like', "%{$piece}%");
                        $query->orWhereHas('translates', function ($query) use ($piece) {
                            $query->where('name', 'like', "%{$piece}%");
                        });
                    });
                }

            } else {
                $query->where('number', 'like', "%{$value}%")->orWhereHas('translates', function ($query) use ($value) {
                    $query->where('name', 'like', "%{$value}%");
                });
            }
        });
    }
}
