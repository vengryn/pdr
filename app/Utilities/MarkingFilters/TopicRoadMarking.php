<?php

namespace App\Utilities\MarkingFilters;

use App\Utilities\FilterContract;

class TopicRoadMarking implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('topic_road_marking_id', $value);
    }
}
