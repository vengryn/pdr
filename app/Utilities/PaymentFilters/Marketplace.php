<?php

namespace App\Utilities\PaymentFilters;

use App\Utilities\FilterContract;

class Marketplace implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('marketplace', $value);
    }
}
