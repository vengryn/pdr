<?php

namespace App\Utilities\PromoCode;

use App\Utilities\FilterContract;

class Code implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('code', 'like', "%$value%");
    }
}
