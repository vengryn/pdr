<?php

namespace App\Utilities\Question;

use App\Utilities\FilterContract;

class Content implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->whereHas('translates', function ($query) use ($value) {
            $query->where('name', 'like', "%{$value}%");
        });
    }
}
