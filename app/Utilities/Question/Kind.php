<?php

namespace App\Utilities\Question;

use App\Utilities\FilterContract;

class Kind implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $value == 'replace'
            ? $this->query->where('is_copy', true)
            : $this->query;
    }
}
