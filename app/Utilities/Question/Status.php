<?php

namespace App\Utilities\Question;

use App\Utilities\FilterContract;

class Status implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        switch ($value) {
            case 'active':
                $this->query->where('status', true);
                break;
            case 'no-active':
                $this->query->where('status', false);
                break;
            default:
                $this->query;
        }
    }
}
