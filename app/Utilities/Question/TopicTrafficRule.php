<?php

namespace App\Utilities\Question;

use App\Utilities\FilterContract;

class TopicTrafficRule implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->whereHas('topicTrafficRule', function ($query) use ($value) {
            $query->where('number', $value);
        });
    }
}
