<?php

namespace App\Utilities\Question;

use App\Utilities\FilterContract;

class Type implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        switch ($value) {
            case 'internal':
                $this->query->where('is_internal', true)->where('is_copy', false);
                break;
            case 'gsc':
                $this->query->where('is_internal', false);
                break;
            default:
                $this->query;
        }
    }
}
