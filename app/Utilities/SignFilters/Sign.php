<?php

namespace App\Utilities\SignFilters;

use App\Utilities\FilterContract;

class Sign implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('signs.id', $value);
    }
}
