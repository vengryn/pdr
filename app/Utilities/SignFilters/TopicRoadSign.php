<?php

namespace App\Utilities\SignFilters;

use App\Utilities\FilterContract;

class TopicRoadSign implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('topic_road_sign_id', $value);
    }
}
