<?php

namespace App\Utilities\TopicTrafficRule;

use App\Utilities\FilterContract;

class Folder implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->whereHas('folders', function ($query) use ($value) {
            $query->where('id', $value);
        });
    }
}
