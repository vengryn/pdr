<?php

namespace App\Utilities\TopicTrafficRule;

use App\Utilities\FilterContract;

class Kind implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $value == 'without_folder'
            ? $this->query->doesntHave('folders')
            : $this->query;
    }
}
