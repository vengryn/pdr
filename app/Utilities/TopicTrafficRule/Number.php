<?php

namespace App\Utilities\TopicTrafficRule;

use App\Utilities\FilterContract;

class Number implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->where('number', $value);
    }
}
