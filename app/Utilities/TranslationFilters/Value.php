<?php

namespace App\Utilities\TranslationFilters;

use App\Utilities\FilterContract;

class Value implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->whereHas('translates', function ($query) use ($value) {
            $query->where('value', 'like', "%{$value}%");
        });
    }
}
