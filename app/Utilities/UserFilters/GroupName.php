<?php

namespace App\Utilities\UserFilters;

use App\Utilities\FilterContract;

class GroupName implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        if ($value == 'without') {
            $this->query->doesntHave('group');
        } else {
            $this->query->whereHas('group', function ($query) use($value) {
                $query->where('title', 'like', "%$value%");
            });
        }
    }
}
