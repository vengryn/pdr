<?php

namespace App\Utilities\UserFilters;

use App\Utilities\FilterContract;
use Carbon\Carbon;

class Period implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        try {
            if (!empty($value[0])) {
                $start = Carbon::parse($value[0]);
                $this->query->where('users.created_at', '>=', $start);
            }

            if (!empty($value[1])) {
                $end = Carbon::parse($value[1])->endOfDay();
                $this->query->where('users.created_at', '<=', $end);
            }
        } catch (\Exception $e) {
            //
        }
    }
}
