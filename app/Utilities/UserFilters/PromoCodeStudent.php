<?php

namespace App\Utilities\UserFilters;

use App\Utilities\FilterContract;

class PromoCodeStudent implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $this->query->whereHas('promoCode', function ($query) use ($value) {
            $query->where('code', 'like', "%$value%");
        });
    }
}
