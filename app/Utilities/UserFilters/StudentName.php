<?php

namespace App\Utilities\UserFilters;

use App\Utilities\FilterContract;

class StudentName implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        $pieces = array_map('trim', explode(' ', $value));

        $this->query->whereHas('infoStudent', function ($query) use($value, $pieces) {
            if (count($pieces) > 1) {
                foreach ($pieces as $piece) {
                    $query->where(function ($query) use ($piece) {
                        $query->where('first_name', 'like', "$piece");
                        $query->orWhere('last_name', 'like', "$piece");
                        $query->orWhere('patronymic', 'like', "$piece");
                    });
                }
            } else {
                $query->where('first_name', 'like', "$value%");
                $query->orWhere('last_name', 'like', "$value%");
                $query->orWhere('patronymic', 'like', "$value%");
            }
        });

    }
}
