<?php

namespace App\Utilities\UserFilters;

use App\Models\User;
use App\Utilities\FilterContract;
use Carbon\Carbon;

class Subscription implements FilterContract
{
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle($value): void
    {
        if ($value == 'without') {
            $this->query
                ->doesntHave('subscription')
                ->orWhereHas('subscription', function ($query) {
                    $query->where('end_at', '<', Carbon::now())->whereLatestRelation('user_subscriptions', 'user_id');
                });
        } else {
            $this->query->whereHas('subscription', function ($query) use ($value) {
                $query->where('subscription_id', $value);
                $query->where('end_at', '>', Carbon::now());
            });
        }
    }
}
