<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'social-auth-logging' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/social/history.log'),
        ],

        'msc-sync-questions' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/msc-sync-questions/history.log'),
        ],

        'payment-mobile-app' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/payment-mobile-app/info.log'),
        ],

        'payment-mobile-app-all' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/payment-mobile-app-all/info.log'),
        ],

        'liq-pay' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/liq-pay/info.log'),
        ],

        'ua-pay' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/ua-pay/info.log'),
        ],

        'reset-histories' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/reset-histories/info.log'),
        ],

        'user-subscription' => [
            'driver' => 'daily',
            'days' => 365,
            'path' => storage_path('logs/user-subscription/info.log'),
        ],

        'web-requests' => [
            'driver' => 'daily',
            'path' => storage_path('logs/web-requests/info.log'),
        ],

        'api-requests' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api-requests/info.log'),
        ],

        'api-sql' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api-sql/info.log'),
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 14,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => env('LOG_LEVEL', 'critical'),
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],
    ],

];
