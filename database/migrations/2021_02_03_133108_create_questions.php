<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('topic_traffic_rule_id')->nullable()->constrained('topic_traffic_rules')->nullOnDelete();
            $table->unsignedInteger('external_id');
            $table->string('external_number')->nullable();
            $table->string('picture')->nullable();
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('translate_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('question_id')->constrained('questions')->onDelete('cascade');
            $table->foreignId('language_id')->constrained('languages')->onDelete('cascade');
            $table->text('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('questions_answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('question_id')->constrained('questions')->onDelete('cascade');
            $table->unsignedInteger('external_id');
            $table->unsignedTinyInteger('is_correct')->default(0);
            $table->unsignedInteger('order');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('translate_questions_answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('answer_id')->constrained('questions_answers')->onDelete('cascade');
            $table->foreignId('language_id')->constrained('languages')->onDelete('cascade');
            $table->text('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translate_questions_answers');
        Schema::dropIfExists('questions_answers');
        Schema::dropIfExists('translate_questions');
        Schema::dropIfExists('questions');
    }
}
