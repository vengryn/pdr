<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryDriverLicenseTopicTrafficRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_driver_license_topic_traffic_rule', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->unsignedBigInteger('topic_traffic_rule_id');
            $table->unsignedBigInteger('category_driver_license_id');

            $table->foreign('topic_traffic_rule_id', 'topic_traffic_rule_id_foreign')
                ->references('id')->on('topic_traffic_rules')->onDelete('cascade');
            $table->foreign('category_driver_license_id', 'category_driver_license_id_foreign')
                ->references('id')->on('category_driver_licenses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_driver_license_topic_traffic_rule');
    }
}
