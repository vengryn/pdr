<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFolderTopicTrafficRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folder_topic_traffic_rule', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->foreignId('folder_id')->constrained()->onDelete('cascade');
            $table->foreignId('topic_traffic_rule_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folder_topic_traffic_rule');
    }
}
