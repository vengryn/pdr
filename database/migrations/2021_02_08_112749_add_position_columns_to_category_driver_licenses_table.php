<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPositionColumnsToCategoryDriverLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_driver_licenses', function (Blueprint $table) {
            $table->integer('position_x')->nullable()->after('title');
            $table->integer('position_y')->nullable()->after('position_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_driver_licenses', function (Blueprint $table) {
            $table->dropColumn('position_x');
            $table->dropColumn('position_y');
        });
    }
}
