<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslateInternalTopicTrafficRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translate_internal_topic_traffic_rules', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('name')->nullable();

            $table->unsignedBigInteger('internal_topic_traffic_rule_id');
            $table->foreign('internal_topic_traffic_rule_id', 'internal_topic_traffic_rule_id_foreign')
                ->references('id')->on('internal_topic_traffic_rules')->onDelete('cascade');

            $table->foreignId('language_id')->constrained()->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translate_internal_topic_traffic_rules');
    }
}
