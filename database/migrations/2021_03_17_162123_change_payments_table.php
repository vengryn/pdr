<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign(['promo_code_id']);
            $table->dropColumn('promo_code_id');

            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->string('subscription')->nullable()->after('user_id');
            $table->integer('validity')->nullable()->after('subscription');
            $table->string('promo_code')->nullable()->after('validity');
            $table->integer('discount')->nullable()->after('promo_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('subscription');
            $table->dropColumn('validity');
            $table->dropColumn('promo_code');
            $table->dropColumn('discount');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->foreignId('promo_code_id')->nullable()->after('user_id')->constrained()->nullOnDelete();
            $table->foreignId('subscription_id')->nullable()->after('promo_code_id')->constrained()->nullOnDelete();
        });
    }
}
