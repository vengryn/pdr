<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePremiumColumnsToInfoStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_students', function (Blueprint $table) {
            Schema::table('info_students', function (Blueprint $table) {
                $table->dropColumn('premium_validity');
                $table->dropColumn('premium_at');
                $table->dropColumn('is_premium');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_students', function (Blueprint $table) {
            $table->boolean('is_premium')->default(0)->after('avatar');
            $table->dateTime('premium_at')->nullable()->after('is_premium');
            $table->integer('premium_validity')->nullable()->after('premium_at');
        });
    }
}
