<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePassageAtToWrongQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wrong_question_answers', function (Blueprint $table) {
            $table->renameColumn('passage_at', 'client_created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wrong_question_answers', function (Blueprint $table) {
            $table->renameColumn('client_created_at', 'passage_at');
        });
    }
}
