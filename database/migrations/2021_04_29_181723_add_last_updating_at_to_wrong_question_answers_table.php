<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastUpdatingAtToWrongQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wrong_question_answers', function (Blueprint $table) {
            $table->timestamp('last_updating_at')->nullable()->after('client_created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wrong_question_answers', function (Blueprint $table) {
            $table->dropColumn('last_updating_at');
        });
    }
}
