<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTopicRoadSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_road_signs', function (Blueprint $table) {
            $table->dropColumn('number');
            $table->integer('order')->default(0)->after('updated_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_road_signs', function (Blueprint $table) {
            $table->integer('number')->after('id');
            $table->dropColumn('order');
        });
    }
}
