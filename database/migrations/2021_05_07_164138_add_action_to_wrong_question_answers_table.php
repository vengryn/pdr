<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActionToWrongQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wrong_question_answers', function (Blueprint $table) {
            $table->boolean('action')->default(true)->after('last_updating_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wrong_question_answers', function (Blueprint $table) {
            $table->dropColumn('action');
        });
    }
}
