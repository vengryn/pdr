<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMarkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('markings', function (Blueprint $table) {
            $table->string('image_example')->nullable()->after('number');
            $table->string('video_example')->nullable()->after('image_example');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('markings', function (Blueprint $table) {
            $table->dropColumn('image_example');
            $table->dropColumn('video_example');
        });
    }
}
