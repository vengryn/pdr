<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePromoCodeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_codes', function (Blueprint $table) {
            $table->foreignId('user_id')->nullable()->after('date_end')
                ->constrained()->onDelete('cascade');
        });

        if (Schema::hasColumn('info_students', 'promo_code')) {
            Schema::table('info_students', function (Blueprint $table) {
                $table->dropColumn('promo_code');
            });
        }

        if (Schema::hasColumn('info_teachers', 'promo_code')) {
            Schema::table('info_teachers', function (Blueprint $table) {
                $table->dropColumn('promo_code');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_codes', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
    }
}
