<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCorrectQuestionsAnswerIdToQuestionAnswerHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('question_answer_histories', function (Blueprint $table) {
            $table->foreignId('correct_questions_answer_id')
                ->after('questions_answer_id')
                ->nullable()
                ->constrained('questions_answers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_answer_histories', function (Blueprint $table) {
            $table->dropForeign(['correct_questions_answer_id']);
            $table->dropColumn('correct_questions_answer_id');
        });
    }
}
