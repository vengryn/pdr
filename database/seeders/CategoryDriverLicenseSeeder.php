<?php

namespace Database\Seeders;

use App\Models\CategoryDriverLicense;
use Illuminate\Database\Seeder;

class CategoryDriverLicenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'A1',
            'A',
            'B1',
            'B',
            'C1',
            'C',
            'D1',
            'D',
            'C1E',
            'BE',
            'CE',
            'D1E',
            'DE',
            'T',
        ];

        foreach ($data as $value) {
            CategoryDriverLicense::updateOrCreate(['title' => $value], []);
        }
    }
}
