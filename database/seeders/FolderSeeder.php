<?php

namespace Database\Seeders;

use App\Models\Folder;
use Illuminate\Database\Seeder;

class FolderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Folder::query()->delete();
        for ($i = 1; $i <= 20; $i++) {
            Folder::create(['name' => $i]);
        }
    }
}
