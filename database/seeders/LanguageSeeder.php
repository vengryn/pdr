<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Language::first()) return;
        Language::create([
            'title'        => 'Українська',
            'abbreviation' => 'uk',
            'is_default'   => true,
            'order'        => 0
        ]);
    }
}
