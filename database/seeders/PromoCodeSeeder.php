<?php

namespace Database\Seeders;

use App\Models\PromoCode;
use Illuminate\Database\Seeder;

class PromoCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PromoCode::updateOrCreate([
            'type' => PromoCode::TYPE_TEACHER
        ]);

        PromoCode::updateOrCreate([
            'type' => PromoCode::TYPE_STUDENT
        ]);
    }
}
