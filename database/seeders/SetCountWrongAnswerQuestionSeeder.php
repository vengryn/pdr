<?php

namespace Database\Seeders;

use App\Helpers\UsefulFunctions;
use App\Models\QuestionAnswerHistory;
use Illuminate\Database\Seeder;

class SetCountWrongAnswerQuestionSeeder extends Seeder
{
    use UsefulFunctions;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questionAnswerHistories = QuestionAnswerHistory::with(['question'])
            ->where('is_passed', false)->get();

        foreach ($questionAnswerHistories as $answerHistory) {
            $answerHistory->question->increment('count_wrong_answer');
        }
    }
}
