<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class SetDefaultLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::where('abbreviation', config('app.locale'))->update([
            'is_default' => true,
            'order' => 0
        ]);

        Language::where('is_default', false)->each(function ($item, $key) {
            $item->order = $key + 1;
            $item->save();
        });
    }
}
