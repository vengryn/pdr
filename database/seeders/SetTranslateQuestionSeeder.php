<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SetTranslateQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultTimeLimit = ini_get('max_execution_time');
        set_time_limit(3600);

        try {
            $languages = DB::table('languages')
                ->where('is_default', false)
                ->whereNull('deleted_at')
                ->get();

            DB::table('questions')
                ->select('questions.id')
                ->get()
                ->each(function ($item) use ($languages) {
                    foreach ($languages as $lang) {
                        DB::table('translate_questions')
                            ->updateOrInsert(
                                ['question_id' => $item->id, 'language_id' => $lang->id],
                                [
                                    'name' => null,
                                    'explanation' => null
                                ]
                            );
                    }
                });

            DB::table('questions_answers')
                ->select('questions_answers.id')
                ->get()
                ->each(function ($item) use ($languages) {
                    foreach ($languages as $lang) {
                        DB::table('translate_questions_answers')
                            ->updateOrInsert(
                                ['answer_id' => $item->id, 'language_id' => $lang->id],
                                ['name' => null]
                            );
                    }
                });
        } catch (\Exception $exception) {
            set_time_limit($defaultTimeLimit);
        }

        set_time_limit($defaultTimeLimit);
    }
}
