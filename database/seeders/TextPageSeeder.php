<?php

namespace Database\Seeders;

use App\Helpers\UsefulFunctions;
use App\Models\Language;
use App\Models\TextPage;
use Illuminate\Database\Seeder;

class TextPageSeeder extends Seeder
{
    use UsefulFunctions;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultLanguage = Language::default()->first();

        $pages = [
            [
                'slug' => 'user_agreement',
                'name_default' => 'Користувацька угода',
            ]
        ];

        foreach ($pages as $page) {
            $textPage = TextPage::updateOrCreate(['slug' => $page['slug']], []);
            $this->saveTranslates($textPage, [
                'name' => [
                    $defaultLanguage->id => $page['name_default']
                ]
            ]);
        }
    }
}
