<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = DB::table('languages')->whereNull('deleted_at')->get();

        foreach ($languages as $lang) {
            $data[lcfirst($lang->abbreviation)] = trans('mobileApp', [], lcfirst($lang->abbreviation));
        }

        $keys = call_user_func_array('array_merge', $data);
        $keys = array_keys($keys);

        \App\Models\Translation::unsetEventDispatcher();

        foreach ($keys as $key) {
            $translationId = \App\Models\Translation::updateOrCreate(['key' => $key], [])->id;

            $trans = [];
            foreach ($languages as $lang) {
                $trans[] = [
                    'translation_id' => $translationId,
                    'language_id' => $lang->id,
                    'value' => $data[lcfirst($lang->abbreviation)][$key] ?? null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }

            DB::table('translate_translations')->insert($trans);
        }
    }
}
