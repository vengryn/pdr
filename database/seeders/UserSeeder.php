<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['email' => 'super.admin@testpdr.com'], [
            'password' => Hash::make('catch2021'),
            'role' => User::ROLE_SUPER_ADMINISTRATOR
        ]);

        User::updateOrCreate(['email' => 'admin@testpdr.com'], [
            'password' => Hash::make('catch2021'),
            'role' => User::ROLE_ADMINISTRATOR
        ]);

        User::updateOrCreate(['email' => 'pravpoka@ukr.net'], [
            'password' => Hash::make('#)bvyXp^J-8=a?W3'),
            'role' => User::ROLE_PAYMENT_REPORT
        ]);
    }
}
