(function ($) {
    function getPickerHTML() {
        var nodes = [];
        var categories = [
            { name: 'emotion',  symbol: 'grinning', unicode: '1F600' },
            { name: 'animal',   symbol: 'whale', unicode: '1F433' },
            { name: 'food',     symbol: 'hamburger', unicode: '1F354' },
            { name: 'thing',    symbol: 'kiss', unicode: '1F48B' },
            { name: 'travel',   symbol: 'rocket', unicode: '1F680' }
        ];
        var aliases = {
            'people':    'emotion',
            'symbol':    'thing',
            'undefined': 'thing'
        }
        var items = {};

        // Re-Sort Emoji table
        $.each(emojis, function(i, emoji) {
            var category = aliases[ emoji.category ] || emoji.category;
            items[ category ] = items[ category ] || [];
            items[ category ].push( emoji );
        });

        nodes.push('<nav>');
        for (var i in categories) {
            nodes.push('<div class="tab' +
                ( i == 0 ? ' active' : '' ) +
                '" data-tab="' +
                categories[i].name +
                '"><div class="emoji">' + toUnicode(categories[i].unicode) + '</div></div>');
        }
        nodes.push('</nav>');
        for (var i in categories) {
            nodes.push('<section class="' +
                categories[i].name +
                ( i == 0 ? '' : ' hidden' ) +
                '">');
            for (var j in items[ categories[i].name ] ) {
                var emoji = items[ categories[i].name ][ j ];
                nodes.push('<div class="emoji" data-code="' + emoji.unicode + '">' + toUnicode(emoji.unicode) + '</div>');
            }
            nodes.push('</section>');
        }
        return nodes.join("\n");
    }

    function toUnicode(code) {
        let codes = code.split('-').map(function(value, index) {
            return parseInt(value, 16);
        });
        return String.fromCodePoint.apply(null, codes);
    }

    $.fn.emojiList = function (editor, params) {
        let _this = $(this);
        let $button = $(editor.toolbarElements.smile);
        let $emojiBlock = _this.siblings('div[data-id="emoji"]');

        if ($emojiBlock.length) {
            if ($emojiBlock.css('display') !== 'none') {
                closeEmoji();
            } else {
                $button.addClass('active');
                $emojiBlock.show();
            }
        } else {
            $button.addClass('active');

            let defaults = {
                button: '&#x1F642;',
                place: 'before',
                fontSize: '20px',
                listCSS: {
                    position: 'absolute',
                    'background-color': '#fff',
                    'z-index': '1000',
                    transform: 'translate3d(0px, 50px, 0px)',
                    border: '1px solid rgba(0,0,0,.15)',
                    'border-radius': '.25rem',
                    'overflow-y': 'scroll',
                    height: '300px',
                    width: '310px',
                    left: $button.position().left,
                    display: 'block'
                },
                rowSize: 10,
            };

            let $list = $('<div class="emojiPicker" data-id="emoji">').css(defaults.listCSS).css(defaults.listCSS);

            $( getPickerHTML() ).appendTo( $list );

            $list.find('nav .tab').click( emojiCategoryClicked );
            $list.find('section div').click( clickEmoji );

            $list.insertAfter($(this));
        }

        function emojiCategoryClicked() {
            var $picker = $(this).parents('div[data-id="emoji"]');
            var section = '';

            // Update tab
            $picker.find('nav .tab').removeClass('active');
            if ($(this).parent().hasClass('tab')) {
                section = $(e.target).parent().attr('data-tab');
                $(this).parent('.tab').addClass('active');
            } else {
                section = $(this).attr('data-tab');
                $(this).addClass('active');
            }

            // Update section
            $picker.find('section').hide();
            $picker.find('section.' + section).show();

            $(document).off('click', closeEmoji);
        }

        function clickEmoji(ev) {
            let pos = editor.codemirror.getCursor();
            editor.codemirror.setSelection(pos, pos);
            editor.codemirror.replaceSelection("&#x" + $(ev.currentTarget).attr('data-code') + ";");

            closeEmoji();
        }

        function closeEmoji() {
            _this.siblings('div[data-id="emoji"]').hide();
            $button.removeClass('active');
        }

        $(document).mouseup( function(e){
            if ($button.is(e.target) || $button.has(e.target).length !== 0) {
                return false;
            }

            var div = _this.siblings('div[data-id="emoji"]');

            if ( !div.is(e.target) && div.has(e.target).length === 0 ) {
                closeEmoji();
            }
        });

        return this;
    };
}
)(jQuery);
