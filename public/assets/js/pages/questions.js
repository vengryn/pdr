/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/questions.js":
/*!*****************************************!*\
  !*** ./resources/js/pages/questions.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  $('a[data-action="copied"]').click(function (e) {
    e.preventDefault();
    var el = document.createElement('textarea');
    el.value = $(this).text();
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  });
  var $answersLanguages = $('div[data-name="questionsAnswers"]');
  $answersLanguages.on('click', '[data-action="remove"]', function (e) {
    if (confirm($(this).attr('data-message'))) {
      var parent = $(this).parents('.form-group');
      var number = parent.attr('data-number');

      if ($answersLanguages.first().find('div[data-number]').length === 1) {
        alert('Відповідь повинна містити мінімум один варіант відповіді');
        return false;
      }

      $answersLanguages.find('div[data-number="' + number + '"]').remove();
      regenerate($answersLanguages);
    }
  });
  $answersLanguages.find('[data-action="create"]').click(function (e) {
    e.preventDefault();
    $answersLanguages.each(function (index, answers) {
      var isDefault = $(answers).attr('data-default');
      var copy = $('#answer-clone>div').clone();

      if (!isDefault) {
        copy.find('button').remove();
        copy.find('input[type="radio"]').remove();
      }

      $(answers).find('div[data-name="items-answer"]').append(copy);
    });
    regenerate($answersLanguages);
  });
  $answersLanguages.on('click', '[name="answer_correct"]', function (e) {
    var type = $(this).attr('data-type');
    $('input[name="type_answer_correct"]').val(type);
  });
  $('div[role="tabpanel"]').each(function (index, tab) {
    if ($(tab).find('.is-invalid').length) {
      var tabId = $(tab).attr('id');
      $('ul[role="tablist"]').find('a[href="#' + tabId + '"]').click();
      return false;
    }
  });
  $('div[role="tabpanel"]').each(function (index, tab) {
    if ($(tab).find('.is-invalid').length) {
      $(tab).find('div[data-type="error"]').removeClass('hide').addClass('show');
    }
  });
});

function regenerate(answersLanguages) {
  answersLanguages.each(function (index, answers) {
    var language = $(answers).attr('data-language');

    if ($(answers).attr('data-default')) {
      $(answers).find('div[data-number]').find('required').text('*');
    }

    $(answers).find('div[data-number]').each(function (index, answer) {
      var number = index + 1;
      $(answer).attr('data-number', index + 1);
      $(answer).find('label span').text(number);
      $(answer).find('label').attr('for', 'answer_' + number);
      $(answer).find('input[type="radio').attr('id', 'answer_' + number);
    });
    $(answers).find('div[data-number][data-internal="true"]').each(function (index, answer) {
      var number = index + 1;
      $(answer).find('input[type="radio').attr('value', number).attr('data-type', 'added_answers');
      $(answer).find('textarea').attr('name', 'added_answers[' + number + '][translate][name][' + language + ']');
    });
  });

  if (!answersLanguages.find('[name="answer_correct"]:checked').length) {
    var firstRadioButton = answersLanguages.find('[name="answer_correct"]').first();

    if (firstRadioButton.length) {
      $(firstRadioButton).prop('checked', true);
    }
  }
}

/***/ }),

/***/ 11:
/*!***********************************************!*\
  !*** multi ./resources/js/pages/questions.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /usr/local/var/www/pdr.loc/resources/js/pages/questions.js */"./resources/js/pages/questions.js");


/***/ })

/******/ });