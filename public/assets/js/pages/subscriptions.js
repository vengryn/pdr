/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/subscriptions.js":
/*!*********************************************!*\
  !*** ./resources/js/pages/subscriptions.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  var $advantagesLanguages = $('div[data-name="advantages"]');
  $advantagesLanguages.on('click', '[data-action="remove"]', function (e) {
    if (confirm($(this).attr('data-message'))) {
      var parent = $(this).parents('.form-group');
      var number = parent.attr('data-number');

      if ($advantagesLanguages.first().find('div[data-number]').length === 1) {
        alert('Підписка повинна містити мінімум один варіант переваги');
        return false;
      }

      $advantagesLanguages.find('div[data-number="' + number + '"]').remove();
      regenerate($advantagesLanguages);
    }
  });
  $advantagesLanguages.find('[data-action="create"]').click(function (e) {
    e.preventDefault();
    $advantagesLanguages.each(function (index, answers) {
      var isDefault = $(answers).attr('data-default');
      var copy = $('#advantage-clone>div').clone();

      if (!isDefault) {
        copy.find('button').remove();
        copy.find('input[type="radio"]').remove();
      }

      $(answers).find('div[data-name="items-answer"]').append(copy);
    });
    regenerate($advantagesLanguages);
  });
});

function regenerate(advantagesLanguages) {
  advantagesLanguages.each(function (index, advantages) {
    var language = $(advantages).attr('data-language');
    $(advantages).find('div[data-number]').each(function (index, advantage) {
      var number = index + 1;
      $(advantage).attr('data-number', number);

      if (number === 1) {
        $(advantage).find('label').html('Переваги <required></required>');
      }

      if (number > 1) {
        $(advantage).find('label').empty();
      }
    });

    if ($(advantages).attr('data-default')) {
      $(advantages).find('div[data-number]').find('required').text('*');
    }

    $(advantages).find('div[data-number]').each(function (index, advantage) {
      $(advantage).find('textarea').attr('name', 'translate[advantages][' + language + '][]');
    });
  });
}

/***/ }),

/***/ 12:
/*!***************************************************!*\
  !*** multi ./resources/js/pages/subscriptions.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /usr/local/var/www/pdr.loc/resources/js/pages/subscriptions.js */"./resources/js/pages/subscriptions.js");


/***/ })

/******/ });