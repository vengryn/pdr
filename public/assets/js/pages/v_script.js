/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/v_script.js":
/*!****************************************!*\
  !*** ./resources/js/pages/v_script.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('[data-action="show-password"]').click(function () {
    $(this).find('i').toggleClass("mdi-eye mdi-eye-off");
    var input = $('[name="' + $(this).attr("data-toggle") + '"]');
    input.attr("type") === "password" ? input.attr("type", "text") : input.attr("type", "password");
  });

  if ($(document).find('#pagination').length > 0) {
    document.getElementById('pagination').onchange = function () {
      var queryParams = new URLSearchParams(window.location.search);
      queryParams.set("paginate", this.value);
      queryParams["delete"]("page");
      window.location.search = queryParams;
    };
  }

  initMagnificPopup();
  $('input[name="uploadMarkdownImg"]').change(function () {
    var maxFileSize = 5242880; // 5 MB

    var form_data = new FormData();
    var len_files = $(this).prop("files").length;

    for (var i = 0; i < len_files; i++) {
      var file_data = $(this).prop("files")[i];

      if (file_data.size > maxFileSize) {
        alert('Вибраний файл зображення завеликий. Будь ласка, виберіть той, розмір якого менше 5 МБ.');
      } else {
        form_data.append(i, file_data);
      }
    }

    $.ajax({
      url: "/image-upload/markdown-editor",
      type: 'post',
      contentType: false,
      processData: false,
      data: form_data,
      success: function success(data) {
        $.each(data, function (index, value) {
          $('#sendForm').append('<input type="hidden" name="markdownImages[]" value="' + value.path + '" />');
          $('div[data-type="markdownImages"] table tbody').append('<tr>' + '<td>' + '<a href="' + value.link + '" class="btn btn btn-secondary btn-sm markdown-preview">' + '<i class="mdi mdi-image-outline"></i>' + '</a>' + '</td>' + '<td style="vertical-align: middle;">' + value.name + '</td>' + '<td style="text-align: right;">' + '<div class="btn-group">' + '<a href="#" class="btn btn-secondary btn-sm" title="Скопіювати посилання на зображення" data-action="copied" data-value="' + value.link + '">' + '<i class="mdi mdi-link-variant"></i>' + '</a>' + '<a href="#" class="btn btn-danger btn-sm" title="Видалити зображення" data-action="remove-markdown-images" data-path="' + value.path + '">' + '<i class="mdi mdi-delete"></i>' + '</a>' + '</div>' + '</td>' + '</tr>');
        });
        initMagnificPopup();
      }
    });
    $(this).val(null);
  });
  $('div[data-type="markdownImages"]').on('click', 'a[data-action="remove-markdown-images"]', function (e) {
    e.preventDefault();

    if (confirm('Ви дійсно бажаєте видалити зображення?')) {
      var _this = $(this);

      var id = _this.attr('data-id');

      if (id !== undefined) {
        $.ajax({
          type: 'DELETE',
          url: '/image-upload/markdown-editor/' + id
        }).done(function () {
          _this.parents('tr').remove();
        });
      } else {
        var path = _this.attr('data-path');

        $.ajax({
          type: 'POST',
          dataType: "json",
          data: {
            path: path
          },
          url: '/image-upload/remove-markdown-editor'
        }).done(function (data) {
          $('#sendForm').find('input[name="markdownImages[]"][value="' + data.path + '"]').remove();

          _this.parents('tr').remove();
        });
      }
    }
  }).on('click', 'a[data-action="remove-duplicate-markdown-images"]', function (e) {
    e.preventDefault();

    if (confirm('Ви дійсно бажаєте видалити зображення?')) {
      var _this = $(this);

      var path = _this.attr('data-path');

      _this.parents('tr').remove();

      $('#sendForm').find('input[name="markdownDuplicateImages[]"][value="' + path + '"]').remove();
    }
  });
  $('div[data-type="markdownImages"] table tbody').on('click', 'a[data-action="copied"]', function (e) {
    e.preventDefault();
    var el = document.createElement('textarea');
    el.value = $(this).attr('data-value');
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  });

  function initMagnificPopup() {
    var $markdownPreview = $('.markdown-preview');

    if ($markdownPreview.length) {
      $markdownPreview.magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-fade'
      });
    }
  }

  $('a[data-action="removeImg"]').click(function (e) {
    e.preventDefault();
    var previewBlock = $(this).parents('div[data-id="img-preview"]');
    var title = $(this).attr('data-title');
    var disabledName = $(this).attr('data-disabled');

    if (confirm(title)) {
      previewBlock.find('input[name="' + disabledName + '"]').prop('disabled', true);
      previewBlock.find('div[data-id="img-content"]').hide();
      previewBlock.find('div[data-id="img-upload"]').show();
    }
  });
});

/***/ }),

/***/ 10:
/*!**********************************************!*\
  !*** multi ./resources/js/pages/v_script.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /usr/local/var/www/pdr.loc/resources/js/pages/v_script.js */"./resources/js/pages/v_script.js");


/***/ })

/******/ });