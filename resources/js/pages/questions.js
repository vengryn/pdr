$( function() {
    $('a[data-action="copied"]').click(function (e) {
        e.preventDefault();
        const el = document.createElement('textarea');

        el.value = $(this).text();
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    });

    let $answersLanguages = $('div[data-name="questionsAnswers"]');

    $answersLanguages.on('click', '[data-action="remove"]', function (e) {
        if (confirm($(this).attr('data-message'))) {
            let parent = $(this).parents('.form-group');
            let number = parent.attr('data-number');

            if ($answersLanguages.first().find('div[data-number]').length === 1) {
                alert('Відповідь повинна містити мінімум один варіант відповіді');
                return false;
            }

            $answersLanguages.find('div[data-number="' + number + '"]').remove();
            regenerate($answersLanguages);
        }
    });

    $answersLanguages.find('[data-action="create"]').click(function (e) {
        e.preventDefault();

        $answersLanguages.each(function ( index, answers ) {
            let isDefault = $(answers).attr('data-default');
            let copy = $('#answer-clone>div').clone();

            if (! isDefault) {
                copy.find('button').remove();
                copy.find('input[type="radio"]').remove();
            }

            $(answers).find('div[data-name="items-answer"]').append(copy);
        });

        regenerate($answersLanguages);
    });

    $answersLanguages.on('click', '[name="answer_correct"]', function (e) {
        let type = $(this).attr('data-type');
        $('input[name="type_answer_correct"]').val(type);
    });

    $('div[role="tabpanel"]').each(function( index, tab ) {
        if ($(tab).find('.is-invalid').length) {
            let tabId = $(tab).attr('id');
            $('ul[role="tablist"]').find('a[href="#' +tabId+ '"]').click();
            return false;
        }
    });

    $('div[role="tabpanel"]').each(function( index, tab ) {
        if ($(tab).find('.is-invalid').length) {
            $(tab).find('div[data-type="error"]').removeClass('hide').addClass('show');
        }
    });

});

function regenerate(answersLanguages) {
    answersLanguages.each(function( index, answers ) {
        let language = $(answers).attr('data-language');

        if ($(answers).attr('data-default')) {
            $(answers).find('div[data-number]').find('required').text('*');
        }

        $(answers).find('div[data-number]').each(function ( index, answer ) {
            let number = index + 1;

            $(answer).attr('data-number', index + 1);
            $(answer).find('label span').text(number);
            $(answer).find('label').attr('for', 'answer_' + number);
            $(answer).find('input[type="radio').attr('id', 'answer_' + number);
        });

        $(answers).find('div[data-number][data-internal="true"]').each(function ( index, answer ) {
            let number = index + 1;

            $(answer).find('input[type="radio').attr('value', number).attr('data-type', 'added_answers');
            $(answer).find('textarea').attr('name', 'added_answers[' +number+ '][translate][name][' +language+ ']');
        });

    });

    if (! answersLanguages.find('[name="answer_correct"]:checked').length) {
        let firstRadioButton = answersLanguages.find('[name="answer_correct"]').first();

        if (firstRadioButton.length) {
            $(firstRadioButton).prop('checked', true);
        }
    }
}
