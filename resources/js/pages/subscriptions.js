$( function() {
    let $advantagesLanguages = $('div[data-name="advantages"]');

    $advantagesLanguages.on('click', '[data-action="remove"]', function (e) {
        if (confirm($(this).attr('data-message'))) {
            let parent = $(this).parents('.form-group');
            let number = parent.attr('data-number');

            if ($advantagesLanguages.first().find('div[data-number]').length === 1) {
                alert('Підписка повинна містити мінімум один варіант переваги');
                return false;
            }

            $advantagesLanguages.find('div[data-number="' + number + '"]').remove();
            regenerate($advantagesLanguages);
        }
    });

    $advantagesLanguages.find('[data-action="create"]').click(function (e) {
        e.preventDefault();

        $advantagesLanguages.each(function ( index, answers ) {
            let isDefault = $(answers).attr('data-default');
            let copy = $('#advantage-clone>div').clone();

            if (! isDefault) {
                copy.find('button').remove();
                copy.find('input[type="radio"]').remove();
            }

            $(answers).find('div[data-name="items-answer"]').append(copy);
        });

        regenerate($advantagesLanguages);
    });
});

function regenerate(advantagesLanguages) {
    advantagesLanguages.each(function( index, advantages ) {
        let language = $(advantages).attr('data-language');

        $(advantages).find('div[data-number]').each(function ( index, advantage ) {
            let number = index + 1;
            $(advantage).attr('data-number', number);

            if (number === 1) {
                $(advantage).find('label').html('Переваги <required></required>');
            }

            if (number > 1) {
                $(advantage).find('label').empty();
            }
        });

        if ($(advantages).attr('data-default')) {
            $(advantages).find('div[data-number]').find('required').text('*');
        }

        $(advantages).find('div[data-number]').each(function ( index, advantage ) {
            $(advantage).find('textarea').attr('name', 'translate[advantages][' +language+ '][]');
        });
    });
}
