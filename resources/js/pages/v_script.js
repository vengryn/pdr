$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('[data-action="show-password"]').click(function () {
        $(this).find('i').toggleClass("mdi-eye mdi-eye-off");
        let input = $('[name="' + $(this).attr("data-toggle") + '"]');

        input.attr("type") === "password" ?
            input.attr("type", "text") :
            input.attr("type", "password");
    });

    if ($(document).find('#pagination').length > 0) {
        document.getElementById('pagination').onchange = function () {
            let queryParams = new URLSearchParams(window.location.search);
            queryParams.set("paginate", this.value);
            queryParams.delete("page");

            window.location.search = queryParams;
        };
    }

    initMagnificPopup();

    $('input[name="uploadMarkdownImg"]').change(function () {
        const maxFileSize = 5242880; // 5 MB

        let form_data = new FormData();
        let len_files = $(this).prop("files").length;

        for (let i = 0; i < len_files; i++) {
            let file_data = $(this).prop("files")[i];

            if (file_data.size > maxFileSize) {
                alert('Вибраний файл зображення завеликий. Будь ласка, виберіть той, розмір якого менше 5 МБ.');
            } else {
                form_data.append(i, file_data);
            }
        }

        $.ajax({
            url: "/image-upload/markdown-editor",
            type: 'post',
            contentType: false,
            processData: false,
            data: form_data,
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#sendForm').append('<input type="hidden" name="markdownImages[]" value="' + value.path + '" />');

                    $('div[data-type="markdownImages"] table tbody').append('<tr>' +
                        '<td>' +
                        '<a href="' + value.link + '" class="btn btn btn-secondary btn-sm markdown-preview">' +
                        '<i class="mdi mdi-image-outline"></i>' +
                        '</a>' +
                        '</td>' +
                        '<td style="vertical-align: middle;">' + value.name + '</td>' +
                        '<td style="text-align: right;">' +
                        '<div class="btn-group">' +
                        '<a href="#" class="btn btn-secondary btn-sm" title="Скопіювати посилання на зображення" data-action="copied" data-value="' + value.link + '">' +
                        '<i class="mdi mdi-link-variant"></i>' +
                        '</a>' +
                        '<a href="#" class="btn btn-danger btn-sm" title="Видалити зображення" data-action="remove-markdown-images" data-path="' + value.path + '">' +
                        '<i class="mdi mdi-delete"></i>' +
                        '</a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                initMagnificPopup();
            }
        });

        $(this).val(null);
    });

    $('div[data-type="markdownImages"]').on('click', 'a[data-action="remove-markdown-images"]', function (e) {
        e.preventDefault();
        if (confirm('Ви дійсно бажаєте видалити зображення?')) {
            let _this = $(this);
            let id = _this.attr('data-id');

            if (id !== undefined) {
                $.ajax({
                    type: 'DELETE',
                    url: '/image-upload/markdown-editor/' + id,
                }).done(function () {
                    _this.parents('tr').remove();
                });
            } else {
                let path = _this.attr('data-path');

                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    data: {path: path},
                    url: '/image-upload/remove-markdown-editor',
                }).done(function (data) {
                    $('#sendForm').find('input[name="markdownImages[]"][value="' + data.path + '"]').remove();
                    _this.parents('tr').remove();
                });
            }
        }
    }).on('click', 'a[data-action="remove-duplicate-markdown-images"]', function (e) {
        e.preventDefault();
        if (confirm('Ви дійсно бажаєте видалити зображення?')) {
            let _this = $(this);
            let path = _this.attr('data-path');

            _this.parents('tr').remove();
            $('#sendForm').find('input[name="markdownDuplicateImages[]"][value="' + path + '"]').remove();
        }
    });

    $('div[data-type="markdownImages"] table tbody').on('click', 'a[data-action="copied"]', function (e) {
        e.preventDefault();
        const el = document.createElement('textarea');

        el.value = $(this).attr('data-value');
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    });

    function initMagnificPopup() {
        let $markdownPreview = $('.markdown-preview');

        if ($markdownPreview.length) {
            $markdownPreview.magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-fade'
            });
        }
    }

    $('a[data-action="removeImg"]').click(function (e) {
        e.preventDefault();

        let previewBlock = $(this).parents('div[data-id="img-preview"]');
        let title = $(this).attr('data-title');
        let disabledName = $(this).attr('data-disabled');

        if (confirm(title)) {
            previewBlock.find('input[name="'+disabledName+'"]').prop('disabled', true);
            previewBlock.find('div[data-id="img-content"]').hide();
            previewBlock.find('div[data-id="img-upload"]').show();
        }
    });
});
