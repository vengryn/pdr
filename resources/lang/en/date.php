<?php

return [
    'month' => [
        'January' => 'january',
        'February' => 'february',
        'March' => 'march',
        'April' => 'april',
        'May' => 'may',
        'June' => 'june',
        'July' => 'july',
        'August' => 'august',
        'September' => 'september',
        'October' => 'october',
        'November' => 'november',
        'December' => 'december',
    ]
];
