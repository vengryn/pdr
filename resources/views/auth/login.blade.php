@extends('layouts.master-without-nav')

@section('title') @lang('auth.login') @endsection

@section('body')
    <body>
@endsection

@section('content')
    <div class="account-pages my-5 pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-primary">
                            <div class="text-primary text-center p-3">
                                <h5 class="text-white font-size-20 p-2">@lang('auth.login')</h5>
                            </div>
                        </div>

                        <div class="card-body p-2">
                            <div class="p-3">
                            <form class="form-horizontal mt-4" method="POST" action="{{ route('login') }}">
                                @csrf
                                    <div class="form-group">
                                        <label for="username">@lang('translation.email')</label>
                                        <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" id="username"
                                               placeholder="@lang('translation.email')" autocomplete="email" autofocus>
                                        @error('email')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="userpassword">@lang('translation.password')</label>
                                        <div class="input-group">
                                            <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" id="userpassword"
                                                   placeholder="@lang('translation.password')">
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary btn-sm" data-toggle="password" data-action="show-password" type="button"><i class="mdi mdi-eye"></i></button>
                                            </div>
                                            @error('password')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customControlInline">
                                                <label class="custom-control-label" for="customControlInline">@lang('translation.remember_me')</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">@lang('auth.login')</button>
                                        </div>
                                    </div>

                                    <div class="form-group mt-2 mb-0 row">
                                        <div class="col-12 mt-4">
                                            <a href="{{ route('password.request') }}"> <i class="mdi mdi-lock"></i>@lang('translation.forgot_password')</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="mt-5 text-center">
                        @if(request()->getHost() == 'teacher.' . env('APP_DOMAIN_NAME'))
                        <p>Не маєте облікового запису ? <a href="{{ route('register') }}"
                                                           class="font-weight-medium text-primary">Зареєструватися</a> </p>
                        @endif
                        <p class="mb-0">© <script>
                                document.write(new Date().getFullYear())
                            </script> "@lang('translation.project_name')"</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
