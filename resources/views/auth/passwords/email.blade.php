@extends('layouts.master-without-nav')

@section('title') @lang('passwords.reset_password') @endsection

@section('body')
    <body>
@endsection

    @section('content')
        <div class="account-pages my-5 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="bg-primary">
                                <div class="text-primary text-center p-3">
                                    <h5 class="text-white font-size-20 p-2">@lang('passwords.reset_password')</h5>
                                </div>
                            </div>

                            <div class="card-body p-4">

                                <div class="p-3">

                                    @if (session('status'))
                                        <div class="alert alert-success mt-5" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif


                                    <form method="POST" action="{{ route('password.email') }}">
                                        @csrf

                                        <div class="form-group">
                                            <label for="email">@lang('translation.email')</label>
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror" name="email"
                                                value="{{ old('email') }}" required autocomplete="email" autofocus
                                                placeholder="@lang('translation.email')">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-12 text-right">
                                                <button type="submit" class="btn btn-primary w-md waves-effect waves-light">
                                                    @lang('translation.send_password_reset_link')
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-group mt-2 mb-0 row">
                                            <div class="col-12 mt-4">
                                                <a href="{{ route('login') }}"> <i class="mdi mdi-login"></i>@lang('translation.sign_in_here')</a>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>

                        <div class="mt-5 text-center">
                            <p class="mb-0">© <script>
                                    document.write(new Date().getFullYear())
                                </script> "@lang('translation.project_name')"</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endsection
