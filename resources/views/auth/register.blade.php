@extends('layouts.master-without-nav')

@section('title') @lang('auth.register') @endsection

@section('body')
    <body>
@endsection

@section('content')
    <div class="account-pages my-5 pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-primary">
                            <div class="text-primary text-center p-3">
                                <h5 class="text-white font-size-20 p-2">@lang('auth.register')</h5>
                            </div>
                        </div>

                        <div class="card-body p-2">
                            <div class="p-3">
                                <form class="form-horizontal mt-4" method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="form-group">
                                        <label for="last_name">Прізвище *</label>
                                        <input name="last_name" type="text"
                                               class="form-control @error('last_name') is-invalid @enderror"
                                               value="{{ old('last_name') }}" id="last_name"
                                               placeholder="Прізвище" autocomplete="off" autofocus>
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="first_name">Ім'я *</label>
                                        <input name="first_name" type="text"
                                               class="form-control @error('first_name') is-invalid @enderror"
                                               value="{{ old('first_name') }}" id="first_name"
                                               placeholder="Ім'я" autocomplete="off" autofocus>
                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="patronymic">По-батькові *</label>
                                        <input name="patronymic" type="text"
                                               class="form-control @error('patronymic') is-invalid @enderror"
                                               value="{{ old('patronymic') }}" id="patronymic"
                                               placeholder="По-батькові" autocomplete="off" autofocus>
                                        @error('patronymic')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="email">@lang('translation.email') *</label>
                                        <input name="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               value="{{ old('email') }}" id="email"
                                               placeholder="@lang('translation.email')" autocomplete="off" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Телефон *</label>
                                        <input name="phone" type="text"
                                               class="form-control input-mask @error('phone') is-invalid @enderror"
                                               value="{{ old('phone') }}" id="username" data-inputmask="'mask': '+380999999999'"
                                               placeholder="Телефон" autocomplete="off" autofocus>
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="userpassword">@lang('translation.password') *</label>
                                        <div class="input-group">
                                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                                                   id="userpassword" placeholder="@lang('translation.password')">
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary btn-sm" data-toggle="password" data-action="show-password" type="button"><i class="mdi mdi-eye"></i></button>
                                            </div>
                                            @error('password')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="userpassword_confirmation">Пiдтвердити Пароль *</label>

                                        <div class="input-group">
                                            <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror"
                                                   id="userpassword_confirmation" placeholder="Пiдтвердити Пароль">
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary btn-sm" data-toggle="password_confirmation" data-action="show-password" type="button"><i class="mdi mdi-eye"></i></button>
                                            </div>
                                            @error('password_confirmation')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12 text-right">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Зареєструватися</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="mt-5 text-center">
                        <p>Вже є аккаунт ? <a href="{{ route('login') }}" class="font-weight-medium text-primary">Вхід</a> </p>
                        <p class="mb-0">©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            "@lang('translation.project_name')"
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- form mask -->
    <script src="{{ URL::asset('/assets/libs/inputmask/inputmask.min.js') }}"></script>
    <!-- form mask js -->
    <script src="{{ URL::asset('/assets/js/pages/form-mask.init.js') }}"></script>
@endsection
