@extends('layouts.master')

@section('title') @lang('translation.auto_premium') @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.auto_premium')</h4>
                {{ Breadcrumbs::render('auto_premium') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('auto-premium.update') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="switch" class="col-sm-3">Ввiмкнути "Авто-Премiум"</label>
                            <div class="col-sm-5">
                                <input type="checkbox" id="switch" switch="none" value="1"
                                       name="is_auto_premium" {{ old('is_auto_premium', $isAutoPremium) ? 'checked' : '' }}/>
                                <label for="switch"></label>
                            </div>
                        </div>

                        <div data-name="auto-premium-date" class="form-group row" style="display: {{ old('is_auto_premium', $isAutoPremium) ? 'flex' : 'none' }};">
                            <label for="auto-premium-days" class="col-sm-3 col-form-label">Кількість Днів "Авто-Преміум"</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="number" min="1" max="999" class="form-control @error('auto_premium_days')is-invalid @enderror" id="auto-premium-days" name="auto_premium_days"
                                           value="{{ old('auto_premium_days', $settings['auto_premium_days']) }}">
                                    @error('auto_premium_days')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div><!-- input-group -->
                            </div>
                        </div>

                        <div data-name="auto-premium-date" class="form-group row" style="display: {{ old('is_auto_premium', $isAutoPremium) ? 'flex' : 'none' }};">
                            <label for="auto_premium_paid_video" class="col-sm-3">Бачить платні відео</label>
                            <div class="col-sm-5">
                                <input type="checkbox" id="auto_premium_paid_video" switch="none" value="1"
                                       name="auto_premium_paid_video" {{ old('auto_premium_paid_video', $settings['auto_premium_paid_video']) ? 'checked' : '' }}/>
                                <label for="auto_premium_paid_video"></label>
                                @error('auto_premium_paid_video')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            @if($isAutoPremium)
                            <p class="font-weight-bold col-sm-3">Ввiмкнуто: </p>
                            <p class="col-sm-3">{{ $settings['auto_premium_date_start'] }}</p>
                            @else
                            <p class="font-weight-bold col-sm-3">Вимкнуто: </p>
                            <p class="col-sm-3">{{ $offDate->updated_at ?? '' }}</p>
                            @endif
                        </div>

                        <div class="row">
                            <p class="font-weight-bold col-sm-3">Останне Збереження: </p>
                            <p class="col-sm-3">{{ $lastUpdate->updated_at ?? '' }}</p>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script')
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.uk.min.js') }}"></script>
    <script>
        $(function () {
            $('#auto-premium-end').datepicker({
                language: 'uk',
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date()
            });

            $('input[name="auto_premium_days"]').keypress(function (e) {
                let charCode = (e.which) ? e.which : event.keyCode

                if (String.fromCharCode(charCode).match(/[^0-9]/g))
                    return false;
            });

            $('input[name="is_auto_premium"]').change(function () {
                if ($(this).is(':checked')) {
                    $('div[data-name="auto-premium-date"]').show();
                } else {
                    $('div[data-name="auto-premium-date"]').hide();
                }
            });
        });
    </script>
@endsection
