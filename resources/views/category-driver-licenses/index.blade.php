@extends('layouts.master')

@section('title') @lang('translation.categories') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.categories')</h4>
                {{ Breadcrumbs::render('category_driver_licenses') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <form action="{{ route('category-driver-licenses.store') }}" method="POST">
                                @csrf
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                        <tr>
                                            <th width="200">№</th>
                                            <th width="200">Назва</th>
                                            <th>X</th>
                                            <th>Y</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($categoryDriverLicenses as $categoryDriverLicense)
                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $categoryDriverLicense->title }}</td>
                                                <td>
                                                    <input type="number" class="form-control" max="5" min="1" style="width: 100px;"
                                                           name="categories[{{ $categoryDriverLicense->id }}][position_x]"
                                                           value="{{ old("categories.{$categoryDriverLicense->id}.position_x") ?? $categoryDriverLicense->position_x }}">
                                                    @error("categories.{$categoryDriverLicense->id}.position_x")<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control" max="4" min="1" style="width: 100px;"
                                                           name="categories[{{ $categoryDriverLicense->id }}][position_y]"
                                                           value="{{ old("categories.{$categoryDriverLicense->id}.position_y") ?? $categoryDriverLicense->position_y }}">
                                                    @error("categories.{$categoryDriverLicense->id}.position_y")<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div>
                                    <button type="submit" name="action" value="save"
                                            class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                    <a href="{{ route('category-driver-licenses.index') }}"
                                       class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
