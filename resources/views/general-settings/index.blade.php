@extends('layouts.master')

@section('title') @lang('translation.general_settings') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.general_settings')</h4>
                {{ Breadcrumbs::render('general-settings') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="alert alert-warning alert-dismissible fade hide" role="alert" id="save-api-key">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Для збереження ключа, натисніть кнопку "Зберегти".
            </div>

            <div class="card">
                <div class="card-body">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#general" role="tab">
                                <span class="d-none d-sm-block">Загальні</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#cdn" role="tab">
                                <span class="d-none d-sm-block">CDN</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#payments" role="tab">
                                <span class="d-none d-sm-block">Платежi</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#chat-bot" role="tab">
                                <span class="d-none d-sm-block">Чат-бот</span>
                            </a>
                        </li>
                    </ul>

                    <form method="POST" action="{{ route('general-settings.store') }}">
                        @csrf

                        <div class="tab-content">
                            <div class="tab-pane p-3 active" id="general" role="tabpanel">
                                <div class="form-group row">
                                    <label for="contact_email" class="col-sm-2 col-form-label">Контактний емейл</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('contact_email')is-invalid @enderror"
                                               type="text" name="contact_email"
                                               value="{{ old('contact_email', $setting['contact_email']) }}"
                                               id="contact_email">
                                        @error('contact_email')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="privacy_policy_page" class="col-sm-2 col-form-label">Сторінка
                                        Користувацької угоди</label>
                                    <div class="col-sm-5">
                                        <select
                                            class="form-control select2 @error('privacy_policy_page')is-invalid @enderror"
                                            name="privacy_policy_page"
                                            data-placeholder="Сторінка Користувацької угоди">
                                            <option></option>
                                            @foreach($textPages as $textPage)
                                                <option
                                                    value="{{ $textPage->id }}" {{ old('privacy_policy_page', $setting['privacy_policy_page']) == $textPage->id ? 'selected' : '' }}>
                                                    {{ $textPage->getTranslate()->name ?? '' }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('privacy_policy_page')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="google_play_link" class="col-sm-2 col-form-label">Google Play
                                        link</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('google_play_link')is-invalid @enderror"
                                               type="text" name="google_play_link"
                                               value="{{ old('google_play_link', $setting['google_play_link']) }}"
                                               id="google_play_link">
                                        @error('google_play_link')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="app_store_link" class="col-sm-2 col-form-label">App Store link</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('app_store_link')is-invalid @enderror"
                                               type="text" name="app_store_link"
                                               value="{{ old('app_store_link', $setting['app_store_link']) }}"
                                               id="app_store_link">
                                        @error('app_store_link')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="contact_email" class="col-sm-2 col-form-label">Дата останніх змін
                                        питань</label>
                                    <div class="col-sm-2">
                                        <p style="padding-top: 5px;">
                                            {{ $setting['date_of_last_response_file_generation'] ? \Carbon\Carbon::createFromTimestamp($setting['date_of_last_response_file_generation'])->format('d.m.Y H:i:s') : '' }}
                                        </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="{{ route('sync-api-questions') }}"
                                           class="btn btn-primary waves-effect waves-light">Оновити дату останніх змін
                                            питань</a>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="contact_email" class="col-sm-2 col-form-label">Дата останніх змін
                                        теорії</label>
                                    <div class="col-sm-2">
                                        <p style="padding-top: 5px;">
                                            {{ $setting['date_of_last_response_theory_file_generation'] ? \Carbon\Carbon::createFromTimestamp($setting['date_of_last_response_theory_file_generation'])->format('d.m.Y H:i:s') : '' }}
                                        </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="{{ route('sync-api-theory') }}"
                                           class="btn btn-primary waves-effect waves-light">Оновити дату останніх змін
                                            теорії</a>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p-3" id="cdn" role="tabpanel">
                                <div class="form-group row">
                                    <label for="digitalocean_key" class="col-sm-2 col-form-label">Key</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('digitalocean.key')is-invalid @enderror"
                                               type="text" name="digitalocean[key]"
                                               value="{{ old('digitalocean.key', $setting['digitalocean']['key'] ?? null) }}"
                                               id="digitalocean_key">
                                        @error('digitalocean.key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="digitalocean_key_secret" class="col-sm-2 col-form-label">Key
                                        Secret</label>
                                    <div class="col-sm-5">
                                        <input
                                            class="form-control @error('digitalocean.key_secret')is-invalid @enderror"
                                            type="text" name="digitalocean[key_secret]"
                                            value="{{ old('digitalocean.key_secret', $setting['digitalocean']['key_secret'] ?? null) }}"
                                            id="digitalocean_key_secret">
                                        @error('digitalocean.key_secret')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="digitalocean_endpoint" class="col-sm-2 col-form-label">Endpoint</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('digitalocean.endpoint')is-invalid @enderror"
                                               type="text" name="digitalocean[endpoint]"
                                               value="{{ old('digitalocean.endpoint', $setting['digitalocean']['endpoint'] ?? null) }}"
                                               id="digitalocean_endpoint">
                                        @error('digitalocean.endpoint')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="digitalocean_region" class="col-sm-2 col-form-label">Region</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('digitalocean.region')is-invalid @enderror"
                                               type="text" name="digitalocean[region]"
                                               value="{{ old('digitalocean.region', $setting['digitalocean']['region'] ?? null) }}"
                                               id="digitalocean_region">
                                        @error('digitalocean.region')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="digitalocean_bucket" class="col-sm-2 col-form-label">Bucket</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('digitalocean.bucket')is-invalid @enderror"
                                               type="text" name="digitalocean[bucket]"
                                               value="{{ old('digitalocean.bucket', $setting['digitalocean']['bucket'] ?? null) }}"
                                               id="digitalocean_bucket">
                                        @error('digitalocean.bucket')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="digitalocean_question_path" class="col-sm-2 col-form-label">Question
                                        Path</label>
                                    <div class="col-sm-5">
                                        <input
                                            class="form-control @error('digitalocean.question_path')is-invalid @enderror"
                                            type="text" name="digitalocean[question_path]"
                                            value="{{ old('digitalocean.question_path', $setting['digitalocean']['question_path'] ?? null) }}"
                                            id="digitalocean_question_path">
                                        @error('digitalocean.question_path')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="digitalocean_theory_path" class="col-sm-2 col-form-label">Theory
                                        Path</label>
                                    <div class="col-sm-5">
                                        <input
                                            class="form-control @error('digitalocean.theory_path')is-invalid @enderror"
                                            type="text" name="digitalocean[theory_path]"
                                            value="{{ old('digitalocean.theory_path', $setting['digitalocean']['theory_path'] ?? null) }}"
                                            id="digitalocean_theory_path">
                                        @error('digitalocean.theory_path')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p-3" id="payments" role="tabpanel">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Оплата за замовчуванням</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-radio custom-control-inline pt-1">
                                            <input type="radio" id="payments_default_liqpay" name="payments[default]" class="custom-control-input" value="liqpay"
                                                {{ old('payments.default', ($setting['payments']['default'] ?? 'liqpay')) == 'liqpay' ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="payments_default_liqpay">LiqPay</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline pt-1">
                                            <input type="radio" id="payments_default_uapay" name="payments[default]" class="custom-control-input" value="uapay"
                                                {{ old('payments.default', ($setting['payments']['default'] ?? 'liqpay')) == 'uapay' ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="payments_default_uapay">UaPay</label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>LiqPay</h5>

                                <div class="form-group row">
                                    <label for="liqpay_is_sandbox" class="col-sm-2 col-form-label">Is Sandbox</label>
                                    <div class="col-sm-5">
                                        <input type="checkbox" id="liqpay_is_sandbox" name="liqpay[is_sandbox]"
                                               switch="none"
                                               value="1" {{ $setting['liqpay']['is_sandbox'] ?? null ? 'checked' : '' }} />
                                        <label for="liqpay_is_sandbox"></label>
                                        @error('liqpay.is_sandbox')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="liqpay_public_key" class="col-sm-2 col-form-label">Public key</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('liqpay.public_key')is-invalid @enderror"
                                               type="text" name="liqpay[public_key]"
                                               value="{{ old('liqpay.public_key', $setting['liqpay']['public_key'] ?? null) }}"
                                               id="liqpay_public_key">
                                        @error('liqpay.public_key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="liqpay_private_key" class="col-sm-2 col-form-label">Private key</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('liqpay.private_key')is-invalid @enderror"
                                               type="text" name="liqpay[private_key]"
                                               value="{{ old('liqpay.private_key', $setting['liqpay']['private_key'] ?? null) }}"
                                               id="liqpay_private_key">
                                        @error('liqpay.private_key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="liqpay_sandbox_public_key" class="col-sm-2 col-form-label">Sandbox Public key</label>
                                    <div class="col-sm-5">
                                        <input
                                            class="form-control @error('liqpay.sandbox_public_key')is-invalid @enderror"
                                            type="text" name="liqpay[sandbox_public_key]"
                                            value="{{ old('liqpay.sandbox_public_key', $setting['liqpay']['sandbox_public_key'] ?? null) }}"
                                            id="liqpay_sandbox_public_key">
                                        @error('liqpay.sandbox_public_key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="liqpay_sandbox_private_key" class="col-sm-2 col-form-label">Sandbox Private key</label>
                                    <div class="col-sm-5">
                                        <input
                                            class="form-control @error('liqpay.sandbox_private_key')is-invalid @enderror"
                                            type="text" name="liqpay[sandbox_private_key]"
                                            value="{{ old('liqpay.sandbox_private_key', $setting['liqpay']['sandbox_private_key'] ?? null) }}"
                                            id="liqpay_sandbox_private_key">
                                        @error('liqpay.sandbox_private_key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <hr>
                                <h5>UaPay</h5>
                                <div class="form-group row">
                                    <label for="uapay_id" class="col-sm-2 col-form-label">ID</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('uapay.id')is-invalid @enderror"
                                            type="text" name="uapay[id]" id="uapay_id"
                                            value="{{ old('uapay.id', $setting['uapay']['id'] ?? null) }}">
                                        @error('uapay.id')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="uapay_merchant_secret_key" class="col-sm-2 col-form-label">Merchant secret key</label>
                                    <div class="col-sm-5">
                                        <input class="form-control @error('uapay.merchant_secret_key')is-invalid @enderror"
                                            type="text" name="uapay[merchant_secret_key]" id="uapay_merchant_secret_key"
                                            value="{{ old('uapay.merchant_secret_key', $setting['uapay']['merchant_secret_key'] ?? null) }}">
                                        @error('uapay.merchant_secret_key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p-3" id="chat-bot" role="tabpanel">
                                <div class="form-group row">
                                    <label for="chat_bot_api_key" class="col-sm-2 col-form-label">API Key</label>
                                    <div class="input-group col-sm-5">
                                        <input class="form-control @error('chat_bot.api_key')is-invalid @enderror"
                                               type="text" name="chat_bot[api_key]"
                                               value="{{ old('chat_bot.api_key', $setting['chat_bot']['api_key'] ?? null) }}"
                                               id="chat_bot_api_key" readonly>
                                        <div class="input-group-append">
                                            <button class="btn btn-secondary btn-sm" type="button"
                                                    id="generate-api-key"><i class="mdi mdi-reload"></i></button>
                                        </div>
                                        <div class="input-group-append">
                                            <button class="btn btn-secondary btn-sm" type="button" id="copy-api-key"><i
                                                    class="mdi mdi-content-copy"></i></button>
                                        </div>
                                        @error('chat_bot.api_key')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <a href="{{ route('general-settings.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(".select2").select2();

        $("#copy-api-key").click(function (e) {
            let copyText = document.getElementById("chat_bot_api_key");
            copyText.select();
            copyText.setSelectionRange(0, 99999);

            document.execCommand("copy");
        });

        $("#generate-api-key").click(function (e) {
            let copyText = document.getElementById("chat_bot_api_key");
            copyText.value = generateHexString(58);

            let alert = $('#save-api-key');

            if (!alert.hasClass('show')) {
                alert.removeClass('hide');
                alert.addClass('show');
            }
        });

        function generateHexString(length) {
            let ret = "";
            while (ret.length < length) {
                ret += Math.random().toString(16).substring(2);
            }
            return ret.substring(0, length);
        }
    </script>
@endsection
