@extends('layouts.master')

@section('title') Редагування групи@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування групи</h4>
                {{ Breadcrumbs::render('groups.edit', $group) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('groups.update', $group) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 col-form-label">Назва *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="title" value="{{  old('title', $group->title) }}" id="title">
                                @error('title')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        @can('editable', $group)
                        <div class="form-group row">
                            <label for="user_id" class="col-sm-2 col-form-label">Викладач *</label>
                            <div class="col-sm-5">
                                <select class="form-control select2" name="user_id">
                                    <option value="">Викладач ...</option>
                                    @foreach($teachers as $teacher)
                                        <option value="{{ $teacher->id }}" {{ old('user_id', $group->user_id) == $teacher->id ? 'selected' : '' }}>
                                            {{ $teacher->infoTeacher->full_name ?? $teacher->id }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('user_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>
                        @endcan

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('groups.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(".select2").select2();
    </script>
@endsection
