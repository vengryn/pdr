@extends('layouts.master')

@section('title') @lang('translation.groups') @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.groups')</h4>
                {{ Breadcrumbs::render('groups') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['teacher_name']) || isset($filters['group_name']) ? '' : 'collapsed' }}"
                           aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter"
                             class="collapse {{ isset($filters['teacher_name']) || isset($filters['group_name']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('groups.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <select class="form-control" name="group_name" id="group_name"
                                                    data-placeholder="Назва групи" data-allow-clear="true">
                                                <option></option>
                                                @isset($filters['group_name'])
                                                    <option value="{{ $filters['group_name'] }}"
                                                            selected>{{ $filters['group_name'] == 'without' ? "Без групи" : $filters['group_name'] }}</option>
                                                @endisset
                                            </select>
                                        </div>
                                        @can('editable', App\Models\Group::class)
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" placeholder="ПІБ викладача"
                                                       value="{{ $filters['teacher_name'] ?? '' }}" name="teacher_name"
                                                       id="teacher_name">
                                            </div>
                                        @endcan
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('groups.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати групу</a>
                            </div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Викладач</th>
                                <th>{{ auth()->user()->isTeacher() ? 'Код викладача' : 'Промокод' }}</th>
                                <th>Учні</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($groups as $group)
                                <tr>
                                    <td>{{ $loop->iteration + $groups->firstItem() - 1 }}</td>
                                    <td>{{ $group->title }}</td>
                                    <td>{{ $group->teacher->infoTeacher->full_name ?? '' }}</td>
                                    <td>
                                        @isset($group->teacher->promoCode->code)
                                            <a href="{{ route('report.index', ['promo_code' => $group->teacher->promoCode->code]) }}">
                                                {{ $group->teacher->promoCode->code }}
                                            </a>
                                        @endisset
                                    </td>
                                    <td>
                                        <a href="{{ route('students.index', ['group_name' => $group->title]) }}">{{ $group->students_count }}</a>
                                    </td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('groups.edit', $group) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('groups.destroy', $group) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити групу?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $groups->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>
        $(function () {
            $('#group_name').select2({
                width: '100%',
                ajax: {
                    url: "{{ route('groups.getAutocompleteGroup') }}",
                    dataType: 'json',
                    delay: 250,
                    type: "post",
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item,
                                    id: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            $("#teacher_name").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('groups.getAutocompleteTeacher') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });
        });
    </script>
@endsection
