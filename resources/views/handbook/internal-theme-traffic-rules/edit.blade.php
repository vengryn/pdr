@extends('layouts.master')

@section('title') Редагування теми @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування теми</h4>
                {{ Breadcrumbs::render('internal_theme_traffic_rules.edit', $internalTopicTrafficRule) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('theory.internal-theme-traffic-rules.update', $internalTopicTrafficRule) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="number" class="col-sm-2 col-form-label">Тема № *</label>
                            <div class="col-sm-2">
                                <input class="form-control @error('number')is-invalid @enderror" type="text" name="number"
                                       value="{{ old('number', $internalTopicTrafficRule->number) }}" id="number">
                                @error('number')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        @foreach($languages as $language)
                            <div class="form-group row">
                                <label for="name_{{ $language->id }}" class="col-sm-2 col-form-label">{{ $language->title }} {{ $language->is_default ? '*' : '' }}</label>
                                <div class="col-sm-7">
                                    <input class="form-control @error('name_' . $language->id)is-invalid @enderror" type="text"
                                           name="translate[name][{{ $language->id }}]"
                                           value="{{ old('translate.name.'. $language->id, $internalTopicTrafficRule->getTranslate($language->id)->name ?? '') }}"
                                           id="name_{{ $language->id }}">
                                    @error('translate.name.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div>
                            </div>
                        @endforeach

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ $redirectRoute }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(function () {
            document.oninput = function () {
                let input = document.getElementById('number');
                input.value = input.value.replace(/[^\d]/g, '');
            }
        });
    </script>
@endsection
