@extends('layouts.master')

@section('title') @lang('translation.internal_theme_traffic_rules') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.internal_theme_traffic_rules')</h4>
                {{ Breadcrumbs::render('internal_theme_traffic_rules') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <form action="{{ route('theory.internal-theme-traffic-rules.index') }}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" value="{{ request()->search ?? '' }}" placeholder="Пошук">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Пошук</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-sm-7">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('theory.internal-theme-traffic-rules.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати тему</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th style="width: 100px;">Тема №</th>
                                <th>{{ $defaultLanguage->title }}</th>
                                <th>Переклади</th>
                                <th style="width: 110px;">Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($internalTopicTrafficRules as $internalTopicTrafficRule)
                                <tr>
                                    <td>{{ $loop->iteration + $internalTopicTrafficRules->firstItem() - 1 }}</td>
                                    <td>{{ $internalTopicTrafficRule->number }}</td>
                                    <td>{{ $internalTopicTrafficRule->getTranslate($defaultLanguage->id)->name ?? '' }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($internalTopicTrafficRule->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('theory.internal-theme-traffic-rules.edit', $internalTopicTrafficRule) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('theory.internal-theme-traffic-rules.destroy', $internalTopicTrafficRule) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити тему?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $internalTopicTrafficRules->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script>
        $(".select2").select2({
            minimumResultsForSearch: Infinity
        });
    </script>
@endsection
