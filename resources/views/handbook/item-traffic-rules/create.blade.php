@extends('layouts.master')

@section('title') Додавання нового пункту @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/emojipicker.css') }}">
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нового пункту</h4>
                {{ Breadcrumbs::render('items_traffic_rules.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab"
                                   href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}"
                                                                         style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('theory.item-traffic-rules.store') }}" id="sendForm">
                        @csrf

                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}"
                                     id="{{ $language->abbreviation }}" role="tabpanel">
                                    @if($language->is_default)
                                        <div class="form-group row">
                                            <label for="topic_traffic_rule_id" class="col-sm-2 col-form-label">Тема *</label>
                                            <div class="col-sm-5 @error('internal_topic_traffic_rule_id')is-invalid @enderror">
                                                <select class="form-control select2" id="internal_topic_traffic_rule_id"
                                                        name="internal_topic_traffic_rule_id" data-placeholder="Виберіть тему"
                                                        data-allow-clear="true" data-minimum-results-for-search="0">
                                                    <option></option>
                                                    @foreach($internalTopicTrafficRules as $internalTopicTrafficRule)
                                                        <option value="{{ $internalTopicTrafficRule->id }}" {{ old('internal_topic_traffic_rule_id') == $internalTopicTrafficRule->id ? 'selected' : '' }}>
                                                            {{ $internalTopicTrafficRule->number }}. {{ $internalTopicTrafficRule->getTranslate()->name ?? '' }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('internal_topic_traffic_rule_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="number" class="col-sm-2 col-form-label">Пункт № *</label>
                                            <div class="col-sm-2">
                                                <input class="form-control @error('number')is-invalid @enderror"
                                                       type="text"
                                                       name="number" value="{{ old('number') }}" id="number">
                                                @error('number')
                                                <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row short-markdown">
                                        <label for="description_{{ $language->id }}" class="col-sm-2 col-form-label">Короткий опис {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control @error('translate.description.' . $language->id)is-invalid @enderror"
                                                id="description_{{ $language->id }}"
                                                data-action="tinymce-description" style="height: 200px;"
                                                name="translate[description][{{ $language->id }}]">{{ old('translate.description.'. $language->id) }}</textarea>
                                            @error('translate.description.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                        @include('inc.token-description')
                                    </div>

                                    <div class="form-group row">
                                        <label for="content_{{ $language->id }}" class="col-sm-2 col-form-label">Опис {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <textarea
                                                class="form-control @error('translate.content.' . $language->id)is-invalid @enderror"
                                                id="content_{{ $language->id }}"
                                                data-action="tinymce-content" style="height: 350px;"
                                                name="translate[content][{{ $language->id }}]">{{ old('translate.content.'. $language->id) }}</textarea>
                                            @error('translate.content.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    @include('inc.markdown-img')
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти
                                </button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися
                                </button>
                                <a href="{{ route('theory.item-traffic-rules.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('/assets/js/inputEmoji.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/emojis.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>

    <script>
        $(function () {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });

            document.oninput = function() {
                let input = document.getElementById('number');
                input.value = input.value.replace (/[^\.\d]/g, '');
            }

            $('textarea[data-action="tinymce-description"]').each(function() {
                new EasyMDE({
                    element: this,
                    toolbar: ["bold", "|", "link", "|", {
                        name: "smile",
                        action: (editor) => {
                            $(this).emojiList(editor);
                        },
                        className: "fa fa-smile",
                        title: "Smile",
                    }, "|", "preview"],
                    status: false,
                    spellChecker: false,
                    renderingConfig: {
                        singleLineBreaks: false
                    }
                });
            });

            $('textarea[data-action="tinymce-content"]').each(function() {
                new EasyMDE({
                    element: this,
                    toolbar: ["bold", "|", "link", "image", "|", {
                        name: "smile",
                        action: (editor) => {
                            $(this).emojiList(editor);
                        },
                        className: "fa fa-smile",
                        title: "Smile",
                    }, "|", "preview"],
                    status: false,
                    spellChecker: false,
                    renderingConfig: {
                        singleLineBreaks: false
                    }
                });
            });
        });
    </script>
@endsection
