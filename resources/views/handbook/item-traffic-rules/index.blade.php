@extends('layouts.master')

@section('title') @lang('translation.items') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.items')</h4>
                {{ Breadcrumbs::render('items_traffic_rules') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['item']) || isset($filters['internal_topic_traffic_rule']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['item']) || isset($filters['internal_topic_traffic_rule']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('theory.item-traffic-rules.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <select class="form-control" name="item" id="item"
                                                    data-placeholder="Пункт №" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @isset($itemFilterSelected)
                                                    <option value="{{ $filters['item'] }}" selected>{{ $itemFilterSelected->number }}</option>
                                                @endisset
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="internal_topic_traffic_rule" id="internal_topic_traffic_rule"
                                                    data-placeholder="Тема" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($internalTopicTrafficRules as $internalTopicTrafficRule)
                                                    <option value="{{ $internalTopicTrafficRule->id }}" {{ ($filters['internal_topic_traffic_rule'] ?? '') == $internalTopicTrafficRule->id ? 'selected' : '' }}>
                                                        {{ $internalTopicTrafficRule->number }}. {{ $internalTopicTrafficRule->getTranslate()->name ?? '' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('theory.item-traffic-rules.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати пункт</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th style="width: 100px;">№</th>
                                <th style="width: 100px;">Пункт №</th>
                                <th>Тема</th>
                                <th>Переклади</th>
                                <th style="width: 110px;">Опції</th>
                                @if($isDisplaySorting)
                                <th style="width: 60px;"></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody id="sorting" data-entityname="item-traffic-rules">
                            @foreach($itemTrafficRules as $itemTrafficRule)
                                <tr data-itemId="{{ $itemTrafficRule->id }}">
                                    <td>{{ $loop->iteration + $itemTrafficRules->firstItem() - 1 }}</td>
                                    <td>{{ $itemTrafficRule->number }}</td>
                                    <td>{{ $itemTrafficRule->internalTopicTrafficRule ? $itemTrafficRule->internalTopicTrafficRule->number . '. ' . $itemTrafficRule->internalTopicTrafficRule->getTranslate()->name ?? '' : '' }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($itemTrafficRule->getTranslate($language->id)->content))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('theory.item-traffic-rules.edit', $itemTrafficRule) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('theory.item-traffic-rules.destroy', $itemTrafficRule) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити пункт?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                    @if($isDisplaySorting)
                                    <td class="sortable-handle align-middle"><i class="mdi mdi-arrow-up-down" style="cursor: move"></i></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $itemTrafficRules->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(".select2").select2();

        $('#internal_topic_traffic_rule').change(function () {
            $('#item').val(null).trigger('change');
        });

        $('#item').select2({
            width: '100%',
            ajax: {
                url: "{{ route('theory.item-traffic-rules.getAutocompleteItem') }}",
                dataType: 'json',
                delay: 250,
                type: "post",
                data: function (params) {
                    return {
                        _type: 'query',
                        q: params.term,
                        internal_topic_traffic_rule: $('select[name="internal_topic_traffic_rule"]').val()
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.number,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $( function() {
            let changePosition = function(requestData){
                $.ajax({
                    'url': '{{ route('theory.item-traffic-rules.sort') }}',
                    'type': 'POST',
                    'data': requestData,
                    'success': function(data) {
                        //
                    },
                    'error': function(){
                        console.error('Something wrong!');
                    }
                });
            };

            let fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            let $sortableTable = $('#sorting');
            let fixed = '.blocked';
            let newParentContainer;

            function sortingAroundFixedPositions(container) {
                let sortable = $(container);
                let statics = $(fixed, container).detach();
                let tagName = statics.prop('tagName');
                let helper = $('<' + tagName + '/>').prependTo(container);
                statics.each(function() {
                    let target = this.dataset.pos;
                    let targetPosition = $(tagName, sortable).eq(target);
                    if (targetPosition.length === 0) {
                        targetPosition = $(tagName, sortable).eq(target - 1)
                    }
                    $(this).insertAfter(targetPosition);
                });
                helper.remove();
            }

            if ($sortableTable.length > 0) {
                $sortableTable.sortable({
                    handle: '.sortable-handle',
                    axis: 'y',
                    cancel: fixed,
                    cursor: "move",

                    start: function() {
                        $(fixed, $sortableTable).each(function() {
                            this.dataset.pos = $(this).index();
                        });
                    },
                    change: function(e, ui) {
                        sortingAroundFixedPositions(this);
                        if (ui.sender) {
                            newParentContainer = this;
                        }
                        if (newParentContainer) {
                            sortingAroundFixedPositions(newParentContainer);
                        }
                    },

                    update: function(a, b){
                        var $sorted = b.item;

                        var $previous = $sorted.prev();
                        var $next = $sorted.next();

                        if ($previous.length > 0) {
                            changePosition({
                                type: 'moveAfter',
                                id: $sorted.data('itemid'),
                                positionEntity: $previous.data('itemid')
                            });
                        } else if ($next.length > 0) {
                            changePosition({
                                type: 'moveBefore',
                                id: $sorted.data('itemid'),
                                positionEntity: $next.data('itemid')
                            });
                        } else {
                            console.error('Something wrong!');
                        }
                    },
                    helper: fixHelper
                });
            }
        });
    </script>
@endsection
