@extends('layouts.master')

@section('title') @lang('translation.markings') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.markings')</h4>
                {{ Breadcrumbs::render('markings') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['marking']) || isset($filters['topic_road_marking']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['marking']) || isset($filters['topic_road_marking']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('theory.markings.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <select class="form-control" name="marking" id="marking"
                                                    data-placeholder="Розмітка" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @isset($markingFilterSelected)
                                                    <option value="{{ $filters['marking'] }}" selected>{{ $markingFilterSelected->name }}</option>
                                                @endisset
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="topic_road_marking" id="topic_road_marking"
                                                    data-placeholder="Тип" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($topicRoadMarkings as $topicRoadMarking)
                                                    <option value="{{ $topicRoadMarking->id }}" {{ ($filters['topic_road_marking'] ?? '') == $topicRoadMarking->id ? 'selected' : '' }}>
                                                        {{ $topicRoadMarking->number }}. {{ $topicRoadMarking->getTranslate()->name ?? '' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="20" {{ request()->paginate == 20 ? 'selected' : '' }}>20</option>
                                <option value="35" {{ request()->paginate == 35 ? 'selected' : '' }}>35</option>
                                <option value="55" {{ request()->paginate == 55 ? 'selected' : '' }}>55</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                                <option value="150" {{ request()->paginate == 150 ? 'selected' : '' }}>150</option>
                                <option value="200" {{ request()->paginate == 200 ? 'selected' : '' }}>200</option>
                                <option value="500" {{ request()->paginate == 500 ? 'selected' : '' }}>500</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('theory.markings.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати розмітку</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th style="width: 100px;">№</th>
                                <th>Зображення</th>
                                <th>Розмітка №</th>
                                <th>{{ $defaultLanguage->title }}</th>
                                <th>Тип</th>
                                <th>Переклади</th>
                                <th style="width: 110px;">Опції</th>
                                @if($isDisplaySorting)
                                <th style="width: 60px;"></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody id="sorting" data-entityname="markings">
                            @foreach($markings as $marking)
                                <tr data-itemId="{{ $marking->id }}">
                                    <td>{{ $loop->iteration + $markings->firstItem() - 1 }}</td>
                                    <td>@empty(!$marking->image)<img src="{{ \Storage::url($marking->image26x26) }}" style="max-width:64px;max-height:24px;">@endempty</td>
                                    <td>{{ $marking->number }}</td>
                                    <td>{{ $marking->getTranslate($defaultLanguage->id)->name ?? '' }}</td>
                                    <td>{{ $marking->topicRoadMarking ? $marking->topicRoadMarking->number . '. ' . $marking->topicRoadMarking->getTranslate()->name ?? '' : '' }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($marking->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('theory.markings.edit', $marking) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('theory.markings.destroy', $marking) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити розмітку?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                    @if($isDisplaySorting)
                                    <td class="sortable-handle align-middle"><i class="mdi mdi-arrow-up-down" style="cursor: move"></i></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $markings->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(".select2").select2();

        $('#topic_road_marking').change(function () {
            $('#marking').val(null).trigger('change');
        });

        $('#marking').select2({
            width: '100%',
            ajax: {
                url: "{{ route('theory.markings.getAutocompleteMarking') }}",
                dataType: 'json',
                delay: 250,
                type: "post",
                data: function (params) {
                    return {
                        _type: 'query',
                        q: params.term,
                        topic_road_marking: $('select[name="topic_road_marking"]').val()
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $( function() {
            let changePosition = function(requestData){
                $.ajax({
                    'url': '{{ route('theory.markings.sort') }}',
                    'type': 'POST',
                    'data': requestData,
                    'success': function(data) {
                        //
                    },
                    'error': function(){
                        console.error('Something wrong!');
                    }
                });
            };

            let fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            let $sortableTable = $('#sorting');
            let fixed = '.blocked';
            let newParentContainer;

            function sortingAroundFixedPositions(container) {
                let sortable = $(container);
                let statics = $(fixed, container).detach();
                let tagName = statics.prop('tagName');
                let helper = $('<' + tagName + '/>').prependTo(container);
                statics.each(function() {
                    let target = this.dataset.pos;
                    let targetPosition = $(tagName, sortable).eq(target);
                    if (targetPosition.length === 0) {
                        targetPosition = $(tagName, sortable).eq(target - 1)
                    }
                    $(this).insertAfter(targetPosition);
                });
                helper.remove();
            }

            if ($sortableTable.length > 0) {
                $sortableTable.sortable({
                    handle: '.sortable-handle',
                    axis: 'y',
                    cancel: fixed,
                    cursor: "move",

                    start: function() {
                        $(fixed, $sortableTable).each(function() {
                            this.dataset.pos = $(this).index();
                        });
                    },
                    change: function(e, ui) {
                        sortingAroundFixedPositions(this);
                        if (ui.sender) {
                            newParentContainer = this;
                        }
                        if (newParentContainer) {
                            sortingAroundFixedPositions(newParentContainer);
                        }
                    },

                    update: function(a, b){
                        var $sorted = b.item;

                        var $previous = $sorted.prev();
                        var $next = $sorted.next();

                        if ($previous.length > 0) {
                            changePosition({
                                type: 'moveAfter',
                                id: $sorted.data('itemid'),
                                positionEntity: $previous.data('itemid')
                            });
                        } else if ($next.length > 0) {
                            changePosition({
                                type: 'moveBefore',
                                id: $sorted.data('itemid'),
                                positionEntity: $next.data('itemid')
                            });
                        } else {
                            console.error('Something wrong!');
                        }
                    },
                    helper: fixHelper
                });
            }
        });
    </script>
@endsection
