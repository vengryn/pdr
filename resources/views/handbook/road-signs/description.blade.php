@extends('layouts.master')

@section('title') Загальний опис до категорій @endsection

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/emojipicker.css') }}">
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Загальний опис до категорій</h4>
                {{ Breadcrumbs::render('type_road_signs.description') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab"
                                   href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}"
                                                                         style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('theory.road-signs.updateDescription') }}">
                        @csrf

                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}" id="{{ $language->abbreviation }}" role="tabpanel">
                                    <div class="form-group row short-markdown">
                                        <label for="description_{{ $language->id }}" class="col-sm-2 col-form-label">Вміст</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control @error('translate.description.' . $language->id)is-invalid @enderror"
                                                id="description_{{ $language->id }}" data-action="markdown"
                                                name="translate[description][{{ $language->id }}]">{{ old('translate.description.'. $language->id, $data[$language->id]['description'] ?? '') }}</textarea>
                                            @error('translate.description.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>

                                        <div class="col-sm-5 token-description">
                                            <p><b>ВАЖЛИВО:</b> для наповнення контентом необхідно керуватися стандартом MarkDown розмітки описаній тут: <a href="https://www.markdownguide.org/basic-syntax" target="_blank">https://www.markdownguide.org/basic-syntax</a></p>
                                            <p>Важливо пам'ятати, що згідно дизайну у мобільному пристрої із усіх стилістичних оформлень тексту, обробляється тільки жирний текст, всі інші стилі ігноруються. В веб-версії будуть відображатись всі стилі.</p>
                                            <p>Так же, в доповнення до описаного стандарту використовуються розширені теги розмітки для вставляння посилань на пункти ПДР, знаки, розмітку.</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('theory.road-signs.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('/assets/js/inputEmoji.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/emojis.js') }}"></script>

    <script>
        $(function () {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });

            $('textarea[data-action="markdown"]').each(function() {
                new EasyMDE({
                    element: this,
                    toolbar: ["bold", "|", "link", "|", {
                        name: "smile",
                        action: (editor) => {
                            $(this).emojiList(editor);
                        },
                        className: "fa fa-smile",
                        title: "Smile",
                    }, "|", "preview"],
                    status: false,
                    spellChecker: false,
                    renderingConfig: {
                        singleLineBreaks: false
                    }
                });
            });
        });
    </script>
@endsection
