@extends('layouts.master')

@section('title') Редагування категорії @endsection

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/emojipicker.css') }}">
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування категорії</h4>
                {{ Breadcrumbs::render('type_road_signs.edit', $topicRoadSign) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab"
                                   href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}"
                                                                         style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('theory.road-signs.update', $topicRoadSign) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}" id="{{ $language->abbreviation }}" role="tabpanel">
                                    @if($language->is_default)
                                        <div class="form-group row">
                                            <label for="number" class="col-sm-2 col-form-label">Категорія № *</label>
                                            <div class="col-sm-2">
                                                <input class="form-control @error('number')is-invalid @enderror" type="text"
                                                       name="number" value="{{ old('number', $topicRoadSign->number) }}" id="number">
                                                @error('number')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="image" class="col-sm-2 col-form-label">Зображення (200x200 px) *</label>
                                            <div class="col-sm-2">
                                                <div data-id="img-preview">
                                                    @if(empty($topicRoadSign->image) || $errors->has('image'))
                                                        <input type="file" name="image" id="image">
                                                        @error('image')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                    @else
                                                        <div data-id="img-content">
                                                            <input type="hidden" name="image_path" value="{{ $topicRoadSign->image }}">
                                                            <img src="{{ \Storage::url($topicRoadSign->image) }}" style="max-width:64px;max-height:28px;margin-right: 10px">
                                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="image_path"
                                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                                        </div>

                                                        <div style="display: none;" data-id="img-upload">
                                                            <input type="file" name="image" id="image">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label for="name_{{ $language->id }}"
                                               class="col-sm-2 col-form-label">Назва {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control @error('translate.name.' . $language->id)is-invalid @enderror"
                                                   id="name_{{ $language->id }}"
                                                   name="translate[name][{{ $language->id }}]"
                                                   value="{{ old('translate.name.'. $language->id, $topicRoadSign->getTranslate($language->id)->name ?? '') }}">

                                            @error('translate.name.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="description_{{ $language->id }}" class="col-sm-2 col-form-label">Опис</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control @error('translate.description.' . $language->id)is-invalid @enderror"
                                                id="description_{{ $language->id }}"
                                                data-action="markdown"
                                                name="translate[description][{{ $language->id }}]">{{ old('translate.description.'. $language->id, $topicRoadSign->getTranslate($language->id)->description ?? '') }}</textarea>
                                            @error('translate.description.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>

                                        @include('inc.token-description')
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ $redirectRoute }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('/assets/js/inputEmoji.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/emojis.js') }}"></script>

    <script>
        $(function () {
            document.oninput = function() {
                let input = document.getElementById('number');
                input.value = input.value.replace (/[^\d]/g, '');
            }

            $('textarea[data-action="markdown"]').each(function() {
                new EasyMDE({
                    element: this,
                    toolbar: ["bold", "|", "link", "|", {
                        name: "smile",
                        action: (editor) => {
                            $(this).emojiList(editor);
                        },
                        className: "fa fa-smile",
                        title: "Smile",
                    }, "|", "preview"],
                    status: false,
                    spellChecker: false,
                    renderingConfig: {
                        singleLineBreaks: false
                    }
                });
            });
        });
    </script>
@endsection
