@extends('layouts.master')

@section('title') @lang('translation.type_road_signs') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.type_road_signs')</h4>
                {{ Breadcrumbs::render('type_road_signs') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-12">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('theory.road-signs.editDescription') }}"
                                   class="btn btn-primary waves-effect waves-light">Загальний опис</a>
                                <a href="{{ route('theory.road-signs.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати категорію</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Категорія №</th>
                                <th>Зображення</th>
                                <th>{{ $defaultLanguage->title }}</th>
                                <th>Переклади</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($topicRoadSigns as $topicRoadSign)
                                <tr data-itemId="{{ $topicRoadSign->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $topicRoadSign->number }}</td>
                                    <td>@empty(!$topicRoadSign->image)<img src="{{ \Storage::url($topicRoadSign->image) }}" style="max-width:64px;max-height:24px;">@endempty</td>
                                    <td>{{ $topicRoadSign->getTranslate($defaultLanguage->id)->name }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($topicRoadSign->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('theory.road-signs.edit', $topicRoadSign) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('theory.road-signs.destroy', $topicRoadSign) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити категорію?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection
