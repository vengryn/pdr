@extends('layouts.master')

@section('title') Редагування знаку @endsection

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/emojipicker.css') }}">
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування знаку</h4>
                {{ Breadcrumbs::render('signs.edit', $sign) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab"
                                   href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}"
                                                                         style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('theory.signs.update', $sign) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}" id="{{ $language->abbreviation }}" role="tabpanel">
                                    @if($language->is_default)
                                        <div class="form-group row">
                                            <label for="topic_road_marking_id" class="col-sm-2 col-form-label">Категорія *</label>
                                            <div class="col-sm-5 @error('topic_road_sign_id')is-invalid @enderror">
                                                <select class="form-control select2" id="topic_road_sign_id"
                                                        name="topic_road_sign_id" data-placeholder="Виберіть категорію"
                                                        data-allow-clear="true" data-minimum-results-for-search="0">
                                                    <option></option>
                                                    @foreach($topicRoadSigns as $topicRoadSign)
                                                        <option value="{{ $topicRoadSign->id }}" {{ old('topic_road_sign_id', $sign->topic_road_sign_id) == $topicRoadSign->id ? 'selected' : '' }}>
                                                            {{ $topicRoadSign->number }}. {{ $topicRoadSign->getTranslate()->name ?? '' }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('topic_road_sign_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="image" class="col-sm-2 col-form-label">Зображення (200X200 px) *</label>
                                            <div class="col-sm-2">
                                                <div data-id="img-preview">
                                                    @if(empty($sign->image) || $errors->has('image'))
                                                        <input type="file" name="image" id="image">
                                                        @error('image')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                    @else
                                                        <div data-id="img-content">
                                                            <input type="hidden" name="image_path" value="{{ $sign->image }}">
                                                            <img src="{{ \Storage::url($sign->image) }}" style="max-width:64px;max-height:28px;margin-right: 10px">
                                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="image_path"
                                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                                        </div>

                                                        <div style="display: none;" data-id="img-upload">
                                                            <input type="file" name="image" id="image">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="number" class="col-sm-2 col-form-label">Знак № *</label>
                                            <div class="col-sm-2">
                                                <input class="form-control @error('number')is-invalid @enderror" type="text"
                                                       name="number" value="{{ old('number', $sign->number) }}" id="number">
                                                @error('number')
                                                <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label for="name_{{ $language->id }}"
                                               class="col-sm-2 col-form-label">Назва {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   class="form-control @error('translate.name.' . $language->id)is-invalid @enderror"
                                                   id="name_{{ $language->id }}"
                                                   name="translate[name][{{ $language->id }}]"
                                                   value="{{ old('translate.name.'. $language->id, $sign->getTranslate($language->id)->name ?? '') }}">

                                            @error('translate.name.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row short-markdown">
                                        <label for="description_{{ $language->id }}" class="col-sm-2 col-form-label">Короткий опис {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <textarea
                                                class="form-control @error('translate.description.' . $language->id)is-invalid @enderror"
                                                id="description_{{ $language->id }}"
                                                data-action="tinymce" style="height: 200px;"
                                                name="translate[description][{{ $language->id }}]">{{ old('translate.description.'. $language->id, $sign->getTranslate($language->id)->description ?? '') }}</textarea>
                                            @error('translate.description.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                        @include('inc.token-description')
                                    </div>

                                    <div class="form-group row">
                                        <label for="content_{{ $language->id }}" class="col-sm-2 col-form-label">Опис {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <textarea
                                                class="form-control @error('translate.content.' . $language->id)is-invalid @enderror"
                                                id="content_{{ $language->id }}"
                                                data-action="tinymce" style="height: 350px;"
                                                name="translate[content][{{ $language->id }}]">{{ old('translate.content.'. $language->id, $sign->getTranslate($language->id)->content ?? '') }}</textarea>
                                            @error('translate.content.' . $language->id)
                                            <div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    @if($language->is_default)
                                        <div class="form-group row">
                                            <label for="image_example" class="col-sm-2 col-form-label">Фото-приклад</label>
                                            <div class="col-sm-5">
                                                <div data-id="img-preview">
                                                    @if(empty($sign->image_example) || $errors->has('image_example'))
                                                        <input type="file" name="image_example" id="image_example">
                                                        @error('image_example')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                    @else
                                                        <div data-id="img-content">
                                                            <input type="hidden" name="image_example_path" value="{{ $sign->image }}">
                                                            <img src="{{ \Storage::disk('digitalocean')->url($sign->image_example) }}" style="max-width:64px;max-height:28px;margin-right: 10px">
                                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="image_example_path"
                                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                                        </div>

                                                        <div style="display: none;" data-id="img-upload">
                                                            <input type="file" name="image_example" id="image_example">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="video_example" class="col-sm-2 col-form-label">Відео-приклад</label>
                                            <div class="col-sm-5">
                                                <input class="form-control @error('video_example')is-invalid @enderror" type="text"
                                                       name="video_example" value="{{ old('video_example', $sign->video_example) }}" id="video_example">
                                                @error('video_example')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти
                                </button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися
                                </button>
                                <a href="{{ $redirectRoute }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('/assets/js/inputEmoji.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/emojis.js') }}"></script>

    <script>
        $(function () {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });

            $('textarea[data-action="tinymce"]').each(function() {
                new EasyMDE({
                    element: this,
                    toolbar: ["bold", "|", "link", "|", {
                        name: "smile",
                        action: (editor) => {
                            $(this).emojiList(editor);
                        },
                        className: "fa fa-smile",
                        title: "Smile",
                    }, "|", "preview"],
                    status: false,
                    spellChecker: false,
                    renderingConfig: {
                        singleLineBreaks: false
                    }
                });
            });
        });
    </script>
@endsection
