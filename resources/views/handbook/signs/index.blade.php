@extends('layouts.master')

@section('title') @lang('translation.signs') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.signs')</h4>
                {{ Breadcrumbs::render('signs') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['sign']) || isset($filters['topic_road_sign']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['sign']) || isset($filters['topic_road_sign']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('theory.signs.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <select class="form-control" name="sign" id="sign"
                                                    data-placeholder="Знак" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @isset($signFilterSelected)
                                                    <option value="{{ $filters['sign'] }}" selected>{{ $signFilterSelected->name }}</option>
                                                @endisset
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="topic_road_sign" id="topic_road_sign"
                                                    data-placeholder="Категорія" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($topicRoadSigns as $topicRoadSign)
                                                    <option value="{{ $topicRoadSign->id }}" {{ ($filters['topic_road_sign'] ?? '') == $topicRoadSign->id ? 'selected' : '' }}>
                                                        {{ $topicRoadSign->number }}. {{ $topicRoadSign->getTranslate()->name ?? '' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="20" {{ request()->paginate == 20 ? 'selected' : '' }}>20</option>
                                <option value="35" {{ request()->paginate == 35 ? 'selected' : '' }}>35</option>
                                <option value="55" {{ request()->paginate == 55 ? 'selected' : '' }}>55</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                                <option value="150" {{ request()->paginate == 150 ? 'selected' : '' }}>150</option>
                                <option value="200" {{ request()->paginate == 200 ? 'selected' : '' }}>200</option>
                                <option value="500" {{ request()->paginate == 500 ? 'selected' : '' }}>500</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('theory.signs.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати знак</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th style="width: 100px;">№</th>
                                <th>Зображення</th>
                                <th>Знак №</th>
                                <th>{{ $defaultLanguage->title }}</th>
                                <th>Категорія</th>
                                <th>Переклади</th>
                                <th style="width: 110px;">Опції</th>
                                @if($isDisplaySorting)
                                <th style="width: 60px;"></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody id="sorting" data-entityname="signs">
                            @foreach($signs as $sign)
                                <tr data-itemId="{{ $sign->id }}">
                                    <td>{{ $loop->iteration + $signs->firstItem() - 1 }}</td>
                                    <td>@empty(!$sign->image)<img src="{{ \Storage::url($sign->image26x26) }}" style="max-width:64px;max-height:24px;">@endempty</td>
                                    <td>{{ $sign->number }}</td>
                                    <td>{{ $sign->getTranslate($defaultLanguage->id)->name ?? '' }}</td>
                                    <td>{{ $sign->topicRoadSign ? $sign->topicRoadSign->number . '. ' . $sign->topicRoadSign->getTranslate()->name ?? '' : '' }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($sign->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('theory.signs.edit', $sign) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('theory.signs.destroy', $sign) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити знак?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                    @if($isDisplaySorting)
                                    <td class="sortable-handle align-middle"><i class="mdi mdi-arrow-up-down" style="cursor: move"></i></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $signs->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(".select2").select2();

        $('#topic_road_sign').change(function () {
            $('#sign').val(null).trigger('change');
        });

        $('#sign').select2({
            width: '100%',
            ajax: {
                url: "{{ route('theory.signs.getAutocompleteSign') }}",
                dataType: 'json',
                delay: 250,
                type: "post",
                data: function (params) {
                    return {
                        _type: 'query',
                        q: params.term,
                        topic_road_sign: $('select[name="topic_road_sign"]').val()
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $( function() {
            let changePosition = function(requestData){
                $.ajax({
                    'url': '{{ route('theory.signs.sort') }}',
                    'type': 'POST',
                    'data': requestData,
                    'success': function(data) {
                        //
                    },
                    'error': function(){
                        console.error('Something wrong!');
                    }
                });
            };

            let fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            let $sortableTable = $('#sorting');
            let fixed = '.blocked';
            let newParentContainer;

            function sortingAroundFixedPositions(container) {
                let sortable = $(container);
                let statics = $(fixed, container).detach();
                let tagName = statics.prop('tagName');
                let helper = $('<' + tagName + '/>').prependTo(container);
                statics.each(function() {
                    let target = this.dataset.pos;
                    let targetPosition = $(tagName, sortable).eq(target);
                    if (targetPosition.length === 0) {
                        targetPosition = $(tagName, sortable).eq(target - 1)
                    }
                    $(this).insertAfter(targetPosition);
                });
                helper.remove();
            }

            if ($sortableTable.length > 0) {
                $sortableTable.sortable({
                    handle: '.sortable-handle',
                    axis: 'y',
                    cancel: fixed,
                    cursor: "move",

                    start: function() {
                        $(fixed, $sortableTable).each(function() {
                            this.dataset.pos = $(this).index();
                        });
                    },
                    change: function(e, ui) {
                        sortingAroundFixedPositions(this);
                        if (ui.sender) {
                            newParentContainer = this;
                        }
                        if (newParentContainer) {
                            sortingAroundFixedPositions(newParentContainer);
                        }
                    },

                    update: function(a, b){
                        var $sorted = b.item;

                        var $previous = $sorted.prev();
                        var $next = $sorted.next();

                        if ($previous.length > 0) {
                            changePosition({
                                type: 'moveAfter',
                                id: $sorted.data('itemid'),
                                positionEntity: $previous.data('itemid')
                            });
                        } else if ($next.length > 0) {
                            changePosition({
                                type: 'moveBefore',
                                id: $sorted.data('itemid'),
                                positionEntity: $next.data('itemid')
                            });
                        } else {
                            console.error('Something wrong!');
                        }
                    },
                    helper: fixHelper
                });
            }
        });
    </script>
@endsection
