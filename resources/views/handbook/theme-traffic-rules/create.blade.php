@extends('layouts.master')

@section('title') Додавання нової теми @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нової теми</h4>
                {{ Breadcrumbs::render('theme_traffic_rules.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('handbook.theme-traffic-rules.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="external_id" class="col-sm-2 col-form-label">ID *</label>
                            <div class="col-sm-2">
                                <input class="form-control @error('external_id')is-invalid @enderror" type="text"
                                       name="external_id" value="{{ old('external_id') }}" id="external_id">
                                @error('external_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="number" class="col-sm-2 col-form-label">Тема № *</label>
                            <div class="col-sm-2">
                                <input class="form-control @error('number')is-invalid @enderror" type="text"
                                       name="number" value="{{ old('number') }}" id="number">
                                @error('number')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        @foreach($languages as $language)
                            <div class="form-group row">
                                <label for="name_{{ $language->id }}" class="col-sm-2 col-form-label">{{ $language->title }} {{ $language->is_default ? '*' : '' }}</label>
                                <div class="col-sm-7">
                                    <input class="form-control @error('translate.name.' . $language->id)is-invalid @enderror" type="text"
                                           name="translate[name][{{ $language->id }}]"
                                           value="{{ old('translate.name.'. $language->id) }}" id="name_{{ $language->id }}">
                                    @error('translate.name.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group row">
                            <label for="category_driver_license_id" class="col-sm-2 col-form-label">Категорії *</label>
                            <div class="col-sm-7 @error('category_driver_license_id')is-invalid @enderror">
                                <select class="form-control select2" id="category_driver_license_id" name="category_driver_license_id[]" data-placeholder="Виберіть категорії" multiple>
                                    @foreach($categoryDriverLicenses as $categoryDriverLicense)
                                        <option value="{{ $categoryDriverLicense->id }}" {{ in_array($categoryDriverLicense->id, (old('category_driver_license_id') ?? [])) ? 'selected' : '' }}>
                                            {{ $categoryDriverLicense->title }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('category_driver_license_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="folder_ids" class="col-sm-2 col-form-label">Папки *</label>
                            <div class="col-sm-7 @error('folder_ids')is-invalid @enderror">
                                <select class="form-control select2" id="folder_ids" name="folder_ids[]" data-placeholder="Виберіть папки" multiple>
                                    @foreach($folders as $folder)
                                        <option value="{{ $folder->id }}" {{ in_array($folder->id, (old('folder_ids') ?? [])) ? 'selected' : '' }}>
                                            {{ $folder->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('folder_ids')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('handbook.theme-traffic-rules.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(".select2").select2({
            minimumResultsForSearch: Infinity
        });
    </script>
@endsection
