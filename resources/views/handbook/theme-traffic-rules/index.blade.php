@extends('layouts.master')

@section('title') @lang('translation.theme_traffic_rules') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.theme_traffic_rules')</h4>
                {{ Breadcrumbs::render('theme_traffic_rules') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')
            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['number']) || isset($filters['name']) || isset($filters['external_id']) || isset($filters['folder']) || !empty($filters['kind']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['number']) || isset($filters['name']) || isset($filters['external_id']) || isset($filters['folder']) || !empty($filters['kind']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('handbook.theme-traffic-rules.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="ID"
                                                   value="{{ $filters['external_id'] ?? '' }}" name="external_id" id="external_id">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Тема №"
                                                   value="{{ $filters['number'] ?? '' }}" name="number" id="number">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Назва теми"
                                                   value="{{ $filters['name'] ?? '' }}" name="name" id="name">
                                        </div>
                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="folder" id="folder" data-placeholder="Папки" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($folders as $folder)
                                                    <option value="{{ $folder->id }}" {{ (isset($filters['folder']) && $filters['folder'] == $folder->id) ? 'selected' : '' }}>{{ $folder->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="without_folder"
                                                       name="kind" id="kind" {{ isset($filters['kind']) && $filters['kind'] == 'without_folder' ? 'checked' : '' }}>
                                                <label class="form-check-label" for="kind">Теми без папок</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('handbook.theme-traffic-rules.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати тему</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th style="width: 100px;">ID</th>
                                <th style="width: 100px;">Тема №</th>
                                <th>{{ $defaultLanguage->title }}</th>
                                <th>Переклади</th>
                                <th>Категорії</th>
                                <th>Питання</th>
                                <th>Папки</th>
                                <th style="width: 110px;">Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($topicTrafficRules as $topicTrafficRule)
                                <tr>
                                    <td>{{ $loop->iteration + $topicTrafficRules->firstItem() - 1 }}</td>
                                    <td>{{ $topicTrafficRule->external_id }}</td>
                                    <td>{{ $topicTrafficRule->number }}</td>
                                    <td>{{ $topicTrafficRule->getTranslate($defaultLanguage->id)->name }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($topicTrafficRule->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ implode(', ', $topicTrafficRule->categoryDriverLicenses->pluck('title')->toArray()) }}</td>
                                    <td>
                                        @if($topicTrafficRule->questions_count)
                                            <a href="{{ route('issue-management.questions.index', ['topic_traffic_rule' => $topicTrafficRule->number]) }}">
                                                {{ $topicTrafficRule->questions_count }}
                                            </a>
                                        @else
                                            {{ $topicTrafficRule->questions_count }}
                                        @endif
                                    </td>
                                    <td>{{ implode(', ', $topicTrafficRule->folders->pluck('name')->toArray()) }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('handbook.theme-traffic-rules.edit', $topicTrafficRule) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('handbook.theme-traffic-rules.destroy', $topicTrafficRule) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити тему?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $topicTrafficRules->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script>
        $(".select2").select2({
            minimumResultsForSearch: Infinity
        });
    </script>
@endsection
