@extends('layouts.master')

@section('title') {{ trans('translation.traffic_lights') }} @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/emojipicker.css') }}">
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">{{ trans('translation.traffic_lights') }}</h4>
                {{ Breadcrumbs::render('traffic-light') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab"
                                   href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('theory.traffic-light.update') }}" id="sendForm">
                        @csrf

                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}" id="{{ $language->abbreviation }}" role="tabpanel">

                                    <div class="form-group row">
                                        <label for="description_{{ $language->id }}" class="col-sm-2 col-form-label">Опис</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control @error('translate.description.' . $language->id)is-invalid @enderror"
                                                id="description_{{ $language->id }}"
                                                data-action="tinymce"
                                                name="translate[description][{{ $language->id }}]">{{ old('translate.description.'. $language->id, $data[$language->id]['description'] ?? '') }}</textarea>
                                            @error('translate.description.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                        @include('inc.token-description')
                                    </div>
                                    @include('inc.markdown-img', ['images' => $markdownImages])
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <a href="{{ route('dashboard') }}" class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('/assets/js/inputEmoji.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/emojis.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>

    <script>
        $(function () {
            $('textarea[data-action="tinymce"]').each(function() {
                new EasyMDE({
                    element: this,
                    toolbar: ["bold", "|", "link", "image", "|", {
                        name: "smile",
                        action: (editor) => {
                            $(this).emojiList(editor);
                        },
                        className: "fa fa-smile",
                        title: "Smile",
                    }, "|", "preview"],
                    status: false,
                    spellChecker: false,
                    renderingConfig: {
                        singleLineBreaks: false
                    }
                });
            });
        });
    </script>
@endsection
