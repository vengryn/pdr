<div class="form-group row">
    <label class="col-sm-2"></label>
    <div class="col-sm-5">
        <input type="file" name="uploadMarkdownImg" id="markdownImg" accept="image/*" multiple>

        <div style="margin-top: 10px" class="table-responsive" data-type="markdownImages">
            <table class="table mb-0">
                <tbody>
                @isset($images)
                    @foreach($images as $image)
                        <tr>
                            <td>
                                <a href="{{ $image->link }}" class="btn btn btn-secondary btn-sm markdown-preview">
                                    <i class="mdi mdi-image-outline"></i>
                                </a>
                            </td>
                            <td style="vertical-align: middle;">{{ basename($image->path) }}</td>
                            <td style="text-align: right;">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-secondary btn-sm" title="Скопіювати посилання на зображення"
                                       data-action="copied" data-value="{{ $image->link }}">
                                        <i class="mdi mdi-link-variant"></i>
                                    </a>
                                    <a href="#" class="btn btn-danger btn-sm" title="Видалити зображення"
                                       data-action="{{ isset($duplicate) ? 'remove-duplicate-markdown-images' : 'remove-markdown-images' }}"
                                       data-path="{{ $image->path }}" data-id="{{ $image->id }}">
                                        <i class="mdi mdi-delete"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endisset
                @foreach(old('markdownImages', []) as $markdownImage)
                    <tr>
                        <td>
                            <a href="{{ \Storage::url($markdownImage) }}" class="btn btn-secondary btn-sm markdown-preview">
                                <i class="mdi mdi-image-outline"></i>
                            </a>
                        </td>
                        <td style="vertical-align: middle;">{{ basename($markdownImage) }}</td>
                        <td style="text-align: right;">
                            <div class="btn-group">
                                <a href="#" class="btn btn-secondary btn-sm" title="Скопіювати посилання на зображення"
                                   data-action="copied" data-value="{{ \Storage::url($markdownImage) }}">
                                    <i class="mdi mdi-link-variant"></i>
                                </a>
                                <a href="#" class="btn btn-danger btn-sm" title="Видалити зображення" data-action="remove-markdown-images"
                                   data-path="{{ $markdownImage }}">
                                    <i class="mdi mdi-delete"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@if($loop->first)
    @foreach(old('markdownImages', []) as $markdownImage)
        <input type="hidden" name="markdownImages[]" value="{{ $markdownImage }}" />
    @endforeach

    @isset($duplicate)
        @foreach($images as $image)
            <input type="hidden" name="markdownDuplicateImages[]" value="{{ $image->path }}" />
        @endforeach
    @endisset
@endif

