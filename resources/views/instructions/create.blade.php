@extends('layouts.master')

@section('title') Додавання нової інструкції @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нової інструкції</h4>
                {{ Breadcrumbs::render('instructions.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('instructions.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="flag" class="col-sm-2 col-form-label">Зображення (25X25 px) *</label>
                            <div class="col-sm-5">
                                <input type="file" name="image" id="image">
                                @error('image')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="text_page_id" class="col-sm-2 col-form-label">Інструкція *</label>
                            <div class="col-sm-5">
                                <select class="form-control select2 @error('text_page_id')is-invalid @enderror" name="text_page_id">
                                    <option></option>
                                    @foreach($textPages as $textPage)
                                        <option value="{{ $textPage->id }}" {{ old('text_page_id') == $textPage->id ? 'selected' : '' }}>
                                            {{ $textPage->getTranslate()->name ?? '' }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('text_page_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('instructions.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(".select2").select2({
            placeholder: "Інструкція",
        });
    </script>
@endsection
