@extends('layouts.master')

@section('title') Рудагування інструкції @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Рудагування інструкції</h4>
                {{ Breadcrumbs::render('instructions.edit', $instruction) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('instructions.update', $instruction) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="flag" class="col-sm-2 col-form-label">Зображення (25X25 px) *</label>
                            <div class="col-sm-5">

                                <div data-id="img-preview">
                                    @if(empty($instruction->image) || $errors->has('image'))
                                        <input type="file" name="image" id="image">
                                        @error('image')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    @else
                                        <div data-id="img-content">
                                            <input type="hidden" name="image_path" value="{{ $instruction->image }}">
                                            <img src="{{ \Storage::url($instruction->image) }}" style="max-width:64px;max-height:28px;margin-right: 10px">
                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="image_path"
                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                        </div>

                                        <div style="display: none;" data-id="img-upload">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="text_page_id" class="col-sm-2 col-form-label">Інструкція *</label>
                            <div class="col-sm-5">
                                <select class="form-control select2 @error('text_page_id')is-invalid @enderror" name="text_page_id">
                                    <option></option>
                                    @foreach($textPages as $textPage)
                                        <option value="{{ $textPage->id }}" {{ old('text_page_id', $instruction->text_page_id) == $textPage->id ? 'selected' : '' }}>
                                            {{ $textPage->getTranslate()->name ?? '' }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('text_page_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('instructions.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(".select2").select2({
            placeholder: "Інструкція",
        });
    </script>
@endsection
