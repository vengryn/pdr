@extends('layouts.master')

@section('title') @lang('translation.instructions') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.instructions')</h4>
                {{ Breadcrumbs::render('instructions') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('instructions.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати інструкцію</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Зображення</th>
                                <th>Назва</th>
                                <th>Переклади</th>
                                <th>Опції</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="sorting" data-entityname="instructions">
                            @foreach($instructions as $instruction)
                                <tr data-itemId="{{ $instruction->id }}">
                                    <td>{{ $loop->iteration + $instructions->firstItem() - 1 }}</td>
                                    <td>@empty(!$instruction->image)<img src="{{ \Storage::url($instruction->image) }}" style="max-width:25px;max-height:25px;">@endempty</td>
                                    <td>{{ $instruction->textPage ?$instruction->textPage->getTranslate()->name ?? '' : '' }}</td>
                                    <td>
                                        @if($instruction->textPage)
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($instruction->textPage->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                        @endif
                                    </td>
                                    <td style="width: 150px;" class="align-middle">
                                        <div class="btn-group">
                                            @if($instruction->textPage)
                                            <a href="{{ route('text-pages.edit', $instruction->textPage) }}" class="btn btn-secondary">
                                                <i class="mdi mdi-eye"></i>
                                            </a>
                                            @endif
                                            <a href="{{ route('instructions.edit', $instruction) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('instructions.destroy', $instruction) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити інструкцію?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                    <td class="sortable-handle align-middle"><i class="mdi mdi-arrow-up-down" style="cursor: move"></i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $instructions->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $( function() {
            let changePosition = function(requestData){
                $.ajax({
                    'url': '{{ route('instructions.sort') }}',
                    'type': 'POST',
                    'data': requestData,
                    'success': function(data) {
                        //
                    },
                    'error': function(){
                        console.error('Something wrong!');
                    }
                });
            };

            let fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            let $sortableTable = $('#sorting');
            let fixed = '.blocked';
            let newParentContainer;

            function sortingAroundFixedPositions(container) {
                let sortable = $(container);
                let statics = $(fixed, container).detach();
                let tagName = statics.prop('tagName');
                let helper = $('<' + tagName + '/>').prependTo(container);
                statics.each(function() {
                    let target = this.dataset.pos;
                    let targetPosition = $(tagName, sortable).eq(target);
                    if (targetPosition.length === 0) {
                        targetPosition = $(tagName, sortable).eq(target - 1)
                    }
                    $(this).insertAfter(targetPosition);
                });
                helper.remove();
            }

            if ($sortableTable.length > 0) {
                $sortableTable.sortable({
                    handle: '.sortable-handle',
                    axis: 'y',
                    cancel: fixed,
                    cursor: "move",

                    start: function() {
                        $(fixed, $sortableTable).each(function() {
                            this.dataset.pos = $(this).index();
                        });
                    },
                    change: function(e, ui) {
                        sortingAroundFixedPositions(this);
                        if (ui.sender) {
                            newParentContainer = this;
                        }
                        if (newParentContainer) {
                            sortingAroundFixedPositions(newParentContainer);
                        }
                    },

                    update: function(a, b){
                        var $sorted = b.item;

                        var $previous = $sorted.prev();
                        var $next = $sorted.next();

                        if ($previous.length > 0) {
                            changePosition({
                                type: 'moveAfter',
                                id: $sorted.data('itemid'),
                                positionEntity: $previous.data('itemid')
                            });
                        } else if ($next.length > 0) {
                            changePosition({
                                type: 'moveBefore',
                                id: $sorted.data('itemid'),
                                positionEntity: $next.data('itemid')
                            });
                        } else {
                            console.error('Something wrong!');
                        }
                    },
                    helper: fixHelper
                });
            }
        });
    </script>
@endsection
