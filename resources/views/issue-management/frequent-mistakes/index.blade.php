@extends('layouts.master')

@section('title') @lang('translation.frequent_mistakes') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.frequent_mistakes')</h4>
                {{ Breadcrumbs::render('frequent-mistakes') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('issue-management.frequent-mistakes.reset') }}"
                                   data-message="Онулити усю статистику кількості невірно даних відповідей для всіх питань?"
                                   data-action="reset"
                                   class="btn btn-primary waves-effect waves-light">Онулити усю статистику</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Питання</th>
                                <th>Кількість невірно даних відповідей</th>
                                <th>Дата початку</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td>{{ $loop->iteration + $questions->firstItem() - 1 }}</td>
                                    <td>
                                        <a href="{{ route('issue-management.questions.edit', $question->id) }}">
                                            {{ $question->getTranslate()->name ?? '' }}
                                        </a>
                                    </td>
                                    <td>{{ $question->count_wrong_answer }}</td>
                                    <td>{{ $question->start_wrong_answer_at ? $question->start_wrong_answer_at->format('d.m.Y') : '' }}</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('issue-management.frequent-mistakes.reset', ['question_id' => $question->id]) }}"
                                               class="btn btn-danger" data-message="Скинути статистику?"
                                               data-action="reset">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $questions->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection

@section('script')
    <script>
        $(document).on('click', '[data-action="reset"]', function (e) {
            e.preventDefault();

            Swal.fire({
                title: $(this).attr('data-message'),
                showCancelButton: true,
                cancelButtonText: 'Ні',
                confirmButtonColor: '#626ed4',
                cancelButtonColor: '#ec4561',
                confirmButtonText: 'Так'
            }).then((result) => {
                if (result.value) {
                    let target = $(this);
                    let src = target.attr('href');

                    $.ajax({
                        type: 'POST',
                        url: src,
                    }).always(function (data) {
                        location.reload();
                    });
                }
            })
        });
    </script>
@endsection
