@extends('layouts.master')

@section('title') @lang('translation.complaints_questions') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.complaints_questions')</h4>
                {{ Breadcrumbs::render('questions-complaints') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th width="3%">№</th>
                                <th width="35%">Питання</th>
                                <th width="35%">Коментар</th>
                                <th width="11%">Учень</th>
                                <th width="11%">Дата</th>
                                <th width="5%">Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questionsComplaints as $questionsComplaint)
                                <tr>
                                    <td>{{ $loop->iteration + $questionsComplaints->firstItem() - 1 }}</td>
                                    <td>
                                        @isset($questionsComplaint->question)
                                        <a href="{{ route('issue-management.questions.edit', $questionsComplaint->question->id) }}">
                                            {{ $questionsComplaint->question->getTranslate()->name ?? '' }}
                                        </a>
                                        @endisset
                                    </td>
                                    <td style="word-break: break-word;">{{ $questionsComplaint->comment }}</td>
                                    <td>
                                        @isset($questionsComplaint->user)
                                            <a href="{{ route('students.edit', $questionsComplaint->user) }}">
                                                {{ $questionsComplaint->user->infoStudent->full_name ?? '' }}
                                            </a>
                                        @endisset
                                    </td>
                                    <td>{{ $questionsComplaint->created_at->format('d.m.Y') }}</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('issue-management.questions-complaints.destroy', $questionsComplaint) }}"
                                               class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити скаргу?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $questionsComplaints->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
