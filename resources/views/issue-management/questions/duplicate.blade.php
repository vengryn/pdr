@extends('layouts.master')

@section('title') Заміна питання @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <style>
        .checkbox {
            position: absolute;
            margin-top: 0.25rem;
            margin-left: 0.5rem;
        }
        .answers .btn-secondary {
            color: #a1a1a1;
        }
        .answers .btn-secondary:hover {
            color: #fff;
            background-color: #ec4561;
            border-color: #ec4561;
        }
        .cm-s-easymde .cm-strikethrough {
            text-decoration: underline;
        }

        .cm-s-easymde del {
            text-decoration: underline;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Заміна питання</h4>
                {{ Breadcrumbs::render('questions.duplicate', $question) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')
            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab" href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('issue-management.questions.store-duplicate', $question) }}" id="sendForm"
                          enctype="multipart/form-data" data-question-internal="{{ $question->is_internal }}">
                        @csrf
                        <input type="hidden" name="type_answer_correct" value="added_answers">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}" id="{{ $language->abbreviation }}" role="tabpanel">
                                    @if(!$language->is_default)
                                        @include('issue-management.questions.inc.alert-answer-error')
                                    @endif

                                    <div class="form-group row">
                                        <label for="external_id" class="col-sm-2 col-form-label">ID *</label>
                                        <div class="col-sm-5">
                                            <input class="form-control @error('external_id')is-invalid @enderror" type="text"
                                                   name="external_id" value="{{ old('external_id', $question->external_id) }}" id="external_id">
                                            @error('external_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    @if($language->is_default)
                                        <div class="form-group row">
                                            <label for="switch" class="col-sm-2 col-form-label">Статус</label>
                                            <div class="col-sm-5">
                                                <input type="checkbox" id="switch" name="status" switch="none" value="1" {{ old('status', $question->status) ? 'checked' : '' }} />
                                                <label for="switch"></label>
                                                @error('status')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="picture" class="col-sm-2 col-form-label">Зображення</label>
                                            <div class="col-sm-5">
                                                <div data-id="img-preview">
                                                    @if(empty($question->picture) || $errors->has('picture'))
                                                        <input type="file" name="picture" id="picture">
                                                        @error('picture')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                    @else
                                                        <div data-id="img-content">
                                                            <input type="hidden" name="picture_path" value="{{ $question->picture }}">
                                                            <a href="{{ \Storage::url($question->picture) }}" class="gallery-popup">
                                                                <img src="{{ \Storage::url($question->picture) }}" style="max-width:64px;max-height:28px;margin-right: 10px">
                                                            </a>
                                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="picture_path"
                                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                                        </div>

                                                        <div style="display: none;" data-id="img-upload">
                                                            <input type="file" name="picture" id="picture">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="topic_traffic_rule_id" class="col-sm-2 col-form-label">Тема *</label>
                                            <div class="col-sm-5 @error('topic_traffic_rule_id')is-invalid @enderror">
                                                <select class="form-control select2" id="topic_traffic_rule_id" name="topic_traffic_rule_id" data-placeholder="Виберіть тему" data-allow-clear="true">
                                                    <option></option>
                                                    @foreach($topicTrafficRules as $topicTrafficRule)
                                                        <option value="{{ $topicTrafficRule->id }}" {{ old('topic_traffic_rule_id', $question->topic_traffic_rule_id) == $topicTrafficRule->id ? 'selected' : '' }}>
                                                            {{ $topicTrafficRule->number }}. {{ $topicTrafficRule->getTranslate()->name ?? '' }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('topic_traffic_rule_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label for="name_{{ $language->id }}" class="col-sm-2 col-form-label">Питання {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control @error('translate.name.' . $language->id)is-invalid @enderror" id="name_{{ $language->id }}"
                                                      name="translate[name][{{ $language->id }}]">{{ old('translate.name.'. $language->id, $question->getTranslate($language->id)->name ?? '') }}</textarea>
                                            @error('translate.name.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="explanation_{{ $language->id }}" class="col-sm-2 col-form-label">Пояснення</label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control @error('translate.explanation.' . $language->id)is-invalid @enderror" id="explanation_{{ $language->id }}"
                                                      data-action="tinymce" name="translate[explanation][{{ $language->id }}]">{{ old('translate.explanation.'. $language->id, $question->getTranslate($language->id)->explanation ?? '') }}</textarea>
                                            @error('translate.explanation.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>

                                        <div class="col-sm-5 token-description">
                                            <p><b>ВАЖЛИВО:</b> для наповнення контентом необхідно керуватися стандартом MarkDown розмітки описаній тут: <a href="https://www.markdownguide.org/basic-syntax" target="_blank">https://www.markdownguide.org/basic-syntax</a></p>
                                            <br>
                                            <p>Важливо пам'ятати, що згідно дизайну у мобільному пристрої із усіх стилістичних оформлень тексту, обробляється:</p>
                                            <p><b>Жирний текст:</b> **Текст**</p>
                                            <p><b>Курсив:</b> _Текст_</p>
                                            <p><b>Підкреслення:</b> ~~Текст~~</p>
                                            <p><b>Цитування:</b> >Текст</p>
                                            <p>Всі інші стилі ігноруються! В веб-версії будуть відображатись всі стилі.</p>
                                            <br>
                                            <p>Так же, в доповнення до описаного стандарту використовуються розширені теги розмітки для вставляння посилань на пункти ПДР, знаки, розмітку.</p>
                                            <p><b>Як добавляти відео-посилання?</b></p>
                                            <p>Посилання на відео: <b>#(Адреса посилання на відео)</b></p>
                                            <p>Посилання на преміум відео: <b>#(Адреса посилання на відео|premium_video)</b></p>
                                        </div>
                                    </div>

                                    @include('inc.markdown-img', ['images' => $question->markdownImages, 'duplicate' => true])

                                    <div class="answers" data-name="questionsAnswers" data-language="{{ $language->id }}" data-default="{{ $language->is_default }}">
                                        <div data-name="items-answer">
                                            @forelse (old('added_answers', []) as $key => $answer)
                                                <div class="form-group row" data-number="{{ $loop->iteration }}" data-internal="true">
                                                    <label for="answer_{{ $loop->iteration }}" class="col-sm-2 col-form-label">Відповідь <span>{{ $loop->iteration }}</span> {{ $language->is_default ? '*' : '' }}
                                                        @if($language->is_default)
                                                            <input class="checkbox" type="radio" name="answer_correct" id="answer_{{ $loop->iteration }}"
                                                                   value="{{ $loop->iteration }}" data-type="added_answers" {{ old('answer_correct') == $key ? 'checked' : '' }} required>
                                                        @endif
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="btn-group mb-2 dropright" style="width: 100%;">
                                                            <textarea class="form-control @error('added_answers.' . $loop->iteration . '.translate.name.' . $language->id)is-invalid @enderror"
                                                                      name="added_answers[{{ $loop->iteration }}][translate][name][{{ $language->id }}]"
                                                            >{{ $answer['translate']['name'][$language->id] ?? '' }}</textarea>

                                                            @if($language->is_default)
                                                                <button type="button" class="btn btn-secondary waves-effect waves-light"
                                                                        data-action="remove" data-message="Ви дійсно хочете видалити відповідь?">
                                                                    <i class="mdi mdi-delete"></i>
                                                                </button>
                                                            @endif
                                                        </div>
                                                        @error('added_answers.' . $loop->iteration . '.translate.name.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                                    </div>
                                                </div>
                                            @empty
                                                @foreach($question->questionsAnswers as $questionsAnswer)
                                                    <div class="form-group row" data-number="{{ $loop->iteration }}" data-internal="true">
                                                        <label for="answer_{{ $loop->iteration }}" class="col-sm-2 col-form-label">Відповідь <span>{{ $loop->iteration }}</span> {{ $language->is_default ? '*' : '' }}
                                                            @if($language->is_default)
                                                                <input class="checkbox" type="radio" name="answer_correct" value="{{ $loop->iteration }}" data-type="added_answers"
                                                                       id="answer_{{ $loop->iteration }}" {{ $questionsAnswer->is_correct ? 'checked' : '' }} required>
                                                            @endif
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="btn-group mb-2 dropright" style="width: 100%;">
                                                            <textarea class="form-control" name="added_answers[{{ $loop->iteration }}][translate][name][{{ $language->id }}]"
                                                            >{{ $questionsAnswer->getTranslate($language->id)->name ?? '' }}</textarea>

                                                                @if($language->is_default)
                                                                    <button type="button" class="btn btn-secondary waves-effect waves-light"
                                                                            data-action="remove" data-message="Ви дійсно хочете видалити відповідь?">
                                                                        <i class="mdi mdi-delete"></i>
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endforelse
                                        </div>

                                        @if($language->is_default)
                                            <div class="col-sm-7 text-right">
                                                <a href="#" data-action="create">Додати відповідь</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('issue-management.questions.edit', $question) }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>

                    <div class="hide" id="answer-clone">
                        <div class="form-group row" data-number="" data-internal="true">
                            <label class="col-sm-2 col-form-label">Відповідь <span></span> <required></required>
                                <input class="checkbox" type="radio" name="answer_correct" required>
                            </label>
                            <div class="col-sm-5">
                                <div class="btn-group mb-2 dropright" style="width: 100%;">
                                    <textarea class="form-control"></textarea>
                                    <button type="button" class="btn btn-secondary waves-effect waves-light"
                                            data-action="remove" data-message="Ви дійсно хочете видалити відповідь?">
                                        <i class="mdi mdi-delete"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/questions.js') }}"></script>

    <script>
        $(".select2").select2();

        $('.gallery-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade'
        });

        $('textarea[data-action="tinymce"]').each(function() {
            new EasyMDE({
                element: this,
                toolbar: ["bold", "italic", {
                    name: "underline",
                    action: EasyMDE.toggleStrikethrough,
                    className: "fa fa-underline",
                    title: "Underline",
                }, {
                    name: "quote",
                    action: (editor) => {
                        var pos = editor.codemirror.getCursor();
                        editor.codemirror.setSelection({line: pos.line, ch: 0});
                        editor.codemirror.replaceSelection(">");
                    },
                    className: "fa fa-quote-left",
                    title: "Quote",
                }, "|", "link", "image", "|", "preview"],
                blockStyles: {
                    italic: '_'
                },
                status: false,
                spellChecker: false,
                renderingConfig: {
                    singleLineBreaks: false
                }
            });
        });
    </script>
@endsection
