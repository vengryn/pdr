<div class="alert alert-warning alert-dismissible fade hide" role="alert" data-type="error">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    Для того, щоб зберегти питання з перекладом вам необхідно заповнити всі поля або залишити їх пустими
</div>
