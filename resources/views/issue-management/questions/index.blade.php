@extends('layouts.master')

@section('title') @lang('translation.questions') @endsection

@section('css')
    <style>
        .icons i {
            display: inline-block;
            font-size: 17px;
        }

        .icons i.success {
            color: #02a499;
        }

        .icons i.secondary {
            color: #808080;
        }

        .icons i.warning {
            color: #f8b425;
        }

        .icons i.primary {
            color: #38a4f8;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.questions')</h4>
                {{ Breadcrumbs::render('questions') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')
            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['content']) || isset($filters['topic_traffic_rule']) || isset($filters['folder']) || isset($filters['status']) || isset($filters['type']) || !empty($filters['kind']) ? '' : 'collapsed' }}"
                           aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter"
                             class="collapse {{ isset($filters['content']) || isset($filters['topic_traffic_rule']) || isset($filters['folder']) || isset($filters['status']) || isset($filters['type']) || !empty($filters['kind']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form"
                                      action="{{ route('issue-management.questions.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Питання"
                                                   value="{{ $filters['content'] ?? '' }}" name="content" id="content">
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="topic_traffic_rule"
                                                    id="topic_traffic_rule" data-minimum-results-for-search="0"
                                                    data-placeholder="Тема" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($topicTrafficRules as $topicTrafficRule)
                                                    <option
                                                        value="{{ $topicTrafficRule->number }}" {{ ($filters['topic_traffic_rule'] ?? '') == $topicTrafficRule->number ? 'selected' : '' }}>
                                                        {{ $topicTrafficRule->number }}
                                                        . {{ $topicTrafficRule->getTranslate()->name ?? '' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="status" id="status"
                                                    data-placeholder="Статус" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                <option
                                                    value="active" {{ ($filters['status'] ?? '') == 'active' ? 'selected' : '' }}>
                                                    Активне
                                                </option>
                                                <option
                                                    value="no-active" {{ ($filters['status'] ?? '') == 'no-active' ? 'selected' : '' }}>
                                                    Неактивне
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="replace"
                                                       name="kind"
                                                       id="replace" {{ isset($filters['kind']) && $filters['kind'] == 'replace' ? 'checked' : '' }}>
                                                <label class="form-check-label" for="replace">Тільки замінені
                                                    питання</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="type" id="type"
                                                    data-placeholder="Тип" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                <option
                                                    value="gsc" {{ ($filters['type'] ?? '') == 'gsc' ? 'selected' : '' }}>
                                                    Питання від ГСЦ
                                                </option>
                                                <option
                                                    value="internal" {{ ($filters['type'] ?? '') == 'internal' ? 'selected' : '' }}>
                                                    Питання ПДР
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="folder" id="folder"
                                                    data-minimum-results-for-search="0"
                                                    data-placeholder="Папки" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($folders as $folder)
                                                    <option
                                                        value="{{ $folder->id }}" {{ ($filters['folder'] ?? '') == $folder->id ? 'selected' : '' }}>
                                                        {{ $folder->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <span>Останнє оновлення: {{ $lastDateUpdating ? \Carbon\Carbon::parse($lastDateUpdating)->format('d.m.Y H:i:s') : '' }}</span>
                        </div>

                        <div class="col-sm-7">
                            <div class="float-right d-md-block">
                                <a href="{{ route('issue-management.questions.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати питання</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th width="70">№</th>
                                <th width="70">ID</th>
                                <th>Питання</th>
                                <th>Тема</th>
                                <th>Папка</th>
                                <th>Пояснення</th>
                                <th>Статус</th>
                                <th style="width: 110px;">Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td>{{ $loop->iteration + $questions->firstItem() - 1 }}</td>
                                    <td>{{ $question->external_id }}</td>
                                    <td><a href="{{ route('issue-management.questions.edit', $question) }}">
                                            {{ $question->copy && $question->is_internal ? 'Копія.' : '' }} {{ $question->getTranslate()->name ?? '' }}
                                        </a></td>
                                    <td>
                                        <a href="{{ route('issue-management.questions.index', ['topic_traffic_rule' => $question->topicTrafficRule->number ?? '' ]) }}">
                                            {{ $question->topicTrafficRule->number ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        @foreach($question->topicTrafficRule->folders as $folder)
                                            <a href="{{ route('issue-management.questions.index', ['folder' => $folder->id ]) }}">
                                                {{ $folder->name }} @if(!$loop->last), @endif
                                            </a>
                                        @endforeach
                                    </td>
                                    <td>
                                        @if(!empty($question->getTranslate()->explanation ?? null))
                                            <span class="icons"><i data-toggle="tooltip" data-placement="top"
                                                                   class="mdi mdi-comment-quote-outline"
                                                                   title="Додано пояснення для питання"></i></span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="icons">
                                            <i class="{{ $question->status ? 'mdi mdi-check-circle success' : 'mdi mdi-close-circle-outline secondary' }}"
                                               data-toggle="tooltip" data-placement="top"
                                               title="{{ $question->status ? 'Активне питання' : 'Неактивне питання' }}"></i>
                                            {!! $question->copy ? '<i class="mdi mdi-file-replace warning" data-toggle="tooltip" data-placement="top" title="Дублікат питання"></i>' : '' !!}
                                            {!! $question->is_internal && !$question->copy ? '<i class="mdi mdi-file-document-box-check primary" data-toggle="tooltip" data-placement="top" title="Внутрішнє питання"></i>' : '' !!}
                                        </span>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('issue-management.questions.edit', $question) }}"
                                               class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('issue-management.questions.destroy', $question) }}"
                                               class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити питання?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    @if($questions->isNotEmpty())
                        <p class="mt-2">{{ $questions->firstItem() }} - {{ $questions->lastItem() }}
                            з {{ $questions->total() }}</p>
                    @endif

                    {{ $questions->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script>
        $(function () {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });
        });
    </script>
@endsection
