@extends('layouts.master')

@section('title') Додавання нової мови @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нової мови</h4>
                {{ Breadcrumbs::render('languages.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('languages.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 col-form-label">Назва *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="title" value="{{ old('title') }}" id="title">
                                @error('title')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="abbreviation" class="col-sm-2 col-form-label">Скорочення *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="abbreviation" value="{{ old('abbreviation') }}" id="abbreviation">
                                @error('abbreviation')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="flag" class="col-sm-2 col-form-label">Прапор *</label>
                            <div class="col-sm-5">
                                <input type="file" name="flag" id="flag">
                                @error('flag')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('languages.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection
