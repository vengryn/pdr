@extends('layouts.master')

@section('title') Редагування мови@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування мови</h4>
                {{ Breadcrumbs::render('languages.edit', $language) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('languages.update', $language) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 col-form-label">Назва *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="title" value="{{ old('title', $language->title) }}" id="title">
                                @error('title')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="abbreviation" class="col-sm-2 col-form-label">Скорочення *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="abbreviation" value="{{ old('abbreviation', $language->abbreviation) }}" id="abbreviation">
                                @error('abbreviation')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="flag" class="col-sm-2 col-form-label">Прапор *</label>
                            <div class="col-sm-5">
                                <div data-id="img-preview">
                                    @if(empty($language->flag) || $errors->has('flag'))
                                        <input type="file" name="flag" id="flag">
                                        @error('flag')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    @else
                                        <div data-id="img-content">
                                            <input type="hidden" name="flag_path" value="{{ $language->flag }}">
                                            <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:28px;margin-right: 10px">
                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="flag_path"
                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                        </div>

                                        <div style="display: none;" data-id="img-upload">
                                            <input type="file" name="flag" id="flag">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('languages.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection
