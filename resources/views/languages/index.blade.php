@extends('layouts.master')

@section('title') @lang('translation.languages') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.languages')</h4>
                {{ Breadcrumbs::render('languages') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">

                    <div class="col-sm-12">
                        <div class="float-right d-md-block mb-3">
                            <a href="{{ route('languages.create') }}"
                               class="btn btn-primary waves-effect waves-light">Додати мову</a>
                        </div>
                    </div>

                    <div class="table-responsive">

                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Скорочення</th>
                                <th>Прапор</th>
                                <th>Опції</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="sorting" data-entityname="languages">
                            @foreach($languages as $language)
                                <tr class="{{ $language->is_default ? 'blocked' : '' }}" data-itemId="{{ $language->id }}">
                                    <td class="align-middle">{{ $loop->iteration + $languages->firstItem() - 1 }}</td>
                                    <td class="align-middle">{{ $language->title }}</td>
                                    <td class="align-middle">{{ $language->abbreviation }}</td>
                                    <td class="align-middle">@empty(!$language->flag)<img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:24px;">@endempty</td>
                                    <td style="width: 150px;" class="align-middle">
                                        <div class="btn-group">
                                            <a href="{{ route('languages.edit', $language) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('languages.destroy', $language) }}" class="btn btn-danger {{ $language->is_default ? 'disabled' : '' }}"
                                               data-message="Ви дійсно хочете видалити мову?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                    <td class="sortable-handle align-middle"><i class="mdi mdi-arrow-up-down" style="cursor: move"></i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $languages->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $( function() {
            let changePosition = function(requestData){
                $.ajax({
                    'url': '{{ route('languages.sort') }}',
                    'type': 'POST',
                    'data': requestData,
                    'success': function(data) {
                        //
                    },
                    'error': function(){
                        console.error('Something wrong!');
                    }
                });
            };

            let fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            let $sortableTable = $('#sorting');
            let fixed = '.blocked';
            let newParentContainer;

            function sortingAroundFixedPositions(container) {
                let sortable = $(container);
                let statics = $(fixed, container).detach();
                let tagName = statics.prop('tagName');
                let helper = $('<' + tagName + '/>').prependTo(container);
                statics.each(function() {
                    let target = this.dataset.pos;
                    let targetPosition = $(tagName, sortable).eq(target);
                    if (targetPosition.length === 0) {
                        targetPosition = $(tagName, sortable).eq(target - 1)
                    }
                    $(this).insertAfter(targetPosition);
                });
                helper.remove();
            }

            if ($sortableTable.length > 0) {
                $sortableTable.sortable({
                    handle: '.sortable-handle',
                    axis: 'y',
                    cancel: fixed,
                    cursor: "move",

                    start: function() {
                        $(fixed, $sortableTable).each(function() {
                            this.dataset.pos = $(this).index();
                        });
                    },
                    change: function(e, ui) {
                        sortingAroundFixedPositions(this);
                        if (ui.sender) {
                            newParentContainer = this;
                        }
                        if (newParentContainer) {
                            sortingAroundFixedPositions(newParentContainer);
                        }
                    },

                    update: function(a, b){
                        var $sorted = b.item;

                        var $previous = $sorted.prev();
                        var $next = $sorted.next();

                        if ($previous.length > 0) {
                            changePosition({
                                type: 'moveAfter',
                                id: $sorted.data('itemid'),
                                positionEntity: $previous.data('itemid')
                            });
                        } else if ($next.length > 0) {
                            changePosition({
                                type: 'moveBefore',
                                id: $sorted.data('itemid'),
                                positionEntity: $next.data('itemid')
                            });
                        } else {
                            console.error('Something wrong!');
                        }
                    },
                    helper: fixHelper
                });
            }
        });
    </script>
@endsection
