<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                © <script>document.write(new Date().getFullYear())</script> "@lang('translation.project_name')"
            </div>
        </div>
    </div>
</footer>
