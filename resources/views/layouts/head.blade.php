<!-- Bootstrap Css -->
<link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="{{ URL::asset('/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
{{--Select 2--}}
<link href="{{ URL::asset('/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<!-- App Css -->
<link href="{{ URL::asset('/assets/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
<!-- Sweetalert2 -->
<link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

@yield('css')

<style>
    .select2-selection__clear {
        z-index: 10;
        margin-right: 10px;
    }
    .is-invalid .select2-selection,
    .needs-validation ~ span > .select2-dropdown{
        border-color: #ec4561 !important;
        padding-right: calc(1.5em + 0.75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23ec4561' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23ec4561' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E");
        background-repeat: no-repeat;
        background-position: right calc(0.375em + 0.1875rem) center;
        background-size: calc(0.75em + 0.375rem) calc(0.75em + 0.375rem);
    }
    .vertical-collpsed .vertical-menu #sidebar-menu > ul ul {
        white-space: normal;
    }
    .hide {
        display: none;
    }
    a.gallery-popup {
        cursor: zoom-in;
    }
    .datepicker {
        z-index: 9999 !important;
    }
    input::-webkit-contacts-auto-fill-button {
        opacity: 0;
    }
    td[data-action="copied"]:hover {
        opacity: 0.7;
    }
    td[data-action="copied"] {
        cursor: pointer;
    }
    .CodeMirror {
        min-height: 350px;
    }
    .CodeMirror-scroll {
       height: 350px;
    }

    .short-markdown .CodeMirror {
        min-height: 250px;
        height: auto;
    }
    .short-markdown .CodeMirror-scroll {
        min-height: 250px;
        height: 250px;
    }
    .token-description p {
        margin-bottom: 0.2rem
    }
    blockquote {
        display: block;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 40px;
        margin-inline-end: 40px;
        font-style: italic;
        border-left: 3px solid #e0e0e0;
        padding-left: 0.6em;
        margin-left: 0;
    }
</style>
