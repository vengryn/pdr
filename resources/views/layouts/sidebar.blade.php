<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">@lang('translation.Main')</li>

                @can('teachers')
                    <li class="{{ request()->routeIs('teachers*') ? 'mm-active' : '' }}">
                        <a href="{{ route('teachers.index') }}"
                           class="waves-effect {{ request()->routeIs('teachers*') ? 'active' : '' }}">
                            <i class="mdi mdi-account-tie-outline"></i>
                            <span>@lang('translation.teachers')</span>
                        </a>
                    </li>
                @endcan

                @can('groups')
                <li class="{{ request()->routeIs('groups*') ? 'mm-active' : '' }}">
                    <a href="{{ route('groups.index') }}"
                       class="waves-effect {{ request()->routeIs('groups*') ? 'active' : '' }}">
                        <i class="mdi mdi-account-group"></i>
                        <span>@lang('translation.groups')</span>
                    </a>
                </li>
                @endcan

                @can('display-students')
                <li class="{{ request()->routeIs('students*') ? 'mm-active' : '' }}">
                    <a href="{{ route('students.index') }}"
                       class="waves-effect {{ request()->routeIs('students*') ? 'active' : '' }}">
                        <i class="ion ion-md-school"></i>
                        <span>@lang('translation.students')</span>
                    </a>
                </li>
                @endcan

                @can('issue-management')
                    <li class="{{ request()->routeIs('issue-management*') ? 'mm-active' : '' }}">
                        <a href="javascript: void(0);"
                           class="has-arrow waves-effect {{ request()->routeIs('issue-management*') ? 'mm-active' : '' }}">
                            <i class="mdi mdi-calendar-question"></i>
                            <span>@lang('translation.issue_management')</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="{{ route('issue-management.questions.index') }}"
                                   class="{{ request()->routeIs('issue-management.questions.*') ? 'active' : '' }}">@lang('translation.questions')</a>
                            </li>
                            <li><a href="{{ route('issue-management.frequent-mistakes.index') }}">@lang('translation.frequent_mistakes')</a></li>
                            <li><a href="{{ route('issue-management.questions-complaints.index') }}">@lang('translation.complaints_questions')</a>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('item-menu')
                    <li class="{{ request()->routeIs('theory*') ? 'mm-active' : '' }}">
                        <a href="javascript: void(0);" class="has-arrow waves-effect {{ request()->routeIs('theory*') ? 'mm-active' : '' }}">
                            <i class="mdi mdi-book-open-variant"></i>
                            <span>@lang('translation.theory')</span>
                        </a>

                        <ul class="sub-menu" aria-expanded="true">
                            <li class="{{ request()->routeIs('theory.internal-theme-traffic-rules*', 'theory.item-traffic-rules*') ? 'mm-active' : '' }}">
                                <a href="javascript: void(0);" class="has-arrow {{ request()->routeIs('theory.internal-theme-traffic-rules*', 'theory.item-traffic-rules*') ? 'mm-active' : '' }}">
                                    @lang('translation.traffic_rules')
                                </a>
                                <ul class="sub-menu" aria-expanded="true">
                                    <li><a href="{{ route('theory.internal-theme-traffic-rules.index') }}"
                                           class="{{ request()->routeIs('theory.internal-theme-traffic-rules*') ? 'active' : '' }}">@lang('translation.internal_theme_traffic_rules')</a>
                                    </li>

                                    <li><a href="{{ route('theory.item-traffic-rules.index') }}"
                                           class="{{ request()->routeIs('theory.item-traffic-rules*') ? 'active' : '' }}">@lang('translation.items')</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="{{ request()->routeIs('theory.road-signs*', 'theory.signs*') ? 'mm-active' : '' }}">
                                <a href="javascript: void(0);" class="has-arrow {{ request()->routeIs('theory.road-signs*', 'theory.signs*') ? 'mm-active' : '' }}">
                                    @lang('translation.road_signs')
                                </a>
                                <ul class="sub-menu" aria-expanded="true">
                                    <li><a href="{{ route('theory.road-signs.index') }}"
                                           class="{{ request()->routeIs('theory.road-signs*') ? 'active' : '' }}">@lang('translation.type_road_signs')</a>
                                    </li>
                                    <li><a href="{{ route('theory.signs.index') }}"
                                           class="{{ request()->routeIs('theory.signs*') ? 'active' : '' }}">@lang('translation.signs')</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="{{ request()->routeIs('theory.road-markings*', 'theory.markings*') ? 'mm-active' : '' }}">
                                <a href="javascript: void(0);" class="has-arrow {{ request()->routeIs('theory.road-markings*', 'theory.markings*') ? 'mm-active' : '' }}">
                                    @lang('translation.marking')
                                </a>
                                <ul class="sub-menu" aria-expanded="true">
                                    <li><a href="{{ route('theory.road-markings.index') }}"
                                           class="{{ request()->routeIs('theory.road-markings*') ? 'active' : '' }}">@lang('translation.type_markings')</a>
                                    </li>
                                    <li><a href="{{ route('theory.markings.index') }}"
                                           class="{{ request()->routeIs('theory.markings*') ? 'active' : '' }}">@lang('translation.markings')</a>
                                    </li>
                                </ul>
                            </li>

                            <li><a href="{{ route('theory.regulator.index') }}">@lang('translation.regulator')</a></li>
                            <li><a href="{{ route('theory.traffic-light.index') }}">@lang('translation.traffic_lights')</a></li>
                            <li><a href="{{ route('theory.video-lessons-traffic-rule.index') }}">@lang('translation.video_lessons_traffic_rule')</a></li>
                        </ul>
                    </li>
                @endcan

                @can('promo-codes')
                    <li class="{{ request()->routeIs('promo-codes*') ? 'mm-active' : '' }}">
                        <a href="{{ route('promo-codes.index') }}"
                           class="waves-effect {{ request()->routeIs('promo-codes*') ? 'mm-active' : '' }}">
                            <i class="mdi mdi-text-shadow"></i>
                            <span>@lang('translation.promo_code')</span>
                        </a>
                    </li>
                @endcan

                @can('payments')
                    <li>
                        <a href="{{ route('payments.index') }}" class="waves-effect">
                            <i class="mdi mdi-playlist-check"></i>
                            <span>@lang('translation.payments')</span>
                        </a>
                    </li>
                @endcan

                @can('push-notifications')
                <li>
                    <a href="{{ route('push-notifications.index') }}" class="waves-effect">
                        <i class="mdi mdi-bell "></i>
                        <span>@lang('translation.push_notification')</span>
                    </a>
                </li>
                @endcan

                @can('item-menu')
                    <li class="menu-title">@lang('translation.Settings')</li>

                    <li class="{{ request()->routeIs('handbook*') ? 'mm-active' : '' }}">
                        <a href="javascript: void(0);"
                           class="has-arrow waves-effect {{ request()->routeIs('handbook*') ? 'mm-active' : '' }}">
                            <i class="mdi mdi-book-outline"></i>
                            <span>@lang('translation.handbook')</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="true">
                            <li><a href="{{ route('handbook.theme-traffic-rules.index') }}"
                                   class="{{ request()->routeIs('handbook.theme-traffic-rules*') ? 'active' : '' }}">@lang('translation.theme_traffic_rules')</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ route('category-driver-licenses.index') }}" class="waves-effect">
                            <i class="mdi mdi-account-card-details"></i>
                            <span>@lang('translation.categories')</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('subscriptions*') ? 'mm-active' : '' }}">
                        <a href="javascript: void(0);"
                           class="has-arrow waves-effect {{ request()->routeIs('subscriptions*') ? 'mm-active' : '' }}">
                            <i class="ion ion-md-pricetags"></i>
                            <span>@lang('translation.premium_subscription')</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="true">
                            <li><a href="{{ route('subscriptions.index') }}"
                                   class="{{ request()->routeIs('subscriptions*') ? 'active' : '' }}">@lang('translation.price_list')</a>
                            </li>
                            @can('auto-premium')
                            <li><a href="{{ route('auto-premium.index') }}">@lang('translation.auto_premium')</a></li>
                            @endcan
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect">
                            <i class="mdi mdi-office-building"></i>
                            <span>@lang('translation.service_centers')</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('text-pages*') ? 'mm-active' : '' }}">
                        <a href="{{ route('text-pages.index') }}"
                           class="waves-effect {{ request()->routeIs('text-pages*') ? 'active' : '' }}">
                            <i class="mdi mdi-text-recognition"></i>
                            <span>@lang('translation.text_pages')</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('languages*') ? 'mm-active' : '' }}">
                        <a href="{{ route('languages.index') }}"
                           class="waves-effect {{ request()->routeIs('languages*') ? 'active' : '' }}">
                            <i class="mdi mdi-translate"></i>
                            <span>@lang('translation.languages')</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('translations*') ? 'mm-active' : '' }}">
                        <a href="{{ route('translations.index') }}"
                           class="waves-effect {{ request()->routeIs('translations*') ? 'active' : '' }}">
                            <i class="mdi mdi-google-translate"></i>
                            <span>@lang('translation.translations')</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('instructions.index') }}" class="waves-effect">
                            <i class="mdi mdi-message-text-outline"></i>
                            <span>@lang('translation.instructions')</span>
                        </a>
                    </li>

                    <li class="{{ request()->routeIs('support-services*') ? 'mm-active' : '' }}">
                        <a href="{{ route('support-services.index') }}"
                           class="waves-effect {{ request()->routeIs('support-services*') ? 'active' : '' }}">
                            <i class="mdi mdi-help-circle-outline"></i>
                            <span>@lang('translation.support_service')</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('general-settings.index') }}" class="waves-effect">
                            <i class="mdi mdi-settings"></i>
                            <span>@lang('translation.general_settings')</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('updating-msc.index') }}" class="waves-effect">
                            <i class="mdi mdi-file-restore"></i>
                            <span>@lang('translation.updating_msc')</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('logs.index') }}" class="waves-effect">
                            <i class="mdi mdi-information-outline"></i>
                            <span>@lang('translation.logs')</span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
