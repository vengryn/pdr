@extends('layouts.master')

@section('title') @lang('translation.payments') @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.payments')</h4>
                {{ Breadcrumbs::render('payments') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['student_name']) || isset($filters['marketplace'])
                                        || isset($filters['date_start']) || isset($filters['date_end']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['student_name']) || isset($filters['marketplace'])
                                        || isset($filters['date_start']) || isset($filters['date_end']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('payments.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="ПІБ учня"
                                                   value="{{ $filters['student_name'] ?? '' }}" name="student_name" id="student_name">
                                        </div>

                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="marketplace" id="marketplace" data-placeholder="Маркетплейс"
                                                    data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($marketplaces as $key => $marketplace)
                                                    <option value="{{ $key }}" {{ ($filters['marketplace'] ?? '') == $key ? 'selected' : '' }}>
                                                        {{ $marketplace }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-avto">Період</div>

                                        <div class="col-sm-3">
                                            <div class="input-daterange input-group" id="date-range">
                                                <input type="text" class="form-control" name="date_start" placeholder="Від"
                                                       autocomplete="off" value="{{ $filters['date_start'] ?? '' }}" />
                                                <input type="text" class="form-control" name="date_end" placeholder="До"
                                                       autocomplete="off" value="{{ $filters['date_end'] ?? '' }}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>ПІБ учня</th>
                                <th>Підписка</th>
                                <th>Промокод</th>
                                <th>Маркетплейс</th>
                                <th width="200">Дата</th>
                                <th width="150">Сума</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $loop->iteration + $payments->firstItem() - 1 }}</td>
                                    <td>{{ $payment->student->infoStudent->full_name ?? '' }}</td>
                                    <td>{{ $payment->subscription }}</td>
                                    <td>{{ $payment->promo_code }}</td>
                                    <td>
                                        @switch($payment->marketplace)
                                            @case(1)
                                                <img src="{{ asset('assets/images/logo/google-play-store.svg') }}">
                                            @break
                                            @case(2)
                                                <img src="{{ asset('assets/images/logo/app-store.svg') }}">
                                            @break
                                            @case(3)
                                                LiqPay
                                            @break
                                            @case(4)
                                                UaPay
                                            @break
                                        @endswitch
                                    </td>
                                    <td>{{ $payment->transaction_at ? $payment->transaction_at->format('d.m.Y H:i:s') : '' }}</td>
                                    <td>{{ number_format($payment->amount, 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $payments->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.uk.min.js') }}"></script>

    <script>
        $( function() {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });

            $('#date-range').datepicker({
                language: 'uk',
                keepEmptyValues: true,
                todayHighlight: true
            });

            $( "#student_name" ).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "{{ route('payments.getAutocompleteStudent') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                }
            });
        });
    </script>
@endsection
