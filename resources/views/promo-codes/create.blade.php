@extends('layouts.master')

@section('title') Додавання нового промокоду @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нового промокоду</h4>
                {{ Breadcrumbs::render('promo_code.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('promo-codes.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="code" class="col-sm-2 col-form-label">Промокод *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('code')is-invalid @enderror" type="text" name="code" value="{{ old('code') }}" id="code">
                                @error('code')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="discount" class="col-sm-2 col-form-label">Розмір знижки %</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('discount')is-invalid @enderror" type="text" name="discount" value="{{ old('discount') }}" id="discount">
                                @error('discount')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="discount" class="col-sm-2 col-form-label">Термін дії *</label>
                            <div class="col-sm-5">
                                <div class="input-daterange input-group" id="date-range">
                                    <input type="text" class="form-control @error('date_start')is-invalid @enderror" name="date_start"
                                           autocomplete="off" value="{{ old('date_start') }}" />
                                    @error('date_start')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror

                                    <input type="text" class="form-control @error('date_end')is-invalid @enderror" name="date_end"
                                           autocomplete="off" value="{{ old('date_end') }}" />
                                    @error('date_end')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('promo-codes.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.uk.min.js') }}"></script>

    <script>
        $('#date-range').datepicker({
            language: 'uk',
        });
    </script>

@endsection
