@extends('layouts.master')

@section('title') Редагування промокоду @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування промокоду</h4>
                {{ Breadcrumbs::render('promo_code.edit', $promoCode) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('promo-codes.update', $promoCode) }}">
                        @csrf
                        @method('PUT')

                        @if($promoCode->isTypeMarketing())
                        <div class="form-group row">
                            <label for="code" class="col-sm-2 col-form-label">Промокод *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('code')is-invalid @enderror" type="text" name="code"
                                       value="{{ old('code', $promoCode->code) }}" id="code">
                                @error('code')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="discount" class="col-sm-2 col-form-label">Розмір знижки %</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('discount')is-invalid @enderror" type="text" name="discount"
                                       value="{{ old('discount', $promoCode->discount) }}" id="discount">
                                @error('discount')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        @if($promoCode->isTypeTeacher() || $promoCode->isTypeStudent())
                        <div class="form-group row">
                            <label for="cashback" class="col-sm-2 col-form-label">Кешбек %</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('cashback')is-invalid @enderror" type="text" name="cashback"
                                       value="{{ old('cashback', $promoCode->cashback) }}" id="code">
                                @error('cashback')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>
                        @endif

                        @if($promoCode->isTypeMarketing())
                        <div class="form-group row">
                            <label for="discount" class="col-sm-2 col-form-label">Термін дії *</label>
                            <div class="col-sm-5">
                                <div class="input-daterange input-group" id="date-range">
                                    <input type="text" class="form-control @error('date_start')is-invalid @enderror" name="date_start" autocomplete="off"
                                           value="{{ old('date_start', ($promoCode->date_start ? $promoCode->date_start->format('d.m.Y') : '')) }}" />
                                    @error('date_start')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror

                                    <input type="text" class="form-control @error('date_end')is-invalid @enderror" name="date_end" autocomplete="off"
                                           value="{{ old('date_end', ($promoCode->date_end ? $promoCode->date_end->format('d.m.Y') : '')) }}" />
                                    @error('date_end')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('promo-codes.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.uk.min.js') }}"></script>

    <script>
        $('#date-range').datepicker({
            language: 'uk',
        });
    </script>

@endsection
