@extends('layouts.master')

@section('title') @lang('translation.promo_code') @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.promo_code')</h4>
                {{ Breadcrumbs::render('promo_code') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['code']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['code']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('promo-codes.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Промокод"
                                                   value="{{ $filters['code'] ?? '' }}" name="code" id="code">
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('promo-codes.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати промокод</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Тип</th>
                                <th>Знижка</th>
                                <th>Кешбек</th>
                                <th>Промокод</th>
                                <th>Термін</th>
                                <th>Статус</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($promoCodes as $promoCode)
                                <tr>
                                    <td>{{ $loop->iteration + $promoCodes->firstItem() - 1 }}</td>
                                    <td>{{ $promoCode->type_name }}</td>
                                    <td>{{ $promoCode->discount }}{{ $promoCode->discount ? '%' : '' }}</td>
                                    <td>{{ $promoCode->cashback }}{{ $promoCode->cashback ? '%' : '' }}</td>
                                    <td>{{ $promoCode->code }}</td>
                                    <td>{{ $promoCode->date_term }}</td>
                                    <td><i class="{{ $promoCode->status ? 'mdi mdi-check-circle success' : 'mdi mdi-close-circle-outline secondary' }}"></i></td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('promo-codes.edit', $promoCode) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('promo-codes.destroy', $promoCode) }}" class="btn btn-danger {{ $promoCode->isTypeTeacher() || $promoCode->isTypeStudent() ? 'disabled' : '' }}"
                                               data-message="Ви дійсно хочете видалити промокод?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $promoCodes->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>
        $( function() {
            $( "#code" ).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "{{ route('promo-codes.getAutocompletePromoCode') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                }
            });
        });
    </script>
@endsection
