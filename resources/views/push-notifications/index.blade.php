@extends('layouts.master')

@section('title') @lang('translation.push_notification') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.push_notification')</h4>
                {{ Breadcrumbs::render('push-notifications') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal"
                                        data-target=".bs-example-modal-lg">Відправити повідомлення</button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th width="5%">№</th>
                                <th width="30%">Заголовок</th>
                                <th width="50%">Повідомлення</th>
                                <th width="15%">Час</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pushNotifications as $pushNotification)
                                <tr>
                                    <td>{{ $loop->iteration + $pushNotifications->firstItem() - 1 }}</td>
                                    <td>{{ $pushNotification->title }}</td>
                                    <td>{{ $pushNotification->message }}</td>
                                    <td>{{ $pushNotification->created_at ? $pushNotification->created_at->format('d.m.Y H:i:s') : '' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $pushNotifications->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

    <!-- start modal -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="pushNotificationModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="pushNotificationModal">Відправка Push Notification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('push-notifications.store') }}" id="pushNotification">
                        @csrf
                        <input type="hidden" name="page" value="{{ request()->page }}">
                        <input type="hidden" name="paginate" value="{{ request()->paginate }}">

                        <div class="form-group row">
                            <label for="title" class="col-lg-12 col-form-label">Заголовок</label>
                            <div class="col-lg-12">
                                <input type="text" name="title" id="title" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-lg-12 col-form-label">Текст повідомлення</label>
                            <div class="col-lg-12">
                                <textarea id="message" name="message" rows="5" class="form-control" required></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect waves-light" form="pushNotification">Відправити</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Відміна</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End modal -->
@endsection
