<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Звіт</title>
    <link href="http://pdr.loc/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="http://pdr.loc/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <style>
        body { font-family: DejaVu Sans, sans-serif; background-color: #fff; }
        table thead td { font-weight: bold }
        table td { font-size: 13px; }
    </style>
</head>
<body>
<div class="table-responsive">
    <table class="table mb-0">
        <thead>
        <tr>
            <td>№</td>
            <td>ПІБ учня</td>
            <td>Група</td>
            <td>Підписка</td>
            <td>Знижка</td>
            <td>Сума до сплати</td>
        </tr>
        </thead>
        <tbody>
        @foreach($payments as $payment)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $payment->student->infoStudent->full_name ?? '' }}</td>
                <td>{{ $payment->student->group->title ?? '' }}</td>
                <td>{{ $payment->subscription }} ({{ $payment->validity }} м)</td>
                <td>{{ $payment->discount }}</td>
                <td class="text-right">{{ number_format($payment->amount, 2) }}</td>
            </tr>
        @endforeach
        <tr>
            <td class="thick-line"></td>
            <td class="thick-line"></td>
            <td class="thick-line"></td>
            <td class="thick-line"></td>
            <td class="thick-line text-center">
                <span style="font-weight: bold;">Загальна сума</span></td>
            <td class="thick-line text-right">
                <h5 class="m-0">{{ number_format($payments->sum('amount'), 2) }}</h5>
            </td>
        </tr>
        </tbody>
    </table>
</body>
</html>
