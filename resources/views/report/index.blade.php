@extends('layouts.master')

@section('title') @lang('translation.report') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.report')</h4>
                {{ Breadcrumbs::render('report') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['promo_code']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['promo_code']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('report.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Промокод"
                                                   value="{{ $filters['promo_code'] ?? '' }}" name="promo_code" id="promo_code">
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('report.import') }}" class="btn btn-primary waves-effect waves-light">Експорт</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th width="70">№</th>
                                <th>ПІБ учня</th>
                                <th>Група</th>
                                <th>Підписка</th>
                                <th width="150">Знижка</th>
                                <th width="150">Сума до сплати</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $loop->iteration + $payments->firstItem() - 1 }}</td>
                                    <td>{{ $payment->student->infoStudent->full_name ?? '' }}</td>
                                    <td>{{ $payment->student->group->title ?? '' }}</td>
                                    <td>{{ $payment->subscription }} ({{ $payment->validity }} м)</td>
                                    <td>{{ $payment->discount }}</td>
                                    <td class="text-right">{{ number_format($payment->amount, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td class="thick-line"></td>
                                <td class="thick-line"></td>
                                <td class="thick-line"></td>
                                <td class="thick-line"></td>
                                <td class="thick-line text-center">
                                    <strong>Загальна сума</strong></td>
                                <td class="thick-line text-right">
                                    <h5 class="m-0">{{ number_format($payments->sum('amount'), 2) }}</h5>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    @if($payments->isNotEmpty())
                        <p class="mt-2">{{ $payments->firstItem() }} - {{ $payments->lastItem() }} з {{ $payments->total() }}</p>
                    @endif

                    {{ $payments->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
