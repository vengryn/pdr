@extends('layouts.master')

@section('title') Додавання нової служби підтримки @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нової служби підтримки</h4>
                {{ Breadcrumbs::render('social-support-service.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('support-services.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Назва *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('name')is-invalid @enderror" type="text" name="name" value="{{ old('name') }}" id="name">
                                @error('name')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="link" class="col-sm-2 col-form-label">Посилання *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('link')is-invalid @enderror" type="text" name="link" value="{{ old('link') }}" id="link">
                                @error('link')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="logo" class="col-sm-2 col-form-label">Логотип *</label>
                            <div class="col-sm-5">
                                <input type="file" name="logo" id="logo">
                                <div><span style="font-size: 11px;">Розмір: до 10кб; формат: jpeg,jpg,png,gif; роздільна здатність: 50x50px</span></div>
                                @error('logo')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('support-services.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
