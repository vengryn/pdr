@extends('layouts.master')

@section('title') Редагування служби підтримки @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування служби підтримки</h4>
                {{ Breadcrumbs::render('social-support-service.edit', $socialSupportService) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('support-services.update', $socialSupportService) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Назва *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('name')is-invalid @enderror" type="text" name="name"
                                       value="{{ old('name', $socialSupportService->name) }}" id="name">
                                @error('name')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="link" class="col-sm-2 col-form-label">Посилання *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('link')is-invalid @enderror" type="text" name="link"
                                       value="{{ old('link', $socialSupportService->link) }}" id="link">
                                @error('link')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="logo" class="col-sm-2 col-form-label">Логотип *</label>
                            <div class="col-sm-5">

                                <div data-id="img-preview">
                                    @if(empty($socialSupportService->logo) || $errors->has('logo'))
                                        <input type="file" name="logo" id="logo">
                                        <div><span style="font-size: 11px;">Розмір: до 10кб; формат: jpeg,jpg,png,gif; роздільна здатність: 50x50px</span></div>
                                        @error('logo')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                    @else
                                        <div data-id="img-content">
                                            <input type="hidden" name="logo_path" value="{{ $socialSupportService->logo }}">
                                            <img src="{{ \Storage::url($socialSupportService->logo) }}" style="max-width:50px;max-height:50px;margin-right: 10px;">
                                            <a href="#" class="btn btn-danger btn-sm" data-action="removeImg" data-disabled="logo_path"
                                               data-title="Ви дійсно бажаєте видалити зображення?"><i class="fa fas fa-trash"></i></a>
                                        </div>

                                        <div style="display: none;" data-id="img-upload">
                                            <input type="file" name="logo" id="logo">
                                            <div><span style="font-size: 11px;">Розмір: до 10кб; формат: jpeg,jpg,png,gif; роздільна здатність: 50x50px</span></div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('support-services.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
