@extends('layouts.master')

@section('title') @lang('translation.support_service') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.support_service')</h4>
                {{ Breadcrumbs::render('social-support-service') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-12">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('support-services.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати службу підтримки</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Посилання</th>
                                <th>Логотип</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($socialSupportServices as $socialSupportService)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $socialSupportService->name }}</td>
                                    <td>{{ $socialSupportService->link }}</td>
                                    <td>@empty(!$socialSupportService->logo)<img src="{{ \Storage::url($socialSupportService->logo) }}" style="max-width:50px;max-height:50px;">@endempty</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('support-services.edit', $socialSupportService) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('support-services.destroy', $socialSupportService) }}" class="btn btn-danger"
                                               data-message="Видалити?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
