@extends('layouts.master')

@section('title')Редагування учня@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування учня</h4>
                {{ Breadcrumbs::render('students.edit', $student) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('students.update', $student) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="last_name" class="col-sm-2 col-form-label">Прізвище *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="last_name"
                                       value="{{ old('last_name', $student->infoStudent->last_name) }}" id="last_name">
                                @error('last_name')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="first_name" class="col-sm-2 col-form-label">Ім'я *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="first_name"
                                       value="{{  old('first_name', $student->infoStudent->first_name) }}" id="first_name">
                                @error('first_name')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="email" name="email" value="{{ old('email', $student->email) }}" id="email">
                                @error('email')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-2 col-form-label">Телефон</label>
                            <div class="col-sm-5">
                                <input class="form-control input-mask" type="text" name="phone" data-inputmask="'mask': '+380999999999'"
                                       value="{{ old('phone', $student->infoStudent->getRawOriginal('phone')) }}" id="phone">
                                @error('phone')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-2 col-form-label">Пароль</label>
                            <div class="input-group col-sm-5">
                                <input class="form-control" type="password" name="password" id="password">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary btn-sm" data-toggle="password" data-action="show-password" type="button"><i class="mdi mdi-eye"></i></button>
                                </div>
                                @error('password')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="group_id" class="col-sm-2 col-form-label">Група @can('group-students') *@endcan</label>
                            <div class="col-sm-5">
                                <select class="form-control select2" id="group_id" name="group_id" data-minimum-results-for-search="0"
                                        data-placeholder="Виберіть групу" data-allow-clear="true">
                                    <option></option>
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}" {{ old('group_id', $student->group_id) == $group->id ? 'selected' : '' }}>
                                            {{ $group->title }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('group_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_driver_license_id" class="col-sm-2 col-form-label">Категорії *</label>
                            <div class="col-sm-5">
                                <select class="form-control select2" id="category_driver_license_id" name="category_driver_license_id[]"
                                        data-minimum-results-for-search="0" data-placeholder="Виберіть категорії" multiple>
                                    @foreach($categoryDriverLicenses as $categoryDriverLicense)
                                        <option value="{{ $categoryDriverLicense->id }}" {{ in_array($categoryDriverLicense->id, old('category_driver_license_id', $student->categoryDriverLicenses->pluck('id')->toArray())) ? 'selected' : '' }}>
                                            {{ $categoryDriverLicense->title }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('category_driver_license_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Промокод</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" value="{{ $student->promoCode->code ?? '' }}" id="promo_code" readonly>
                            </div>
                        </div>

                        @can('edit-premium-students')
                        <div class="form-group row">
                            <label for="switch" class="col-sm-2 col-form-label">Premium</label>
                            <div class="col-sm-1">
                                <input type="checkbox" id="switch" name="is_premium" switch="none" value="1" {{ old('is_premium', $student->isUserActiveSubscription()) ? 'checked' : '' }} />
                                <label for="switch"></label>
                                @error('is_premium')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                            <div class="col-sm-4">
                                Дата останнього оновлення: {{ $student->isUserActiveSubscription()->start_at ?? null }}
                            </div>
                        </div>

                        <div class="form-group row" id="content-premium-validity" style="display: {{ old('is_premium', $student->isUserActiveSubscription()) ? 'flex' : 'none' }};">
                            <label for="subscription" class="col-sm-2 col-form-label">Термін дії, місяці *</label>
                            <div class="col-sm-5">
                                <select class="form-control select2" id="subscription" name="subscription"
                                        data-placeholder="Виберіть термін дії доступу" data-width="100%">
                                    <option></option>
                                    @if($student->isUserActiveSubscription() && !$student->isUserActiveSubscription()->subscription)
                                        <option value="auto-premium" selected>Авто-преміум</option>
                                    @endif
                                    @foreach($subscriptions as $subscription)
                                        <option value="{{ $subscription->id }}" {{ old('subscription', ($student->isUserActiveSubscription()->subscription_id ?? null)) == $subscription->id ? 'selected' : '' }}>
                                            {{ $subscription->validity }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('subscription')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>
                        @endcan

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('students.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- form mask -->
    <script src="{{ URL::asset('/assets/libs/inputmask/inputmask.min.js') }}"></script>

    <!-- form mask js -->
    <script src="{{ URL::asset('/assets/js/pages/form-mask.init.js') }}"></script>

    <script>
        $( function() {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });

            @can('edit-premium-students')
            $('[type="checkbox"][name="is_premium"]').change(function (){
                if ($(this).is(':checked')) {
                    $('#content-premium-validity').show();
                } else {
                    $('#content-premium-validity').hide();
                }
            });
            @endcan
        });
    </script>
@endsection
