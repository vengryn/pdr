@extends('layouts.master')

@section('title') @lang('translation.students') @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .table tr td p:last-child {
            margin: 0;
        }

        .table tr td p {
            cursor: default;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.students')</h4>
                {{ Breadcrumbs::render('students', $filters['group_name'] ?? false) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')
            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['email']) || isset($filters['student_name']) || isset($filters['teacher_name']) || isset($filters['group_name']) || isset($filters['subscription'])
                                || isset($filters['promo_code_student']) || isset($filters['created_start']) || isset($filters['created_end']) ? '' : 'collapsed' }}"
                           aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter"
                             class="collapse {{ isset($filters['email']) || isset($filters['student_name']) || isset($filters['teacher_name']) || isset($filters['group_name']) || isset($filters['subscription'])
                                || isset($filters['promo_code_student']) || isset($filters['created_start']) || isset($filters['created_end']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('students.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="ПІБ учня"
                                                   value="{{ $filters['student_name'] ?? '' }}" name="student_name"
                                                   id="student_name">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Email"
                                                   value="{{ $filters['email'] ?? '' }}" name="email" id="email">
                                        </div>
                                        @can('editable-students')
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" placeholder="ПІБ викладача"
                                                       value="{{ $filters['teacher_name'] ?? '' }}" name="teacher_name"
                                                       id="teacher_name">
                                            </div>
                                        @endcan

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Промокод"
                                                   value="{{ $filters['promo_code_student'] ?? '' }}" name="promo_code_student" id="promo_code">
                                        </div>

                                        <div class="col-sm-3 @can('editable-students') mt-3 @endcan">
                                            <select class="form-control" name="group_name" id="group_name"
                                                    data-placeholder="Назва групи" data-allow-clear="true">
                                                <option></option>
                                                @isset($filters['group_name'])
                                                    <option value="{{ $filters['group_name'] }}"
                                                            selected>{{ $filters['group_name'] == 'without' ? "Без групи" : $filters['group_name'] }}</option>
                                                @endisset
                                            </select>
                                        </div>

                                        <div class="col-sm-3 mt-3">
                                            <select class="form-control select2" name="subscription" id="subscription"
                                                    data-placeholder="Підписка" data-allow-clear="true"
                                                    data-width="100%">
                                                <option></option>
                                                <option
                                                    value="without" {{ ($filters['subscription'] ?? '') == 'without' ? 'selected' : '' }}>
                                                    Без підписки
                                                </option>
                                                @foreach($subscriptions as $subscription)
                                                    <option
                                                        value="{{ $subscription->id }}" {{ ($filters['subscription'] ?? '') == $subscription->id ? 'selected' : '' }}>
                                                        {{ $subscription->getTranslate()->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-avto mt-3">Дата реєстрації</div>

                                        <div class="col-sm-3 mt-3">
                                            <div class="input-daterange input-group" id="date-range">
                                                <input type="text" class="form-control" name="created_start"
                                                       placeholder="Від"
                                                       autocomplete="off"
                                                       value="{{ $filters['created_start'] ?? '' }}"/>
                                                <input type="text" class="form-control" name="created_end"
                                                       placeholder="До"
                                                       autocomplete="off" value="{{ $filters['created_end'] ?? '' }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                                <option value="500" {{ request()->paginate == 500 ? 'selected' : '' }}>500</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('students.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати учня</a>
                                @can('editable-students')
                                <a href="{{ route('students.export') }}"
                                   class="btn btn-primary waves-effect waves-light">Експорт CSV</a>
                                <div class="btn-group">
                                    <button id="btnGroupVerticalDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-action="multiple"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>Дії
                                        <i class="mdi mdi-chevron-down"></i></button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-premium">Преміум</a>
                                    </div>
                                </div>
                                @endcan
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                @can('editable-students')
                                <th width="25px">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="check_all" name="check_all">
                                        <label for="check_all" class="custom-control-label"></label>
                                    </div>
                                </th>
                                @endcan
                                <th>№</th>
                                <th>ПІБ</th>
                                <th>Контакти</th>
                                @cannot('editable-students')
                                <th>Викладач</th>
                                <th>Група</th>
                                <th>Категорії</th>
                                @endcan
                                <th>Промокод</th>
                                @can('editable-students')
                                <th>@sortablelink('created_at', 'Дата реєстрації')</th>
                                @else
                                <th>Дата реєстрації</th>
                                @endcan
                                <th width="200">Прогрес</th>
                                @can('editable-students')
                                <th>Підписка</th>
                                <th>@sortablelink('subscriptions_start_at', 'Старт Пр.')</th>
                                <th>@sortablelink('subscriptions_end_at', 'Кінець Пр.')</th>
                                @else
                                <th width="200">Підписки</th>
                                @endcan
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    @can('editable-students')
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="user_{{ $student->id }}" name="ids[]" value="{{ $student->id }}">
                                            <label for="user_{{ $student->id }}" class="custom-control-label"></label>
                                        </div>
                                    </td>
                                    @endcan
                                    <td>{{ $loop->iteration + $students->firstItem() - 1 }}</td>
                                    <td>{{ $student->infoStudent->full_name ?? '' }}</td>
                                    <td>
                                        <a href="mailto:{{ $student->email }}">{{ $student->email }}</a>
                                        @if(!empty($student->infoStudent->phone))<br/><a
                                            href="tel:{{ $student->infoStudent->phone ?? '' }}">{{ $student->infoStudent->phone ?? '' }}</a>@endif
                                    </td>
                                    @cannot('editable-students')
                                    <td>{{ $student->group->teacher->infoTeacher->full_name ?? '' }}</td>
                                    <td>{{ $student->group->title ?? '' }}</td>
                                    <td>{{ implode(', ', $student->categoryDriverLicenses->pluck('title')->toArray()) }}</td>
                                    @endcan
                                    <td>{{ $student->promoCode->code ?? null }}</td>
                                    <td>{{ $student->created_at ? $student->created_at->format('d.m.Y') : '' }}</td>
                                    <td>
                                        @if($student->getStatistics('questions_passed'))
                                            <p>
                                            <span data-toggle="tooltip" data-placement="top"
                                                  title="Зданих іспитів"><span
                                                    style="color: #2EB371;">{{ $student->getStatistics('exam_success_number_attempts') }}</span>/{{ $student->getStatistics('exam_total_number_attempts') }}&nbsp;&nbsp;</span>
                                                <span data-toggle="tooltip" data-placement="top"
                                                      title="Середній рахунок іспиту"><span
                                                        style="color: {{ $student->getStatistics('average_score_exam') <= 16 ? '#E8472E;' : ($student->getStatistics('average_score_exam') <= 18 ? '#F5B540;' : '#2EB371') }}"
                                                    >{{ $student->getStatistics('average_score_exam') }}</span>/20&nbsp;&nbsp;</span>
                                                <span
                                                    style="color: {{ $student->getStatistics('total_progress_theme') <= 39 ? '#E8472E;' : ($student->getStatistics('total_progress_theme') <= 89 ? '#F5B540;' : '#2EB371') }}"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Загальний прогрес по темах">{{ $student->getStatistics('total_progress_theme') }}%</span>
                                            </p>
                                            <p>
                                                <span data-toggle="tooltip" data-placement="top"
                                                      title="Запитань пройдено">{{ $student->getStatistics('questions_passed') }}&nbsp;&nbsp;</span>
                                                <span style="color: #E8472E;" data-toggle="tooltip" data-placement="top"
                                                      title="Невірних відповідей">{{ $student->getStatistics('wrong_answers') }}&nbsp;&nbsp;</span>
                                                <span style="color: #2EB371;" data-toggle="tooltip" data-placement="top"
                                                      title="Правильних відповідей">{{ $student->getStatistics('correct_answers') }}</span>
                                            </p>
                                        @endif
                                    </td>
                                    @can('editable-students')
                                    <td>{{ $student->subscriptionInfo['name'] }}</td>
                                    <td>{{ $student->subscriptionInfo['start'] }}</td>
                                    <td>{{ $student->subscriptionInfo['end'] }}</td>
                                    @else
                                    <td>
                                        @if($student->isUserActiveSubscription())
                                            <p>{{ $student->subscriptionInfo['name'] }}</p>
                                            <p>{{ $student->subscriptionInfo['start'] . '-' . $student->subscriptionInfo['end'] }}</p>
                                        @endif
                                    </td>
                                    @endcan
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('students.edit', $student) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            @can('delete-students')
                                                <a href="{{ route('students.destroy', $student) }}"
                                                   class="btn btn-danger"
                                                   data-message="Ви дійсно хочете видалити учня?" data-action="delete">
                                                    <i class="mdi mdi-delete"></i>
                                                </a>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $students->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

    @can('editable-students')
    <div class="modal fade" id="modal-premium">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Преміум</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="changeSubscription" action="{{ route('students.actionMultiple', ['action' => 'premium']) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <select name="subscription" class="form-control select2" data-placeholder="Підписка" data-allow-clear="true"
                                    data-width="100%" required>
                                <option></option>
                                @foreach ($subscriptions as $subscription)
                                    <option value="{{ $subscription->id }}">{{ $subscription->getTranslate()->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Скасувати</button>
                    <button type="submit" form="changeSubscription" class="btn btn-primary">Застосувати</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    @endcan
@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.uk.min.js') }}"></script>
    <script>
        $(function () {
            @can('editable-students')
            $(document).on('submit', '#changeSubscription', function (e) {
                e.preventDefault();
                $.each($("input[name='ids[]']:checked"), function(){
                    $('<input>', {
                        type: 'hidden',
                        name: 'ids[]',
                        value: $(this).val()
                    }).appendTo('#changeSubscription');
                });

                e.currentTarget.submit();
            });

            $(document).on('change', '[name="ids[]"]', function () {
                if ($("input[name='ids[]']:checked").length) {
                    $('button[data-action="multiple"]').attr('disabled', false);
                } else {
                    $('button[data-action="multiple"]').attr('disabled', true);
                }
            });

            $('input[name="check_all"]').change(function () {
                $('input[name="ids[]"]').prop('checked', $(this).prop('checked')).change();
            });
            @endcan

            $('#date-range').datepicker({
                language: 'uk',
                keepEmptyValues: true,
                todayHighlight: true
            });

            $(".select2").select2({
                minimumResultsForSearch: Infinity,
                width: '100%'
            });

            $('#group_name').select2({
                width: '100%',
                ajax: {
                    url: "{{ route('students.getAutocompleteGroup') }}",
                    dataType: 'json',
                    delay: 250,
                    type: "post",
                    processResults: function (data) {
                        let result = {
                            results: $.map(data, function (item) {
                                return {
                                    text: item,
                                    id: item
                                }
                            })
                        };
                        @can('editable-students')
                        result.results.unshift({text: "Без групи", id: "without"});
                        @endcan
                            return result;
                    },
                    cache: true
                }
            });

            @can('editable-students')
            $("#teacher_name").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('students.getAutocompleteTeacher') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });
            @endcan

            $("#student_name").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('students.getAutocompleteStudent') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });

            $("#email").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('students.getAutocompleteEmail') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });

            $( "#promo_code" ).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "{{ route('students.getAutocompletePromoCode') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                }
            });
        });
    </script>
@endsection
