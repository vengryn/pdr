@extends('layouts.master')

@section('title') Додавання нової підписки @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нової підписки</h4>
                {{ Breadcrumbs::render('subscriptions.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('subscriptions.store') }}" id="sendForm" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="product_id" class="col-sm-2 col-form-label">Product ID <i data-toggle="tooltip" data-placement="top"
                                      title="Дане поле повинно містити ідентифікатор відповідної підписки, створенної в Google Play / Apple Store"
                                      class="mdi mdi-information-outline" style="font-size: 17px;"></i> *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('product_id')is-invalid @enderror" type="text" name="product_id"
                                       value="{{ old('product_id') }}" id="product_id">
                                @error('product_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        @foreach($languages as $language)
                            <div class="form-group row">
                                <label for="name_{{ $language->id }}" class="col-sm-2 col-form-label">{{ $language->title }} {{ $language->is_default ? '*' : '' }}</label>
                                <div class="col-sm-5">
                                    <input class="form-control @error('translate.name.' . $language->id)is-invalid @enderror" type="text"
                                           name="translate[name][{{ $language->id }}]"
                                           value="{{ old('translate.name.'. $language->id) }}" id="name_{{ $language->id }}">
                                    @error('translate.name.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group row">
                            <label for="validity" class="col-sm-2 col-form-label">Термін дії *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('validity')is-invalid @enderror" type="text" placeholder="Кількість місяців"
                                       name="validity" value="{{ old('validity') }}" id="validity">
                                @error('validity')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-sm-2 col-form-label">Ціна *</label>
                            <div class="col-sm-5">
                                <input class="form-control @error('price')is-invalid @enderror" type="text" placeholder="Грн"
                                       name="price" value="{{ old('price') }}" id="price">
                                @error('price')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="is_paid_video" class="col-sm-2">Бачить платні відео</label>
                            <div class="col-sm-5">
                                <input type="checkbox" id="is_paid_video" switch="none" value="1"
                                       name="is_paid_video" {{ old('is_paid_video') ? 'checked' : '' }}/>
                                <label for="is_paid_video"></label>
                                @error('is_paid_video')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('subscriptions.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
