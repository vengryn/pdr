
@extends('layouts.master')

@section('title') @lang('translation.price_list') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.price_list')</h4>
                {{ Breadcrumbs::render('subscriptions') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-12">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('subscriptions.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати підписку</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Product ID</th>
                                <th>Переклади</th>
                                <th>Термін, м</th>
                                <th>Ціна, грн</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subscriptions as $subscription)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $subscription->getTranslate()->name ?? '' }}</td>
                                    <td>{{ $subscription->product_id }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($subscription->getTranslate($language->id)->name))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ $subscription->validity }}</td>
                                    <td>{{ $subscription->price }}</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('subscriptions.edit', $subscription) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('subscriptions.destroy', $subscription) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити підписку?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
