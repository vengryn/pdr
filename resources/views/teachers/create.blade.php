@extends('layouts.master')

@section('title') Додавання нового викладача @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нового викладача</h4>
                {{ Breadcrumbs::render('teachers.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('teachers.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="last_name" class="col-sm-2 col-form-label">Прізвище *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" id="last_name">
                                @error('last_name')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="first_name" class="col-sm-2 col-form-label">Ім'я *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}" id="first_name">
                                @error('first_name')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="patronymic" class="col-sm-2 col-form-label">По-батькові *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="patronymic" value="{{ old('patronymic') }}" id="patronymic">
                                @error('patronymic')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email *</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}" id="email">
                                @error('email')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-2 col-form-label">Пароль *</label>
                            <div class="input-group col-sm-5">
                                <input class="form-control" type="password" name="password" id="password">

                                <div class="input-group-append">
                                    <button class="btn btn-secondary btn-sm" data-toggle="password" data-action="show-password" type="button"><i class="mdi mdi-eye"></i></button>
                                </div>

                                @error('password')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-2 col-form-label">Телефон *</label>
                            <div class="col-sm-5">
                                <input class="form-control input-mask" type="text" name="phone" data-inputmask="'mask': '+380999999999'"
                                       value="{{ old('phone') }}" id="phone">
                                @error('phone')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Промокод</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" placeholder="Промокод згенерується автоматично" id="promo_code" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('teachers.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- form mask -->
    <script src="{{ URL::asset('/assets/libs/inputmask/inputmask.min.js') }}"></script>

    <!-- form mask js -->
    <script src="{{ URL::asset('/assets/js/pages/form-mask.init.js') }}"></script>
@endsection
