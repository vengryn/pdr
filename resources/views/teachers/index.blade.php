@extends('layouts.master')

@section('title') @lang('translation.teachers') @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.teachers')</h4>
                {{ Breadcrumbs::render('teachers') }}
            </div>
        </div>

    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['name']) || isset($filters['email']) || isset($filters['promo_code'])
                                    || isset($filters['created_start']) || isset($filters['created_end']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['name']) || isset($filters['email']) || isset($filters['promo_code'])
                                    || isset($filters['created_start']) || isset($filters['created_end']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('teachers.index') }}">

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="ПІБ викладача"
                                                   value="{{ $filters['name'] ?? '' }}" name="name" id="name">
                                        </div>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Email"
                                                   value="{{ $filters['email'] ?? '' }}" name="email" id="email">
                                        </div>

                                        <div class="col-sm-avto">Дата реєстрації</div>

                                        <div class="col-sm-3">
                                            <div class="input-daterange input-group" id="date-range">
                                                <input type="text" class="form-control" name="created_start"
                                                       placeholder="Від"
                                                       autocomplete="off"
                                                       value="{{ $filters['created_start'] ?? '' }}"/>
                                                <input type="text" class="form-control" name="created_end"
                                                       placeholder="До"
                                                       autocomplete="off" value="{{ $filters['created_end'] ?? '' }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Промокод"
                                                   value="{{ $filters['promo_code'] ?? '' }}" name="promo_code"
                                                   id="promo_code">
                                        </div>

                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('teachers.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати викладача</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>ПІБ</th>
                                <th>Email</th>
                                <th>Телефон</th>
                                <th>Промокод</th>
                                <th>Дата реєстрації</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($teachers as $teacher)
                                <tr>
                                    <th scope="row">{{ $loop->iteration + $teachers->firstItem() - 1 }}</th>
                                    <td>{{ $teacher->infoTeacher->full_name ?? '' }}</td>
                                    <td><a href="mailto:{{ $teacher->email }}">{{ $teacher->email }}</a></td>
                                    <td>
                                        <a href="tel:{{ $teacher->infoTeacher->phone ?? '' }}">{{ $teacher->infoTeacher->phone ?? '' }}</a>
                                    </td>
                                    <td>
                                        @isset($teacher->promoCode->code)
                                            <a href="{{ route('report.index', ['promo_code' => $teacher->promoCode->code]) }}">
                                                {{ $teacher->promoCode->code }}
                                            </a>
                                        @endisset
                                    </td>
                                    <td>{{ $teacher->created_at ? $teacher->created_at->format('d.m.Y') : '' }}</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('teachers.edit', $teacher) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('teachers.destroy', $teacher) }}" class="btn btn-danger"
                                               data-message="Ви дійсно хочете видалити викладача?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $teachers->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script-bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.uk.min.js') }}"></script>

    <script>
        $(function () {
            $('#date-range').datepicker({
                language: 'uk',
                keepEmptyValues: true,
                todayHighlight: true
            });

            $("#name").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('teachers.getAutocompleteTeacher') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });

            $("#email").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('teachers.getAutocompleteEmail') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });

            $("#promo_code").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('teachers.getAutocompletePromoCode') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                }
            });
        });
    </script>
@endsection
