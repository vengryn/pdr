@extends('layouts.master')

@section('title') Додавання нової сторінки @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/emojipicker.css') }}">
@endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Додавання нової сторінки</h4>
                {{ Breadcrumbs::render('text-pages.create') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link {{ $language->is_default ? 'active' : '' }}" data-toggle="tab" href="#{{ $language->abbreviation }}" role="tab">
                                    <span class="d-block d-sm-none"><img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:24px;"></span>
                                    <span class="d-none d-sm-block">{{ $language->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('text-pages.store') }}" id="sendForm">
                        @csrf
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane p-3 {{ $language->is_default ? 'active' : '' }}" id="{{ $language->abbreviation }}" role="tabpanel">

                                    @if($language->is_default)
                                        <div class="form-group row">
                                            <label for="slug" class="col-sm-2 col-form-label">Slug</label>
                                            <div class="col-sm-10">
                                                <input class="form-control @error('slug')is-invalid @enderror" type="text"
                                                       name="slug" value="{{ old('slug') }}" id="slug">
                                                @error('slug')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label for="meta_title_{{ $language->id }}" class="col-sm-2 col-form-label">Meta заголовок</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('translate.meta_title.' . $language->id)is-invalid @enderror" type="text"
                                                   name="translate[meta_title][{{ $language->id }}]"
                                                   value="{{ old('translate.meta_title.'. $language->id) }}" id="meta_title_{{ $language->id }}">
                                            @error('translate.meta_title.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="meta_description_{{ $language->id }}" class="col-sm-2 col-form-label">Meta опис</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control @error('translate.meta_description.' . $language->id)is-invalid @enderror" id="meta_description_{{ $language->id }}"
                                                name="translate[meta_description][{{ $language->id }}]" rows="3">{{ old('translate.meta_description.'. $language->id) }}</textarea>
                                            @error('translate.meta_description.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="meta_keywords_{{ $language->id }}" class="col-sm-2 col-form-label">Meta ключові слова</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control @error('translate.meta_keywords.' . $language->id)is-invalid @enderror" id="meta_keywords_{{ $language->id }}"
                                                      name="translate[meta_keywords][{{ $language->id }}]" rows="3">{{ old('translate.meta_keywords.'. $language->id) }}</textarea>
                                            @error('translate.meta_keywords.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group row">
                                        <label for="name_{{ $language->id }}" class="col-sm-2 col-form-label">Назва {{ $language->is_default ? '*' : '' }}</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('translate.name.' . $language->id)is-invalid @enderror" type="text"
                                                   name="translate[name][{{ $language->id }}]"
                                                   value="{{ old('translate.name.'. $language->id) }}" id="name_{{ $language->id }}">
                                            @error('translate.name.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="content_{{ $language->id }}" class="col-sm-2 col-form-label">Опис</label>
                                        <div class="col-sm-10">
                                            <textarea id="content_{{ $language->id }}"
                                                      data-action="tinymce"
                                                      name="translate[content][{{ $language->id }}]">{{ old('translate.content.'. $language->id) }}</textarea>
                                            @error('translate.content.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                        </div>
                                    </div>
                                    @include('inc.markdown-img')
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('text-pages.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
    <script src="{{ URL::asset('/assets/js/inputEmoji.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/emojis.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>

    <script>
        $('textarea[data-action="tinymce"]').each(function() {
            new EasyMDE({
                element: this,
                toolbar: ["bold", "|", "link", "image", "|", {
                    name: "smile",
                    action: (editor) => {
                        $(this).emojiList(editor);
                    },
                    className: "fa fa-smile",
                    title: "Smile",
                }, "|", "preview"],
                status: false,
                spellChecker: false,
                renderingConfig: {
                    singleLineBreaks: false
                }
            });
        });
    </script>
@endsection
