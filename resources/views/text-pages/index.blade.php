@extends('layouts.master')

@section('title') @lang('translation.text_pages') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.text_pages')</h4>
                {{ Breadcrumbs::render('text-pages') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('text-pages.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати сторінку</a>
                            </div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Переклади</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($textPages as $textPage)
                                <tr>
                                    <td>{{ $loop->iteration + $textPages->firstItem() - 1 }}</td>
                                    <td>{{ $textPage->getTranslate()->name ?? '' }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($textPage->getTranslate($language->id)->content))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('text-pages.edit', $textPage) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('text-pages.destroy', $textPage) }}" class="btn btn-danger {{ $textPage->slug == 'user_agreement' ? 'disabled' : '' }}"
                                               data-message="{{ $textPage->instructions->count() ? 'Дана "текстова сторінка" використовується у "Інструкціях" і її видалення призведе до видалення відповідної "Інструкції"' : 'Ви дійсно хочете видалити сторінку?' }}" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $textPages->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
