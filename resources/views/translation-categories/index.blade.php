@extends('layouts.master')

@section('title') @lang('translation.translation_categories') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.translation_categories')</h4>
                {{ Breadcrumbs::render('translation_categories') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('translations.index') }}"
                                   class="btn btn-outline-info waves-effect waves-light">До перекладів</a>
                                @can('superAdmin')
                                <a href="{{ route('translation-categories.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати категорію перекладу</a>
                                @endcan
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th width="70">№</th>
                                <th>Назва</th>
                                <th width="100">Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($translationCategories as $translationCategory)
                                <tr>
                                    <td>{{ $loop->iteration + $translationCategories->firstItem() - 1 }}</td>
                                    <td>{{ $translationCategory->name }}</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('translation-categories.edit', $translationCategory) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('translation-categories.destroy', $translationCategory) }}"
                                               class="btn btn-danger @cannot('superAdmin') disabled @endcan"
                                               data-message="Ви дійсно хочете видалити категорію перекладу?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $translationCategories->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection
