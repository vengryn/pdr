@extends('layouts.master')

@section('title') Редагування перекладу @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Редагування перекладу</h4>
                {{ Breadcrumbs::render('translations.edit', $translation) }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('translations.update', $translation) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="key" class="col-sm-2 col-form-label">Ключ *</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control @error('key')is-invalid @enderror" name="key" id="key"
                                       value="{{ old('key', $translation->key) }}" @cannot('superAdmin') readonly @endcan>
                                @error('key')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        @foreach($languages as $language)
                            <div class="form-group row">
                                <label for="value_{{ $language->id }}" class="col-sm-2 col-form-label">{{ $language->title }} {{ $language->is_default ? '*' : '' }}</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control @error('translate.value.' . $language->id)is-invalid @enderror" id="value_{{ $language->id }}"
                                           name="translate[value][{{ $language->id }}]" value="{{ old('translate.value.'. $language->id, $translation->getTranslate($language->id)->value ?? '') }}">
                                    @error('translate.value.' . $language->id)<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group row">
                            <label for="translation_category_id" class="col-sm-2 col-form-label">Категорія</label>
                            <div class="col-sm-5">
                                <select class="form-control select2" id="translation_category_id" name="translation_category_id"
                                        data-placeholder="Виберіть категорію" data-allow-clear="true">
                                    <option></option>
                                    @foreach($translationCategories as $translationCategory)
                                        <option value="{{ $translationCategory->id }}" {{ old('translation_category_id', $translation->translation_category_id) == $translationCategory->id ? 'selected' : '' }}>
                                            {{ $translationCategory->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('translation_category_id')<div class="invalid-feedback d-block">{{ $message }}</div>@enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" name="action" value="save"
                                        class="btn btn-primary waves-effect waves-light">Зберегти</button>
                                <button type="submit" name="action" value="save_and_stay"
                                        class="btn btn-primary waves-effect waves-light">Зберегти і залишитися</button>
                                <a href="{{ route('translations.index') }}"
                                   class="btn btn-secondary waves-effect waves-light">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')
    <script>
        $(".select2").select2();
    </script>
@endsection
