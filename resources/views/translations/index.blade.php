@extends('layouts.master')

@section('title') @lang('translation.translations') @endsection

@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.translations')</h4>
                {{ Breadcrumbs::render('translations') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="accordion" id="filtering">
                    <div class="card mb-0">
                        <a data-toggle="collapse" href="#collapseFilter"
                           class="faq {{ isset($filters['value']) || isset($filters['category']) || isset($filters['key']) ? '' : 'collapsed' }}" aria-controls="collapseFilter">
                            <div class="card-header text-dark" id="headingFilter">
                                <h6 class="m-0 font-size-16">Фільтр</h6>
                            </div>
                        </a>

                        <div id="collapseFilter" class="collapse {{ isset($filters['value']) || isset($filters['category']) || isset($filters['key']) ? 'show' : '' }}"
                             aria-labelledby="headingFilter" data-parent="#filtering">
                            <div class="card-body">
                                <form method="GET" id="filter-form" action="{{ route('translations.index') }}">
                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-3">
                                            <select class="form-control select2" name="category" id="category"
                                                    data-placeholder="Категорія" data-allow-clear="true" data-width="100%">
                                                <option></option>
                                                @foreach($translationCategories as $translationCategory)
                                                    <option value="{{ $translationCategory->id }}" {{ ($filters['category'] ?? '') == $translationCategory->id ? 'selected' : '' }}>{{ $translationCategory->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Ключ"
                                                   value="{{ $filters['key'] ?? '' }}" name="key" id="key">
                                        </div>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Текст"
                                                   value="{{ $filters['value'] ?? '' }}" name="value" id="value">
                                        </div>
                                    </div>

                                    <div class="row align-items-center mb-3">
                                        <div class="col-sm-12">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Пошук</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="submit" name="action" value="clear_filter"
                                                        class="btn btn-secondary">Скинути</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center mb-3">
                        <div class="col-sm-2">
                            <select id="pagination" class="custom-select">
                                <option value="25" {{ request()->paginate == 25 ? 'selected' : '' }}>25</option>
                                <option value="50" {{ request()->paginate == 50 ? 'selected' : '' }}>50</option>
                                <option value="100" {{ request()->paginate == 100 ? 'selected' : '' }}>100</option>
                            </select>
                        </div>

                        <div class="col-sm-10">
                            <div class="float-right d-md-block ">
                                <a href="{{ route('translation-categories.index') }}"
                                   class="btn btn-outline-info waves-effect waves-light">Управління категоріями</a>
                                @can('superAdmin')
                                <a href="{{ route('translations.create') }}"
                                   class="btn btn-primary waves-effect waves-light">Додати переклад</a>
                                @endcan
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Ключ</th>
                                <th>Текст</th>
                                <th>Переклад</th>
                                <th>Категорія</th>
                                <th>Опції</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($translations as $translation)
                                <tr>
                                    <td>{{ $loop->iteration + $translations->firstItem() - 1 }}</td>
                                    <td>{{ $translation->key }}</td>
                                    <td>{{ $translation->getTranslate()->value }}</td>
                                    <td>
                                        @foreach($languages as $language)
                                            @if(!$language->is_default)
                                                @if(empty($translation->getTranslate($language->id)->value))
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;opacity: 0.2;">
                                                @else
                                                    <img src="{{ \Storage::url($language->flag) }}" style="max-width:64px;max-height:16px;">
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ $translation->translationCategory->name ?? '' }}</td>
                                    <td style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('translations.edit', $translation) }}" class="btn btn-primary">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{ route('translations.destroy', $translation) }}"
                                               class="btn btn-danger @cannot('superAdmin') disabled @endcan"
                                               data-message="Ви дійсно хочете видалити переклад?" data-action="delete">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $translations->appends(request()->except('page'))->links() }}

                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->
@endsection

@section('script-bottom')
    <script>
        $( function() {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });
        });
    </script>
@endsection
