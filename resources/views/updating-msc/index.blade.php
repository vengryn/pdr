@extends('layouts.master')

@section('title') @lang('translation.updating_msc') @endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">@lang('translation.updating_msc')</h4>
                {{ Breadcrumbs::render('updating_msc') }}
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.alerts')

            <div class="card">
                <div class="card-body">

                    <p>Останнє оновлення: {{ $lastDateUpdating ? \Carbon\Carbon::parse($lastDateUpdating)->format('d.m.Y H:i:s') : '' }}</p>
                    <a href="{{ route('sync-msc') }}" class="btn btn-primary waves-effect waves-light" id="updating_msc">Оновити</a>
                    <a href="{{ route('diff-sync-msc') }}" class="btn btn-secondary waves-effect waves-light"
                       data-toggle="tooltip" data-placement="top" title="Отримати файл відмінностей в оновлених питаннях">Останні зміни по оновленню</a>

                    <hr>
                    <p><b>Дозвіл на оновлення комплекту</b></p>
                    <div class="row">
                        <label for="switch" class="col-sm-5 col-lg-2">Дозволити</label>
                        <div class="col-sm-5">
                            <input type="checkbox" id="switch" switch="none" name="update" {{ $isUpdating ? 'checked' : '' }}/>
                            <label for="switch"></label>
                        </div>
                    </div>

                    <p><i>* Всі питання та відповіді з попереднього комплекту буде видалено</i></p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page-content -->

@endsection

@section('script')
    <script>
        $( function() {
            $('[type="checkbox"][name="update"]').change(function (){
                let isUpdating = $(this).is(':checked') ? 1 : 0;

                $.ajax({
                    'url': '{{ route('updating-msc.update') }}',
                    'type': 'POST',
                    'data': {updating: isUpdating},
                    'success': function(data) {
                        //
                    },
                    'error': function(){
                        console.error('Something wrong!');
                    }
                });
            });

            $('#updating_msc').click(function (e) {
                e.preventDefault();

                Swal.fire({
                    title: 'Ви дійсно хочете запустити механізм оновлення питань?',
                    showCancelButton: true,
                    cancelButtonText: 'Ні',
                    confirmButtonColor: '#626ed4',
                    cancelButtonColor: '#ec4561',
                    confirmButtonText: 'Так'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = $(this).attr('href');
                    }
                })
            });
        });
    </script>

@endsection
