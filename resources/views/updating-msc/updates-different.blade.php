<table>
    <thead>
    <tr>
        <th><b>Before</b></th>
        <th></th>
        <th><b>After</b></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($questions as $question)
        <tr>
            <td><b>ID</b></td>
            <td>{{ $question->id }}</td>
        </tr>
        <tr>
            <td><b>Питання</b></td>
            <td>{{ $question->changes['translate_questions']['name'] ?? $question->name }}</td>
            <td><b>Питання</b></td>
            <td>{{ $question->name }}</td>
        </tr>

        @foreach($question->questions_answers as $answer)
            <tr>
                @if (isset($question->changes['add_question_answers']))
                    @if(!in_array($answer->external_id, $question->changes['add_question_answers']))
                        <td><b>Відповідь {{ $loop->iteration }}
                                {{ isset($question->changes['is_correct_question_answers']['id']) && $question->changes['is_correct_question_answers']['id'] == $answer->id ? '(true)' : '' }}</b></td>
                        <td>{{ $question->changes['translate_question_answers'][$answer->id]['name'] ?? $answer->name }}</td>
                    @else
                        <td></td>
                        <td></td>
                    @endif
                @else
                    <td><b>Відповідь {{ $loop->iteration }}
                            {{ isset($question->changes['is_correct_question_answers']['id']) && $question->changes['is_correct_question_answers']['id'] == $answer->id ? '(true)' : '' }}</b></td>
                    <td>{{ $question->changes['translate_question_answers'][$answer->id]['name'] ?? $answer->name }}</td>
                @endif

                <td><b>Відповідь {{ $loop->iteration }} {{ $answer->is_correct ? '(true)' : '' }}</b></td>
                <td>{{ $answer->name }}</td>
            </tr>
        @endforeach

        @foreach(($question->changes['remove_question_answers'] ?? []) as $removeQuestion)
            <tr>
                <td>Відповідь {{ count($question->questions_answers) + $loop->iteration }}</td>
                <td>{{ $removeQuestion['name'] }} {{ $removeQuestion['is_correct'] ? '(true)' : '' }}</td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
        <tr></tr>
    @endforeach
    </tbody>
</table>
