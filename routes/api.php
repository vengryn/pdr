<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\AuthController;
use App\Http\Controllers\API\v1\UserController;
use App\Http\Controllers\API\v1\TranslateController;
use App\Http\Controllers\API\v1\QuestionController;
use App\Http\Controllers\API\v1\QuestionsComplaintController;
use App\Http\Controllers\API\v1\CategoryDriverLicenseController;
use App\Http\Controllers\API\v1\SettingController;
use App\Http\Controllers\API\v1\PaymentController;
use App\Http\Controllers\API\v1\SubscriptionController;
use App\Http\Controllers\API\v1\SocialSupportServiceController;
use App\Http\Controllers\API\v1\GroupController;
use App\Http\Controllers\API\v1\TicketTrainingController;
use App\Http\Controllers\API\v1\TheoryController;
use App\Http\Controllers\API\v1\ExamQuestionController;
use App\Http\Controllers\API\v1\TextPageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::middleware('check_api_token')->group(function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('register', [AuthController::class, 'register']);
        Route::post('login-social', [AuthController::class, 'loginSocial']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::post('password-reset', [AuthController::class, 'passwordReset']);
        Route::post('validate-password-reset-token', [AuthController::class, 'validatePasswordResetToken']);
        Route::post('change-password', [AuthController::class, 'changePassword']);

        Route::get('translates', [TranslateController::class, 'translate']);
        Route::get('groups', [GroupController::class, 'teacherGroupByPromoCode']);
        Route::get('questions-v2', [QuestionController::class, 'indexV2']);
        Route::get('theories-v2', [TheoryController::class, 'indexV2']);
        Route::apiResource('subscriptions', SubscriptionController::class)->only(['index']);
        Route::apiResource('category-driver-licenses', CategoryDriverLicenseController::class)->only(['index']);

        Route::middleware('gzip')->group( function () {
            Route::apiResource('social-support-services', SocialSupportServiceController::class)->only(['index']);
            Route::get('questions', [QuestionController::class, 'index']);
            Route::get('theories', [TheoryController::class, 'index']);
            Route::get('settings', [SettingController::class, 'index']);
            Route::get('web-settings', [SettingController::class, 'indexWeb']);
        });

        Route::middleware('jwt-auth-optionally')->group(function () {
            Route::get('theme-traffic-rules', [\App\Http\Controllers\API\v1\InternalTopicTrafficRuleController::class, 'index']);
            Route::get('theme-traffic-rules/{number}', [\App\Http\Controllers\API\v1\InternalTopicTrafficRuleController::class, 'show']);

            Route::get('road-markings', [\App\Http\Controllers\API\v1\TopicRoadMarkingController::class, 'index']);
            Route::get('road-markings/{number}', [\App\Http\Controllers\API\v1\TopicRoadMarkingController::class, 'show']);

            Route::get('road-signs', [\App\Http\Controllers\API\v1\TopicRoadSignController::class, 'index']);
            Route::get('road-signs/{number}', [\App\Http\Controllers\API\v1\TopicRoadSignController::class, 'show']);

            Route::get('regulator', [TheoryController::class, 'regulator']);
            Route::get('traffic-light', [TheoryController::class, 'trafficLight']);
            Route::get('video-lessons-traffic-rule', [TheoryController::class, 'videoLessonsTrafficRule']);
            Route::get('theory-search', [TheoryController::class, 'search']);
        });

        Route::resource('instructions', \App\Http\Controllers\API\v1\InstructionController::class)
            ->only(['index', 'show'])
            ->parameters(['road-signs' => 'topic_road_sign']);

        Route::get('pages/{slug}', [TextPageController::class, 'index'])->where('slug', '(.*)');
    });

    Route::middleware('chek_chat_bot_api_token')->group(function () {
        Route::get('referer-statistics', [UserController::class, 'refererStatistics']);
    });

    Route::post('payments', [PaymentController::class, 'store'])
        ->name('payments.store')
        ->middleware(['store_log', 'jwt-auth']);

    Route::middleware('jwt-auth')->group(function () {
        Route::get('me', [AuthController::class, 'me']);
        Route::get('ticket-training', [AuthController::class, 'ticketTraining']);
        Route::get('user-subscription', [UserController::class, 'userSubscription']);
        Route::put('user', [UserController::class, 'update']);
        Route::put('update-user', [UserController::class, 'updateUser']);
        Route::post('logout', [AuthController::class, 'logout']);

        Route::get('histories', [UserController::class, 'histories']);
        Route::post('reset-histories', [UserController::class, 'resetHistories']);

        Route::post('question-answer-histories', [QuestionController::class, 'storeQuestionAnswerHistory']);
        Route::post('question-answer-all-histories', [QuestionController::class, 'storeQuestionAnswerAllHistory']);
        Route::post('exams', [QuestionController::class, 'storeExam']);
        Route::post('exam-questions-answer-histories', [QuestionController::class, 'storeExamQuestionAnswerHistory']);
        Route::post('ticket-trainings', [TicketTrainingController::class, 'store']);

        Route::post('questions-complaint', [QuestionsComplaintController::class, 'storeComplaint']);
        Route::post('questions-complaints', [QuestionsComplaintController::class, 'storeComplaints']);

        Route::get('check-promo-code', [PaymentController::class, 'checkPromoCode']);

        Route::post('create-payment', [PaymentController::class, 'createPayment']);
        Route::post('liqpay-checkout', [PaymentController::class, 'liqPayCheckout'])->name('liqPay.checkout')
            ->withoutMiddleware('jwt-auth');
        Route::post('uapay-checkout', [PaymentController::class, 'uaPayCheckout'])->name('uaPay.checkout')
            ->withoutMiddleware('jwt-auth');

        Route::middleware('gzip')->group( function () {
            Route::get('exam-questions', [ExamQuestionController::class, 'index']);
        });

        Route::post('cancel-exam', [ExamQuestionController::class, 'cancelExam']);

        Route::get('user-themes-with-progress', [UserController::class, 'userThemesWithProgress']);
        Route::get('user-questions-by-theme/{topic_traffic_rule}', [UserController::class, 'userQuestionsByTheme']);
        Route::get('user-wrong-question-answers', [UserController::class, 'userWrongQuestionAnswers']);
        Route::post('user-wrong-question-answers', [UserController::class, 'storeUserWrongQuestionAnswers']);
        Route::get('user-frequent-mistakes', [UserController::class, 'userFrequentMistakes']);
    });
});
