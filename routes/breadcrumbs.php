<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Головна', route('dashboard'));
});

// Home > Teachers
Breadcrumbs::for('teachers', function ($trail) {
    $trail->parent('home');
    $trail->push('Викладачі', route('teachers.index'));
});

// Home > Teachers > Add New Teacher
Breadcrumbs::for('teachers.create', function ($trail) {
    $trail->parent('teachers');
    $trail->push('Додавання нового викладача', route('teachers.create'));
});

// Home > Teachers > Edit Teacher
Breadcrumbs::for('teachers.edit', function ($trail, $teacher) {
    $trail->parent('teachers');
    $trail->push('Редагування викладача', route('teachers.edit', $teacher));
});


// Home > Students
Breadcrumbs::for('students', function ($trail, $group = false) {
    $trail->parent('home');

    if ($group && $group != 'without') {
        $trail->push('Групи', route('groups.index'));
        $trail->push($group, route('groups.index', ['group_name' => $group]));
    }

    $trail->push(trans('translation.students'), route('students.index'));
});

// Home > Students > Add New Student
Breadcrumbs::for('students.create', function ($trail) {
    $trail->parent('students');
    $trail->push('Додавання нового учня', route('students.create'));
});

// Home > Students > Edit Student
Breadcrumbs::for('students.edit', function ($trail, $student) {
    $trail->parent('students');
    $trail->push('Редагування учня', route('students.edit', $student));
});


// Home > Languages
Breadcrumbs::for('languages', function ($trail) {
    $trail->parent('home');
    $trail->push('Мови', route('languages.index'));
});

// Home > Languages > Add New Language
Breadcrumbs::for('languages.create', function ($trail) {
    $trail->parent('languages');
    $trail->push('Додавання нової мови', route('languages.create'));
});

// Home > Languages > Edit Language
Breadcrumbs::for('languages.edit', function ($trail, $language) {
    $trail->parent('languages');
    $trail->push('Редагування мови', route('languages.edit', $language));
});


// Home > Social Support Service
Breadcrumbs::for('social-support-service', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.support_service'), route('support-services.index'));
});

// Home > Social Support Service > Add New SocialSupportService
Breadcrumbs::for('social-support-service.create', function ($trail) {
    $trail->parent('social-support-service');
    $trail->push('Додавання нової служби підтримки', route('support-services.create'));
});

// Home > Social Support Service > Edit SocialSupportService
Breadcrumbs::for('social-support-service.edit', function ($trail, $socialSupportService) {
    $trail->parent('social-support-service');
    $trail->push('Редагування служби підтримки', route('support-services.edit', $socialSupportService));
});


// Home > Groups
Breadcrumbs::for('groups', function ($trail) {
    $trail->parent('home');
    $trail->push('Групи', route('groups.index'));
});

// Home > Groups > Add New Group
Breadcrumbs::for('groups.create', function ($trail) {
    $trail->parent('groups');
    $trail->push('Додавання нової групи', route('groups.create'));
});

// Home > Groups > Edit Group
Breadcrumbs::for('groups.edit', function ($trail, $group) {
    $trail->parent('groups');
    $trail->push('Редагування групи', route('groups.edit', $group));
});


// Home > Category Driver Licenses
Breadcrumbs::for('category_driver_licenses', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.categories'), route('category-driver-licenses.index'));
});


// Home > Report
Breadcrumbs::for('report', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.report'), route('report.index'));
});

// Home > PushNotification
Breadcrumbs::for('push-notifications', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.push_notification'), route('push-notifications.index'));
});

// Home > Instructions
Breadcrumbs::for('instructions', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.instructions'), route('instructions.index'));
});

// Home > Instructions Create
Breadcrumbs::for('instructions.create', function ($trail) {
    $trail->parent('instructions');
    $trail->push('Додавання нової інструкції', route('instructions.create'));
});

// Home > Instructions Create
Breadcrumbs::for('instructions.edit', function ($trail, $instruction) {
    $trail->parent('instructions');
    $trail->push('Рудагування інструкції', route('instructions.edit', $instruction));
});

// Home > Payments
Breadcrumbs::for('payments', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.payments'), route('payments.index'));
});


// Home > Handbook
Breadcrumbs::for('handbook', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.handbook'));
});

// Home > Theory
Breadcrumbs::for('theory', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.theory'));
});

// Home > Markings
Breadcrumbs::for('marking', function ($trail) {
    $trail->parent('theory');
    $trail->push(trans('translation.marking'));
});

// Home > Traffic rules
Breadcrumbs::for('traffic_rules', function ($trail) {
    $trail->parent('theory');
    $trail->push(trans('translation.traffic_rules'));
});

// Home > Road Signs
Breadcrumbs::for('road_signs', function ($trail) {
    $trail->parent('theory');
    $trail->push(trans('translation.road_signs'));
});

// Home > Theme Traffic Rules
Breadcrumbs::for('theme_traffic_rules', function ($trail) {
    $trail->parent('handbook');
    $trail->push(trans('translation.theme_traffic_rules'), route('handbook.theme-traffic-rules.index'));
});

// Home > Theme Traffic Rules > Add New Theme Traffic Rule
Breadcrumbs::for('theme_traffic_rules.create', function ($trail) {
    $trail->parent('theme_traffic_rules');
    $trail->push('Додавання нової теми', route('handbook.theme-traffic-rules.create'));
});

// Home > Theme Traffic Rules > Edit Theme Traffic Rule
Breadcrumbs::for('theme_traffic_rules.edit', function ($trail, $topicTrafficRule) {
    $trail->parent('theme_traffic_rules');
    $trail->push('Редагування теми', route('handbook.theme-traffic-rules.edit', $topicTrafficRule));
});


// Home > Internal Theme Traffic Rules
Breadcrumbs::for('internal_theme_traffic_rules', function ($trail) {
    $trail->parent('traffic_rules');
    $trail->push(trans('translation.internal_theme_traffic_rules'), route('theory.internal-theme-traffic-rules.index'));
});

// Home > Internal Theme Traffic Rules > Add New Internal Theme Traffic Rule
Breadcrumbs::for('internal_theme_traffic_rules.create', function ($trail) {
    $trail->parent('internal_theme_traffic_rules');
    $trail->push('Додавання нової теми', route('theory.internal-theme-traffic-rules.create'));
});

// Home > Internal Theme Traffic Rules > Edit Internal Theme Traffic Rule
Breadcrumbs::for('internal_theme_traffic_rules.edit', function ($trail, $topicTrafficRule) {
    $trail->parent('internal_theme_traffic_rules');
    $trail->push('Редагування теми', route('theory.internal-theme-traffic-rules.edit', $topicTrafficRule));
});


// Home > Items Traffic Rules
Breadcrumbs::for('items_traffic_rules', function ($trail) {
    $trail->parent('traffic_rules');
    $trail->push(trans('translation.items'), route('theory.item-traffic-rules.index'));
});

// Home > Items Traffic Rules > Add New Items Traffic Rule
Breadcrumbs::for('items_traffic_rules.create', function ($trail) {
    $trail->parent('items_traffic_rules');
    $trail->push('Додавання нового пункту', route('theory.item-traffic-rules.create'));
});

// Home > Items Traffic Rules > Edit Items Traffic Rule
Breadcrumbs::for('items_traffic_rules.edit', function ($trail, $itemTrafficRule) {
    $trail->parent('items_traffic_rules');
    $trail->push('Редагування пункту', route('theory.item-traffic-rules.edit', $itemTrafficRule));
});


// Home > Road Markings
Breadcrumbs::for('type_road_markings', function ($trail) {
    $trail->parent('marking');
    $trail->push(trans('translation.type_markings'), route('theory.road-markings.index'));
});

// Home > Road Markings > Add New Road Markings
Breadcrumbs::for('type_road_markings.create', function ($trail) {
    $trail->parent('type_road_markings');
    $trail->push('Додавання нового типу розмітки', route('theory.road-markings.create'));
});

// Home > Road Markings > Edit Road Markings
Breadcrumbs::for('type_road_markings.edit', function ($trail, $topicRoadMarking) {
    $trail->parent('type_road_markings');
    $trail->push('Редагування типу розмітки', route('theory.road-markings.edit', $topicRoadMarking));
});

// Home > Road Markings > Description
Breadcrumbs::for('type_road_markings.description', function ($trail) {
    $trail->parent('marking');
    $trail->push('Загальний опис до типів', route('theory.road-markings.editDescription'));
});

// Home > Markings
Breadcrumbs::for('markings', function ($trail) {
    $trail->parent('marking');
    $trail->push(trans('translation.markings'), route('theory.markings.index'));
});

// Home > Markings > Add Sign
Breadcrumbs::for('markings.create', function ($trail) {
    $trail->parent('markings');
    $trail->push('Додавання нової розмітки', route('theory.markings.create'));
});

// Home > Markings > Edit Sign
Breadcrumbs::for('markings.edit', function ($trail, $marking) {
    $trail->parent('markings');
    $trail->push('Редагування розмітки', route('theory.markings.edit', $marking));
});


// Home > Signs
Breadcrumbs::for('signs', function ($trail) {
    $trail->parent('road_signs');
    $trail->push(trans('translation.signs'), route('theory.signs.index'));
});

// Home > Signs > Add Sign
Breadcrumbs::for('signs.create', function ($trail) {
    $trail->parent('signs');
    $trail->push('Додавання нового знаку', route('theory.signs.create'));
});

// Home > Signs > Edit Sign
Breadcrumbs::for('signs.edit', function ($trail, $sign) {
    $trail->parent('signs');
    $trail->push('Редагування знаку', route('theory.signs.edit', $sign));
});

// Home > Road Signs > Description
Breadcrumbs::for('type_road_signs.description', function ($trail) {
    $trail->parent('road_signs');
    $trail->push('Загальний опис до категорій', route('theory.road-signs.editDescription'));
});

// Home > Road Signs
Breadcrumbs::for('type_road_signs', function ($trail) {
    $trail->parent('road_signs');
    $trail->push(trans('translation.type_road_signs'), route('theory.road-signs.index'));
});

// Home > Road Signs > Add New Road Sign
Breadcrumbs::for('type_road_signs.create', function ($trail) {
    $trail->parent('type_road_signs');
    $trail->push('Додавання нової категорії', route('theory.road-signs.create'));
});

// Home > Road Signs > Edit Road Sign
Breadcrumbs::for('type_road_signs.edit', function ($trail, $topicRoadSign) {
    $trail->parent('type_road_signs');
    $trail->push('Редагування категорії', route('theory.road-signs.edit', $topicRoadSign));
});


// Home > Issue Management
Breadcrumbs::for('issue_management', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.issue_management'));
});


// Home > Regulator
Breadcrumbs::for('regulator', function ($trail) {
    $trail->parent('theory');
    $trail->push(trans('translation.regulator'), route('theory.regulator.index'));
});

// Home > traffic-light
Breadcrumbs::for('traffic-light', function ($trail) {
    $trail->parent('theory');
    $trail->push(trans('translation.traffic_lights'), route('theory.traffic-light.index'));
});

// Home > video-lessons-traffic-rule
Breadcrumbs::for('video-lessons-traffic-rule', function ($trail) {
    $trail->parent('theory');
    $trail->push(trans('translation.video_lessons_traffic_rule'), route('theory.video-lessons-traffic-rule.index'));
});


// Home > Issue Management > Questions
Breadcrumbs::for('questions', function ($trail) {
    $trail->parent('issue_management');
    $trail->push(trans('translation.questions'), route('issue-management.questions.index'));
});

// Home > Issue Management > Add New Question
Breadcrumbs::for('questions.create', function ($trail) {
    $trail->parent('questions');
    $trail->push('Додавання нового питання', route('issue-management.questions.create'));
});

// Home > Issue Management > Edit Question
Breadcrumbs::for('questions.edit', function ($trail, $question) {
    $trail->parent('questions');
    $trail->push('Редагування питання', route('issue-management.questions.edit', $question));
});

// Home > Issue Management > Edit Question
Breadcrumbs::for('questions.duplicate', function ($trail, $question) {
    $trail->parent('questions');
    $trail->push('Заміна питання', route('issue-management.questions.duplicate', $question));
});


// Home > PromoCodes
Breadcrumbs::for('promo_code', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.promo_code'), route('promo-codes.index'));
});

// Home > PromoCodes > Add New PromoCode
Breadcrumbs::for('promo_code.create', function ($trail) {
    $trail->parent('promo_code');
    $trail->push('Додавання нового промокоду', route('promo-codes.create'));
});

// Home > PromoCodes > Edit PromoCode
Breadcrumbs::for('promo_code.edit', function ($trail, $promoCode) {
    $trail->parent('promo_code');
    $trail->push('Редагування промокоду', route('promo-codes.edit', $promoCode));
});

// Home > Updating Hsc
Breadcrumbs::for('updating_msc', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.updating_msc'));
});

// Home > Auto premium
Breadcrumbs::for('auto_premium', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.auto_premium'));
});

// Home > Subscriptions
Breadcrumbs::for('subscriptions', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.price_list'), route('subscriptions.index'));
});

// Home > Subscriptions > Add New Subscription
Breadcrumbs::for('subscriptions.create', function ($trail) {
    $trail->parent('subscriptions');
    $trail->push('Додавання нової підписки', route('subscriptions.create'));
});

// Home > Subscriptions > Edit Subscription
Breadcrumbs::for('subscriptions.edit', function ($trail, $subscription) {
    $trail->parent('subscriptions');
    $trail->push('Редагування підписки', route('subscriptions.edit', $subscription));
});


// Home > Translations
Breadcrumbs::for('translations', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.translations'), route('translations.index'));
});

// Home > Translations > Add New Translation
Breadcrumbs::for('translations.create', function ($trail) {
    $trail->parent('translations');
    $trail->push('Додавання нового перекладу', route('translations.create'));
});

// Home > Translations > Edit Translation
Breadcrumbs::for('translations.edit', function ($trail, $translation) {
    $trail->parent('translations');
    $trail->push('Редагування перекладу', route('translations.edit', $translation));
});


// Home > Translation Categories
Breadcrumbs::for('translation_categories', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.translation_categories'), route('translation-categories.index'));
});

// Home > Translation Categories > Add Translation Category
Breadcrumbs::for('translation_categories.create', function ($trail) {
    $trail->parent('translation_categories');
    $trail->push('Додавання нової категорії перекладу', route('translation-categories.create'));
});

// Home > Translation Categories > Edit Translation Category
Breadcrumbs::for('translation_categories.edit', function ($trail, $translation) {
    $trail->parent('translation_categories');
    $trail->push('Редагування категорії перекладу', route('translation-categories.edit', $translation));
});


// Home > Text Pages
Breadcrumbs::for('text-pages', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.text_pages'), route('text-pages.index'));
});

// Home > Text Pages > Add Text Page
Breadcrumbs::for('text-pages.create', function ($trail) {
    $trail->parent('text-pages');
    $trail->push('Додавання нової сторінки', route('text-pages.create'));
});

// Home > Text Pages > Edit Text Page
Breadcrumbs::for('text-pages.edit', function ($trail, $textPage) {
    $trail->parent('text-pages');
    $trail->push('Редагування сторінки', route('text-pages.edit', $textPage));
});


// Home > Complaints Questions
Breadcrumbs::for('questions-complaints', function ($trail) {
    $trail->parent('issue_management');
    $trail->push(trans('translation.complaints_questions'), route('issue-management.questions-complaints.index'));
});

// Home > Frequent Mistakes
Breadcrumbs::for('frequent-mistakes', function ($trail) {
    $trail->parent('issue_management');
    $trail->push(trans('translation.frequent_mistakes'), route('issue-management.frequent-mistakes.index'));
});


// Home > Settings
Breadcrumbs::for('general-settings', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.general_settings'));
});

// Home > Settings
Breadcrumbs::for('settings', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('translation.Settings'));
});

// Home > Settings > Logs
Breadcrumbs::for('logs', function ($trail) {
    $trail->parent('settings');
    $trail->push(trans('translation.logs'), route('logs.index'));
});
