<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\QuestionsComplaintController;
use App\Http\Controllers\CategoryDriverLicenseController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\TopicRoadMarkingController;
use App\Http\Controllers\TopicRoadSignController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::domain(env('APP_DOMAIN_NAME'))->group(function () {
    Route::get('/', function () {
        return view('errors.soon');
    });
});

Route::domain('teacher.' . env('APP_DOMAIN_NAME'))->group(function () {
    Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('/register', [RegisterController::class, 'register'])->name('register');
});

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('index', [\App\Http\Controllers\AdminController::class, 'index'])->name('dashboard');

    Route::middleware('can:languages')->group(function () {
        Route::resource('languages', LanguageController::class)->except(['show']);
        Route::post('languages/sort', [LanguageController::class, 'sort'])->name('languages.sort');
    });

    Route::resource('logs', \App\Http\Controllers\LogController::class)->only(['index'])->middleware('can:logs');

    Route::resource('instructions', \App\Http\Controllers\InstructionController::class)->except('show');
    Route::post('instructions/sort', [\App\Http\Controllers\InstructionController::class, 'sort'])->name('instructions.sort');

    Route::middleware(['can:teachers'])->group(function () {
        Route::resource('teachers', TeacherController::class)->except(['show']);
        Route::post('teachers/get-autocomplete-teacher', [TeacherController::class, 'getAutocompleteTeacher'])
            ->name('teachers.getAutocompleteTeacher');
        Route::post('teachers/get-autocomplete-email', [TeacherController::class, 'getAutocompleteEmail'])
            ->name('teachers.getAutocompleteEmail');
        Route::post('teachers/get-autocomplete-promo-code', [TeacherController::class, 'getAutocompletePromoCode'])
            ->name('teachers.getAutocompletePromoCode');
    });

    Route::middleware(['can:display-students'])->group(function () {
        Route::resource('students', StudentController::class)->except(['show']);
        Route::post('students/get-autocomplete-teacher', [StudentController::class, 'getAutocompleteTeacher'])
            ->name('students.getAutocompleteTeacher');
        Route::post('students/get-autocomplete-student', [StudentController::class, 'getAutocompleteStudent'])
            ->name('students.getAutocompleteStudent');
        Route::post('students/get-autocomplete-group', [StudentController::class, 'getAutocompleteGroup'])
            ->name('students.getAutocompleteGroup');
        Route::post('students/get-autocomplete-email', [StudentController::class, 'getAutocompleteEmail'])
            ->name('students.getAutocompleteEmail');
        Route::post('students/get-autocomplete-promo-code', [StudentController::class, 'getAutocompletePromoCode'])
            ->name('students.getAutocompletePromoCode');
    });

    Route::get('students/export', [StudentController::class, 'export'])->name('students.export')
        ->middleware('can:teachers');
    Route::post('students/action-multiple', [StudentController::class, 'actionMultiple'])
        ->name('students.actionMultiple')
        ->middleware('can:student-actions');

    Route::middleware(['can:groups'])->group(function () {
        Route::resource('groups', GroupController::class)->except(['show']);
        Route::post('groups/get-autocomplete-teacher', [GroupController::class, 'getAutocompleteTeacher'])->name('groups.getAutocompleteTeacher');
        Route::post('groups/get-autocomplete-group', [GroupController::class, 'getAutocompleteGroup'])->name('groups.getAutocompleteGroup');
    });

    Route::resource('category-driver-licenses', CategoryDriverLicenseController::class)
        ->only(['index', 'store'])->middleware('can:category-driver-licenses');

    Route::name('handbook.')->middleware('can:handbook')->prefix('handbook')->group(function () {
        Route::resource('theme-traffic-rules', \App\Http\Controllers\TopicTrafficRuleController::class)
            ->except(['show'])
            ->parameters(['theme-traffic-rules' => 'topic_traffic_rule']);
    });

    Route::name('theory.')->middleware('can:theory')->prefix('theories')->group(function () {
        Route::resource('internal-theme-traffic-rules', \App\Http\Controllers\InternalTopicTrafficRuleController::class)
            ->except(['show'])
            ->parameters(['internal-theme-traffic-rules' => 'internal_topic_traffic_rule']);

        Route::resource('item-traffic-rules', \App\Http\Controllers\ItemTrafficRuleController::class)->except(['show']);
        Route::post('item-traffic-rules/get-autocomplete-item', [\App\Http\Controllers\ItemTrafficRuleController::class, 'getAutocompleteItem'])->name('item-traffic-rules.getAutocompleteItem');
        Route::post('item-traffic-rules/sort', [\App\Http\Controllers\ItemTrafficRuleController::class, 'sort'])->name('item-traffic-rules.sort');

        Route::resource('road-markings', TopicRoadMarkingController::class)->except(['show'])->parameters(['road-markings' => 'topic_road_marking']);
        Route::get('road-markings/description', [TopicRoadMarkingController::class, 'editDescription'])->name('road-markings.editDescription');
        Route::post('road-markings/description', [TopicRoadMarkingController::class, 'updateDescription'])->name('road-markings.updateDescription');

        Route::resource('road-signs', TopicRoadSignController::class)->except(['show'])->parameters(['road-signs' => 'topic_road_sign']);
        Route::get('road-signs/description', [TopicRoadSignController::class, 'editDescription'])->name('road-signs.editDescription');
        Route::post('road-signs/description', [TopicRoadSignController::class, 'updateDescription'])->name('road-signs.updateDescription');

        Route::resource('markings', \App\Http\Controllers\MarkingController::class)->except(['show']);
        Route::post('markings/get-autocomplete-markings', [\App\Http\Controllers\MarkingController::class, 'getAutocompleteMarking'])->name('markings.getAutocompleteMarking');
        Route::post('markings/sort', [\App\Http\Controllers\MarkingController::class, 'sort'])->name('markings.sort');

        Route::resource('signs', \App\Http\Controllers\SignController::class)->except(['show']);
        Route::post('signs/get-autocomplete-signs', [\App\Http\Controllers\SignController::class, 'getAutocompleteSign'])->name('signs.getAutocompleteSign');
        Route::post('signs/sort', [\App\Http\Controllers\SignController::class, 'sort'])->name('signs.sort');

        Route::get('regulator', [\App\Http\Controllers\TranslateRegulatorController::class, 'index'])->name('regulator.index');
        Route::post('regulator', [\App\Http\Controllers\TranslateRegulatorController::class, 'update'])->name('regulator.update');

        Route::get('traffic-light', [\App\Http\Controllers\TranslateTrafficLightController::class, 'index'])->name('traffic-light.index');
        Route::post('traffic-light', [\App\Http\Controllers\TranslateTrafficLightController::class, 'update'])->name('traffic-light.update');

        Route::get('video-lessons-traffic-rule', [\App\Http\Controllers\TranslateVideoLessonsTrafficRuleController::class, 'index'])->name('video-lessons-traffic-rule.index');
        Route::post('video-lessons-traffic-rule', [\App\Http\Controllers\TranslateVideoLessonsTrafficRuleController::class, 'update'])->name('video-lessons-traffic-rule.update');
    });

    Route::name('issue-management.')->middleware('can:issue-management')->prefix('issue-management')->group(function () {
        Route::resource('questions', QuestionController::class)->except(['show']);
        Route::get('questions/{question}/duplicate', [QuestionController::class, 'duplicate'])->name('questions.duplicate');
        Route::post('questions/{duplicateQuestion}/store-duplicate', [QuestionController::class, 'storeDuplicate'])->name('questions.store-duplicate');

        Route::resource('questions-complaints', QuestionsComplaintController::class)->only(['index', 'destroy']);

        Route::get('frequent-mistakes', [QuestionController::class, 'frequentMistakes'])->name('frequent-mistakes.index');
        Route::post('frequent-mistakes/reset', [QuestionController::class, 'resetFrequentMistakes'])->name('frequent-mistakes.reset');
    });

    Route::middleware('can:support-services')->group(function () {
        Route::resource('support-services', \App\Http\Controllers\SocialSupportServiceController::class)
            ->except(['show'])
            ->parameters(['support-services' => 'social_support_service']);
    });

    Route::resource('translation-categories', \App\Http\Controllers\TranslationCategoryController::class)
        ->except(['show'])->middleware('can:translations');
    Route::resource('translations', \App\Http\Controllers\TranslationController::class)
        ->except(['show'])->middleware('can:translations');

    Route::resource('subscriptions', SubscriptionController::class)->except(['show']);

    Route::middleware('can:auto-premium')->prefix('auto-premium')->name('auto-premium.')->group(function () {
        Route::get('/', [SubscriptionController::class, 'autoPremium'])->name('index');
        Route::post('/', [SubscriptionController::class, 'updateAutoPremium'])->name('update');
    });

    Route::resource('promo-codes', \App\Http\Controllers\PromoCodeController::class)
        ->except(['show'])->middleware('can:promo-codes');

    Route::middleware('can:updating-msc')->group(function () {
        Route::get('updating-msc', [\App\Http\Controllers\UpdatingMSCController::class, 'index'])->name('updating-msc.index');
        Route::post('updating-msc', [\App\Http\Controllers\UpdatingMSCController::class, 'update'])->name('updating-msc.update');
        Route::get('sync-msc', [\App\Http\Controllers\UpdatingMSCController::class, 'sync'])->name('sync-msc');
        Route::get('diff-sync-msc', [\App\Http\Controllers\UpdatingMSCController::class, 'diffSync'])->name('diff-sync-msc');
    });
    Route::post('promo-codes/get-autocomplete-promo-code', [\App\Http\Controllers\PromoCodeController::class, 'getAutocompletePromoCode'])
        ->name('promo-codes.getAutocompletePromoCode');

    Route::get('report', [\App\Http\Controllers\ReportController::class, 'index'])->name('report.index');
    Route::get('report/import', [\App\Http\Controllers\ReportController::class, 'import'])->name('report.import');

    Route::middleware('can:payments')->group(function () {
        Route::resource('payments', PaymentController::class)->only(['index']);
        Route::post('payments/get-autocomplete-student', [PaymentController::class, 'getAutocompleteStudent'])
            ->name('payments.getAutocompleteStudent');
    });

    Route::resource('text-pages', \App\Http\Controllers\TextPageController::class)->except(['show'])
        ->middleware('can:text-pages');

    Route::resource('push-notifications', \App\Http\Controllers\PushNotificationController::class)
        ->only(['index', 'store'])
        ->middleware('can:push-notifications');

    Route::middleware('can:general-settings')->group(function () {
        Route::name('general-settings.')->prefix('general-settings')->group(function () {
            Route::get('/', [\App\Http\Controllers\GeneralSettingController::class, 'index'])->name('index');
            Route::post('/', [\App\Http\Controllers\GeneralSettingController::class, 'store'])->name('store');
        });

        Route::get('sync-api-questions', function () {
            Artisan::call('api-question:sync');
            request()->session()->flash('success', nl2br(Artisan::output()));

            return redirect()->back();
        })->name('sync-api-questions');

        Route::get('sync-api-theory', function () {
            Artisan::call('api-theory:sync');
            request()->session()->flash('success', nl2br(Artisan::output()));

            return redirect()->back();
        })->name('sync-api-theory');
    });

    Route::name('imageUpload.')->prefix('image-upload')->group(function () {
        Route::post('/tinymce', [\App\Http\Controllers\ImageUploadController::class, 'tinymce'])->name('tinymce');
        Route::post('/markdown-editor', [\App\Http\Controllers\ImageUploadController::class, 'markdownEditor'])->name('markdownEditor');
        Route::post('/remove-markdown-editor', [\App\Http\Controllers\ImageUploadController::class, 'removeMarkdownEditor'])->name('removeMarkdownEditor');
        Route::delete('/markdown-editor/{id}', [\App\Http\Controllers\ImageUploadController::class, 'destroyMarkdownEditor'])->name('destroyMarkdownEditor');
    });
});
